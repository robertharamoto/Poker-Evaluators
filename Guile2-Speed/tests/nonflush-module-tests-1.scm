;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for nonflush-module.scm                   ###
;;;###                                                       ###
;;;###  last updated August 8, 2024                          ###
;;;###                                                       ###
;;;###  created June 24, 2024                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### ice-9 format to automatically commafy using format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((card-module)
              :renamer (symbol-prefix-proc 'card-module:)))

(use-modules ((nonflush-module)
              :renamer (symbol-prefix-proc 'nonflush-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################

;;;###  card representation
;;;###    3          2          1          0
;;;###   10987654 32109876 54321098 76543210
;;;###  +--------+--------+--------+--------+
;;;###  |xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
;;;###  +--------+--------+--------+--------+
;;;###
;;;###  p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
;;;###  r = rank of card (deuce=2,trey=3,four=4,five=4,...,ace=14)
;;;###  cdhs = suit of card (bit turned on based on suit of card)
;;;###  b = bit turned on depending on rank of card
;;;###
;;;###  Using such a scheme, here are some bit pattern examples:
;;;###
;;;###  xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
;;;###  00001000 00000000 01001011 00100101    King of Diamonds
;;;###  00000000 00001000 00010011 00000111    Five of Spades
;;;###  00000010 00000000 10001001 00011101    Jack of Clubs

;;;###
;;;###  hand values
;;;###  royal flush 100
;;;###  straight flush 90
;;;###  four-of-a-kind 80
;;;###  full-house 70
;;;###  flush 60
;;;###  straight 50
;;;###  three-of-a-kind 40
;;;###  two-pairs 30
;;;###  one-pairs 20
;;;###  high-cards 10
;;;###

;;;###
;;;###  data for hash of hash tables
;;;###  hash(prime-signature) -> hash(string) -> value
;;;###
;;;###  "value" - hand values (from 10 to 100)
;;;###  "rank" - hand rank (from 1 to 7,462)
;;;###  "mask" - rank of 5 cards in bit format
;;;###  "count" - number of hands with this prime signature
;;;###  "signature" - prime signature
;;;###  "isflush" - bool flush indicator
;;;###  "quads-value" - card value of 4-of-a-kind
;;;###  "triples-value" - card value of 3-of-a-kind
;;;###  "top-pair" - card value of top pair
;;;###  "second-pair" - card value of second pair
;;;###  "kicker1" - card value of top kicker
;;;###  "kicker2" - card value of second kicker
;;;###  "kicker3" - card value of third kicker
;;;###
;;;###

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-nonflush-is-straight-mask-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-nonflush-is-straight-mask-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "kh")
                 (card-module:string-to-card "qd")
                 (card-module:string-to-card "jc")
                 (card-module:string-to-card "ts")
                 #x1F00)
           (list (card-module:string-to-card "ks")
                 (card-module:string-to-card "qh")
                 (card-module:string-to-card "jd")
                 (card-module:string-to-card "tc")
                 (card-module:string-to-card "9s")
                 #x0F80)
           (list (card-module:string-to-card "8s")
                 (card-module:string-to-card "9h")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "jc")
                 (card-module:string-to-card "qs")
                 #x07C0)
           (list (card-module:string-to-card "9s")
                 (card-module:string-to-card "8h")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "js")
                 #x03E0)
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "8d")
                 (card-module:string-to-card "9c")
                 (card-module:string-to-card "ts")
                 #x01F0)
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "9d")
                 (card-module:string-to-card "8c")
                 (card-module:string-to-card "5s")
                 #x00F8)
           (list (card-module:string-to-card "5s")
                 (card-module:string-to-card "4h")
                 (card-module:string-to-card "6d")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "8s")
                 #x007C)
           (list (card-module:string-to-card "7s")
                 (card-module:string-to-card "6h")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "3s")
                 #x003E)
           (list (card-module:string-to-card "2s")
                 (card-module:string-to-card "6h")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "3s")
                 #x001F)
           (list (card-module:string-to-card "2s")
                 (card-module:string-to-card "3h")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "as")
                 #x100F)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1 (list-ref alist 0))
                  (card-2 (list-ref alist 1))
                  (card-3 (list-ref alist 2))
                  (card-4 (list-ref alist 3))
                  (card-5 (list-ref alist 4))
                  (shouldbe (list-ref alist 5)))
              (let ((result
                     (nonflush-module:is-straight-mask
                      card-1 card-2 card-3 card-4 card-5))
                    (description (card-module:card-list-to-string
                                  (list card-1 card-2 card-3
                                        card-4 card-5))))
                (let ((err-1
                       (format
                        #f "~a : (~a) : ~a : error : "
                        sub-name test-label-index description))
                      (err-2
                       (ice-9-format:format
                        #f "shouldbe = ~x, result = ~x"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax nonflush-populate-freq-htable-macro
  (syntax-rules ()
    ((nonflush-populate-freq-htable-macro
      card-1 card-2 card-3 card-4 card-5
      freq-htable)
     (begin
       (let ((card-list
              (list card-1 card-2 card-3 card-4 card-5)))
         (begin
           (for-each
            (lambda (acard)
              (begin
                (let ((avalue
                       (card-module:card-to-value acard)))
                  (let ((acount
                         (hash-ref freq-htable avalue 0)))
                    (begin
                      (hash-set!
                       freq-htable avalue (1+ acount))
                      )))
                )) card-list)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (nonflush-assert-quad-hash-results
         sub-name err-1
         count-string count-value
         value-string value-value
         signature-string signature-value
         isflush-string isflush-value
         quads-string quads-value
         kicker-string kicker-value
         sub-htable result-hash-table)
  (begin
    (let ((result-count
           (hash-ref
            sub-htable count-string #f))
          (result-value
           (hash-ref
            sub-htable value-string #f))
          (result-signature
           (hash-ref
            sub-htable signature-string #f))
          (result-isflush
           (hash-ref
            sub-htable isflush-string #f))
          (result-quads
           (hash-ref
            sub-htable quads-string #f))
          (result-kicker
           (hash-ref
            sub-htable kicker-string #f)))
      (let ((err-3
             (format
              #f "shouldbe count = ~a, result = ~a"
              count-value result-count))
            (err-4
             (format
              #f "shouldbe hand value = ~a, result = ~a"
              value-value result-value))
            (err-5
             (format
              #f "shouldbe signature = ~a, result = ~a"
              signature-value result-signature))
            (err-6
             (format
              #f "shouldbe isflush = ~a, result = ~a"
              isflush-value result-isflush))
            (err-7
             (format
              #f "shouldbe quads = ~a, result = ~a"
              quads-value result-quads))
            (err-8
             (format
              #f "shouldbe kicker = ~a, result = ~a"
              kicker-value result-kicker)))
        (begin
          (unittest2:assert?
           (equal? count-value result-count)
           sub-name
           (string-append err-1 err-3)
           result-hash-table)

          (unittest2:assert?
           (equal? value-value result-value)
           sub-name
           (string-append err-1 err-4)
           result-hash-table)

          (unittest2:assert?
           (equal? signature-value
                   result-signature)
           sub-name
           (string-append err-1 err-5)
           result-hash-table)

          (unittest2:assert?
           (equal? isflush-value
                   result-isflush)
           sub-name
           (string-append err-1 err-6)
           result-hash-table)

          (unittest2:assert?
           (equal? quads-value
                   result-quads)
           sub-name
           (string-append err-1 err-7)
           result-hash-table)

          (unittest2:assert?
           (equal? kicker-value
                   result-kicker)
           sub-name
           (string-append err-1 err-8)
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-quads-hand-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-quads-hand-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "ah")
                 (card-module:string-to-card "ad")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "ac")
                 #t (list "count" 1) (list "value" 80)
                 (list "signature" 104553157)
                 (list "isflush" #f)
                 (list "quads-value" 14)
                 (list "kicker1" 13))
           (list (card-module:string-to-card "kh")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "jh")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "kc")
                 #t (list "count" 1) (list "value" 80)
                 (list "signature" 54350669)
                 (list "isflush" #f)
                 (list "quads-value" 13)
                 (list "kicker1" 11))
           (list (card-module:string-to-card "8d")
                 (card-module:string-to-card "qd")
                 (card-module:string-to-card "qc")
                 (card-module:string-to-card "qs")
                 (card-module:string-to-card "qh")
                 #t (list "count" 1) (list "value" 80)
                 (list "signature" 15699857)
                 (list "isflush" #f)
                 (list "quads-value" 12)
                 (list "kicker1" 8))
           (list (card-module:string-to-card "jc")
                 (card-module:string-to-card "8c")
                 (card-module:string-to-card "js")
                 (card-module:string-to-card "jh")
                 (card-module:string-to-card "jd")
                 #t (list "count" 1) (list "value" 80)
                 (list "signature" 12023777)
                 (list "isflush" #f)
                 (list "quads-value" 11)
                 (list "kicker1" 8))
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "9c")
                 (card-module:string-to-card "ts")
                 #f)
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "ts")
                 (card-module:string-to-card "th")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "tc")
                 #t (list "count" 1) (list "value" 80)
                 (list "signature" 3078251)
                 (list "isflush" #f)
                 (list "quads-value" 10)
                 (list "kicker1" 6))
           (list (card-module:string-to-card "9d")
                 (card-module:string-to-card "9c")
                 (card-module:string-to-card "9h")
                 (card-module:string-to-card "9s")
                 (card-module:string-to-card "5d")
                 #t (list "count" 1) (list "value" 80)
                 (list "signature" 912247)
                 (list "isflush" #f)
                 (list "quads-value" 9)
                 (list "kicker1" 5))
           (list (card-module:string-to-card "8c")
                 (card-module:string-to-card "8h")
                 (card-module:string-to-card "8d")
                 (card-module:string-to-card "ac")
                 (card-module:string-to-card "8s")
                 #t (list "count" 1) (list "value" 80)
                 (list "signature" 3424361)
                 (list "isflush" #f)
                 (list "quads-value" 8)
                 (list "kicker1" 14))
           (list (card-module:string-to-card "7h")
                 (card-module:string-to-card "7s")
                 (card-module:string-to-card "kh")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "7d")
                 #t (list "count" 1) (list "value" 80)
                 (list "signature" 1056757)
                 (list "isflush" #f)
                 (list "quads-value" 7)
                 (list "kicker1" 13))
           (list (card-module:string-to-card "6d")
                 (card-module:string-to-card "qc")
                 (card-module:string-to-card "6h")
                 (card-module:string-to-card "6c")
                 (card-module:string-to-card "6s")
                 #t (list "count" 1) (list "value" 80)
                 (list "signature" 453871)
                 (list "isflush" #f)
                 (list "quads-value" 6)
                 (list "kicker1" 12))
           (list (card-module:string-to-card "2s")
                 (card-module:string-to-card "3s")
                 (card-module:string-to-card "5s")
                 (card-module:string-to-card "4s")
                 (card-module:string-to-card "ad")
                 #f)
           (list (card-module:string-to-card "2h")
                 (card-module:string-to-card "5h")
                 (card-module:string-to-card "5c")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "5s")
                 #t (list "count" 1) (list "value" 80)
                 (list "signature" 4802)
                 (list "isflush" #f)
                 (list "quads-value" 5)
                 (list "kicker1" 2))
           (list (card-module:string-to-card "4h")
                 (card-module:string-to-card "jc")
                 (card-module:string-to-card "4d")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "4s")
                 #t (list "count" 1) (list "value" 80)
                 (list "signature" 18125)
                 (list "isflush" #f)
                 (list "quads-value" 4)
                 (list "kicker1" 11))
           (list (card-module:string-to-card "3d")
                 (card-module:string-to-card "3s")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "3h")
                 (card-module:string-to-card "3c")
                 #t (list "count" 1) (list "value" 80)
                 (list "signature" 1863)
                 (list "isflush" #f)
                 (list "quads-value" 3)
                 (list "kicker1" 10))
           (list (card-module:string-to-card "2h")
                 (card-module:string-to-card "2c")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "2s")
                 #t (list "count" 1) (list "value" 80)
                 (list "signature" 592)
                 (list "isflush" #f)
                 (list "quads-value" 2)
                 (list "kicker1" 13))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1 (list-ref alist 0))
                  (card-2 (list-ref alist 1))
                  (card-3 (list-ref alist 2))
                  (card-4 (list-ref alist 3))
                  (card-5 (list-ref alist 4))
                  (shouldbe-bool (list-ref alist 5))
                  (sub-htable (make-hash-table 20))
                  (freq-htable (make-hash-table 20)))
              (let ((description
                     (card-module:card-list-to-string
                      (list card-1 card-2 card-3 card-4 card-5)))
                    (prime-1 (card-module:card-to-prime-value card-1))
                    (prime-2 (card-module:card-to-prime-value card-2))
                    (prime-3 (card-module:card-to-prime-value card-3))
                    (prime-4 (card-module:card-to-prime-value card-4))
                    (prime-5 (card-module:card-to-prime-value card-5)))
                (let ((prime-signature
                       (* prime-1 prime-2 prime-3 prime-4 prime-5)))
                  (begin
                    (nonflush-populate-freq-htable-macro
                     card-1 card-2 card-3 card-4 card-5
                     freq-htable)

                    (let ((result-bool
                           (nonflush-module:is-quads-hand
                            freq-htable prime-signature
                            sub-htable)))
                      (let ((err-1
                             (format
                              #f "~a : (~a) : ~a : error : "
                              sub-name test-label-index description))
                            (err-2
                             (format
                              #f "shouldbe = ~a, result = ~a"
                              (if shouldbe-bool "true" "false")
                              (if result-bool "true" "false"))))
                        (begin
                          (unittest2:assert?
                           (equal? shouldbe-bool result-bool)
                           sub-name
                           (string-append err-1 err-2)
                           result-hash-table)

                          (if (equal? shouldbe-bool #t)
                              (begin
                                (let ((count-list (list-ref alist 6))
                                      (value-list (list-ref alist 7))
                                      (signature-list (list-ref alist 8))
                                      (isflush-list (list-ref alist 9))
                                      (quads-list (list-ref alist 10))
                                      (kicker-list (list-ref alist 11)))
                                  (let ((count-string
                                         (list-ref count-list 0))
                                        (count-value
                                         (list-ref count-list 1))
                                        (value-string
                                         (list-ref value-list 0))
                                        (value-value
                                         (list-ref value-list 1))
                                        (signature-string
                                         (list-ref signature-list 0))
                                        (signature-value
                                         (list-ref signature-list 1))
                                        (isflush-string
                                         (list-ref isflush-list 0))
                                        (isflush-value
                                         (list-ref isflush-list 1))
                                        (quads-string
                                         (list-ref quads-list 0))
                                        (quads-value
                                         (list-ref quads-list 1))
                                        (kicker-string
                                         (list-ref kicker-list 0))
                                        (kicker-value
                                         (list-ref kicker-list 1)))
                                    (begin
                                      (nonflush-assert-quad-hash-results
                                       sub-name err-1
                                       count-string count-value
                                       value-string value-value
                                       signature-string signature-value
                                       isflush-string isflush-value
                                       quads-string quads-value
                                       kicker-string kicker-value
                                       sub-htable
                                       result-hash-table)
                                      )))
                                ))
                          )))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (nonflush-assert-fulls-hash-results
         sub-name err-1
         count-string count-value
         value-string value-value
         signature-string signature-value
         isflush-string isflush-value
         triples-string triples-value
         toppair-string toppair-value
         sub-htable result-hash-table)
  (begin
    (let ((result-count
           (hash-ref
            sub-htable count-string #f))
          (result-value
           (hash-ref
            sub-htable value-string #f))
          (result-signature
           (hash-ref
            sub-htable signature-string #f))
          (result-isflush
           (hash-ref
            sub-htable isflush-string #f))
          (result-triples
           (hash-ref
            sub-htable triples-string #f))
          (result-toppair
           (hash-ref
            sub-htable toppair-string #f)))
      (let ((err-3
             (format
              #f "shouldbe count = ~a, result = ~a"
              count-value result-count))
            (err-4
             (format
              #f "shouldbe hand value = ~a, result = ~a"
              value-value result-value))
            (err-5
             (format
              #f "shouldbe signature = ~a, result = ~a"
              signature-value result-signature))
            (err-6
             (format
              #f "shouldbe isflush = ~a, result = ~a"
              isflush-value result-isflush))
            (err-7
             (format
              #f "shouldbe triples = ~a, result = ~a"
              triples-value result-triples))
            (err-8
             (format
              #f "shouldbe toppair = ~a, result = ~a"
              toppair-value result-toppair)))
        (begin
          (unittest2:assert?
           (equal? count-value result-count)
           sub-name
           (string-append err-1 err-3)
           result-hash-table)

          (unittest2:assert?
           (equal? value-value result-value)
           sub-name
           (string-append err-1 err-4)
           result-hash-table)

          (unittest2:assert?
           (equal? signature-value
                   result-signature)
           sub-name
           (string-append err-1 err-5)
           result-hash-table)

          (unittest2:assert?
           (equal? isflush-value
                   result-isflush)
           sub-name
           (string-append err-1 err-6)
           result-hash-table)

          (unittest2:assert?
           (equal? triples-value
                   result-triples)
           sub-name
           (string-append err-1 err-7)
           result-hash-table)

          (unittest2:assert?
           (equal? toppair-value
                   result-toppair)
           sub-name
           (string-append err-1 err-8)
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-nonflush-fulls-hand-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-nonflush-fulls-hand-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "ah")
                 (card-module:string-to-card "ad")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "kc")
                 #t (list "count" 1) (list "value" 70)
                 (list "signature" 94352849)
                 (list "isflush" #f)
                 (list "triples-value" 14)
                 (list "top-pair" 13))
           (list (card-module:string-to-card "kh")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "jh")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "jc")
                 #t (list "count" 1) (list "value" 70)
                 (list "signature" 42599173)
                 (list "isflush" #f)
                 (list "triples-value" 13)
                 (list "top-pair" 11))
           (list (card-module:string-to-card "8d")
                 (card-module:string-to-card "qd")
                 (card-module:string-to-card "qc")
                 (card-module:string-to-card "qs")
                 (card-module:string-to-card "8h")
                 #t (list "count" 1) (list "value" 70)
                 (list "signature" 8609599)
                 (list "isflush" #f)
                 (list "triples-value" 12)
                 (list "top-pair" 8))
           (list (card-module:string-to-card "7c")
                 (card-module:string-to-card "7d")
                 (card-module:string-to-card "js")
                 (card-module:string-to-card "jh")
                 (card-module:string-to-card "jd")
                 #t (list "count" 1) (list "value" 70)
                 (list "signature" 4121741)
                 (list "isflush" #f)
                 (list "triples-value" 11)
                 (list "top-pair" 7))
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "9c")
                 (card-module:string-to-card "ts")
                 #f)
           (list (card-module:string-to-card "ts")
                 (card-module:string-to-card "6s")
                 (card-module:string-to-card "6h")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "tc")
                 #t (list "count" 1) (list "value" 70)
                 (list "signature" 1472207)
                 (list "isflush" #f)
                 (list "triples-value" 10)
                 (list "top-pair" 6))
           (list (card-module:string-to-card "5d")
                 (card-module:string-to-card "5c")
                 (card-module:string-to-card "9h")
                 (card-module:string-to-card "9s")
                 (card-module:string-to-card "9d")
                 #t (list "count" 1) (list "value" 70)
                 (list "signature" 336091)
                 (list "isflush" #f)
                 (list "triples-value" 9)
                 (list "top-pair" 5))
           (list (card-module:string-to-card "8c")
                 (card-module:string-to-card "8h")
                 (card-module:string-to-card "8d")
                 (card-module:string-to-card "ac")
                 (card-module:string-to-card "as")
                 #t (list "count" 1) (list "value" 70)
                 (list "signature" 8258753)
                 (list "isflush" #f)
                 (list "triples-value" 8)
                 (list "top-pair" 14))
           (list (card-module:string-to-card "kh")
                 (card-module:string-to-card "7s")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "7d")
                 #t (list "count" 1) (list "value" 70)
                 (list "signature" 3007693)
                 (list "isflush" #f)
                 (list "triples-value" 7)
                 (list "top-pair" 13))
           (list (card-module:string-to-card "6d")
                 (card-module:string-to-card "qc")
                 (card-module:string-to-card "6h")
                 (card-module:string-to-card "6c")
                 (card-module:string-to-card "qs")
                 #t (list "count" 1) (list "value" 70)
                 (list "signature" 1279091)
                 (list "isflush" #f)
                 (list "triples-value" 6)
                 (list "top-pair" 12))
           (list (card-module:string-to-card "2s")
                 (card-module:string-to-card "3s")
                 (card-module:string-to-card "5s")
                 (card-module:string-to-card "4s")
                 (card-module:string-to-card "ad")
                 #f)
           (list (card-module:string-to-card "2h")
                 (card-module:string-to-card "5h")
                 (card-module:string-to-card "5c")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "2s")
                 #t (list "count" 1) (list "value" 70)
                 (list "signature" 1372)
                 (list "isflush" #f)
                 (list "triples-value" 5)
                 (list "top-pair" 2))
           (list (card-module:string-to-card "4h")
                 (card-module:string-to-card "jc")
                 (card-module:string-to-card "jd")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "4s")
                 #t (list "count" 1) (list "value" 70)
                 (list "signature" 105125)
                 (list "isflush" #f)
                 (list "triples-value" 4)
                 (list "top-pair" 11))
           (list (card-module:string-to-card "3d")
                 (card-module:string-to-card "3s")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "3h")
                 (card-module:string-to-card "tc")
                 #t (list "count" 1) (list "value" 70)
                 (list "signature" 14283)
                 (list "isflush" #f)
                 (list "triples-value" 3)
                 (list "top-pair" 10))
           (list (card-module:string-to-card "2h")
                 (card-module:string-to-card "2c")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "ks")
                 #t (list "count" 1) (list "value" 70)
                 (list "signature" 10952)
                 (list "isflush" #f)
                 (list "triples-value" 2)
                 (list "top-pair" 13))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1 (list-ref alist 0))
                  (card-2 (list-ref alist 1))
                  (card-3 (list-ref alist 2))
                  (card-4 (list-ref alist 3))
                  (card-5 (list-ref alist 4))
                  (shouldbe-bool (list-ref alist 5))
                  (sub-htable (make-hash-table 20))
                  (freq-htable (make-hash-table 20)))
              (let ((description
                     (card-module:card-list-to-string
                      (list card-1 card-2 card-3 card-4 card-5)))
                    (prime-1 (card-module:card-to-prime-value card-1))
                    (prime-2 (card-module:card-to-prime-value card-2))
                    (prime-3 (card-module:card-to-prime-value card-3))
                    (prime-4 (card-module:card-to-prime-value card-4))
                    (prime-5 (card-module:card-to-prime-value card-5)))
                (let ((prime-signature
                       (* prime-1 prime-2 prime-3 prime-4 prime-5)))
                  (begin
                    (nonflush-populate-freq-htable-macro
                     card-1 card-2 card-3 card-4 card-5
                     freq-htable)

                    (let ((result-bool
                           (nonflush-module:is-fulls-hand
                            freq-htable prime-signature
                            sub-htable)))
                      (let ((err-1
                             (format
                              #f "~a : (~a) : ~a : error : "
                              sub-name test-label-index description))
                            (err-2
                             (format
                              #f "shouldbe = ~a, result = ~a"
                              (if shouldbe-bool "true" "false")
                              (if result-bool "true" "false"))))
                        (begin
                          (unittest2:assert?
                           (equal? shouldbe-bool result-bool)
                           sub-name
                           (string-append err-1 err-2)
                           result-hash-table)

                          (if (equal? shouldbe-bool #t)
                              (begin
                                (let ((count-list (list-ref alist 6))
                                      (value-list (list-ref alist 7))
                                      (signature-list (list-ref alist 8))
                                      (isflush-list (list-ref alist 9))
                                      (triples-list (list-ref alist 10))
                                      (toppair-list (list-ref alist 11)))
                                  (let ((count-string
                                         (list-ref count-list 0))
                                        (count-value
                                         (list-ref count-list 1))
                                        (value-string
                                         (list-ref value-list 0))
                                        (value-value
                                         (list-ref value-list 1))
                                        (signature-string
                                         (list-ref signature-list 0))
                                        (signature-value
                                         (list-ref signature-list 1))
                                        (isflush-string
                                         (list-ref isflush-list 0))
                                        (isflush-value
                                         (list-ref isflush-list 1))
                                        (triples-string
                                         (list-ref triples-list 0))
                                        (triples-value
                                         (list-ref triples-list 1))
                                        (toppair-string
                                         (list-ref toppair-list 0))
                                        (toppair-value
                                         (list-ref toppair-list 1)))
                                    (begin
                                      (nonflush-assert-fulls-hash-results
                                       sub-name err-1
                                       count-string count-value
                                       value-string value-value
                                       signature-string signature-value
                                       isflush-string isflush-value
                                       triples-string triples-value
                                       toppair-string toppair-value
                                       sub-htable result-hash-table)
                                      )))
                                ))
                          )))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (nonflush-assert-straight-hash-results
         sub-name err-1
         count-string count-value
         value-string value-value
         signature-string signature-value
         isflush-string isflush-value
         mask-string mask-value
         sub-htable result-hash-table)
  (begin
    (let ((result-count
           (hash-ref
            sub-htable count-string -1))
          (result-value
           (hash-ref
            sub-htable value-string -1))
          (result-signature
           (hash-ref
            sub-htable signature-string -1))
          (result-isflush
           (hash-ref
            sub-htable isflush-string -1))
          (result-mask
           (hash-ref
            sub-htable mask-string -1)))
      (let ((err-3
             (format
              #f "shouldbe count = ~a, result = ~a"
              count-value result-count))
            (err-4
             (format
              #f "shouldbe value = ~a, result = ~a"
              value-value result-value))
            (err-5
             (format
              #f "shouldbe signature = ~a, result = ~a"
              signature-value result-signature))
            (err-6
             (format
              #f "shouldbe isflush = ~a, result = ~a"
              isflush-value result-isflush))
            (err-7
             (ice-9-format:format
              #f "shouldbe mask = ~x, result = ~x"
              mask-value result-mask)))
        (begin
          (unittest2:assert?
           (equal? count-value result-count)
           sub-name
           (string-append err-1 err-3)
           result-hash-table)

          (unittest2:assert?
           (equal? value-value result-value)
           sub-name
           (string-append err-1 err-4)
           result-hash-table)

          (unittest2:assert?
           (equal? signature-value
                   result-signature)
           sub-name
           (string-append err-1 err-5)
           result-hash-table)

          (unittest2:assert?
           (equal? isflush-value
                   result-isflush)
           sub-name
           (string-append err-1 err-6)
           result-hash-table)

          (unittest2:assert?
           (equal? mask-value result-mask)
           sub-name
           (string-append err-1 err-7)
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-nonflush-straight-hand-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-nonflush-straight-hand-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "kh")
                 (card-module:string-to-card "qd")
                 (card-module:string-to-card "js")
                 (card-module:string-to-card "tc")
                 #t (list "count" 1) (list "value" 50)
                 (list "signature" 31367009)
                 (list "isflush" #f)
                 (list "mask" #x1F00))
           (list (card-module:string-to-card "th")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "jh")
                 (card-module:string-to-card "qs")
                 (card-module:string-to-card "9c")
                 #t (list "count" 1) (list "value" 50)
                 (list "signature" 14535931)
                 (list "isflush" #f)
                 (list "mask" #x0F80))
           (list (card-module:string-to-card "8d")
                 (card-module:string-to-card "9d")
                 (card-module:string-to-card "qc")
                 (card-module:string-to-card "ts")
                 (card-module:string-to-card "jh")
                 #t (list "count" 1) (list "value" 50)
                 (list "signature" 6678671)
                 (list "isflush" #f)
                 (list "mask" #x07C0))
           (list (card-module:string-to-card "jc")
                 (card-module:string-to-card "9d")
                 (card-module:string-to-card "7s")
                 (card-module:string-to-card "8h")
                 (card-module:string-to-card "td")
                 #t (list "count" 1) (list "value" 50)
                 (list "signature" 2800733)
                 (list "isflush" #f)
                 (list "mask" #x03E0))
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "9c")
                 (card-module:string-to-card "ts")
                 #f)
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7s")
                 (card-module:string-to-card "8h")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "9c")
                 #t (list "count" 1) (list "value" 50)
                 (list "signature" 1062347)
                 (list "isflush" #f)
                 (list "mask" #x01F0))
           (list (card-module:string-to-card "5d")
                 (card-module:string-to-card "6c")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "8s")
                 (card-module:string-to-card "9d")
                 #t (list "count" 1) (list "value" 50)
                 (list "signature" 323323)
                 (list "isflush" #f)
                 (list "mask" #x00F8))
           (list (card-module:string-to-card "8c")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "6d")
                 (card-module:string-to-card "5c")
                 (card-module:string-to-card "4s")
                 #t (list "count" 1) (list "value" 50)
                 (list "signature" 85085)
                 (list "isflush" #f)
                 (list "mask" #x007C))
           (list (card-module:string-to-card "4h")
                 (card-module:string-to-card "3s")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "6d")
                 #t (list "count" 1) (list "value" 50)
                 (list "signature" 15015)
                 (list "isflush" #f)
                 (list "mask" #x003E))
           (list (card-module:string-to-card "6d")
                 (card-module:string-to-card "2c")
                 (card-module:string-to-card "3h")
                 (card-module:string-to-card "5c")
                 (card-module:string-to-card "4s")
                 #t (list "count" 1) (list "value" 50)
                 (list "signature" 2310)
                 (list "isflush" #f)
                 (list "mask" #x001F))
           (list (card-module:string-to-card "2s")
                 (card-module:string-to-card "3h")
                 (card-module:string-to-card "5s")
                 (card-module:string-to-card "4s")
                 (card-module:string-to-card "kd")
                 #f)
           (list (card-module:string-to-card "2h")
                 (card-module:string-to-card "5h")
                 (card-module:string-to-card "3c")
                 (card-module:string-to-card "ad")
                 (card-module:string-to-card "4s")
                 #t (list "count" 1) (list "value" 50)
                 (list "signature" 8610)
                 (list "isflush" #f)
                 (list "mask" #x100F))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1 (list-ref alist 0))
                  (card-2 (list-ref alist 1))
                  (card-3 (list-ref alist 2))
                  (card-4 (list-ref alist 3))
                  (card-5 (list-ref alist 4))
                  (shouldbe-bool (list-ref alist 5))
                  (sub-htable (make-hash-table 20)))
              (let ((description
                     (card-module:card-list-to-string
                      (list card-1 card-2 card-3 card-4 card-5)))
                    (prime-1 (card-module:card-to-prime-value card-1))
                    (prime-2 (card-module:card-to-prime-value card-2))
                    (prime-3 (card-module:card-to-prime-value card-3))
                    (prime-4 (card-module:card-to-prime-value card-4))
                    (prime-5 (card-module:card-to-prime-value card-5)))
                (let ((prime-signature
                       (* prime-1 prime-2 prime-3 prime-4 prime-5)))
                  (let ((result-bool
                         (nonflush-module:is-straight-hand
                          card-1 card-2 card-3 card-4 card-5
                          prime-signature sub-htable)))
                    (let ((err-1
                           (format
                            #f "~a : (~a) : ~a : error : "
                            sub-name test-label-index description))
                          (err-2
                           (format
                            #f "shouldbe = ~a, result = ~a"
                            (if shouldbe-bool "true" "false")
                            (if result-bool "true" "false"))))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe-bool result-bool)
                         sub-name
                         (string-append err-1 err-2)
                         result-hash-table)

                        (if (equal? shouldbe-bool #t)
                            (begin
                              (let ((count-list (list-ref alist 6))
                                    (value-list (list-ref alist 7))
                                    (signature-list (list-ref alist 8))
                                    (isflush-list (list-ref alist 9))
                                    (mask-list (list-ref alist 10)))
                                (let ((count-string
                                       (list-ref count-list 0))
                                      (count-value
                                       (list-ref count-list 1))
                                      (value-string
                                       (list-ref value-list 0))
                                      (value-value
                                       (list-ref value-list 1))
                                      (signature-string
                                       (list-ref signature-list 0))
                                      (signature-value
                                       (list-ref signature-list 1))
                                      (isflush-string
                                       (list-ref isflush-list 0))
                                      (isflush-value
                                       (list-ref
                                        isflush-list 1))
                                      (mask-string
                                       (list-ref mask-list 0))
                                      (mask-value
                                       (list-ref mask-list 1)))
                                  (begin
                                    (nonflush-assert-straight-hash-results
                                     sub-name err-1
                                     count-string count-value
                                     value-string value-value
                                     signature-string signature-value
                                     isflush-string isflush-value
                                     mask-string mask-value
                                     sub-htable result-hash-table)
                                    )))
                              ))
                        )))
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (nonflush-assert-triples-hash-results
         sub-name err-1
         count-string count-value
         value-string value-value
         signature-string signature-value
         isflush-string isflush-value
         triples-string triples-value
         kicker1-string kicker1-value
         kicker2-string kicker2-value
         sub-htable result-hash-table)
  (begin
    (let ((result-count
           (hash-ref
            sub-htable count-string #f))
          (result-value
           (hash-ref
            sub-htable value-string #f))
          (result-signature
           (hash-ref
            sub-htable signature-string #f))
          (result-isflush
           (hash-ref
            sub-htable isflush-string #f))
          (result-triples
           (hash-ref
            sub-htable triples-string #f))
          (result-kicker1
           (hash-ref
            sub-htable kicker1-string #f))
          (result-kicker2
           (hash-ref
            sub-htable kicker2-string #f)))
      (let ((err-3
             (format
              #f "shouldbe count = ~a, result = ~a"
              count-value result-count))
            (err-4
             (format
              #f "shouldbe hand value = ~a, result = ~a"
              value-value result-value))
            (err-5
             (format
              #f "shouldbe signature = ~a, result = ~a"
              signature-value result-signature))
            (err-6
             (format
              #f "shouldbe isflush = ~a, result = ~a"
              isflush-value result-isflush))
            (err-7
             (format
              #f "shouldbe triples = ~a, result = ~a"
              triples-value result-triples))
            (err-8
             (format
              #f "shouldbe kicker1 = ~a, result = ~a"
              kicker1-value result-kicker1))
            (err-9
             (format
              #f "shouldbe kicker2 = ~a, result = ~a"
              kicker2-value result-kicker2)))
        (begin
          (unittest2:assert?
           (equal? count-value result-count)
           sub-name
           (string-append err-1 err-3)
           result-hash-table)

          (unittest2:assert?
           (equal? value-value result-value)
           sub-name
           (string-append err-1 err-4)
           result-hash-table)

          (unittest2:assert?
           (equal? signature-value
                   result-signature)
           sub-name
           (string-append err-1 err-5)
           result-hash-table)

          (unittest2:assert?
           (equal? isflush-value
                   result-isflush)
           sub-name
           (string-append err-1 err-6)
           result-hash-table)

          (unittest2:assert?
           (equal? triples-value
                   result-triples)
           sub-name
           (string-append err-1 err-7)
           result-hash-table)

          (unittest2:assert?
           (equal? kicker1-value
                   result-kicker1)
           sub-name
           (string-append err-1 err-8)
           result-hash-table)

          (unittest2:assert?
           (equal? kicker2-value
                   result-kicker2)
           sub-name
           (string-append err-1 err-9)
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-nonflush-triples-hand-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-nonflush-triples-hand-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "ah")
                 (card-module:string-to-card "ad")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "qc")
                 #t (list "count" 1) (list "value" 40)
                 (list "signature" 79052387)
                 (list "isflush" #f)
                 (list "triples-value" 14)
                 (list "kicker1" 13)
                 (list "kicker2" 12))
           (list (card-module:string-to-card "kh")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "th")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "jc")
                 #t (list "count" 1) (list "value" 40)
                 (list "signature" 33785551)
                 (list "isflush" #f)
                 (list "triples-value" 13)
                 (list "kicker1" 11)
                 (list "kicker2" 10))
           (list (card-module:string-to-card "9d")
                 (card-module:string-to-card "qd")
                 (card-module:string-to-card "qc")
                 (card-module:string-to-card "qs")
                 (card-module:string-to-card "8h")
                 #t (list "count" 1) (list "value" 40)
                 (list "signature" 9622493)
                 (list "isflush" #f)
                 (list "triples-value" 12)
                 (list "kicker1" 9)
                 (list "kicker2" 8))
           (list (card-module:string-to-card "7c")
                 (card-module:string-to-card "9d")
                 (card-module:string-to-card "js")
                 (card-module:string-to-card "jh")
                 (card-module:string-to-card "jd")
                 #t (list "count" 1) (list "value" 40)
                 (list "signature" 6024083)
                 (list "isflush" #f)
                 (list "triples-value" 11)
                 (list "kicker1" 9)
                 (list "kicker2" 7))
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "9c")
                 (card-module:string-to-card "ts")
                 #f)
           (list (card-module:string-to-card "ts")
                 (card-module:string-to-card "6s")
                 (card-module:string-to-card "4h")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "tc")
                 #t (list "count" 1) (list "value" 40)
                 (list "signature" 669185)
                 (list "isflush" #f)
                 (list "triples-value" 10)
                 (list "kicker1" 6)
                 (list "kicker2" 4))
           (list (card-module:string-to-card "jd")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "9h")
                 (card-module:string-to-card "9s")
                 (card-module:string-to-card "9d")
                 #t (list "count" 1) (list "value" 40)
                 (list "signature" 2585843)
                 (list "isflush" #f)
                 (list "triples-value" 9)
                 (list "kicker1" 11)
                 (list "kicker2" 7))
           (list (card-module:string-to-card "8c")
                 (card-module:string-to-card "8h")
                 (card-module:string-to-card "8d")
                 (card-module:string-to-card "ac")
                 (card-module:string-to-card "2s")
                 #t (list "count" 1) (list "value" 40)
                 (list "signature" 402866)
                 (list "isflush" #f)
                 (list "triples-value" 8)
                 (list "kicker1" 14)
                 (list "kicker2" 2))
           (list (card-module:string-to-card "qh")
                 (card-module:string-to-card "7s")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "7d")
                 #t (list "count" 1) (list "value" 40)
                 (list "signature" 2519959)
                 (list "isflush" #f)
                 (list "triples-value" 7)
                 (list "kicker1" 13)
                 (list "kicker2" 12))
           (list (card-module:string-to-card "6d")
                 (card-module:string-to-card "6c")
                 (card-module:string-to-card "6h")
                 (card-module:string-to-card "5c")
                 (card-module:string-to-card "qs")
                 #t (list "count" 1) (list "value" 40)
                 (list "signature" 288827)
                 (list "isflush" #f)
                 (list "triples-value" 6)
                 (list "kicker1" 12)
                 (list "kicker2" 5))
           (list (card-module:string-to-card "2s")
                 (card-module:string-to-card "3s")
                 (card-module:string-to-card "5s")
                 (card-module:string-to-card "4s")
                 (card-module:string-to-card "ad")
                 #f)
           (list (card-module:string-to-card "th")
                 (card-module:string-to-card "5h")
                 (card-module:string-to-card "5c")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "7s")
                 #t (list "count" 1) (list "value" 40)
                 (list "signature" 102557)
                 (list "isflush" #f)
                 (list "triples-value" 5)
                 (list "kicker1" 10)
                 (list "kicker2" 7))
           (list (card-module:string-to-card "4h")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "4d")
                 (card-module:string-to-card "3c")
                 (card-module:string-to-card "2s")
                 #t (list "count" 1) (list "value" 40)
                 (list "signature" 750)
                 (list "isflush" #f)
                 (list "triples-value" 4)
                 (list "kicker1" 3)
                 (list "kicker2" 2))
           (list (card-module:string-to-card "3d")
                 (card-module:string-to-card "3s")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "3h")
                 (card-module:string-to-card "tc")
                 #t (list "count" 1) (list "value" 40)
                 (list "signature" 1242)
                 (list "isflush" #f)
                 (list "triples-value" 3)
                 (list "kicker1" 10)
                 (list "kicker2" 2))
           (list (card-module:string-to-card "2h")
                 (card-module:string-to-card "2c")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "ad")
                 (card-module:string-to-card "ks")
                 #t (list "count" 1) (list "value" 40)
                 (list "signature" 12136)
                 (list "isflush" #f)
                 (list "triples-value" 2)
                 (list "kicker1" 14)
                 (list "kicker2" 13))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1 (list-ref alist 0))
                  (card-2 (list-ref alist 1))
                  (card-3 (list-ref alist 2))
                  (card-4 (list-ref alist 3))
                  (card-5 (list-ref alist 4))
                  (shouldbe-bool (list-ref alist 5))
                  (sub-htable (make-hash-table 20))
                  (freq-htable (make-hash-table 20)))
              (let ((description
                     (card-module:card-list-to-string
                      (list card-1 card-2 card-3 card-4 card-5)))
                    (prime-1 (card-module:card-to-prime-value card-1))
                    (prime-2 (card-module:card-to-prime-value card-2))
                    (prime-3 (card-module:card-to-prime-value card-3))
                    (prime-4 (card-module:card-to-prime-value card-4))
                    (prime-5 (card-module:card-to-prime-value card-5)))
                (let ((prime-signature
                       (* prime-1 prime-2 prime-3 prime-4 prime-5)))
                  (begin
                    (nonflush-populate-freq-htable-macro
                     card-1 card-2 card-3 card-4 card-5
                     freq-htable)

                    (let ((result-bool
                           (nonflush-module:is-triples-hand
                            freq-htable prime-signature
                            sub-htable)))
                      (let ((err-1
                             (format
                              #f "~a : (~a) : ~a : error : "
                              sub-name test-label-index description))
                            (err-2
                             (format
                              #f "shouldbe = ~a, result = ~a"
                              (if shouldbe-bool "true" "false")
                              (if result-bool "true" "false"))))
                        (begin
                          (unittest2:assert?
                           (equal? shouldbe-bool result-bool)
                           sub-name
                           (string-append err-1 err-2)
                           result-hash-table)

                          (if (equal? shouldbe-bool #t)
                              (begin
                                (let ((count-list (list-ref alist 6))
                                      (value-list (list-ref alist 7))
                                      (signature-list (list-ref alist 8))
                                      (isflush-list (list-ref alist 9))
                                      (triples-list (list-ref alist 10))
                                      (kicker1-list (list-ref alist 11))
                                      (kicker2-list (list-ref alist 12)))
                                  (let ((count-string
                                         (list-ref count-list 0))
                                        (count-value
                                         (list-ref count-list 1))
                                        (value-string
                                         (list-ref value-list 0))
                                        (value-value
                                         (list-ref value-list 1))
                                        (signature-string
                                         (list-ref signature-list 0))
                                        (signature-value
                                         (list-ref signature-list 1))
                                        (isflush-string
                                         (list-ref isflush-list 0))
                                        (isflush-value
                                         (list-ref isflush-list 1))
                                        (triples-string
                                         (list-ref triples-list 0))
                                        (triples-value
                                         (list-ref triples-list 1))
                                        (kicker1-string
                                         (list-ref kicker1-list 0))
                                        (kicker1-value
                                         (list-ref kicker1-list 1))
                                        (kicker2-string
                                         (list-ref kicker2-list 0))
                                        (kicker2-value
                                         (list-ref kicker2-list 1)))
                                    (begin
                                      (nonflush-assert-triples-hash-results
                                       sub-name err-1
                                       count-string count-value
                                       value-string value-value
                                       signature-string signature-value
                                       isflush-string isflush-value
                                       triples-string triples-value
                                       kicker1-string kicker1-value
                                       kicker2-string kicker2-value
                                       sub-htable result-hash-table)
                                      )))
                                ))
                          )))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (nonflush-assert-twopairs-hash-results
         sub-name err-1
         count-string count-value
         value-string value-value
         signature-string signature-value
         isflush-string isflush-value
         top-pair-string top-pair-value
         second-pair-string second-pair-value
         kicker1-string kicker1-value
         sub-htable result-hash-table)
  (begin
    (let ((result-count
           (hash-ref
            sub-htable count-string #f))
          (result-value
           (hash-ref
            sub-htable value-string #f))
          (result-signature
           (hash-ref
            sub-htable signature-string #f))
          (result-isflush
           (hash-ref
            sub-htable isflush-string #f))
          (result-top-pair
           (hash-ref
            sub-htable top-pair-string #f))
          (result-second-pair
           (hash-ref
            sub-htable second-pair-string #f))
          (result-kicker1
           (hash-ref
            sub-htable kicker1-string #f)))
      (let ((err-3
             (format
              #f "shouldbe count = ~a, result = ~a"
              count-value result-count))
            (err-4
             (format
              #f "shouldbe hand value = ~a, result = ~a"
              value-value result-value))
            (err-5
             (format
              #f "shouldbe signature = ~a, result = ~a"
              signature-value result-signature))
            (err-6
             (format
              #f "shouldbe isflush = ~a, result = ~a"
              isflush-value result-isflush))
            (err-7
             (format
              #f "shouldbe top-pair = ~a, result = ~a"
              top-pair-value result-top-pair))
            (err-8
             (format
              #f "shouldbe second-pair = ~a, result = ~a"
              second-pair-value result-second-pair))
            (err-9
             (format
              #f "shouldbe kicker1 = ~a, result = ~a"
              kicker1-value result-kicker1)))
        (begin
          (unittest2:assert?
           (equal? count-value result-count)
           sub-name
           (string-append err-1 err-3)
           result-hash-table)

          (unittest2:assert?
           (equal? value-value result-value)
           sub-name
           (string-append err-1 err-4)
           result-hash-table)

          (unittest2:assert?
           (equal? signature-value
                   result-signature)
           sub-name
           (string-append err-1 err-5)
           result-hash-table)

          (unittest2:assert?
           (equal? isflush-value
                   result-isflush)
           sub-name
           (string-append err-1 err-6)
           result-hash-table)

          (unittest2:assert?
           (equal? top-pair-value
                   result-top-pair)
           sub-name
           (string-append err-1 err-7)
           result-hash-table)

          (unittest2:assert?
           (equal? second-pair-value
                   result-second-pair)
           sub-name
           (string-append err-1 err-8)
           result-hash-table)

          (unittest2:assert?
           (equal? kicker1-value result-kicker1)
           sub-name
           (string-append err-1 err-9)
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-nonflush-twopairs-hand-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-nonflush-twopairs-hand-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "ah")
                 (card-module:string-to-card "qd")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "qc")
                 #t (list "count" 1) (list "value" 30)
                 (list "signature" 59771317)
                 (list "isflush" #f)
                 (list "top-pair" 14)
                 (list "second-pair" 12)
                 (list "kicker1" 13))
           (list (card-module:string-to-card "th")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "tc")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "jc")
                 #t (list "count" 1) (list "value" 30)
                 (list "signature" 21001829)
                 (list "isflush" #f)
                 (list "top-pair" 13)
                 (list "second-pair" 10)
                 (list "kicker1" 11))
           (list (card-module:string-to-card "9d")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "qc")
                 (card-module:string-to-card "qs")
                 (card-module:string-to-card "5h")
                 #t (list "count" 1) (list "value" 30)
                 (list "signature" 894691)
                 (list "isflush" #f)
                 (list "top-pair" 12)
                 (list "second-pair" 5)
                 (list "kicker1" 9))
           (list (card-module:string-to-card "7c")
                 (card-module:string-to-card "9d")
                 (card-module:string-to-card "7s")
                 (card-module:string-to-card "jh")
                 (card-module:string-to-card "jd")
                 #t (list "count" 1) (list "value" 30)
                 (list "signature" 2700451)
                 (list "isflush" #f)
                 (list "top-pair" 11)
                 (list "second-pair" 7)
                 (list "kicker1" 9))
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "9c")
                 (card-module:string-to-card "ts")
                 #f)
           (list (card-module:string-to-card "ts")
                 (card-module:string-to-card "6s")
                 (card-module:string-to-card "4h")
                 (card-module:string-to-card "6d")
                 (card-module:string-to-card "tc")
                 #t (list "count" 1) (list "value" 30)
                 (list "signature" 320045)
                 (list "isflush" #f)
                 (list "top-pair" 10)
                 (list "second-pair" 6)
                 (list "kicker1" 4))
           (list (card-module:string-to-card "2d")
                 (card-module:string-to-card "2c")
                 (card-module:string-to-card "9h")
                 (card-module:string-to-card "9s")
                 (card-module:string-to-card "ad")
                 #t (list "count" 1) (list "value" 30)
                 (list "signature" 59204)
                 (list "isflush" #f)
                 (list "top-pair" 9)
                 (list "second-pair" 2)
                 (list "kicker1" 14))
           (list (card-module:string-to-card "8c")
                 (card-module:string-to-card "8h")
                 (card-module:string-to-card "ad")
                 (card-module:string-to-card "ac")
                 (card-module:string-to-card "2s")
                 #t (list "count" 1) (list "value" 30)
                 (list "signature" 971618)
                 (list "isflush" #f)
                 (list "top-pair" 14)
                 (list "second-pair" 8)
                 (list "kicker1" 2))
           (list (card-module:string-to-card "qh")
                 (card-module:string-to-card "7s")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "qd")
                 #t (list "count" 1) (list "value" 30)
                 (list "signature" 6009133)
                 (list "isflush" #f)
                 (list "top-pair" 12)
                 (list "second-pair" 7)
                 (list "kicker1" 13))
           (list (card-module:string-to-card "6d")
                 (card-module:string-to-card "6c")
                 (card-module:string-to-card "3h")
                 (card-module:string-to-card "3c")
                 (card-module:string-to-card "qs")
                 #t (list "count" 1) (list "value" 30)
                 (list "signature" 33759)
                 (list "isflush" #f)
                 (list "top-pair" 6)
                 (list "second-pair" 3)
                 (list "kicker1" 12))
           (list (card-module:string-to-card "2s")
                 (card-module:string-to-card "3s")
                 (card-module:string-to-card "5s")
                 (card-module:string-to-card "4s")
                 (card-module:string-to-card "ad")
                 #f)
           (list (card-module:string-to-card "th")
                 (card-module:string-to-card "5h")
                 (card-module:string-to-card "5c")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "2s")
                 #t (list "count" 1) (list "value" 30)
                 (list "signature" 4508)
                 (list "isflush" #f)
                 (list "top-pair" 5)
                 (list "second-pair" 2)
                 (list "kicker1" 10))
           (list (card-module:string-to-card "4h")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "3c")
                 (card-module:string-to-card "2s")
                 #t (list "count" 1) (list "value" 30)
                 (list "signature" 300)
                 (list "isflush" #f)
                 (list "top-pair" 4)
                 (list "second-pair" 2)
                 (list "kicker1" 3))
           (list (card-module:string-to-card "3d")
                 (card-module:string-to-card "3s")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "th")
                 (card-module:string-to-card "tc")
                 #t (list "count" 1) (list "value" 30)
                 (list "signature" 9522)
                 (list "isflush" #f)
                 (list "top-pair" 10)
                 (list "second-pair" 3)
                 (list "kicker1" 2))
           (list (card-module:string-to-card "2h")
                 (card-module:string-to-card "2c")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "ad")
                 (card-module:string-to-card "ks")
                 #t (list "count" 1) (list "value" 30)
                 (list "signature" 224516)
                 (list "isflush" #f)
                 (list "top-pair" 13)
                 (list "second-pair" 2)
                 (list "kicker1" 14))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1 (list-ref alist 0))
                  (card-2 (list-ref alist 1))
                  (card-3 (list-ref alist 2))
                  (card-4 (list-ref alist 3))
                  (card-5 (list-ref alist 4))
                  (shouldbe-bool (list-ref alist 5))
                  (sub-htable (make-hash-table 20))
                  (freq-htable (make-hash-table 20)))
              (let ((description
                     (card-module:card-list-to-string
                      (list card-1 card-2 card-3 card-4 card-5)))
                    (prime-1 (card-module:card-to-prime-value card-1))
                    (prime-2 (card-module:card-to-prime-value card-2))
                    (prime-3 (card-module:card-to-prime-value card-3))
                    (prime-4 (card-module:card-to-prime-value card-4))
                    (prime-5 (card-module:card-to-prime-value card-5)))
                (let ((prime-signature
                       (* prime-1 prime-2 prime-3 prime-4 prime-5)))
                  (begin
                    (nonflush-populate-freq-htable-macro
                     card-1 card-2 card-3 card-4 card-5
                     freq-htable)

                    (let ((result-bool
                           (nonflush-module:is-two-pairs-hand
                            freq-htable prime-signature
                            sub-htable)))
                      (let ((err-1
                             (format
                              #f "~a : (~a) : ~a : error : "
                              sub-name test-label-index description))
                            (err-2
                             (format
                              #f "shouldbe = ~a, result = ~a"
                              (if shouldbe-bool "true" "false")
                              (if result-bool "true" "false"))))
                        (begin
                          (unittest2:assert?
                           (equal? shouldbe-bool result-bool)
                           sub-name
                           (string-append err-1 err-2)
                           result-hash-table)

                          (if (equal? shouldbe-bool #t)
                              (begin
                                (let ((count-list (list-ref alist 6))
                                      (value-list (list-ref alist 7))
                                      (signature-list (list-ref alist 8))
                                      (isflush-list (list-ref alist 9))
                                      (top-pair-list (list-ref alist 10))
                                      (second-pair-list (list-ref alist 11))
                                      (kicker1-list (list-ref alist 12)))
                                  (let ((count-string
                                         (list-ref count-list 0))
                                        (count-value
                                         (list-ref count-list 1))
                                        (value-string
                                         (list-ref value-list 0))
                                        (value-value
                                         (list-ref value-list 1))
                                        (signature-string
                                         (list-ref signature-list 0))
                                        (signature-value
                                         (list-ref signature-list 1))
                                        (isflush-string
                                         (list-ref isflush-list 0))
                                        (isflush-value
                                         (list-ref isflush-list 1))
                                        (top-pair-string
                                         (list-ref top-pair-list 0))
                                        (top-pair-value
                                         (list-ref top-pair-list 1))
                                        (second-pair-string
                                         (list-ref second-pair-list 0))
                                        (second-pair-value
                                         (list-ref second-pair-list 1))
                                        (kicker1-string
                                         (list-ref kicker1-list 0))
                                        (kicker1-value
                                         (list-ref kicker1-list 1)))
                                    (begin
                                      (nonflush-assert-twopairs-hash-results
                                       sub-name err-1
                                       count-string count-value
                                       value-string value-value
                                       signature-string signature-value
                                       isflush-string isflush-value
                                       top-pair-string top-pair-value
                                       second-pair-string second-pair-value
                                       kicker1-string kicker1-value
                                       sub-htable result-hash-table)
                                      )))
                                ))
                          )))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (nonflush-assert-onepair-hash-results
         sub-name err-1
         count-string count-value
         value-string value-value
         signature-string signature-value
         isflush-string isflush-value
         top-pair-string top-pair-value
         kicker1-string kicker1-value
         kicker2-string kicker2-value
         kicker3-string kicker3-value
         sub-htable result-hash-table)
  (begin
    (let ((result-count
           (hash-ref
            sub-htable count-string #f))
          (result-value
           (hash-ref
            sub-htable value-string #f))
          (result-signature
           (hash-ref
            sub-htable signature-string #f))
          (result-isflush
           (hash-ref
            sub-htable isflush-string #f))
          (result-top-pair
           (hash-ref
            sub-htable top-pair-string #f))
          (result-kicker1
           (hash-ref
            sub-htable kicker1-string #f))
          (result-kicker2
           (hash-ref
            sub-htable kicker2-string #f))
          (result-kicker3
           (hash-ref
            sub-htable kicker3-string #f)))
      (let ((err-3
             (format
              #f "shouldbe count = ~a, result = ~a"
              count-value result-count))
            (err-4
             (format
              #f "shouldbe hand value = ~a, result = ~a"
              value-value result-value))
            (err-5
             (format
              #f "shouldbe signature = ~a, result = ~a"
              signature-value result-signature))
            (err-6
             (format
              #f "shouldbe isflush = ~a, result = ~a"
              isflush-value result-isflush))
            (err-7
             (format
              #f "shouldbe top-pair = ~a, result = ~a"
              top-pair-value result-top-pair))
            (err-8
             (format
              #f "shouldbe kicker1 = ~a, result = ~a"
              kicker1-value result-kicker1))
            (err-9
             (format
              #f "shouldbe kicker2 = ~a, result = ~a"
              kicker2-value result-kicker2))
            (err-10
             (format
              #f "shouldbe kicker3 = ~a, result = ~a"
              kicker3-value result-kicker3)))
        (begin
          (unittest2:assert?
           (equal? count-value
                   result-count)
           sub-name
           (string-append err-1 err-3)
           result-hash-table)

          (unittest2:assert?
           (equal? value-value result-value)
           sub-name
           (string-append err-1 err-4)
           result-hash-table)

          (unittest2:assert?
           (equal? signature-value
                   result-signature)
           sub-name
           (string-append err-1 err-5)
           result-hash-table)

          (unittest2:assert?
           (equal? isflush-value
                   result-isflush)
           sub-name
           (string-append err-1 err-6)
           result-hash-table)

          (unittest2:assert?
           (equal? top-pair-value
                   result-top-pair)
           sub-name
           (string-append err-1 err-7)
           result-hash-table)

          (unittest2:assert?
           (equal? kicker1-value
                   result-kicker1)
           sub-name
           (string-append err-1 err-8)
           result-hash-table)

          (unittest2:assert?
           (equal? kicker2-value
                   result-kicker2)
           sub-name
           (string-append err-1 err-9)
           result-hash-table)

          (unittest2:assert?
           (equal? kicker3-value
                   result-kicker3)
           sub-name
           (string-append err-1 err-10)
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
(test-is-nonflush-onepairs-hand-1 result-hash-table)
(begin
   (let ((sub-name
          (format
           #f "~a:test-is-nonflush-onepairs-hand-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "ah")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "qc")
                 #t (list "count" 1) (list "value" 20)
                 (list "signature" 44346461)
                 (list "isflush" #f)
                 (list "top-pair" 14)
                 (list "kicker1" 13)
                 (list "kicker2" 12)
                 (list "kicker3" 10))
           (list (card-module:string-to-card "9h")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "tc")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "jc")
                 #t (list "count" 1) (list "value" 20)
                 (list "signature" 17349337)
                 (list "isflush" #f)
                 (list "top-pair" 13)
                 (list "kicker1" 11)
                 (list "kicker2" 10)
                 (list "kicker3" 9))
           (list (card-module:string-to-card "9d")
                 (card-module:string-to-card "4d")
                 (card-module:string-to-card "qc")
                 (card-module:string-to-card "qs")
                 (card-module:string-to-card "5h")
                 #t (list "count" 1) (list "value" 20)
                 (list "signature" 639065)
                 (list "isflush" #f)
                 (list "top-pair" 12)
                 (list "kicker1" 9)
                 (list "kicker2" 5)
                 (list "kicker3" 4))
           (list (card-module:string-to-card "7c")
                 (card-module:string-to-card "9d")
                 (card-module:string-to-card "2s")
                 (card-module:string-to-card "jh")
                 (card-module:string-to-card "jd")
                 #t (list "count" 1) (list "value" 20)
                 (list "signature" 415454)
                 (list "isflush" #f)
                 (list "top-pair" 11)
                 (list "kicker1" 9)
                 (list "kicker2" 7)
                 (list "kicker3" 2))
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "9c")
                 (card-module:string-to-card "ts")
                 #f)
           (list (card-module:string-to-card "ts")
                 (card-module:string-to-card "6s")
                 (card-module:string-to-card "4h")
                 (card-module:string-to-card "jd")
                 (card-module:string-to-card "tc")
                 #t (list "count" 1) (list "value" 20)
                 (list "signature" 843755)
                 (list "isflush" #f)
                 (list "top-pair" 10)
                 (list "kicker1" 11)
                 (list "kicker2" 6)
                 (list "kicker3" 4))
           (list (card-module:string-to-card "2d")
                 (card-module:string-to-card "qc")
                 (card-module:string-to-card "9h")
                 (card-module:string-to-card "9s")
                 (card-module:string-to-card "ad")
                 #t (list "count" 1) (list "value" 20)
                 (list "signature" 917662)
                 (list "isflush" #f)
                 (list "top-pair" 9)
                 (list "kicker1" 14)
                 (list "kicker2" 12)
                 (list "kicker3" 2))
           (list (card-module:string-to-card "8c")
                 (card-module:string-to-card "8h")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "ac")
                 (card-module:string-to-card "2s")
                 #t (list "count" 1) (list "value" 20)
                 (list "signature" 876826)
                 (list "isflush" #f)
                 (list "top-pair" 8)
                 (list "kicker1" 14)
                 (list "kicker2" 13)
                 (list "kicker3" 2))
           (list (card-module:string-to-card "qh")
                 (card-module:string-to-card "7s")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "jd")
                 #t (list "count" 1) (list "value" 20)
                 (list "signature" 5621447)
                 (list "isflush" #f)
                 (list "top-pair" 7)
                 (list "kicker1" 13)
                 (list "kicker2" 12)
                 (list "kicker3" 11))
           (list (card-module:string-to-card "6d")
                 (card-module:string-to-card "6c")
                 (card-module:string-to-card "3h")
                 (card-module:string-to-card "2c")
                 (card-module:string-to-card "qs")
                 #t (list "count" 1) (list "value" 20)
                 (list "signature" 22506)
                 (list "isflush" #f)
                 (list "top-pair" 6)
                 (list "kicker1" 12)
                 (list "kicker2" 3)
                 (list "kicker3" 2))
           (list (card-module:string-to-card "2s")
                 (card-module:string-to-card "3s")
                 (card-module:string-to-card "5s")
                 (card-module:string-to-card "4s")
                 (card-module:string-to-card "ad")
                 #f)
           (list (card-module:string-to-card "th")
                 (card-module:string-to-card "5h")
                 (card-module:string-to-card "5c")
                 (card-module:string-to-card "3d")
                 (card-module:string-to-card "2s")
                 #t (list "count" 1) (list "value" 20)
                 (list "signature" 6762)
                 (list "isflush" #f)
                 (list "top-pair" 5)
                 (list "kicker1" 10)
                 (list "kicker2" 3)
                 (list "kicker3" 2))
           (list (card-module:string-to-card "4h")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "3c")
                 (card-module:string-to-card "7s")
                 #t (list "count" 1) (list "value" 20)
                 (list "signature" 1950)
                 (list "isflush" #f)
                 (list "top-pair" 4)
                 (list "kicker1" 7)
                 (list "kicker2" 3)
                 (list "kicker3" 2))
           (list (card-module:string-to-card "3d")
                 (card-module:string-to-card "3s")
                 (card-module:string-to-card "2d")
                 (card-module:string-to-card "qh")
                 (card-module:string-to-card "tc")
                 #t (list "count" 1) (list "value" 20)
                 (list "signature" 12834)
                 (list "isflush" #f)
                 (list "top-pair" 3)
                 (list "kicker1" 12)
                 (list "kicker2" 10)
                 (list "kicker3" 2))
           (list (card-module:string-to-card "2h")
                 (card-module:string-to-card "2c")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "ad")
                 (card-module:string-to-card "ks")
                 #t (list "count" 1) (list "value" 20)
                 (list "signature" 42476)
                 (list "isflush" #f)
                 (list "top-pair" 2)
                 (list "kicker1" 14)
                 (list "kicker2" 13)
                 (list "kicker3" 5))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1 (list-ref alist 0))
                  (card-2 (list-ref alist 1))
                  (card-3 (list-ref alist 2))
                  (card-4 (list-ref alist 3))
                  (card-5 (list-ref alist 4))
                  (shouldbe-bool (list-ref alist 5))
                  (sub-htable (make-hash-table 20))
                  (freq-htable (make-hash-table 20)))
              (let ((description
                     (card-module:card-list-to-string
                      (list card-1 card-2 card-3 card-4 card-5)))
                    (prime-1 (card-module:card-to-prime-value card-1))
                    (prime-2 (card-module:card-to-prime-value card-2))
                    (prime-3 (card-module:card-to-prime-value card-3))
                    (prime-4 (card-module:card-to-prime-value card-4))
                    (prime-5 (card-module:card-to-prime-value card-5)))
                (let ((prime-signature
                       (* prime-1 prime-2 prime-3 prime-4 prime-5)))
                  (begin
                    (nonflush-populate-freq-htable-macro
                     card-1 card-2 card-3 card-4 card-5
                     freq-htable)

                    (let ((result-bool
                           (nonflush-module:is-one-pairs-hand
                            freq-htable prime-signature
                            sub-htable)))
                      (let ((err-1
                             (format
                              #f "~a : (~a) : ~a : error : "
                              sub-name test-label-index description))
                            (err-2
                             (format
                              #f "shouldbe = ~a, result = ~a"
                              (if shouldbe-bool "true" "false")
                              (if result-bool "true" "false"))))
                        (begin
                          (unittest2:assert?
                           (equal? shouldbe-bool result-bool)
                           sub-name
                           (string-append err-1 err-2)
                           result-hash-table)

                          (if (equal? shouldbe-bool #t)
                              (begin
                                (let ((count-list (list-ref alist 6))
                                      (value-list (list-ref alist 7))
                                      (signature-list (list-ref alist 8))
                                      (isflush-list (list-ref alist 9))
                                      (top-pair-list (list-ref alist 10))
                                      (kicker1-list (list-ref alist 11))
                                      (kicker2-list (list-ref alist 12))
                                      (kicker3-list (list-ref alist 13)))
                                  (let ((count-string
                                         (list-ref count-list 0))
                                        (count-value
                                         (list-ref count-list 1))
                                        (value-string
                                         (list-ref value-list 0))
                                        (value-value
                                         (list-ref value-list 1))
                                        (signature-string
                                         (list-ref signature-list 0))
                                        (signature-value
                                         (list-ref signature-list 1))
                                        (isflush-string
                                         (list-ref isflush-list 0))
                                        (isflush-value
                                         (list-ref isflush-list 1))
                                        (top-pair-string
                                         (list-ref top-pair-list 0))
                                        (top-pair-value
                                         (list-ref top-pair-list 1))
                                        (kicker1-string
                                         (list-ref kicker1-list 0))
                                        (kicker1-value
                                         (list-ref kicker1-list 1))
                                        (kicker2-string
                                         (list-ref kicker2-list 0))
                                        (kicker2-value
                                         (list-ref kicker2-list 1))
                                        (kicker3-string
                                         (list-ref kicker3-list 0))
                                        (kicker3-value
                                         (list-ref kicker3-list 1)))
                                    (begin
                                      (nonflush-assert-onepair-hash-results
                                       sub-name err-1
                                       count-string count-value
                                       value-string value-value
                                       signature-string signature-value
                                       isflush-string isflush-value
                                       top-pair-string top-pair-value
                                       kicker1-string kicker1-value
                                       kicker2-string kicker2-value
                                       kicker3-string kicker3-value
                                       sub-htable result-hash-table)
                                      )))
                                ))
                          )))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))


;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-nonflush-convert-value-to-bitmask-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-nonflush-convert-value-to-bitmask-1"
           (utils-module:get-basename (current-source-location))))
         (test-list
          (list
           (list 2 #x01)
           (list 3 #x02)
           (list 4 #x04)
           (list 5 #x08)
           (list 6 #x010)
           (list 7 #x020)
           (list 8 #x040)
           (list 9 #x080)
           (list 10 #x0100)
           (list 11 #x0200)
           (list 12 #x0400)
           (list 13 #x0800)
           (list 14 #x01000)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((value (list-ref alist 0))
                  (shouldbe-mask (list-ref alist 1)))
              (let ((result-mask
                     (nonflush-module:convert-value-to-bitmask
                      value)))
                (let ((err-1
                       (format
                        #f "~a : (~a) : error : value = ~a "
                        sub-name test-label-index value))
                      (err-2
                       (ice-9-format:format
                        #f "shouldbe = ~x, result = ~x"
                        shouldbe-mask result-mask)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-mask result-mask)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)

                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-nonflush-convert-bitmask-to-value-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-nonflush-convert-bitmask-to-value-1"
           (utils-module:get-basename (current-source-location))))
         (test-list
          (list
           (list #x01 1 2)
           (list #x02 1 3)
           (list #x03 1 2)
           (list #x03 2 3)
           (list #x04 1 4)
           (list #x08 1 5)
           (list #x010 1 6)
           (list #x020 1 7)
           (list #x040 1 8)
           (list #x080 1 9)
           (list #x0100 1 10)
           (list #x0200 1 11)
           (list #x0400 1 12)
           (list #x0800 1 13)
           (list #x01000 1 14)
           (list #x01000 2 -1)
           (list #x00F 1 2)
           (list #x00F 2 3)
           (list #x00F 3 4)
           (list #x00F 4 5)
           (list #x00FF 1 2)
           (list #x00FF 2 3)
           (list #x00FF 3 4)
           (list #x00FF 4 5)
           (list #x00FF 5 6)
           (list #x00FF 6 7)
           (list #x00FF 7 8)
           (list #x00FF 8 9)
           (list #x00FF 9 -1)
           (list #x00FF 10 -1)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((mask (list-ref alist 0))
                  (nth-bit (list-ref alist 1))
                  (shouldbe-value (list-ref alist 2)))
              (let ((result-value
                     (nonflush-module:convert-bitmask-to-value
                      mask nth-bit)))
                (let ((err-1
                       (ice-9-format:format
                        #f "~a : (~a) : error : mask = ~x, nth-bit = ~a "
                        sub-name test-label-index mask nth-bit))
                      (err-2
                       (ice-9-format:format
                        #f "shouldbe = ~a, result = ~a"
                        shouldbe-value result-value)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-value result-value)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)

                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (nonflush-assert-highcards-hash-results
         sub-name err-1
         count-string count-value
         value-string value-value
         signature-string signature-value
         isflush-string isflush-value
         mask-string mask-value
         sub-htable result-hash-table)
  (begin
    (let ((result-count
           (hash-ref
            sub-htable count-string #f))
          (result-value
           (hash-ref
            sub-htable value-string #f))
          (result-signature
           (hash-ref
            sub-htable signature-string #f))
          (result-isflush
           (hash-ref
            sub-htable isflush-string #f))
          (result-mask
           (hash-ref
            sub-htable mask-string #f)))
      (let ((err-3
             (format
              #f "shouldbe count = ~a, result = ~a"
              count-value result-count))
            (err-4
             (format
              #f "shouldbe hand value = ~a, result = ~a"
              value-value result-value))
            (err-5
             (format
              #f "shouldbe signature = ~a, result = ~a"
              signature-value result-signature))
            (err-6
             (format
              #f "shouldbe isflush = ~a, result = ~a"
              isflush-value result-isflush))
            (err-7
             (ice-9-format:format
              #f "shouldbe mask = ~x, result = ~x"
              mask-value result-mask)))
        (begin
          (unittest2:assert?
           (equal? count-value
                   result-count)
           sub-name
           (string-append err-1 err-3)
           result-hash-table)

          (unittest2:assert?
           (equal? value-value
                   result-value)
           sub-name
           (string-append err-1 err-4)
           result-hash-table)

          (unittest2:assert?
           (equal? signature-value
                   result-signature)
           sub-name
           (string-append err-1 err-5)
           result-hash-table)

          (unittest2:assert?
           (equal? isflush-value
                   result-isflush)
           sub-name
           (string-append err-1 err-6)
           result-hash-table)

          (unittest2:assert?
           (equal? mask-value
                   result-mask)
           sub-name
           (string-append err-1 err-7)
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-nonflush-high-cards-hand-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-nonflush-high-cards-hand-1"
           (utils-module:get-basename (current-source-location))))
         (test-list
          (list
           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "jd")
            (card-module:string-to-card "ts")
            (card-module:string-to-card "9c")
            #t (list "count" 1) (list "value" 10)
            (list "signature" 19224941)
            (list "isflush" #f)
            (list "mask" #x1B80))
           (list
            (card-module:string-to-card "9h")
            (card-module:string-to-card "kd")
            (card-module:string-to-card "tc")
            (card-module:string-to-card "2s")
            (card-module:string-to-card "jc")
            #t (list "count" 1) (list "value" 10)
            (list "signature" 937802)
            (list "isflush" #f)
            (list "mask" #x0B81))
           (list
            (card-module:string-to-card "9d")
            (card-module:string-to-card "4d")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "ts")
            (card-module:string-to-card "5h")
            #t (list "count" 1) (list "value" 10)
            (list "signature" 474145)
            (list "isflush" #f)
            (list "mask" #x058C))
           (list
            (card-module:string-to-card "7c")
            (card-module:string-to-card "8d")
            (card-module:string-to-card "2s")
            (card-module:string-to-card "jh")
            (card-module:string-to-card "kd")
            #t (list "count" 1) (list "value" 10)
            (list "signature" 474266)
            (list "isflush" #f)
            (list "mask" #x0A61))
           (list
            (card-module:string-to-card "6s")
            (card-module:string-to-card "7h")
            (card-module:string-to-card "3d")
            (card-module:string-to-card "9c")
            (card-module:string-to-card "ts")
            #t (list "count" 1) (list "value" 10)
            (list "signature" 187473)
            (list "isflush" #f)
            (list "mask" #x01B2))
           (list
            (card-module:string-to-card "ts")
            (card-module:string-to-card "6s")
            (card-module:string-to-card "4h")
            (card-module:string-to-card "jd")
            (card-module:string-to-card "qc")
            #t (list "count" 1) (list "value" 10)
            (list "signature" 1137235)
            (list "isflush" #f)
            (list "mask" #x0714))
           (list
            (card-module:string-to-card "4d")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "5h")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "ad")
            #t (list "count" 1) (list "value" 10)
            (list "signature" 845215)
            (list "isflush" #f)
            (list "mask" #x148C))
           (list
            (card-module:string-to-card "8c")
            (card-module:string-to-card "7h")
            (card-module:string-to-card "kd")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "3s")
            #t (list "count" 1) (list "value" 10)
            (list "signature" 1005771)
            (list "isflush" #f)
            (list "mask" #x1862))
           (list
            (card-module:string-to-card "qh")
            (card-module:string-to-card "4s")
            (card-module:string-to-card "kd")
            (card-module:string-to-card "7c")
            (card-module:string-to-card "jd")
            #t (list "count" 1) (list "value" 10)
            (list "signature" 2162095)
            (list "isflush" #f)
            (list "mask" #x0E24))
           (list
            (card-module:string-to-card "6d")
            (card-module:string-to-card "5c")
            (card-module:string-to-card "3h")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "qs")
            #t (list "count" 1) (list "value" 10)
            (list "signature" 14322)
            (list "isflush" #f)
            (list "mask" #x041B))
           (list
            (card-module:string-to-card "2s")
            (card-module:string-to-card "3s")
            (card-module:string-to-card "5s")
            (card-module:string-to-card "4s")
            (card-module:string-to-card "td")
            #t (list "count" 1) (list "value" 10)
            (list "signature" 4830)
            (list "isflush" #f)
            (list "mask" #x10F))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1 (list-ref alist 0))
                  (card-2 (list-ref alist 1))
                  (card-3 (list-ref alist 2))
                  (card-4 (list-ref alist 3))
                  (card-5 (list-ref alist 4))
                  (shouldbe-bool (list-ref alist 5))
                  (sub-htable (make-hash-table 20)))
              (let ((description
                     (card-module:card-list-to-string
                      (list card-1 card-2 card-3 card-4 card-5)))
                    (prime-1 (card-module:card-to-prime-value card-1))
                    (prime-2 (card-module:card-to-prime-value card-2))
                    (prime-3 (card-module:card-to-prime-value card-3))
                    (prime-4 (card-module:card-to-prime-value card-4))
                    (prime-5 (card-module:card-to-prime-value card-5)))
                (let ((prime-signature
                       (* prime-1 prime-2 prime-3 prime-4 prime-5)))
                  (let ((result-bool
                         (nonflush-module:is-high-cards-hand
                          card-1 card-2 card-3 card-4 card-5
                          prime-signature sub-htable)))
                    (let ((err-1
                           (format
                            #f "~a : (~a) : ~a : error : "
                            sub-name test-label-index description))
                          (err-2
                           (format
                            #f "shouldbe = ~a, result = ~a"
                            (if shouldbe-bool "true" "false")
                            (if result-bool "true" "false"))))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe-bool result-bool)
                         sub-name
                         (string-append err-1 err-2)
                         result-hash-table)

                        (if (equal? shouldbe-bool #t)
                            (begin
                              (let ((count-list (list-ref alist 6))
                                    (value-list (list-ref alist 7))
                                    (signature-list (list-ref alist 8))
                                    (isflush-list (list-ref alist 9))
                                    (mask-list (list-ref alist 10)))
                                (let ((count-string
                                       (list-ref count-list 0))
                                      (count-value
                                       (list-ref count-list 1))
                                      (value-string
                                       (list-ref value-list 0))
                                      (value-value
                                       (list-ref value-list 1))
                                      (signature-string
                                       (list-ref signature-list 0))
                                      (signature-value
                                       (list-ref signature-list 1))
                                      (isflush-string
                                       (list-ref isflush-list 0))
                                      (isflush-value
                                       (list-ref isflush-list 1))
                                      (mask-string
                                       (list-ref mask-list 0))
                                      (mask-value
                                       (list-ref mask-list 1)))
                                  (begin
                                    (nonflush-assert-highcards-hash-results
                                     sub-name err-1
                                     count-string count-value
                                     value-string value-value
                                     signature-string signature-value
                                     isflush-string isflush-value
                                     mask-string mask-value
                                     sub-htable result-hash-table)
                                    )))
                              ))
                        ))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-nonflush-hand-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-nonflush-high-cards-hand-1"
           (utils-module:get-basename (current-source-location))))
         (test-list
          (list
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "kh")
                 (card-module:string-to-card "jd")
                 (card-module:string-to-card "ts")
                 (card-module:string-to-card "9c")
                 #t)
           (list (card-module:string-to-card "9h")
                 (card-module:string-to-card "kd")
                 (card-module:string-to-card "tc")
                 (card-module:string-to-card "2s")
                 (card-module:string-to-card "jc")
                 #t)
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "ah")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "qc")
                 #t)
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "ah")
                 (card-module:string-to-card "qd")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "qc")
                 #t)
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "ah")
                 (card-module:string-to-card "ad")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "qc")
                 #t)
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "kh")
                 (card-module:string-to-card "qd")
                 (card-module:string-to-card "js")
                 (card-module:string-to-card "tc")
                 #t)
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "ah")
                 (card-module:string-to-card "ad")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "kc")
                 #t)
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "ah")
                 (card-module:string-to-card "ad")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "ac")
                 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1 (list-ref alist 0))
                  (card-2 (list-ref alist 1))
                  (card-3 (list-ref alist 2))
                  (card-4 (list-ref alist 3))
                  (card-5 (list-ref alist 4))
                  (shouldbe-bool (list-ref alist 5))
                  (sub-htable (make-hash-table 20)))
              (let ((description
                     (card-module:card-list-to-string
                      (list card-1 card-2 card-3 card-4 card-5)))
                    (prime-1 (card-module:card-to-prime-value card-1))
                    (prime-2 (card-module:card-to-prime-value card-2))
                    (prime-3 (card-module:card-to-prime-value card-3))
                    (prime-4 (card-module:card-to-prime-value card-4))
                    (prime-5 (card-module:card-to-prime-value card-5)))
                (let ((prime-signature
                       (* prime-1 prime-2 prime-3 prime-4 prime-5)))
                  (let ((result-bool
                         (nonflush-module:is-nonflush-hand
                          card-1 card-2 card-3 card-4 card-5
                          prime-signature sub-htable)))
                    (let ((err-1
                           (format
                            #f "~a : (~a) : ~a : error : "
                            sub-name test-label-index description))
                          (err-2
                           (format
                            #f "shouldbe = ~a, result = ~a"
                            (if shouldbe-bool "true" "false")
                            (if result-bool "true" "false"))))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe-bool result-bool)
                         sub-name
                         (string-append err-1 err-2)
                         result-hash-table)
                        ))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for distinct-module.scm                   ###
;;;###                                                       ###
;;;###  last updated August 8, 2024                          ###
;;;###                                                       ###
;;;###  created June 24, 2024                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### ice-9 format to automatically commafy using format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((card-module)
              :renamer (symbol-prefix-proc 'card-module:)))

(use-modules ((flush-module)
              :renamer (symbol-prefix-proc 'flush-module:)))

(use-modules ((nonflush-module)
              :renamer (symbol-prefix-proc 'nonflush-module:)))

(use-modules ((distinct-module)
              :renamer (symbol-prefix-proc 'distinct-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################

;;;###  card representation
;;;###    3          2          1          0
;;;###   10987654 32109876 54321098 76543210
;;;###  +--------+--------+--------+--------+
;;;###  |xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
;;;###  +--------+--------+--------+--------+
;;;###
;;;###  p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
;;;###  r = rank of card (deuce=2,trey=3,four=4,five=4,...,ace=14)
;;;###  cdhs = suit of card (bit turned on based on suit of card)
;;;###  b = bit turned on depending on rank of card
;;;###
;;;###  Using such a scheme, here are some bit pattern examples:
;;;###
;;;###  xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
;;;###  00001000 00000000 01001011 00100101    King of Diamonds
;;;###  00000000 00001000 00010011 00000111    Five of Spades
;;;###  00000010 00000000 10001001 00011101    Jack of Clubs

;;;###
;;;###  hand values
;;;###  royal flush 100
;;;###  straight flush 90
;;;###  four-of-a-kind 80
;;;###  full-house 70
;;;###  flush 60
;;;###  straight 50
;;;###  three-of-a-kind 40
;;;###  two-pairs 30
;;;###  one-pairs 20
;;;###  high-cards 10
;;;###

;;;###
;;;###  data for hash of hash tables
;;;###  hash(prime-signature) -> hash(string) -> value
;;;###
;;;###  "value" - hand values (from 10 to 100)
;;;###  "rank" - hand rank (from 1 to 7,462)
;;;###  "mask" - rank of 5 cards in bit format
;;;###  "count" - number of hands with this prime signature
;;;###  "signature" - prime signature
;;;###  "isflush" - bool flush indicator
;;;###  "quads-value" - card value of 4-of-a-kind
;;;###  "triples-value" - card value of 3-of-a-kind
;;;###  "top-pair" - card value of top pair
;;;###  "second-pair" - card value of second pair
;;;###  "kicker1" - card value of top kicker
;;;###  "kicker2" - card value of second kicker
;;;###  "kicker3" - card value of third kicker
;;;###
;;;###

;;;#############################################################
;;;#############################################################
(define (distinct-assert-mask-function
         sub-name test-label-index description
         shouldbe-mask result-mask
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) : ~a : error : "
            sub-name test-label-index description))
          (err-2
           (ice-9-format:format
            #f "shouldbe = ~x, result = ~x"
            shouldbe-mask result-mask)))
      (begin
        (unittest2:assert?
         (equal? shouldbe-mask result-mask)
         sub-name
         (string-append err-1 err-2)
         result-hash-table)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-distinct-calculate-hand-rankings-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-distinct-calculate-hand-rankings-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "qd")
            (card-module:string-to-card "jc")
            (card-module:string-to-card "ts")
            #x1F00)
           (list
            (card-module:string-to-card "ks")
            (card-module:string-to-card "qh")
            (card-module:string-to-card "jd")
            (card-module:string-to-card "tc")
            (card-module:string-to-card "9s")
            #x0F80)
           (list (card-module:string-to-card "8s")
                 (card-module:string-to-card "9h")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "jc")
                 (card-module:string-to-card "qs")
                 #x07C0)
           (list (card-module:string-to-card "9s")
                 (card-module:string-to-card "8h")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "js")
                 #x03E0)
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "8d")
                 (card-module:string-to-card "9c")
                 (card-module:string-to-card "ts")
                 #x01F0)
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "9d")
                 (card-module:string-to-card "8c")
                 (card-module:string-to-card "5s")
                 #x00F8)
           (list (card-module:string-to-card "5s")
                 (card-module:string-to-card "4h")
                 (card-module:string-to-card "6d")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "8s")
                 #x007C)
           (list (card-module:string-to-card "7s")
                 (card-module:string-to-card "6h")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "3s")
                 #x003E)
           (list (card-module:string-to-card "2s")
                 (card-module:string-to-card "6h")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "3s")
                 #x001F)
           (list (card-module:string-to-card "2s")
                 (card-module:string-to-card "3h")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "as")
                 #x100F)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1 (list-ref alist 0))
                  (card-2 (list-ref alist 1))
                  (card-3 (list-ref alist 2))
                  (card-4 (list-ref alist 3))
                  (card-5 (list-ref alist 4))
                  (shouldbe-mask (list-ref alist 5)))
              (let ((result-mask
                     (nonflush-module:is-straight-mask
                      card-1 card-2 card-3 card-4 card-5))
                    (description (card-module:card-list-to-string
                                  (list card-1 card-2 card-3
                                        card-4 card-5))))
                (begin
                  (distinct-assert-mask-function
                   sub-name test-label-index description
                   shouldbe-mask result-mask
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))



;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

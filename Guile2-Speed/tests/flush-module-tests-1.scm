;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for flush-module.scm                      ###
;;;###                                                       ###
;;;###  last updated August 8, 2024                          ###
;;;###                                                       ###
;;;###  created June 24, 2024                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### ice-9 format to automatically commafy using format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((card-module)
              :renamer (symbol-prefix-proc 'card-module:)))

(use-modules ((flush-module)
              :renamer (symbol-prefix-proc 'flush-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;###  card representation
;;;###    3          2          1          0
;;;###   10987654 32109876 54321098 76543210
;;;###  +--------+--------+--------+--------+
;;;###  |xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
;;;###  +--------+--------+--------+--------+
;;;###
;;;###  p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
;;;###  r = rank of card (deuce=2,trey=3,four=4,five=4,...,ace=14)
;;;###  cdhs = suit of card (bit turned on based on suit of card)
;;;###  b = bit turned on depending on rank of card
;;;###
;;;###  Using such a scheme, here are some bit pattern examples:
;;;###
;;;###  xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
;;;###  00001000 00000000 01001011 00100101    King of Diamonds
;;;###  00000000 00001000 00010011 00000111    Five of Spades
;;;###  00000010 00000000 10001001 00011101    Jack of Clubs


;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-straight-mask-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-straight-mask-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "kh")
                 (card-module:string-to-card "qd")
                 (card-module:string-to-card "jc")
                 (card-module:string-to-card "ts")
                 #x1F00)
           (list (card-module:string-to-card "ks")
                 (card-module:string-to-card "qh")
                 (card-module:string-to-card "jd")
                 (card-module:string-to-card "tc")
                 (card-module:string-to-card "9s")
                 #x0F80)
           (list (card-module:string-to-card "8s")
                 (card-module:string-to-card "9h")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "jc")
                 (card-module:string-to-card "qs")
                 #x07C0)
           (list (card-module:string-to-card "9s")
                 (card-module:string-to-card "8h")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "js")
                 #x03E0)
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "8d")
                 (card-module:string-to-card "9c")
                 (card-module:string-to-card "ts")
                 #x01F0)
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "9d")
                 (card-module:string-to-card "8c")
                 (card-module:string-to-card "5s")
                 #x00F8)
           (list (card-module:string-to-card "5s")
                 (card-module:string-to-card "4h")
                 (card-module:string-to-card "6d")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "8s")
                 #x007C)
           (list (card-module:string-to-card "7s")
                 (card-module:string-to-card "6h")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "3s")
                 #x003E)
           (list (card-module:string-to-card "2s")
                 (card-module:string-to-card "6h")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "3s")
                 #x001F)
           (list (card-module:string-to-card "2s")
                 (card-module:string-to-card "3h")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "as")
                 #x100F)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1 (list-ref alist 0))
                  (card-2 (list-ref alist 1))
                  (card-3 (list-ref alist 2))
                  (card-4 (list-ref alist 3))
                  (card-5 (list-ref alist 4))
                  (shouldbe (list-ref alist 5)))
              (let ((result
                     (flush-module:is-straight-mask
                      card-1 card-2 card-3 card-4 card-5))
                    (description
                     (card-module:card-list-to-string
                      (list card-1 card-2 card-3
                            card-4 card-5))))
                (let ((err-1
                       (format
                        #f "~a : (~a) : ~a : error : "
                        sub-name test-label-index description))
                      (err-2
                       (ice-9-format:format
                        #f "shouldbe = ~x, result = ~x"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-flush-bool-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-flush-bool-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "qs")
                 (card-module:string-to-card "js")
                 (card-module:string-to-card "ts")
                 #t)
           (list (card-module:string-to-card "kh")
                 (card-module:string-to-card "qh")
                 (card-module:string-to-card "jh")
                 (card-module:string-to-card "th")
                 (card-module:string-to-card "9h")
                 #t)
           (list (card-module:string-to-card "8d")
                 (card-module:string-to-card "9d")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "jd")
                 (card-module:string-to-card "qd")
                 #t)
           (list (card-module:string-to-card "9c")
                 (card-module:string-to-card "8c")
                 (card-module:string-to-card "tc")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "jc")
                 #t)
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "8d")
                 (card-module:string-to-card "9c")
                 (card-module:string-to-card "ts")
                 #f)
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7s")
                 (card-module:string-to-card "9s")
                 (card-module:string-to-card "8s")
                 (card-module:string-to-card "5s")
                 #t)
           (list (card-module:string-to-card "5c")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "6c")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "8c")
                 #t)
           (list (card-module:string-to-card "7h")
                 (card-module:string-to-card "6h")
                 (card-module:string-to-card "5h")
                 (card-module:string-to-card "4h")
                 (card-module:string-to-card "3h")
                 #t)
           (list (card-module:string-to-card "2d")
                 (card-module:string-to-card "6d")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "4d")
                 (card-module:string-to-card "3d")
                 #t)
           (list (card-module:string-to-card "2s")
                 (card-module:string-to-card "3s")
                 (card-module:string-to-card "5s")
                 (card-module:string-to-card "4s")
                 (card-module:string-to-card "ad")
                 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1 (list-ref alist 0))
                  (card-2 (list-ref alist 1))
                  (card-3 (list-ref alist 2))
                  (card-4 (list-ref alist 3))
                  (card-5 (list-ref alist 4))
                  (shouldbe-bool (list-ref alist 5)))
              (let ((result-bool
                     (flush-module:is-flush-bool
                      card-1 card-2 card-3 card-4 card-5))
                    (description
                     (card-module:card-list-to-string
                      (list card-1 card-2 card-3
                            card-4 card-5))))
                (let ((err-1
                       (format
                        #f "~a : (~a) : ~a : error : "
                        sub-name test-label-index description))
                      (err-2
                       (format
                        #f "shouldbe = ~a, result = ~a"
                        (if shouldbe-bool "true" "false")
                        (if result-bool "true" "false"))))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-bool result-bool)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (flush-assert-flush-hash-results
         sub-name err-1
         count-string count-value
         value-string value-value
         signature-string signature-value
         isflush-string isflush-value
         mask-string mask-value
         sub-htable result-hash-table)
  (begin
    (let ((result-count
           (hash-ref
            sub-htable count-string #f))
          (result-value
           (hash-ref
            sub-htable value-string #f))
          (result-signature
           (hash-ref
            sub-htable signature-string #f))
          (result-isflush
           (hash-ref
            sub-htable isflush-string #f))
          (result-mask
           (hash-ref
            sub-htable mask-string #f)))
      (begin
        (let ((err-3
               (format
                #f "shouldbe count = ~a, result = ~a"
                count-value result-count))
              (err-4
               (format
                #f "shouldbe value = ~a, result = ~a"
                value-value result-value))
              (err-5
               (format
                #f "shouldbe signature = ~a, result = ~a"
                signature-value result-signature))
              (err-6
               (format
                #f "shouldbe isflush = ~a, result = ~a"
                isflush-value result-isflush))
              (err-7
               (ice-9-format:format
                #f "shouldbe mask = ~x, result = ~x"
                mask-value result-mask)))
          (begin
            (unittest2:assert?
             (equal? count-value result-count)
             sub-name
             (string-append err-1 err-3)
             result-hash-table)

            (unittest2:assert?
             (equal? value-value result-value)
             sub-name
             (string-append err-1 err-4)
             result-hash-table)

            (unittest2:assert?
             (equal? signature-value
                     result-signature)
             sub-name
             (string-append err-1 err-5)
             result-hash-table)

            (unittest2:assert?
             (equal? isflush-value
                     result-isflush)
             sub-name
             (string-append err-1 err-6)
             result-hash-table)

            (unittest2:assert?
             (equal? mask-value result-mask)
             sub-name
             (string-append err-1 err-7)
             result-hash-table)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-flush-hand-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-flush-hand-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (card-module:string-to-card "as")
                 (card-module:string-to-card "ks")
                 (card-module:string-to-card "qs")
                 (card-module:string-to-card "js")
                 (card-module:string-to-card "ts")
                 #t (list "count" 1) (list "value" 100)
                 (list "signature" 31367009)
                 (list "isflush" #t)
                 (list "mask" #x1F00))
           (list (card-module:string-to-card "kh")
                 (card-module:string-to-card "qh")
                 (card-module:string-to-card "jh")
                 (card-module:string-to-card "th")
                 (card-module:string-to-card "9h")
                 #t (list "count" 1) (list "value" 90)
                 (list "signature" 14535931)
                 (list "isflush" #t)
                 (list "mask" #x0F80))
           (list (card-module:string-to-card "8d")
                 (card-module:string-to-card "9d")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "jd")
                 (card-module:string-to-card "qd")
                 #t (list "count" 1) (list "value" 90)
                 (list "signature" 6678671)
                 (list "isflush" #t)
                 (list "mask" #x07C0))
           (list (card-module:string-to-card "9c")
                 (card-module:string-to-card "8c")
                 (card-module:string-to-card "tc")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "jc")
                 #t (list "count" 1) (list "value" 90)
                 (list "signature" 2800733)
                 (list "isflush" #t)
                 (list "mask" #x03E0))
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7h")
                 (card-module:string-to-card "8d")
                 (card-module:string-to-card "9c")
                 (card-module:string-to-card "ts")
                 #f)
           (list (card-module:string-to-card "6s")
                 (card-module:string-to-card "7s")
                 (card-module:string-to-card "8s")
                 (card-module:string-to-card "9s")
                 (card-module:string-to-card "ts")
                 #t (list "count" 1) (list "value" 90)
                 (list "signature" 1062347)
                 (list "isflush" #t)
                 (list "mask" #x01F0))
           (list (card-module:string-to-card "6d")
                 (card-module:string-to-card "7d")
                 (card-module:string-to-card "9d")
                 (card-module:string-to-card "8d")
                 (card-module:string-to-card "5d")
                 #t (list "count" 1) (list "value" 90)
                 (list "signature" 323323)
                 (list "isflush" #t)
                 (list "mask" #x00F8))
           (list (card-module:string-to-card "5c")
                 (card-module:string-to-card "4c")
                 (card-module:string-to-card "6c")
                 (card-module:string-to-card "7c")
                 (card-module:string-to-card "8c")
                 #t (list "count" 1) (list "value" 90)
                 (list "signature" 85085)
                 (list "isflush" #t)
                 (list "mask" #x007C))
           (list (card-module:string-to-card "7h")
                 (card-module:string-to-card "6h")
                 (card-module:string-to-card "5h")
                 (card-module:string-to-card "4h")
                 (card-module:string-to-card "3h")
                 #t (list "count" 1) (list "value" 90)
                 (list "signature" 15015)
                 (list "isflush" #t)
                 (list "mask" #x003E))
           (list (card-module:string-to-card "2d")
                 (card-module:string-to-card "6d")
                 (card-module:string-to-card "5d")
                 (card-module:string-to-card "4d")
                 (card-module:string-to-card "3d")
                 #t (list "count" 1) (list "value" 90)
                 (list "signature" 2310)
                 (list "isflush" #t)
                 (list "mask" #x001F))
           (list (card-module:string-to-card "2s")
                 (card-module:string-to-card "3s")
                 (card-module:string-to-card "5s")
                 (card-module:string-to-card "4s")
                 (card-module:string-to-card "ad")
                 #f)
           (list (card-module:string-to-card "2h")
                 (card-module:string-to-card "3h")
                 (card-module:string-to-card "5h")
                 (card-module:string-to-card "4h")
                 (card-module:string-to-card "ah")
                 #t (list "count" 1) (list "value" 90)
                 (list "signature" 8610)
                 (list "isflush" #t)
                 (list "mask" #x100F))
           (list (card-module:string-to-card "2h")
                 (card-module:string-to-card "3h")
                 (card-module:string-to-card "6h")
                 (card-module:string-to-card "4h")
                 (card-module:string-to-card "ah")
                 #t (list "count" 1) (list "value" 60)
                 (list "signature" 13530)
                 (list "isflush" #t)
                 (list "mask" #x1017))
           (list (card-module:string-to-card "2d")
                 (card-module:string-to-card "td")
                 (card-module:string-to-card "6d")
                 (card-module:string-to-card "4d")
                 (card-module:string-to-card "kd")
                 #t (list "count" 1) (list "value" 60)
                 (list "signature" 93610)
                 (list "isflush" #t)
                 (list "mask" #x0915))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1 (list-ref alist 0))
                  (card-2 (list-ref alist 1))
                  (card-3 (list-ref alist 2))
                  (card-4 (list-ref alist 3))
                  (card-5 (list-ref alist 4))
                  (shouldbe-bool (list-ref alist 5))
                  (sub-htable (make-hash-table 20)))
            (let ((description
                   (card-module:card-list-to-string
                    (list card-1 card-2 card-3 card-4 card-5)))
                  (prime-1 (card-module:card-to-prime-value card-1))
                  (prime-2 (card-module:card-to-prime-value card-2))
                  (prime-3 (card-module:card-to-prime-value card-3))
                  (prime-4 (card-module:card-to-prime-value card-4))
                  (prime-5 (card-module:card-to-prime-value card-5)))
              (let ((prime-signature (* prime-1 prime-2 prime-3
                                        prime-4 prime-5)))
                (let ((result-bool
                       (flush-module:is-flush-hand
                        card-1 card-2 card-3 card-4 card-5
                        prime-signature sub-htable)))
                  (let ((err-1
                         (format
                          #f "~a : (~a) : ~a : error : "
                          sub-name test-label-index description))
                        (err-2
                         (format
                          #f "shouldbe = ~a, result = ~a"
                          (if shouldbe-bool "true" "false")
                          (if result-bool "true" "false"))))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-bool result-bool)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (if (equal? shouldbe-bool #t)
                          (begin
                            (let ((count-list (list-ref alist 6))
                                  (value-list (list-ref alist 7))
                                  (signature-list (list-ref alist 8))
                                  (isflush-list (list-ref alist 9))
                                  (mask-list (list-ref alist 10)))
                              (let ((count-string
                                     (list-ref count-list 0))
                                    (count-value
                                     (list-ref count-list 1))
                                    (value-string
                                     (list-ref value-list 0))
                                    (value-value
                                     (list-ref value-list 1))
                                    (signature-string
                                     (list-ref signature-list 0))
                                    (signature-value
                                     (list-ref signature-list 1))
                                    (isflush-string
                                     (list-ref isflush-list 0))
                                    (isflush-value
                                     (list-ref isflush-list 1))
                                    (mask-string
                                     (list-ref mask-list 0))
                                    (mask-value
                                     (list-ref mask-list 1)))
                                (begin
                                  (flush-assert-flush-hash-results
                                   sub-name err-1
                                   count-string count-value
                                   value-string value-value
                                   signature-string signature-value
                                   isflush-string isflush-value
                                   mask-string mask-value
                                   sub-htable result-hash-table)
                                  )))
                            ))
                      )))
                )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

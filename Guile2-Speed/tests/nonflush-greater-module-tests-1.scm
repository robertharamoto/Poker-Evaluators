;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for nonflush-greater-module.scm           ###
;;;###                                                       ###
;;;###  last updated August 8, 2024                          ###
;;;###                                                       ###
;;;###  created June 26, 2024                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### ice-9 format to automatically commafy using format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((card-module)
              :renamer (symbol-prefix-proc 'card-module:)))

(use-modules ((nonflush-module)
              :renamer (symbol-prefix-proc 'nonflush-module:)))

(use-modules ((nonflush-greater-module)
              :renamer (symbol-prefix-proc 'nonflush-greater-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;###  card representation
;;;###    3          2          1          0
;;;###   10987654 32109876 54321098 76543210
;;;###  +--------+--------+--------+--------+
;;;###  |xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
;;;###  +--------+--------+--------+--------+
;;;###
;;;###  p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
;;;###  r = rank of card (deuce=2,trey=3,four=4,five=4,...,ace=14)
;;;###  cdhs = suit of card (bit turned on based on suit of card)
;;;###  b = bit turned on depending on rank of card
;;;###
;;;###  Using such a scheme, here are some bit pattern examples:
;;;###
;;;###  xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
;;;###  00001000 00000000 01001011 00100101    King of Diamonds
;;;###  00000000 00001000 00010011 00000111    Five of Spades
;;;###  00000010 00000000 10001001 00011101    Jack of Clubs

;;;###
;;;###  hand values
;;;###  royal flush 100
;;;###  straight flush 90
;;;###  four-of-a-kind 80
;;;###  full-house 70
;;;###  flush 60
;;;###  straight 50
;;;###  three-of-a-kind 40
;;;###  two-pairs 30
;;;###  one-pairs 20
;;;###  high-cards 10
;;;###

;;;###
;;;###  data for hash of hash tables
;;;###  hash(prime-signature) -> hash(string) -> value
;;;###
;;;###  "value" - hand values (from 10 to 100)
;;;###  "rank" - hand rank (from 1 to 7,462)
;;;###  "mask" - rank of 5 cards in bit format
;;;###  "count" - number of hands with this prime signature
;;;###  "signature" - prime signature
;;;###  "isflush" - bool flush indicator
;;;###  "quads-value" - card value of 4-of-a-kind
;;;###  "triples-value" - card value of 3-of-a-kind
;;;###  "top-pair" - card value of top pair
;;;###  "second-pair" - card value of second pair
;;;###  "kicker1" - card value of top kicker
;;;###  "kicker2" - card value of second kicker
;;;###  "kicker3" - card value of third kicker
;;;###
;;;###

;;;#############################################################
;;;#############################################################
(define (nonflush-greater-assert-bool-function
         sub-name test-label-index description
         shouldbe-bool result-bool
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) : ~a : error : "
            sub-name test-label-index description))
          (err-2
           (ice-9-format:format
            #f "shouldbe = ~a, result = ~a"
            (if shouldbe-bool "true" "false")
            (if result-bool "true" "false"))))
      (begin
        (unittest2:assert?
         (equal? shouldbe-bool result-bool)
         sub-name
         (string-append err-1 err-2)
         result-hash-table)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax greater-populate-freq-htable-macro
  (syntax-rules ()
    ((greater-populate-freq-htable-macro
      card-1 card-2 card-3 card-4 card-5
      freq-htable)
     (begin
       (let ((card-list
              (list card-1 card-2 card-3 card-4 card-5)))
         (begin
           (for-each
            (lambda (acard)
              (begin
                (let ((avalue
                       (card-module:card-to-value acard)))
                  (let ((acount
                         (hash-ref freq-htable avalue 0)))
                    (begin
                      (hash-set!
                       freq-htable avalue (1+ acount))
                      )))
                )) card-list)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-nonflush-quads1-greater-quads2-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-nonflush-quads1-greater-quads2-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "ad")
            (card-module:string-to-card "qs")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "ah")

            (card-module:string-to-card "as")
            (card-module:string-to-card "js")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "ah")
            (card-module:string-to-card "ad")
            #t
            "quad aces vs quad aces")

           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "ah")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "ad")

            (card-module:string-to-card "ks")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "kc")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "kd")
            #t
            "quad aces vs quad kings")

           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "ah")
            (card-module:string-to-card "ad")

            (card-module:string-to-card "as")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "ah")
            (card-module:string-to-card "ad")
            #f
            "quad aces vs quad aces")

           (list
            (card-module:string-to-card "2s")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "2h")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "2d")

            (card-module:string-to-card "ks")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "kc")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "kd")
            #f
            "quad twos vs quad kings")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1-h1 (list-ref alist 0))
                  (card-2-h1 (list-ref alist 1))
                  (card-3-h1 (list-ref alist 2))
                  (card-4-h1 (list-ref alist 3))
                  (card-5-h1 (list-ref alist 4))
                  (card-1-h2 (list-ref alist 5))
                  (card-2-h2 (list-ref alist 6))
                  (card-3-h2 (list-ref alist 7))
                  (card-4-h2 (list-ref alist 8))
                  (card-5-h2 (list-ref alist 9))
                  (shouldbe-bool (list-ref alist 10))
                  (description (list-ref alist 11))
                  (sub-h1-htable (make-hash-table 20))
                  (sub-h2-htable (make-hash-table 20))
                  (freq1-htable (make-hash-table 10))
                  (freq2-htable (make-hash-table 10)))
              (let ((prime-h1-signature
                     (* (card-module:card-to-prime-value card-1-h1)
                        (card-module:card-to-prime-value card-2-h1)
                        (card-module:card-to-prime-value card-3-h1)
                        (card-module:card-to-prime-value card-4-h1)
                        (card-module:card-to-prime-value card-5-h1)))
                    (prime-h2-signature
                     (* (card-module:card-to-prime-value card-1-h2)
                        (card-module:card-to-prime-value card-2-h2)
                        (card-module:card-to-prime-value card-3-h2)
                        (card-module:card-to-prime-value card-4-h2)
                        (card-module:card-to-prime-value card-5-h2))))
                (begin
                  (greater-populate-freq-htable-macro
                   card-1-h1 card-2-h1 card-3-h1 card-4-h1 card-5-h1
                   freq1-htable)
                  (greater-populate-freq-htable-macro
                   card-1-h2 card-2-h2 card-3-h2 card-4-h2 card-5-h2
                   freq2-htable)

                  (nonflush-module:is-quads-hand
                   freq1-htable prime-h1-signature sub-h1-htable)

                  (nonflush-module:is-quads-hand
                   freq2-htable prime-h2-signature sub-h2-htable)

                  (let ((result-bool
                         (nonflush-greater-module:quads1-greater-quads2
                          sub-h1-htable sub-h2-htable)))
                    (begin
                      (nonflush-greater-assert-bool-function
                       sub-name test-label-index description
                       shouldbe-bool result-bool
                       result-hash-table)
                      ))
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-nonflush-fulls1-greater-fulls2-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-nonflush-fulls1-greater-fulls2-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "ad")
            (card-module:string-to-card "qs")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "qh")

            (card-module:string-to-card "as")
            (card-module:string-to-card "js")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "jh")
            (card-module:string-to-card "ad")
            #t
            "aces full of queens vs aces full of jacks")

           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "kc")
            (card-module:string-to-card "ah")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "ad")

            (card-module:string-to-card "ks")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "kc")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "kd")
            #t
            "aces full of kings vs kings full of nines")

           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "kd")

            (card-module:string-to-card "ks")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "qh")
            (card-module:string-to-card "qd")
            #t
            "kings full of aces vs queens full of kings")

           (list
            (card-module:string-to-card "2s")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "2d")

            (card-module:string-to-card "ks")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "kc")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "9d")
            #f
            "twos full of kings vs nines vs kings")

           (list
            (card-module:string-to-card "ks")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "kc")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "9d")

            (card-module:string-to-card "2s")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "2d")
            #t
            "nines full of kings vs twos vs kings")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1-h1 (list-ref alist 0))
                  (card-2-h1 (list-ref alist 1))
                  (card-3-h1 (list-ref alist 2))
                  (card-4-h1 (list-ref alist 3))
                  (card-5-h1 (list-ref alist 4))
                  (card-1-h2 (list-ref alist 5))
                  (card-2-h2 (list-ref alist 6))
                  (card-3-h2 (list-ref alist 7))
                  (card-4-h2 (list-ref alist 8))
                  (card-5-h2 (list-ref alist 9))
                  (shouldbe-bool (list-ref alist 10))
                  (description (list-ref alist 11))
                  (sub-h1-htable (make-hash-table 20))
                  (sub-h2-htable (make-hash-table 20))
                  (freq1-htable (make-hash-table 10))
                  (freq2-htable (make-hash-table 10)))
              (let ((prime-h1-signature
                     (* (card-module:card-to-prime-value card-1-h1)
                        (card-module:card-to-prime-value card-2-h1)
                        (card-module:card-to-prime-value card-3-h1)
                        (card-module:card-to-prime-value card-4-h1)
                        (card-module:card-to-prime-value card-5-h1)))
                    (prime-h2-signature
                     (* (card-module:card-to-prime-value card-1-h2)
                        (card-module:card-to-prime-value card-2-h2)
                        (card-module:card-to-prime-value card-3-h2)
                        (card-module:card-to-prime-value card-4-h2)
                        (card-module:card-to-prime-value card-5-h2))))
                (begin
                  (greater-populate-freq-htable-macro
                   card-1-h1 card-2-h1 card-3-h1 card-4-h1 card-5-h1
                   freq1-htable)
                  (greater-populate-freq-htable-macro
                   card-1-h2 card-2-h2 card-3-h2 card-4-h2 card-5-h2
                   freq2-htable)

                  (nonflush-module:is-fulls-hand
                   freq1-htable prime-h1-signature sub-h1-htable)

                  (nonflush-module:is-fulls-hand
                   freq2-htable prime-h2-signature sub-h2-htable)

                  (let ((result-bool
                         (nonflush-greater-module:fulls1-greater-fulls2
                          sub-h1-htable sub-h2-htable)))
                    (begin
                      (nonflush-greater-assert-bool-function
                       sub-name test-label-index description
                       shouldbe-bool result-bool
                       result-hash-table)
                      ))
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-nonflush-straight1-greater-straight2-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-nonflush-straight1-greater-straight2-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "kd")
            (card-module:string-to-card "qs")
            (card-module:string-to-card "jc")
            (card-module:string-to-card "th")

            (card-module:string-to-card "ts")
            (card-module:string-to-card "js")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "9d")
            #t
            "ace-high vs king-high")

           (list
            (card-module:string-to-card "qs")
            (card-module:string-to-card "jc")
            (card-module:string-to-card "th")
            (card-module:string-to-card "8s")
            (card-module:string-to-card "9d")

            (card-module:string-to-card "7s")
            (card-module:string-to-card "js")
            (card-module:string-to-card "tc")
            (card-module:string-to-card "8h")
            (card-module:string-to-card "9d")
            #t
            "queen-high vs jack-high")

           (list
            (card-module:string-to-card "5s")
            (card-module:string-to-card "4s")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "ah")
            (card-module:string-to-card "3d")

            (card-module:string-to-card "as")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "jh")
            (card-module:string-to-card "td")
            #f
            "five-high vs ace-high")

           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "jh")
            (card-module:string-to-card "td")

            (card-module:string-to-card "5s")
            (card-module:string-to-card "4s")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "ah")
            (card-module:string-to-card "3d")
            #t
            "ace-high vs five-high")

           (list
            (card-module:string-to-card "3s")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "4h")
            (card-module:string-to-card "5s")
            (card-module:string-to-card "6d")

            (card-module:string-to-card "as")
            (card-module:string-to-card "4c")
            (card-module:string-to-card "3c")
            (card-module:string-to-card "2h")
            (card-module:string-to-card "5d")
            #t
            "six-high vs five-high")

           (list
            (card-module:string-to-card "8s")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "7c")
            (card-module:string-to-card "6h")
            (card-module:string-to-card "5d")

            (card-module:string-to-card "6s")
            (card-module:string-to-card "3c")
            (card-module:string-to-card "4h")
            (card-module:string-to-card "2s")
            (card-module:string-to-card "5d")
            #t
            "nine-high vs six-high")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1-h1 (list-ref alist 0))
                  (card-2-h1 (list-ref alist 1))
                  (card-3-h1 (list-ref alist 2))
                  (card-4-h1 (list-ref alist 3))
                  (card-5-h1 (list-ref alist 4))
                  (card-1-h2 (list-ref alist 5))
                  (card-2-h2 (list-ref alist 6))
                  (card-3-h2 (list-ref alist 7))
                  (card-4-h2 (list-ref alist 8))
                  (card-5-h2 (list-ref alist 9))
                  (shouldbe-bool (list-ref alist 10))
                  (description (list-ref alist 11))
                  (sub-h1-htable (make-hash-table 20))
                  (sub-h2-htable (make-hash-table 20)))
              (let ((prime-h1-signature
                     (* (card-module:card-to-prime-value card-1-h1)
                        (card-module:card-to-prime-value card-2-h1)
                        (card-module:card-to-prime-value card-3-h1)
                        (card-module:card-to-prime-value card-4-h1)
                        (card-module:card-to-prime-value card-5-h1)))
                    (prime-h2-signature
                     (* (card-module:card-to-prime-value card-1-h2)
                        (card-module:card-to-prime-value card-2-h2)
                        (card-module:card-to-prime-value card-3-h2)
                        (card-module:card-to-prime-value card-4-h2)
                        (card-module:card-to-prime-value card-5-h2))))
                (begin
                  (nonflush-module:is-straight-hand
                   card-1-h1 card-2-h1 card-3-h1 card-4-h1 card-5-h1
                   prime-h1-signature sub-h1-htable)

                  (nonflush-module:is-straight-hand
                   card-1-h2 card-2-h2 card-3-h2 card-4-h2 card-5-h2
                   prime-h2-signature sub-h2-htable)

                  (let ((result-bool
                         (nonflush-greater-module:straight1-greater-straight2
                          sub-h1-htable sub-h2-htable)))
                    (begin
                      (nonflush-greater-assert-bool-function
                       sub-name test-label-index description
                       shouldbe-bool result-bool
                       result-hash-table)
                      ))
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-nonflush-triples1-greater-triples2-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-nonflush-triples1-greater-triples2-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "ad")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "qh")

            (card-module:string-to-card "as")
            (card-module:string-to-card "js")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "th")
            (card-module:string-to-card "ad")
            #t
            "trip aces vs trip aces")

           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "kc")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "2d")

            (card-module:string-to-card "qs")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "7h")
            (card-module:string-to-card "qd")
            #t
            "trip kings vs trip nines")

           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "ts")
            (card-module:string-to-card "3c")
            (card-module:string-to-card "th")
            (card-module:string-to-card "td")

            (card-module:string-to-card "4s")
            (card-module:string-to-card "4h")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "4d")
            #t
            "trip tens vs trip fours")

           (list
            (card-module:string-to-card "2s")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "qs")
            (card-module:string-to-card "2d")

            (card-module:string-to-card "2s")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "2d")
            #t
            "trip twos vs trip twos")

           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "9d")

            (card-module:string-to-card "qs")
            (card-module:string-to-card "kc")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "9d")
            #t
            "trip nines vs trip nines")

           (list
            (card-module:string-to-card "ks")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "9d")

            (card-module:string-to-card "qs")
            (card-module:string-to-card "kc")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "9d")
            #f
            "trip nines vs trip nines")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1-h1 (list-ref alist 0))
                  (card-2-h1 (list-ref alist 1))
                  (card-3-h1 (list-ref alist 2))
                  (card-4-h1 (list-ref alist 3))
                  (card-5-h1 (list-ref alist 4))
                  (card-1-h2 (list-ref alist 5))
                  (card-2-h2 (list-ref alist 6))
                  (card-3-h2 (list-ref alist 7))
                  (card-4-h2 (list-ref alist 8))
                  (card-5-h2 (list-ref alist 9))
                  (shouldbe-bool (list-ref alist 10))
                  (description (list-ref alist 11))
                  (sub-h1-htable (make-hash-table 20))
                  (sub-h2-htable (make-hash-table 20))
                  (freq1-htable (make-hash-table 10))
                  (freq2-htable (make-hash-table 10)))
              (let ((prime-h1-signature
                     (* (card-module:card-to-prime-value card-1-h1)
                        (card-module:card-to-prime-value card-2-h1)
                        (card-module:card-to-prime-value card-3-h1)
                        (card-module:card-to-prime-value card-4-h1)
                        (card-module:card-to-prime-value card-5-h1)))
                    (prime-h2-signature
                     (* (card-module:card-to-prime-value card-1-h2)
                        (card-module:card-to-prime-value card-2-h2)
                        (card-module:card-to-prime-value card-3-h2)
                        (card-module:card-to-prime-value card-4-h2)
                        (card-module:card-to-prime-value card-5-h2))))
                (begin
                  (greater-populate-freq-htable-macro
                   card-1-h1 card-2-h1 card-3-h1 card-4-h1 card-5-h1
                   freq1-htable)

                  (greater-populate-freq-htable-macro
                   card-1-h2 card-2-h2 card-3-h2 card-4-h2 card-5-h2
                   freq2-htable)

                  (nonflush-module:is-triples-hand
                   freq1-htable prime-h1-signature sub-h1-htable)

                  (nonflush-module:is-triples-hand
                   freq2-htable prime-h2-signature sub-h2-htable)

                  (let ((result-bool
                         (nonflush-greater-module:triples1-greater-triples2
                          sub-h1-htable sub-h2-htable)))
                    (begin
                      (nonflush-greater-assert-bool-function
                       sub-name test-label-index description
                       shouldbe-bool result-bool
                       result-hash-table)
                      ))
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-nonflush-twopairs1-greater-twopairs2-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-nonflush-twopairs1-greater-twopairs2-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "ad")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "kc")
            (card-module:string-to-card "qh")

            (card-module:string-to-card "as")
            (card-module:string-to-card "js")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "th")
            (card-module:string-to-card "jd")
            #t
            "aces and kings vs aces and jacks")

           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "2d")

            (card-module:string-to-card "ks")
            (card-module:string-to-card "kc")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "7h")
            (card-module:string-to-card "qd")
            #t
            "aces and kings vs kings and queens")

           (list
            (card-module:string-to-card "5s")
            (card-module:string-to-card "ts")
            (card-module:string-to-card "3c")
            (card-module:string-to-card "5h")
            (card-module:string-to-card "3d")

            (card-module:string-to-card "4s")
            (card-module:string-to-card "4h")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "2h")
            (card-module:string-to-card "2d")
            #t
            "fives and threes vs fours and twos")

           (list
            (card-module:string-to-card "2s")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "qs")
            (card-module:string-to-card "kd")

            (card-module:string-to-card "2s")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "kd")
            #t
            "kings and twos vs kings and twos")

           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "qd")

            (card-module:string-to-card "js")
            (card-module:string-to-card "kc")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "jd")
            #t
            "queens and nines vs jacks and nines")

           (list
            (card-module:string-to-card "7s")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "7h")
            (card-module:string-to-card "9d")

            (card-module:string-to-card "7s")
            (card-module:string-to-card "5c")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "7d")
            #t
            "nines and sevens vs nines and sevens")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1-h1 (list-ref alist 0))
                  (card-2-h1 (list-ref alist 1))
                  (card-3-h1 (list-ref alist 2))
                  (card-4-h1 (list-ref alist 3))
                  (card-5-h1 (list-ref alist 4))
                  (card-1-h2 (list-ref alist 5))
                  (card-2-h2 (list-ref alist 6))
                  (card-3-h2 (list-ref alist 7))
                  (card-4-h2 (list-ref alist 8))
                  (card-5-h2 (list-ref alist 9))
                  (shouldbe-bool (list-ref alist 10))
                  (description (list-ref alist 11))
                  (sub-h1-htable (make-hash-table 20))
                  (sub-h2-htable (make-hash-table 20))
                  (freq1-htable (make-hash-table 10))
                  (freq2-htable (make-hash-table 10)))
              (let ((prime-h1-signature
                     (* (card-module:card-to-prime-value card-1-h1)
                        (card-module:card-to-prime-value card-2-h1)
                        (card-module:card-to-prime-value card-3-h1)
                        (card-module:card-to-prime-value card-4-h1)
                        (card-module:card-to-prime-value card-5-h1)))
                    (prime-h2-signature
                     (* (card-module:card-to-prime-value card-1-h2)
                        (card-module:card-to-prime-value card-2-h2)
                        (card-module:card-to-prime-value card-3-h2)
                        (card-module:card-to-prime-value card-4-h2)
                        (card-module:card-to-prime-value card-5-h2))))
                (begin
                  (greater-populate-freq-htable-macro
                   card-1-h1 card-2-h1 card-3-h1 card-4-h1 card-5-h1
                   freq1-htable)

                  (greater-populate-freq-htable-macro
                   card-1-h2 card-2-h2 card-3-h2 card-4-h2 card-5-h2
                   freq2-htable)

                  (nonflush-module:is-two-pairs-hand
                   freq1-htable prime-h1-signature sub-h1-htable)

                  (nonflush-module:is-two-pairs-hand
                   freq2-htable prime-h2-signature sub-h2-htable)

                  (let ((result-bool
                         (nonflush-greater-module:twopairs1-greater-twopairs2
                          sub-h1-htable sub-h2-htable)))
                    (begin
                      (nonflush-greater-assert-bool-function
                       sub-name test-label-index description
                       shouldbe-bool result-bool
                       result-hash-table)
                      ))
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-nonflush-onepairs1-greater-onepairs2-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-nonflush-onepairs1-greater-onepairs2-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "ad")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "jc")
            (card-module:string-to-card "qh")

            (card-module:string-to-card "as")
            (card-module:string-to-card "qs")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "th")
            (card-module:string-to-card "jd")
            #t
            "pair of aces vs pair of aces")

           (list
            (card-module:string-to-card "3s")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "2d")

            (card-module:string-to-card "3s")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "ah")
            (card-module:string-to-card "qd")
            #t
            "pair of kings vs pair of queens")

           (list
            (card-module:string-to-card "5s")
            (card-module:string-to-card "ts")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "5h")
            (card-module:string-to-card "3d")

            (card-module:string-to-card "5s")
            (card-module:string-to-card "5h")
            (card-module:string-to-card "9c")
            (card-module:string-to-card "2h")
            (card-module:string-to-card "3d")
            #t
            "pair of fives vs pair of fives")

           (list
            (card-module:string-to-card "2s")
            (card-module:string-to-card "5c")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "qs")
            (card-module:string-to-card "kd")

            (card-module:string-to-card "2s")
            (card-module:string-to-card "4c")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "qh")
            (card-module:string-to-card "kd")
            #t
            "pair of kings vs pair of kings")

           (list
            (card-module:string-to-card "7s")
            (card-module:string-to-card "8s")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "qd")

            (card-module:string-to-card "qs")
            (card-module:string-to-card "9c")
            (card-module:string-to-card "8h")
            (card-module:string-to-card "6s")
            (card-module:string-to-card "qd")
            #t
            "pair of queens vs pair of queens")

           (list
            (card-module:string-to-card "7s")
            (card-module:string-to-card "8s")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "7h")
            (card-module:string-to-card "9d")

            (card-module:string-to-card "5s")
            (card-module:string-to-card "5c")
            (card-module:string-to-card "ah")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "7d")
            #t
            "pair of sevens vs pair of fives")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1-h1 (list-ref alist 0))
                  (card-2-h1 (list-ref alist 1))
                  (card-3-h1 (list-ref alist 2))
                  (card-4-h1 (list-ref alist 3))
                  (card-5-h1 (list-ref alist 4))
                  (card-1-h2 (list-ref alist 5))
                  (card-2-h2 (list-ref alist 6))
                  (card-3-h2 (list-ref alist 7))
                  (card-4-h2 (list-ref alist 8))
                  (card-5-h2 (list-ref alist 9))
                  (shouldbe-bool (list-ref alist 10))
                  (description (list-ref alist 11))
                  (sub-h1-htable (make-hash-table 20))
                  (sub-h2-htable (make-hash-table 20))
                  (freq1-htable (make-hash-table 10))
                  (freq2-htable (make-hash-table 10)))
              (let ((prime-h1-signature
                     (* (card-module:card-to-prime-value card-1-h1)
                        (card-module:card-to-prime-value card-2-h1)
                        (card-module:card-to-prime-value card-3-h1)
                        (card-module:card-to-prime-value card-4-h1)
                        (card-module:card-to-prime-value card-5-h1)))
                    (prime-h2-signature
                     (* (card-module:card-to-prime-value card-1-h2)
                        (card-module:card-to-prime-value card-2-h2)
                        (card-module:card-to-prime-value card-3-h2)
                        (card-module:card-to-prime-value card-4-h2)
                        (card-module:card-to-prime-value card-5-h2))))
                (begin
                  (greater-populate-freq-htable-macro
                   card-1-h1 card-2-h1 card-3-h1 card-4-h1 card-5-h1
                   freq1-htable)

                  (greater-populate-freq-htable-macro
                   card-1-h2 card-2-h2 card-3-h2 card-4-h2 card-5-h2
                   freq2-htable)

                  (nonflush-module:is-one-pairs-hand
                   freq1-htable prime-h1-signature sub-h1-htable)

                  (nonflush-module:is-one-pairs-hand
                   freq2-htable prime-h2-signature sub-h2-htable)

                  (let ((result-bool
                         (nonflush-greater-module:onepairs1-greater-onepairs2
                          sub-h1-htable sub-h2-htable)))
                    (begin
                      (nonflush-greater-assert-bool-function
                       sub-name test-label-index description
                       shouldbe-bool result-bool
                       result-hash-table)
                      ))
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-nonflush-highcards1-greater-highcards2-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-nonflush-highcards1-greater-highcards2-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "5d")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "jc")
            (card-module:string-to-card "qh")

            (card-module:string-to-card "ks")
            (card-module:string-to-card "qs")
            (card-module:string-to-card "5c")
            (card-module:string-to-card "th")
            (card-module:string-to-card "jd")
            #t
            "ace-high cards vs king-high cards")

           (list
            (card-module:string-to-card "3s")
            (card-module:string-to-card "ac")
            (card-module:string-to-card "kh")
            (card-module:string-to-card "qs")
            (card-module:string-to-card "2d")

            (card-module:string-to-card "3s")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "jc")
            (card-module:string-to-card "ah")
            (card-module:string-to-card "qd")
            #t
            "ace-high vs ace-high")

           (list
            (card-module:string-to-card "ks")
            (card-module:string-to-card "ts")
            (card-module:string-to-card "2c")
            (card-module:string-to-card "6h")
            (card-module:string-to-card "3d")

            (card-module:string-to-card "ks")
            (card-module:string-to-card "5h")
            (card-module:string-to-card "tc")
            (card-module:string-to-card "2h")
            (card-module:string-to-card "3d")
            #t
            "king-high vs king-high")

           (list
            (card-module:string-to-card "2s")
            (card-module:string-to-card "5c")
            (card-module:string-to-card "jh")
            (card-module:string-to-card "qs")
            (card-module:string-to-card "td")

            (card-module:string-to-card "2s")
            (card-module:string-to-card "4c")
            (card-module:string-to-card "jh")
            (card-module:string-to-card "qh")
            (card-module:string-to-card "td")
            #t
            "queen-high vs queen-high")

           (list
            (card-module:string-to-card "7s")
            (card-module:string-to-card "8s")
            (card-module:string-to-card "jc")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "qd")

            (card-module:string-to-card "qs")
            (card-module:string-to-card "jc")
            (card-module:string-to-card "8h")
            (card-module:string-to-card "6s")
            (card-module:string-to-card "9d")
            #t
            "jack-high vs jack-high")

           (list
            (card-module:string-to-card "ts")
            (card-module:string-to-card "8s")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "7h")
            (card-module:string-to-card "9d")

            (card-module:string-to-card "5s")
            (card-module:string-to-card "qc")
            (card-module:string-to-card "6h")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "td")
            #t
            "queen-high vs queen-high")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1-h1 (list-ref alist 0))
                  (card-2-h1 (list-ref alist 1))
                  (card-3-h1 (list-ref alist 2))
                  (card-4-h1 (list-ref alist 3))
                  (card-5-h1 (list-ref alist 4))
                  (card-1-h2 (list-ref alist 5))
                  (card-2-h2 (list-ref alist 6))
                  (card-3-h2 (list-ref alist 7))
                  (card-4-h2 (list-ref alist 8))
                  (card-5-h2 (list-ref alist 9))
                  (shouldbe-bool (list-ref alist 10))
                  (description (list-ref alist 11))
                  (sub-h1-htable (make-hash-table 20))
                  (sub-h2-htable (make-hash-table 20)))
              (let ((prime-h1-signature
                     (* (card-module:card-to-prime-value card-1-h1)
                        (card-module:card-to-prime-value card-2-h1)
                        (card-module:card-to-prime-value card-3-h1)
                        (card-module:card-to-prime-value card-4-h1)
                        (card-module:card-to-prime-value card-5-h1)))
                    (prime-h2-signature
                     (* (card-module:card-to-prime-value card-1-h2)
                        (card-module:card-to-prime-value card-2-h2)
                        (card-module:card-to-prime-value card-3-h2)
                        (card-module:card-to-prime-value card-4-h2)
                        (card-module:card-to-prime-value card-5-h2))))
                (begin
                  (nonflush-module:is-high-cards-hand
                   card-1-h1 card-2-h1 card-3-h1 card-4-h1 card-5-h1
                   prime-h1-signature sub-h1-htable)

                  (nonflush-module:is-high-cards-hand
                   card-1-h2 card-2-h2 card-3-h2 card-4-h2 card-5-h2
                   prime-h2-signature sub-h2-htable)

                  (let ((result-bool
                         (nonflush-greater-module:highcards1-greater-highcards2
                          sub-h1-htable sub-h2-htable)))
                    (begin
                      (nonflush-greater-assert-bool-function
                       sub-name test-label-index description
                       shouldbe-bool result-bool
                       result-hash-table)
                      ))
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for card-module.scm                       ###
;;;###                                                       ###
;;;###  last updated August 8, 2024                          ###
;;;###                                                       ###
;;;###  created June 22, 2024                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((card-module)
              :renamer (symbol-prefix-proc 'card-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################


;;;#############################################################
;;;#############################################################
;;;###
;;;###  +--------+--------+--------+--------+
;;;###  |xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
;;;###  +--------+--------+--------+--------+
;;;###
;;;###  p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
;;;###  r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
;;;###  cdhs = suit of card (bit turned on based on suit of card)
;;;###  b = bit turned on depending on rank of card
;;;###
;;;###  Using such a scheme, here are some bit pattern examples:
;;;###
;;;###  checked 6/22/2024
;;;###  xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
;;;###  00010000 00000000 00011110 00101001    ace of spades
;;;###  00001000 00000000 00101101 00100101    king of hearts
;;;###  00000100 00000000 01001100 00011111    queen of diamonds
;;;###  00000010 00000000 10001011 00011101    jack of clubs
;;;###  xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
;;;###  00000001 00000000 00011010 00010111    ten of spades
;;;###  00000000 10000000 00101001 00010011    nine of hearts
;;;###  00000000 01000000 01001000 00010001    eight of diamonds
;;;###  00000000 00100000 10000111 00001101    seven of clubs
;;;###  00000000 00010000 00010110 00001011    six of spades
;;;###  00000000 00001000 00100101 00000111    five of hearts
;;;###  00000000 00000100 01000100 00000101    four of diamonds
;;;###  00000000 00000010 10000011 00000011    three of clubs
;;;###  00000000 00000001 00010010 00000010    two of spades


;;;#############################################################
;;;#############################################################
(define (card-assert-simple-function
         sub-name test-label-index description
         shouldbe result
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : error (~a) : ~a : "
            sub-name test-label-index description))
          (err-2
           (ice-9-format:format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (let ((error-string
             (string-append err-1 err-2)))
        (begin
          (unittest2:assert?
           (equal? shouldbe result)
           sub-name
           error-string
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (card-assert-string-function
         sub-name test-label-index description
         shouldbe-string result-string
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : error (~a) : ~a : "
            sub-name test-label-index description))
          (err-2
           (ice-9-format:format
            #f "shouldbe = ~s, result = ~s"
            shouldbe-string result-string)))
      (let ((error-string
             (string-append err-1 err-2)))
        (begin
          (unittest2:assert?
           (string-ci=?
            shouldbe-string result-string)
           sub-name
           error-string
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-string-to-card-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-string-to-card-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list "ac" #x10008e29)
           (list "ad" #x10004e29)
           (list "ah" #x10002e29)
           (list "as" #x10001e29)
           (list "kh" #x08002d25)
           (list "qd" #x04004c1f)
           (list "jc" #x02008b1d)
           (list "ts" #x01001a17)
           (list "9h" #x00802913)
           (list "8d" #x00404811)
           (list "7c" #x0020870d)
           (list "6s" #x0010160b)
           (list "5h" #x00082507)
           (list "4d" #x00044405)
           (list "3c" #x00028303)
           (list "2s" #x00011202)
           ;;;###  checked 6/22/2024
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-card (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (card-module:string-to-card test-card)))
                (begin
                  (card-assert-simple-function
                   sub-name test-label-index test-card
                   shouldbe result
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-card-to-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-card-to-string-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list #x10008e29 "ac")
           (list #x10004e29 "ad")
           (list #x10002e29 "ah")
           (list #x10001e29 "as")
           (list #x08002d25 "kh")
           (list #x04004c1f "qd")
           (list #x02008b1d "jc")
           (list #x01001a17 "ts")
           (list #x00802913 "9h")
           (list #x00404811 "8d")
           (list #x0020870d "7c")
           (list #x0010160b "6s")
           (list #x00082507 "5h")
           (list #x00044405 "4d")
           (list #x00028303 "3c")
           (list #x00011202 "2s")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-card (list-ref alist 0))
                  (shouldbe-string (list-ref alist 1)))
              (let ((result-string
                     (card-module:card-to-string test-card)))
                (begin
                  (card-assert-string-function
                   sub-name test-label-index test-card
                   shouldbe-string result-string
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-card-list-to-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-card-list-to-string-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list #x10008e29) "[ac]")
           (list (list #x10004e29 #x10002e29) "[ad, ah]")
           (list (list #x10008e29 #x10004e29 #x10002e29)
                 "[ac, ad, ah]")
           (list (list #x10008e29 #x10004e29
                       #x10002e29 #x10001e29)
                 "[ac, ad, ah, as]")
           (list (list #x10008e29 #x10004e29
                       #x10002e29 #x10001e29
                       #x08002d25)
                 "[ac, ad, ah, as, kh]")
           (list (list #x04004c1f #x02008b1d
                       #x01001a17 #x00802913
                       #x00404811)
                 "[qd, jc, ts, 9h, 8d]")
           (list (list #x0020870d #x0010160b
                       #x00082507 #x00044405
                       #x00028303 #x00011202)
                 "[7c, 6s, 5h, 4d, 3c, 2s]")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-list (list-ref alist 0))
                  (shouldbe-string (list-ref alist 1)))
              (let ((result-string
                     (card-module:card-list-to-string
                      card-list)))
                (begin
                  (card-assert-string-function
                   sub-name test-label-index card-list
                   shouldbe-string result-string
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-card1-greater-card2-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-card1-greater-card2-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list #x10008e29 #x10004e29 #f)
           (list #x10004e29 #x10002e29 #f)
           (list #x10002e29 #x10001e29 #f)
           (list #x10001e29 #x08002d25 #t)
           (list #x08002d25 #x04004c1f #t)
           (list #x04004c1f #x02008b1d #t)
           (list #x02008b1d #x01001a17 #t)
           (list #x01001a17 #x00802913 #t)
           (list #x00802913 #x00404811 #t)
           (list #x00404811 #x0020870d #t)
           (list #x0020870d #x0010160b #t)
           (list #x0010160b #x00082507 #t)
           (list #x00082507 #x00044405 #t)
           (list #x00044405 #x00028303 #t)
           (list #x00028303 #x00011202 #t)
           (list #x00011202 #x00028303 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card1 (list-ref alist 0))
                  (card2 (list-ref alist 1))
                  (shouldbe-bool (list-ref alist 2)))
              (let ((result-bool
                     (card-module:card1-greater-card2?
                      card1 card2))
                    (description
                     (format #f "(~a, ~a)"
                             (card-module:card-to-string card1)
                             (card-module:card-to-string card2))))
                (begin
                  (card-assert-simple-function
                   sub-name test-label-index description
                   shouldbe-bool result-bool
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-card1-equal-card2-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-card1-equal-card2-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list #x10008e29 #x10004e29 #t)
           (list #x10004e29 #x10002e29 #t)
           (list #x10002e29 #x10001e29 #t)
           (list #x10001e29 #x08002d25 #f)
           (list #x08002d25 #x04004c1f #f)
           (list #x04004c1f #x02008b1d #f)
           (list #x02008b1d #x01001a17 #f)
           (list #x01001a17 #x00802913 #f)
           (list #x00802913 #x00404811 #f)
           (list #x00404811 #x0020870d #f)
           (list #x0020870d #x0010160b #f)
           (list #x0010160b #x00082507 #f)
           (list #x00082507 #x00044405 #f)
           (list #x00044405 #x00028303 #f)
           (list #x00028303 #x00011202 #f)
           (list #x00011202 #x00028303 #f)
           (list #x00011202 #x00018202 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card1 (list-ref alist 0))
                  (card2 (list-ref alist 1))
                  (shouldbe-bool (list-ref alist 2)))
              (let ((result-bool
                     (card-module:card1-equal-card2?
                      card1 card2))
                    (description
                     (format #f "(~a, ~a)"
                             (card-module:card-to-string card1)
                             (card-module:card-to-string card2))))
                (begin
                  (card-assert-simple-function
                   sub-name test-label-index description
                   shouldbe-bool result-bool
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-card1-identical-card2-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-card1-identical-card2-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list #x10008e29 #x10008e29 #t)
           (list #x10008e29 #x10004e29 #f)
           (list #x10004e29 #x10002e29 #f)
           (list #x10002e29 #x10001e29 #f)
           (list #x10001e29 #x08002d25 #f)
           (list #x08002d25 #x04004c1f #f)
           (list #x04004c1f #x02008b1d #f)
           (list #x02008b1d #x01001a17 #f)
           (list #x01001a17 #x00802913 #f)
           (list #x00802913 #x00404811 #f)
           (list #x00404811 #x0020870d #f)
           (list #x0020870d #x0010160b #f)
           (list #x0010160b #x00082507 #f)
           (list #x00082507 #x00044405 #f)
           (list #x00044405 #x00028303 #f)
           (list #x00028303 #x00011202 #f)
           (list #x00011202 #x00028303 #f)
           (list #x00011202 #x00011202 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card1 (list-ref alist 0))
                  (card2 (list-ref alist 1))
                  (shouldbe-bool (list-ref alist 2)))
              (let ((result-bool
                     (card-module:card1-identical-card2?
                      card1 card2))
                    (description
                     (format #f "(~a, ~a)"
                             (card-module:card-to-string card1)
                             (card-module:card-to-string card2))))
                (begin
                  (card-assert-simple-function
                   sub-name test-label-index description
                   shouldbe-bool result-bool
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-card-to-value-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-card-to-value-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list #x10008e29 14)
           (list #x10004e29 14)
           (list #x10002e29 14)
           (list #x10001e29 14)
           (list #x08002d25 13)
           (list #x04004c1f 12)
           (list #x02008b1d 11)
           (list #x01001a17 10)
           (list #x00802913 9)
           (list #x00404811 8)
           (list #x0020870d 7)
           (list #x0010160b 6)
           (list #x00082507 5)
           (list #x00044405 4)
           (list #x00028303 3)
           (list #x00011202 2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (card-module:card-to-value card))
                    (description
                     (card-module:card-to-string card)))
                (begin
                  (card-assert-simple-function
                   sub-name test-label-index description
                   shouldbe result
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-card-to-prime-value-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-card-to-prime-value-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list #x10008e29 41)
           (list #x10004e29 41)
           (list #x10002e29 41)
           (list #x10001e29 41)
           (list #x08002d25 37)
           (list #x04004c1f 31)
           (list #x02008b1d 29)
           (list #x01001a17 23)
           (list #x00802913 19)
           (list #x00404811 17)
           (list #x0020870d 13)
           (list #x0010160b 11)
           (list #x00082507 7)
           (list #x00044405 5)
           (list #x00028303 3)
           (list #x00011202 2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (card-module:card-to-prime-value card))
                    (description
                     (card-module:card-to-string card)))
                (begin
                  (card-assert-simple-function
                   sub-name test-label-index description
                   shouldbe result
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-card-to-suit-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-card-to-suit-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list #x10008e29 #x08)
           (list #x10004e29 #x04)
           (list #x10002e29 #x02)
           (list #x10001e29 #x01)
           (list #x08002d25 #x02)
           (list #x04004c1f #x04)
           (list #x02008b1d #x08)
           (list #x01001a17 #x01)
           (list #x00802913 #x02)
           (list #x00404811 #x04)
           (list #x0020870d #x08)
           (list #x0010160b #x01)
           (list #x00082507 #x02)
           (list #x00044405 #x04)
           (list #x00028303 #x08)
           (list #x00011202 #x01)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (card-module:card-to-suit card))
                    (description
                     (card-module:card-to-string card)))
                (begin
                  (card-assert-simple-function
                   sub-name test-label-index description
                   shouldbe result
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-card-to-suit-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-card-to-suit-string-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list #x10008e29 "c")
           (list #x10004e29 "d")
           (list #x10002e29 "h")
           (list #x10001e29 "s")
           (list #x08002d25 "h")
           (list #x04004c1f "d")
           (list #x02008b1d "c")
           (list #x01001a17 "s")
           (list #x00802913 "h")
           (list #x00404811 "d")
           (list #x0020870d "c")
           (list #x0010160b "s")
           (list #x00082507 "h")
           (list #x00044405 "d")
           (list #x00028303 "c")
           (list #x00011202 "s")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card (list-ref alist 0))
                  (shouldbe-string (list-ref alist 1)))
              (let ((result-string
                     (card-module:card-to-suit-string card))
                    (description
                     (card-module:card-to-string card)))
                (begin
                  (card-assert-string-function
                   sub-name test-label-index description
                   shouldbe-string result-string
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

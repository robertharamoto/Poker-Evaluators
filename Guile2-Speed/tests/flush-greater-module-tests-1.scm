;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for flush-greater-module.scm              ###
;;;###                                                       ###
;;;###  last updated June 27, 2024                           ###
;;;###                                                       ###
;;;###  created June 26, 2024                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### ice-9 format to automatically commafy using format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((card-module)
              :renamer (symbol-prefix-proc 'card-module:)))

(use-modules ((flush-module)
              :renamer (symbol-prefix-proc 'flush-module:)))

(use-modules ((flush-greater-module)
              :renamer (symbol-prefix-proc 'flush-greater-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;###  card representation
;;;###    3          2          1          0
;;;###   10987654 32109876 54321098 76543210
;;;###  +--------+--------+--------+--------+
;;;###  |xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
;;;###  +--------+--------+--------+--------+
;;;###
;;;###  p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
;;;###  r = rank of card (deuce=2,trey=3,four=4,five=4,...,ace=14)
;;;###  cdhs = suit of card (bit turned on based on suit of card)
;;;###  b = bit turned on depending on rank of card
;;;###
;;;###  Using such a scheme, here are some bit pattern examples:
;;;###
;;;###  xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
;;;###  00001000 00000000 01001011 00100101    King of Diamonds
;;;###  00000000 00001000 00010011 00000111    Five of Spades
;;;###  00000010 00000000 10001001 00011101    Jack of Clubs

;;;###
;;;###  hand values
;;;###  royal flush 100
;;;###  straight flush 90
;;;###  four-of-a-kind 80
;;;###  full-house 70
;;;###  flush 60
;;;###  straight 50
;;;###  three-of-a-kind 40
;;;###  two-pairs 30
;;;###  one-pairs 20
;;;###  high-cards 10
;;;###

;;;###
;;;###  data for hash of hash tables
;;;###  hash(prime-signature) -> hash(string) -> value
;;;###
;;;###  "value" - hand values (from 10 to 100)
;;;###  "rank" - hand rank (from 1 to 7,462)
;;;###  "mask" - rank of 5 cards in bit format
;;;###  "count" - number of hands with this prime signature
;;;###  "signature" - prime signature
;;;###  "isflush" - bool flush indicator
;;;###  "quads-value" - card value of 4-of-a-kind
;;;###  "triples-value" - card value of 3-of-a-kind
;;;###  "top-pair" - card value of top pair
;;;###  "second-pair" - card value of second pair
;;;###  "kicker1" - card value of top kicker
;;;###  "kicker2" - card value of second kicker
;;;###  "kicker3" - card value of third kicker
;;;###
;;;###

;;;#############################################################
;;;#############################################################
(define (flush-greater-assert-bool-function
         sub-name test-label-index description
         shouldbe-bool result-bool
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) : ~a : error : "
            sub-name test-label-index description))
          (err-2
           (ice-9-format:format
            #f "shouldbe = ~a, result = ~a"
            (if shouldbe-bool "true" "false")
            (if result-bool "true" "false"))))
      (begin
        (unittest2:assert?
         (equal? shouldbe-bool result-bool)
         sub-name
         (string-append err-1 err-2)
         result-hash-table)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-flush1-greater-flush2-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-flush1-greater-flush2-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "qs")
            (card-module:string-to-card "js")
            (card-module:string-to-card "ts")

            (card-module:string-to-card "as")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "js")
            (card-module:string-to-card "ts")
            #t
            "royal flush vs ace-high flush")

           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "qs")
            (card-module:string-to-card "js")
            (card-module:string-to-card "ts")

            (card-module:string-to-card "qs")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "9s")
            (card-module:string-to-card "js")
            (card-module:string-to-card "ts")
            #t
            "royal flush vs king-high straight flush")

           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "qs")
            (card-module:string-to-card "js")
            (card-module:string-to-card "ts")

            (card-module:string-to-card "as")
            (card-module:string-to-card "ks")
            (card-module:string-to-card "qs")
            (card-module:string-to-card "js")
            (card-module:string-to-card "ts")
            #f
            "royal flush vs royal flush")

           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "5s")
            (card-module:string-to-card "4s")
            (card-module:string-to-card "2s")
            (card-module:string-to-card "3s")

            (card-module:string-to-card "6d")
            (card-module:string-to-card "4d")
            (card-module:string-to-card "5d")
            (card-module:string-to-card "3d")
            (card-module:string-to-card "2d")
            #f
            "5-high straight flush vs 6-high straight flush")

           (list
            (card-module:string-to-card "6s")
            (card-module:string-to-card "5s")
            (card-module:string-to-card "4s")
            (card-module:string-to-card "2s")
            (card-module:string-to-card "3s")

            (card-module:string-to-card "ad")
            (card-module:string-to-card "4d")
            (card-module:string-to-card "5d")
            (card-module:string-to-card "3d")
            (card-module:string-to-card "2d")
            #t
            "6-high straight flush vs 5-high straight flush")

           (list
            (card-module:string-to-card "as")
            (card-module:string-to-card "5s")
            (card-module:string-to-card "4s")
            (card-module:string-to-card "2s")
            (card-module:string-to-card "3s")

            (card-module:string-to-card "ad")
            (card-module:string-to-card "kd")
            (card-module:string-to-card "5d")
            (card-module:string-to-card "3d")
            (card-module:string-to-card "2d")
            #t
            "5-high straight flush vs ace-high flush")

           (list
            (card-module:string-to-card "kh")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "th")
            (card-module:string-to-card "jh")
            (card-module:string-to-card "qh")

            (card-module:string-to-card "8d")
            (card-module:string-to-card "9d")
            (card-module:string-to-card "td")
            (card-module:string-to-card "jd")
            (card-module:string-to-card "2d")
            #t
            "king-high straight flush vs flush")

           (list
            (card-module:string-to-card "8d")
            (card-module:string-to-card "9d")
            (card-module:string-to-card "td")
            (card-module:string-to-card "jd")
            (card-module:string-to-card "2d")

            (card-module:string-to-card "kh")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "th")
            (card-module:string-to-card "jh")
            (card-module:string-to-card "qh")
            #f
            "flush vs king-high straight flush")

           (list
            (card-module:string-to-card "8d")
            (card-module:string-to-card "9d")
            (card-module:string-to-card "ad")
            (card-module:string-to-card "jd")
            (card-module:string-to-card "2d")

            (card-module:string-to-card "kh")
            (card-module:string-to-card "9h")
            (card-module:string-to-card "th")
            (card-module:string-to-card "7h")
            (card-module:string-to-card "qh")
            #t
            "flush vs flush")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((card-1-h1 (list-ref alist 0))
                  (card-2-h1 (list-ref alist 1))
                  (card-3-h1 (list-ref alist 2))
                  (card-4-h1 (list-ref alist 3))
                  (card-5-h1 (list-ref alist 4))
                  (card-1-h2 (list-ref alist 5))
                  (card-2-h2 (list-ref alist 6))
                  (card-3-h2 (list-ref alist 7))
                  (card-4-h2 (list-ref alist 8))
                  (card-5-h2 (list-ref alist 9))
                  (shouldbe-bool (list-ref alist 10))
                  (description (list-ref alist 11))
                  (sub-h1-htable (make-hash-table 20))
                  (sub-h2-htable (make-hash-table 20)))
              (let ((prime-h1-signature
                     (* (card-module:card-to-prime-value card-1-h1)
                        (card-module:card-to-prime-value card-2-h1)
                        (card-module:card-to-prime-value card-3-h1)
                        (card-module:card-to-prime-value card-4-h1)
                        (card-module:card-to-prime-value card-5-h1)))
                    (prime-h2-signature
                     (* (card-module:card-to-prime-value card-1-h2)
                        (card-module:card-to-prime-value card-2-h2)
                        (card-module:card-to-prime-value card-3-h2)
                        (card-module:card-to-prime-value card-4-h2)
                        (card-module:card-to-prime-value card-5-h2))))
                (begin
                  (flush-module:is-flush-hand
                   card-1-h1 card-2-h1 card-3-h1 card-4-h1 card-5-h1
                   prime-h1-signature sub-h1-htable)

                  (flush-module:is-flush-hand
                   card-1-h2 card-2-h2 card-3-h2 card-4-h2 card-5-h2
                   prime-h2-signature sub-h2-htable)

                  (let ((result-bool
                         (flush-greater-module:flush1-greater-flush2
                          sub-h1-htable sub-h2-htable)))
                    (begin
                      (flush-greater-assert-bool-function
                       sub-name test-label-index description
                       shouldbe-bool result-bool
                       result-hash-table)
                      ))
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

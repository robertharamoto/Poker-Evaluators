;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  flush module functions                               ###
;;;###                                                       ###
;;;###  last updated June 27, 2024                           ###
;;;###                                                       ###
;;;###  created June 23, 2024                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start flush module
(define-module (flush-module)
  #:export
  (is-straight-mask
   is-flush-bool

   is-flush-hand
   ))

;;;#############################################################
;;;#############################################################

(use-modules ((card-module)
              :renamer (symbol-prefix-proc 'card-module:)))

;;;#############################################################
;;;#############################################################

;;;###  card representation
;;;###    3          2          1          0
;;;###   10987654 32109876 54321098 76543210
;;;###  +--------+--------+--------+--------+
;;;###  |xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
;;;###  +--------+--------+--------+--------+
;;;###
;;;###  p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
;;;###  r = rank of card (deuce=2,trey=3,four=4,five=4,...,ace=14)
;;;###  cdhs = suit of card (bit turned on based on suit of card)
;;;###  b = bit turned on depending on rank of card
;;;###
;;;###  Using such a scheme, here are some bit pattern examples:
;;;###
;;;###  xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
;;;###  00001000 00000000 01001011 00100101    King of Diamonds
;;;###  00000000 00001000 00010011 00000111    Five of Spades
;;;###  00000010 00000000 10001001 00011101    Jack of Clubs

;;;###
;;;###  hand values
;;;###  royal flush 100
;;;###  straight flush 90
;;;###  four-of-a-kind 80
;;;###  full-house 70
;;;###  flush 60
;;;###  straight 50
;;;###  three-of-a-kind 40
;;;###  two-pairs 30
;;;###  one-pair 20
;;;###  high-card 10
;;;###

;;;###
;;;###  data for hash of hash tables
;;;###  hash(prime-signature) -> hash(string) -> value
;;;###
;;;###  "value" - hand values (from 10 to 100)
;;;###  "rank" - hand rank (from 1 to 7,462)
;;;###  "mask" - rank of 5 cards in bit format
;;;###  "count" - number of hands with this prime signature
;;;###  "signature" - prime signature
;;;###  "isflush" - bool flush indicator
;;;###  "quad-value" - card value of 4-of-a-kind
;;;###  "trip-value" - card value of 3-of-a-kind
;;;###  "top-pair" - card value of top pair
;;;###  "second-pair" - card value of second pair
;;;###  "kicker1" - card value of top kicker
;;;###  "kicker2" - card value of second kicker
;;;###  "kicker3" - card value of third kicker
;;;###
;;;###

;;;#############################################################
;;;#############################################################
(define (is-straight-mask
         card-1 card-2 card-3 card-4 card-5)
  (begin
    (let ((a-high-mask #x1F00)
          (k-high-mask #x0F80)
          (q-high-mask #x07C0)
          (j-high-mask #x03E0)
          (t-high-mask #x01F0)
          (nine-high-mask #x00F8)
          (eight-high-mask #x007C)
          (seven-high-mask #x003E)
          (six-high-mask #x001F)
          (five-high-mask #x100F))
      (let ((this-mask
             (ash
              (logior
               card-1 card-2 card-3 card-4 card-5)
              -16)))
        (begin
          (cond
           ((equal? this-mask a-high-mask)
            (begin
              a-high-mask
              ))
           ((equal? this-mask k-high-mask)
            (begin
              k-high-mask
              ))
           ((equal? this-mask q-high-mask)
            (begin
              q-high-mask
              ))
           ((equal? this-mask j-high-mask)
            (begin
              j-high-mask
              ))
           ((equal? this-mask t-high-mask)
            (begin
              t-high-mask
              ))
           ((equal? this-mask nine-high-mask)
            (begin
              nine-high-mask
              ))
           ((equal? this-mask eight-high-mask)
            (begin
              eight-high-mask
              ))
           ((equal? this-mask seven-high-mask)
            (begin
              seven-high-mask
              ))
           ((equal? this-mask six-high-mask)
            (begin
              six-high-mask
              ))
           ((equal? this-mask five-high-mask)
            (begin
              five-high-mask
              ))
           (else
            (begin
              -1
              )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (is-flush-bool
         card-1 card-2 card-3 card-4 card-5)
  (begin
    (let ((atmp (logior card-1 card-2 card-3 card-4 card-5)))
      (let ((btmp (logand #x0F (ash atmp -12))))
        (let ((ctmp (logcount btmp)))
          (begin
            (= ctmp 1)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-flush-hand
         card-1 card-2 card-3 card-4 card-5
         prime-signature
         sub-htable)
  (begin
    (let ((result-flag #f)
          (str-mask
           (is-straight-mask
            card-1 card-2 card-3 card-4 card-5))
          (card-mask
           (logand
            #x1FFF
            (ash
             (logior card-1 card-2 card-3
                     card-4 card-5)
             -16)))
          (suit-1 (card-module:card-to-suit card-1))
          (suit-2 (card-module:card-to-suit card-2))
          (suit-3 (card-module:card-to-suit card-3))
          (suit-4 (card-module:card-to-suit card-4))
          (suit-5 (card-module:card-to-suit card-5)))
      (begin
        (if (not (equal?
                  (logand suit-1 suit-2
                          suit-3 suit-4 suit-5) 0))
            (begin
              (let ((this-count
                     (hash-ref sub-htable "count" 0)))
                (begin
                  (cond
                   ((equal? str-mask #x1F00)
                    (begin
                      (hash-set!
                       sub-htable "value" 100)
                      ))
                   ((> str-mask 0)
                    (begin
                      (hash-set!
                       sub-htable "value" 90)
                      ))
                   (else
                    (begin
                      ;;; this is not a straight flush but a flush
                      (hash-set!
                       sub-htable "value" 60)
                      )))

                  (hash-set!
                   sub-htable "count" (1+ this-count))

                  (hash-set!
                   sub-htable "signature" prime-signature)

                  (hash-set!
                   sub-htable "isflush" #t)

                  (hash-set!
                   sub-htable "mask" card-mask)

                  (set! result-flag #t)
                  result-flag
                  )))
              (begin
                (set! result-flag #f)
                result-flag
                ))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  distinct poker module functions                      ###
;;;###  for equivalent hands                                 ###
;;;###                                                       ###
;;;###  last updated June 28, 2024                           ###
;;;###                                                       ###
;;;###  created June 27, 2024                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start distinct poker hand module
;;;### return equivalent hand rankings for
;;;### flushes and non-flushes
(define-module (distinct-module)
  #:export
  (populate-distinct-maps

   create-card-deck
   load-up-5-mask-maps

   ;;; order hands using sorting approach
   calculate-hand-rankings

   ;;; summarize 7462 distinct 5-card hands
   summarize-maps

   ))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format used for advanced formatting
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

(use-modules ((card-module)
              :renamer (symbol-prefix-proc 'card-module:)))

(use-modules ((flush-module)
              :renamer (symbol-prefix-proc 'flush-module:)))

(use-modules ((nonflush-module)
              :renamer (symbol-prefix-proc 'nonflush-module:)))

(use-modules ((flush-greater-module)
              :renamer (symbol-prefix-proc 'flush-greater-module:)))

(use-modules ((nonflush-greater-module)
              :renamer (symbol-prefix-proc 'nonflush-greater-module:)))


;;;#############################################################
;;;#############################################################

;;;###  card representation
;;;###    3          2          1          0
;;;###   10987654 32109876 54321098 76543210
;;;###  +--------+--------+--------+--------+
;;;###  |xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
;;;###  +--------+--------+--------+--------+
;;;###
;;;###  p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
;;;###  r = rank of card (deuce=2,trey=3,four=4,five=4,...,ace=14)
;;;###  cdhs = suit of card (bit turned on based on suit of card)
;;;###  b = bit turned on depending on rank of card
;;;###
;;;###  Using such a scheme, here are some bit pattern examples:
;;;###
;;;###  xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
;;;###  00001000 00000000 01001011 00100101    King of Diamonds
;;;###  00000000 00001000 00010011 00000111    Five of Spades
;;;###  00000010 00000000 10001001 00011101    Jack of Clubs

;;;###
;;;###  hand values
;;;###  royal flush 100
;;;###  straight flush 90
;;;###  four-of-a-kind 80
;;;###  full-house 70
;;;###  flush 60
;;;###  straight 50
;;;###  three-of-a-kind 40
;;;###  two-pairs 30
;;;###  one-pair 20
;;;###  high-card 10
;;;###

;;;###
;;;###  data for hash of hash tables
;;;###  hash(prime-signature) -> hash(string) -> value
;;;###
;;;###  "value" - hand values (from 10 to 100)
;;;###  "rank" - hand rank (from 1 to 7,462)
;;;###  "mask" - rank of 5 cards in bit format
;;;###  "count" - number of hands with this prime signature
;;;###  "signature" - prime signature
;;;###  "isflush" - bool flush indicator
;;;###  "quad-value" - card value of 4-of-a-kind
;;;###  "trip-value" - card value of 3-of-a-kind
;;;###  "top-pair" - card value of top pair
;;;###  "second-pair" - card value of second pair
;;;###  "kicker1" - card value of top kicker
;;;###  "kicker2" - card value of second kicker
;;;###  "kicker3" - card value of third kicker
;;;###
;;;###


;;;#############################################################
;;;#############################################################
(define-syntax inner-loop-5-macro
  (syntax-rules ()
    ((inner-loop-5-macro
      card-deck
      ii-1
      card-1 suit-1 prime-1
      flush-primes-htable
      nonflush-primes-htable)
     (begin
       (do ((ii-2 (1+ ii-1) (1+ ii-2)))
           ((>= ii-2 49))
         (begin
           (let ((card-2
                  (list-ref card-deck ii-2)))
             (let ((suit-2
                    (card-module:card-to-suit card-2))
                   (prime-2
                    (card-module:card-to-prime-value
                     card-2)))
               (begin
                 (do ((ii-3 (1+ ii-2) (1+ ii-3)))
                     ((>= ii-3 50))
                   (begin
                     (let ((card-3
                            (list-ref card-deck ii-3)))
                       (let ((suit-3
                              (card-module:card-to-suit card-3))
                             (prime-3
                              (card-module:card-to-prime-value
                               card-3)))
                         (begin
                           (inner-inner-loop-5-macro
                            card-deck
                            ii-3
                            card-1 suit-1 prime-1
                            card-2 suit-2 prime-2
                            card-3 suit-3 prime-3
                            flush-primes-htable
                            nonflush-primes-htable)
                           )))
                     ))
                 )))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-in-sub-htable
         prime-signature sub-htable primes-htable)
  (begin
    (let ((prev-htable
           (hash-ref primes-htable prime-signature #f)))
      (begin
        (if (hash-table? prev-htable)
            (begin
              (let ((this-count
                     (hash-ref sub-htable "count" 0))
                    (prev-count
                     (hash-ref prev-htable "count" 0)))
                (begin
                  (hash-set!
                   prev-htable
                   "count" (+ this-count prev-count))

                  (hash-set!
                   primes-htable prime-signature prev-htable)
                  )))
            (begin
              (hash-set!
               primes-htable prime-signature sub-htable)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax inner-inner-loop-5-macro
  (syntax-rules ()
    ((inner-inner-loop-5-macro
      card-deck
      ii-3
      card-1 suit-1 prime-1
      card-2 suit-2 prime-2
      card-3 suit-3 prime-3
      flush-primes-htable
      nonflush-primes-htable)
     (begin
       (do ((ii-4 (1+ ii-3) (1+ ii-4)))
           ((>= ii-4 51))
         (begin
           (let ((card-4
                  (list-ref card-deck ii-4)))
             (let ((suit-4
                    (card-module:card-to-suit card-4))
                   (prime-4
                    (card-module:card-to-prime-value
                     card-4)))
               (begin
                 (do ((ii-5 (1+ ii-4) (1+ ii-5)))
                     ((>= ii-5 52))
                   (begin
                     (let ((card-5
                            (list-ref card-deck ii-5)))
                       (let ((suit-5
                              (card-module:card-to-suit card-5))
                             (prime-5
                              (card-module:card-to-prime-value
                               card-5)))
                         (let ((prime-signature
                                (* prime-1 prime-2 prime-3 prime-4 prime-5))
                               (sub-htable (make-hash-table 20)))
                           (begin
                             (if (not
                                  (equal? (logand
                                           suit-1 suit-2 suit-3
                                           suit-4 suit-5)
                                          0))
                                 (begin
                                   (flush-module:is-flush-hand
                                    card-1 card-2 card-3
                                    card-4 card-5
                                    prime-signature
                                    sub-htable)

                                   (add-in-sub-htable
                                    prime-signature sub-htable
                                    flush-primes-htable))
                                 (begin
                                   (nonflush-module:is-nonflush-hand
                                    card-1 card-2 card-3
                                    card-4 card-5
                                    prime-signature
                                    sub-htable)

                                   (add-in-sub-htable
                                    prime-signature sub-htable
                                    nonflush-primes-htable)
                                   ))
                             ))
                         ))
                     ))
                 )))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (load-up-5-mask-maps
         flush-primes-htable
         nonflush-primes-htable)
  (begin
    (let ((card-deck (create-card-deck)))
      (begin
        (do ((ii-1 0 (1+ ii-1)))
            ((>= ii-1 48))
          (begin
            (let ((card-1 (list-ref card-deck ii-1)))
              (let ((suit-1
                     (card-module:card-to-suit card-1))
                    (prime-1
                     (card-module:card-to-prime-value card-1)))
                (begin
                  (inner-loop-5-macro
                   card-deck
                   ii-1
                   card-1 suit-1 prime-1
                   flush-primes-htable
                   nonflush-primes-htable)
                  )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sort-function sub-h1-htable sub-h2-htable)
  (begin
    (let ((result-flag #t)
          (hand-value-h1 (hash-ref sub-h1-htable "value"))
          (hand-value-h2 (hash-ref sub-h2-htable "value")))
      (begin
        (cond
         ((> hand-value-h1 hand-value-h2)
          (begin
            (set! result-flag #t)
            #t
            ))
         ((< hand-value-h1 hand-value-h2)
          (begin
            (set! result-flag #f)
            #f
            ))
         (else
          (begin
            ;;; both hand values are equal
            (cond
             ((equal? hand-value-h1 100)
              (begin
                (set! result-flag #t)
                result-flag
                ))
             ((or (equal? hand-value-h1 90)
                  (equal? hand-value-h1 60))
              (begin
                (let ((rflag
                       (flush-greater-module:flush1-greater-flush2
                        sub-h1-htable sub-h2-htable)))
                  (begin
                    (set! result-flag rflag)
                    result-flag
                    ))
                ))
             ((equal? hand-value-h1 80)
              (begin
                (let ((rflag
                       (nonflush-greater-module:quads1-greater-quads2
                        sub-h1-htable sub-h2-htable)))
                  (begin
                    (set! result-flag rflag)
                    result-flag
                    ))
                ))
             ((equal? hand-value-h1 70)
              (begin
                (let ((rflag
                       (nonflush-greater-module:fulls1-greater-fulls2
                        sub-h1-htable sub-h2-htable)))
                  (begin
                    (set! result-flag rflag)
                    result-flag
                    ))
                ))
             ((equal? hand-value-h1 50)
              (begin
                (let ((rflag
                       (nonflush-greater-module:straight1-greater-straight2
                        sub-h1-htable sub-h2-htable)))
                  (begin
                    (set! result-flag rflag)
                    result-flag
                    ))
                ))
             ((equal? hand-value-h1 40)
              (begin
                (let ((rflag
                       (nonflush-greater-module:triples1-greater-triples2
                        sub-h1-htable sub-h2-htable)))
                  (begin
                    (set! result-flag rflag)
                    result-flag
                    ))
                ))
             ((equal? hand-value-h1 30)
              (begin
                (let ((rflag
                       (nonflush-greater-module:twopairs1-greater-twopairs2
                        sub-h1-htable sub-h2-htable)))
                  (begin
                    (set! result-flag rflag)
                    result-flag
                    ))
                ))
             ((equal? hand-value-h1 20)
              (begin
                (let ((rflag
                       (nonflush-greater-module:onepairs1-greater-onepairs2
                        sub-h1-htable sub-h2-htable)))
                  (begin
                    (set! result-flag rflag)
                    result-flag
                    ))
                ))
             (else
              (begin
                (let ((rflag
                       (nonflush-greater-module:highcards1-greater-highcards2
                        sub-h1-htable sub-h2-htable)))
                  (begin
                    (set! result-flag rflag)
                    result-flag
                    ))
                )))
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (calculate-hand-rankings
         flush-primes-htable nonflush-primes-htable)
  (begin
    (let ((flist
           (hash-map->list
            (lambda (signature sub-htable)
              (begin
                sub-htable
                )) flush-primes-htable))
          (nflist
           (hash-map->list
            (lambda (signature sub-htable)
              (begin
                sub-htable
                )) nonflush-primes-htable)))
      (let ((all-hands-list (append flist nflist)))
        (let ((sorted-list
               (sort all-hands-list
                     sort-function))
              (slen (length all-hands-list)))
          (begin
            (hash-clear! flush-primes-htable)
            (hash-clear! nonflush-primes-htable)

            (do ((ii 0 (1+ ii)))
                ((>= ii slen))
              (begin
                (let ((irank (- slen ii)))
                  (begin
                    (let ((this-htable (list-ref sorted-list ii)))
                      (let ((flush-flag (hash-ref this-htable "isflush"))
                            (signature (hash-ref this-htable "signature")))
                        (begin
                          (hash-set! this-htable "rank" irank)
                          (if (equal? flush-flag #t)
                              (begin
                                (hash-set!
                                 flush-primes-htable
                                 signature this-htable))
                              (begin
                                (hash-set!
                                 nonflush-primes-htable
                                 signature this-htable)
                                ))
                          )))
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-statistics
         this-string this-count this-distinct)
  (begin
    (display
     (ice-9-format:format #f "  ~a  :  ~:d  :  ~:d~%"
                          this-string this-count this-distinct))
    (force-output)
    ))

;;;#############################################################
;;;#############################################################
(define (summarize-maps
         start-time-secs-1 end-time-secs-1
         flush-primes-htable nonflush-primes-htable)
  (begin
    (let ((hand-values-list
           (list 100 90 80 70 60 50 40 30 20 10))
          (hand-string-list
           (list "royal flush" "straight-flush"
                 "four-of-a-kind" "full-house"
                 "flush" "straight" "three-of-a-kind"
                 "two-pairs" "one-pair" "high-card"))
          (nseconds
           (inexact->exact
            (truncate
             (- end-time-secs-1 start-time-secs-1)))))
      (let ((nh-len (length hand-values-list))
            (total-string "totals")
            (total-count 0)
            (total-distinct 0))
        (begin
          (do ((ii 0 (1+ ii)))
              ((>= ii nh-len))
            (begin
              (let ((main-value (list-ref hand-values-list ii))
                    (main-string (list-ref hand-string-list ii))
                    (main-count 0)
                    (distinct-count 0))
                (begin
                  (if (or (equal? main-value 100)
                          (equal? main-value 90)
                          (equal? main-value 60))
                      (begin
                        (hash-for-each
                         (lambda (signature sub-htable)
                           (begin
                             (let ((sub-value
                                    (hash-ref sub-htable "value"))
                                   (sub-count
                                    (hash-ref sub-htable "count")))
                               (begin
                                 (if (equal? sub-value main-value)
                                     (begin
                                       (set! main-count
                                             (+ main-count sub-count))
                                       (set! total-count
                                             (+ total-count sub-count))
                                       (set! distinct-count
                                             (1+ distinct-count))
                                       (set! total-distinct
                                             (1+ total-distinct))
                                       ))
                                 ))
                             )) flush-primes-htable)
                        (display-statistics
                         main-string main-count distinct-count))
                      (begin
                        (hash-for-each
                         (lambda (signature sub-htable)
                           (begin
                             (let ((sub-value
                                    (hash-ref sub-htable "value"))
                                   (sub-count
                                    (hash-ref sub-htable "count")))
                               (begin
                                 (if (equal? sub-value main-value)
                                     (begin
                                       (set! main-count
                                             (+ main-count sub-count))
                                       (set! total-count
                                             (+ total-count sub-count))
                                       (set! distinct-count
                                             (1+ distinct-count))
                                       (set! total-distinct
                                             (1+ total-distinct))
                                       ))
                                 ))
                             )) nonflush-primes-htable)
                        (display-statistics
                         main-string main-count distinct-count)
                        ))
                  ))
              ))

          (display-statistics
           total-string total-count total-distinct)

          (let ((ips
                 (inexact->exact
                  (truncate
                   (+ (/ total-count nseconds) 0.50)))))
            (begin
              (display
               (ice-9-format:format
                #f "number of hands = ~:d, number of seconds = ~:d~%"
                total-count nseconds))
              (display
               (ice-9-format:format
                #f "5-card hand calculations per second = ~:d~%"
                ips))
              ))
          )))
    ))


;;;#############################################################
;;;#############################################################
(define (populate-distinct-maps
         flush-primes-htable nonflush-primes-htable)
  (begin
    (let ((start-time-secs-1 (current-time)))
      (begin
        (load-up-5-mask-maps
         flush-primes-htable nonflush-primes-htable)

        (calculate-hand-rankings
         flush-primes-htable nonflush-primes-htable)

        (let ((end-time-secs-1 (current-time)))
          (begin
            (summarize-maps
             start-time-secs-1 end-time-secs-1
             flush-primes-htable nonflush-primes-htable)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (create-card-deck)
  (begin
    (let ((suit-list (list "s" "c" "d" "h"))
          (value-list
           (list "2" "3" "4" "5" "6" "7" "8" "9"
                 "t" "j" "q" "k" "a"))
          (card-deck (list)))
      (begin
        (for-each
         (lambda (asuit)
           (begin
             (for-each
              (lambda (avalue)
                (begin
                  (let ((new-string
                         (string-append avalue asuit)))
                    (let ((this-card
                           (card-module:string-to-card
                            new-string)))
                      (begin
                        (set! card-deck
                              (cons this-card card-deck))
                        )))
                  )) value-list)
             )) suit-list)
        card-deck
        ))
    ))


;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

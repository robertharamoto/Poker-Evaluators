;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  nonflush-greater-module functions                    ###
;;;###                                                       ###
;;;###  last updated June 27, 2024                           ###
;;;###                                                       ###
;;;###  created June 26, 2024                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start nonflush-greater-module
(define-module (nonflush-greater-module)
  #:export (quads1-greater-quads2
            fulls1-greater-fulls2
            straight1-greater-straight2
            triples1-greater-triples2
            twopairs1-greater-twopairs2
            onepairs1-greater-onepairs2
            highcards1-greater-highcards2
            ))

;;;#############################################################
;;;#############################################################

(use-modules ((card-module)
              :renamer (symbol-prefix-proc 'card-module:)))

;;;#############################################################
;;;#############################################################

;;;###  card representation
;;;###    3          2          1          0
;;;###   10987654 32109876 54321098 76543210
;;;###  +--------+--------+--------+--------+
;;;###  |xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
;;;###  +--------+--------+--------+--------+
;;;###
;;;###  p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
;;;###  r = rank of card (deuce=2,trey=3,four=4,five=4,...,ace=14)
;;;###  cdhs = suit of card (bit turned on based on suit of card)
;;;###  b = bit turned on depending on rank of card
;;;###
;;;###  Using such a scheme, here are some bit pattern examples:
;;;###
;;;###  xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
;;;###  00001000 00000000 01001011 00100101    King of Diamonds
;;;###  00000000 00001000 00010011 00000111    Five of Spades
;;;###  00000010 00000000 10001001 00011101    Jack of Clubs

;;;###
;;;###  hand values
;;;###  royal flush 100
;;;###  straight flush 90
;;;###  four-of-a-kind 80
;;;###  full-house 70
;;;###  flush 60
;;;###  straight 50
;;;###  three-of-a-kind 40
;;;###  two-pairs 30
;;;###  one-pairs 20
;;;###  high-cards 10
;;;###

;;;###
;;;###  data for hash of hash tables
;;;###  hash(prime-signature) -> hash(string) -> value
;;;###
;;;###  "value" - hand values (from 10 to 100)
;;;###  "rank" - hand rank (from 1 to 7,462)
;;;###  "mask" - rank of 5 cards in bit format
;;;###  "count" - number of hands with this prime signature
;;;###  "signature" - prime signature
;;;###  "isflush" - bool flush indicator
;;;###  "quads-value" - card value of 4-of-a-kind
;;;###  "triples-value" - card value of 3-of-a-kind
;;;###  "top-pair" - card value of top pair
;;;###  "second-pair" - card value of second pair
;;;###  "kicker1" - card value of top kicker
;;;###  "kicker2" - card value of second kicker
;;;###  "kicker3" - card value of third kicker
;;;###
;;;###

;;;#############################################################
;;;#############################################################
(define (quads1-greater-quads2 sub1-htable sub2-htable)
  (begin
    (if (and (hash-table? sub1-htable)
             (hash-table? sub2-htable))
        (begin
          (let ((value-h1 (hash-ref sub1-htable "value" 0))
                (value-h2 (hash-ref sub2-htable "value" 0))
                (quads-h1 (hash-ref sub1-htable "quads-value" 0))
                (quads-h2 (hash-ref sub2-htable "quads-value" 0))
                (kicker-h1 (hash-ref sub1-htable "kicker1" 0))
                (kicker-h2 (hash-ref sub2-htable "kicker1" 0)))
            (begin
              (if (not (equal? value-h1 value-h2))
                  (begin
                    (> value-h1 value-h2))
                  (begin
                    (cond
                     ((> quads-h1 quads-h2)
                      (begin
                        #t
                        ))
                     ((< quads-h1 quads-h2)
                      (begin
                        #f
                        ))
                     (else
                      (begin
                        (> kicker-h1 kicker-h2)
                        )))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (fulls1-greater-fulls2 sub1-htable sub2-htable)
  (begin
    (if (and (hash-table? sub1-htable)
             (hash-table? sub2-htable))
        (begin
          (let ((value-h1 (hash-ref sub1-htable "value" 0))
                (value-h2 (hash-ref sub2-htable "value" 0))
                (triples-h1
                 (hash-ref sub1-htable "triples-value" 0))
                (triples-h2
                 (hash-ref sub2-htable "triples-value" 0))
                (toppair-h1 (hash-ref sub1-htable "top-pair" 0))
                (toppair-h2 (hash-ref sub2-htable "top-pair" 0)))
            (begin
              (if (not (equal? value-h1 value-h2))
                  (begin
                    (> value-h1 value-h2))
                  (begin
                    (cond
                     ((> triples-h1 triples-h2)
                      (begin
                        #t
                        ))
                     ((< triples-h1 triples-h2)
                      (begin
                        #f
                        ))
                     (else
                      (begin
                        (> toppair-h1 toppair-h2)
                        )))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (straight1-greater-straight2 sub1-htable sub2-htable)
  (begin
    (if (and (hash-table? sub1-htable)
             (hash-table? sub2-htable))
        (begin
          (let ((value-h1 (hash-ref sub1-htable "value" 0))
                (value-h2 (hash-ref sub2-htable "value" 0))
                (mask-h1
                 (hash-ref sub1-htable "mask" 0))
                (mask-h2
                 (hash-ref sub2-htable "mask" 0)))
            (begin
              (if (not (equal? value-h1 value-h2))
                  (begin
                    (> value-h1 value-h2))
                  (begin
                    (if (equal? mask-h1 #x100F)
                        (begin
                          #f)
                        (begin
                          (if (equal? mask-h2 #x100F)
                              (begin
                                #t)
                              (begin
                                (> mask-h1 mask-h2)
                                ))
                          ))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (triples1-greater-triples2 sub1-htable sub2-htable)
  (begin
    (if (and (hash-table? sub1-htable)
             (hash-table? sub2-htable))
        (begin
          (let ((value-h1 (hash-ref sub1-htable "value" 0))
                (value-h2 (hash-ref sub2-htable "value" 0))
                (triples-h1
                 (hash-ref sub1-htable "triples-value" 0))
                (triples-h2
                 (hash-ref sub2-htable "triples-value" 0))
                (kicker1-h1 (hash-ref sub1-htable "kicker1" 0))
                (kicker1-h2 (hash-ref sub2-htable "kicker1" 0))
                (kicker2-h1 (hash-ref sub1-htable "kicker2" 0))
                (kicker2-h2 (hash-ref sub2-htable "kicker2" 0)))
            (begin
              (if (not (equal? value-h1 value-h2))
                  (begin
                    (> value-h1 value-h2))
                  (begin
                    (cond
                     ((> triples-h1 triples-h2)
                      (begin
                        #t
                        ))
                     ((< triples-h1 triples-h2)
                      (begin
                        #f
                        ))
                     (else
                      (begin
                        (cond
                         ((> kicker1-h1 kicker1-h2)
                          (begin
                            #t
                            ))
                         ((< kicker1-h1 kicker1-h2)
                          (begin
                            #f
                            ))
                         (else
                          (begin
                            (> kicker2-h1 kicker2-h2)
                            )))
                        )))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (twopairs1-greater-twopairs2 sub1-htable sub2-htable)
  (begin
    (if (and (hash-table? sub1-htable)
             (hash-table? sub2-htable))
        (begin
          (let ((value-h1 (hash-ref sub1-htable "value" 0))
                (value-h2 (hash-ref sub2-htable "value" 0))
                (toppairs-h1 (hash-ref sub1-htable "top-pair" 0))
                (toppairs-h2 (hash-ref sub2-htable "top-pair" 0))
                (secondpair-h1 (hash-ref sub1-htable "second-pair" 0))
                (secondpair-h2 (hash-ref sub2-htable "second-pair" 0))
                (kicker1-h1 (hash-ref sub1-htable "kicker1" 0))
                (kicker1-h2 (hash-ref sub2-htable "kicker1" 0)))
            (begin
              (if (not (equal? value-h1 value-h2))
                  (begin
                    (> value-h1 value-h2))
                  (begin
                    (cond
                     ((> toppairs-h1 toppairs-h2)
                      (begin
                        #t
                        ))
                     ((< toppairs-h1 toppairs-h2)
                      (begin
                        #f
                        ))
                     (else
                      (begin
                        (cond
                         ((> secondpair-h1 secondpair-h2)
                          (begin
                            #t
                            ))
                         ((< secondpair-h1 secondpair-h2)
                          (begin
                            #f
                            ))
                         (else
                          (begin
                            (> kicker1-h1 kicker1-h2)
                            )))
                        )))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (onepairs1-greater-onepairs2 sub1-htable sub2-htable)
  (begin
    (if (and (hash-table? sub1-htable)
             (hash-table? sub2-htable))
        (begin
          (let ((value-h1 (hash-ref sub1-htable "value" 0))
                (value-h2 (hash-ref sub2-htable "value" 0))
                (onepair-h1 (hash-ref sub1-htable "top-pair" 0))
                (onepair-h2 (hash-ref sub2-htable "top-pair" 0))
                (kicker1-h1 (hash-ref sub1-htable "kicker1" 0))
                (kicker1-h2 (hash-ref sub2-htable "kicker1" 0))
                (kicker2-h1 (hash-ref sub1-htable "kicker2" 0))
                (kicker2-h2 (hash-ref sub2-htable "kicker2" 0))
                (kicker3-h1 (hash-ref sub1-htable "kicker3" 0))
                (kicker3-h2 (hash-ref sub2-htable "kicker3" 0)))
            (begin
              (if (not (equal? value-h1 value-h2))
                  (begin
                    (> value-h1 value-h2))
                  (begin
                    (cond
                     ((> onepair-h1 onepair-h2)
                      (begin
                        #t
                        ))
                     ((< onepair-h1 onepair-h2)
                      (begin
                        #f
                        ))
                     (else
                      (begin
                        (cond
                         ((> kicker1-h1 kicker1-h2)
                          (begin
                            #t
                            ))
                         ((< kicker1-h1 kicker1-h2)
                          (begin
                            #f
                            ))
                         (else
                          (begin
                            (cond
                             ((> kicker2-h1 kicker2-h2)
                              (begin
                                #t
                                ))
                             ((< kicker2-h1 kicker2-h2)
                              (begin
                                #f
                                ))
                             (else
                              (begin
                                (> kicker3-h1 kicker3-h2)
                                )))
                            )))
                        )))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (highcards1-greater-highcards2 sub1-htable sub2-htable)
  (begin
    (if (and (hash-table? sub1-htable)
             (hash-table? sub2-htable))
        (begin
          (let ((value-h1 (hash-ref sub1-htable "value" 0))
                (value-h2 (hash-ref sub2-htable "value" 0))
                (mask-h1 (hash-ref sub1-htable "mask" 0))
                (mask-h2 (hash-ref sub2-htable "mask" 0)))
            (begin
              (if (not (equal? value-h1 value-h2))
                  (begin
                    (> value-h1 value-h2))
                  (begin
                    (> mask-h1 mask-h2)
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

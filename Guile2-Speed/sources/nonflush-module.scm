;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  non-flush module functions                           ###
;;;###                                                       ###
;;;###  last updated June 27, 2024                           ###
;;;###                                                       ###
;;;###  created June 23, 2024                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start nonflush module
(define-module (nonflush-module)
  #:export
  (is-straight-mask

   is-straight-hand
   is-quads-hand
   is-fulls-hand
   is-straight-hand
   is-triples-hand
   is-two-pairs-hand
   is-one-pairs-hand
   is-high-cards-hand

   convert-value-to-bitmask
   convert-bitmask-to-value

   is-nonflush-hand
   ))

;;;#############################################################
;;;#############################################################
(use-modules ((card-module)
              :renamer (symbol-prefix-proc 'card-module:)))

;;;#############################################################
;;;#############################################################

;;;###  card representation
;;;###    3          2          1          0
;;;###   10987654 32109876 54321098 76543210
;;;###  +--------+--------+--------+--------+
;;;###  |xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
;;;###  +--------+--------+--------+--------+
;;;###
;;;###  p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
;;;###  r = rank of card (deuce=2,trey=3,four=4,five=4,...,ace=14)
;;;###  cdhs = suit of card (bit turned on based on suit of card)
;;;###  b = bit turned on depending on rank of card
;;;###
;;;###  Using such a scheme, here are some bit pattern examples:
;;;###
;;;###  xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
;;;###  00001000 00000000 01001011 00100101    King of Diamonds
;;;###  00000000 00001000 00010011 00000111    Five of Spades
;;;###  00000010 00000000 10001001 00011101    Jack of Clubs

;;;###
;;;###  hand values
;;;###  royal flush 100
;;;###  straight flush 90
;;;###  four-of-a-kind 80
;;;###  full-house 70
;;;###  flush 60
;;;###  straight 50
;;;###  three-of-a-kind 40
;;;###  two-pairs 30
;;;###  one-pairs 20
;;;###  high-cards 10
;;;###

;;;###
;;;###  data for hash of hash tables
;;;###  hash(prime-signature) -> hash(string) -> value
;;;###
;;;###  "value" - hand values (from 10 to 100)
;;;###  "rank" - hand rank (from 1 to 7,462)
;;;###  "mask" - rank of 5 cards in bit format
;;;###  "count" - number of hands with this prime signature
;;;###  "signature" - prime signature
;;;###  "isflush" - bool flush indicator
;;;###  "quads-value" - card value of 4-of-a-kind
;;;###  "triples-value" - card value of 3-of-a-kind
;;;###  "top-pair" - card value of top pair
;;;###  "second-pair" - card value of second pair
;;;###  "kicker1" - card value of top kicker
;;;###  "kicker2" - card value of second kicker
;;;###  "kicker3" - card value of third kicker
;;;###
;;;###

;;;#############################################################
;;;#############################################################
(define (is-straight-mask
         card-1 card-2 card-3 card-4 card-5)
  (begin
    (let ((a-high-mask #x1F00)
          (k-high-mask #x0F80)
          (q-high-mask #x07C0)
          (j-high-mask #x03E0)
          (t-high-mask #x01F0)
          (nine-high-mask #x00F8)
          (eight-high-mask #x007C)
          (seven-high-mask #x003E)
          (six-high-mask #x001F)
          (five-high-mask #x100F))
      (let ((this-mask
             (ash
              (logior
               card-1 card-2 card-3 card-4 card-5)
              -16)))
        (begin
          (cond
           ((equal? this-mask a-high-mask)
            (begin
              a-high-mask
              ))
           ((equal? this-mask k-high-mask)
            (begin
              k-high-mask
              ))
           ((equal? this-mask q-high-mask)
            (begin
              q-high-mask
              ))
           ((equal? this-mask j-high-mask)
            (begin
              j-high-mask
              ))
           ((equal? this-mask t-high-mask)
            (begin
              t-high-mask
              ))
           ((equal? this-mask nine-high-mask)
            (begin
              nine-high-mask
              ))
           ((equal? this-mask eight-high-mask)
            (begin
              eight-high-mask
              ))
           ((equal? this-mask seven-high-mask)
            (begin
              seven-high-mask
              ))
           ((equal? this-mask six-high-mask)
            (begin
              six-high-mask
              ))
           ((equal? this-mask five-high-mask)
            (begin
              five-high-mask
              ))
           (else
            (begin
              -1
              )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (is-quads-hand
         freq-htable prime-signature sub-htable)
  (begin
    (let ((result-flag #f)
          (quads-value -1)
          (qflag #f)
          (kicker -1)
          (kflag #f))
      (begin
        (hash-for-each
         (lambda (this-value this-count)
           (begin
             (if (>= this-count 4)
                 (begin
                   (set! qflag #t)
                   (set! quads-value this-value)
                   ))
             (if (< this-count 4)
                 (begin
                   (set! kflag #t)
                   (set! kicker this-value)
                   ))
             )) freq-htable)

        (if (and (equal? qflag #t)
                 (equal? qflag kflag))
            (begin
              (let ((this-count
                     (hash-ref sub-htable "count" 0)))
                (begin
                  (hash-set!
                   sub-htable "value" 80)
                  (hash-set!
                   sub-htable "count" (1+ this-count))
                  (hash-set!
                   sub-htable "signature" prime-signature)
                  (hash-set!
                   sub-htable "isflush" #f)
                  (hash-set!
                   sub-htable "quads-value" quads-value)
                  (hash-set!
                   sub-htable "kicker1" kicker)
                  (set! result-flag #t)
                  ))
              ))

        result-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-fulls-hand
         freq-htable prime-signature sub-htable)
  (begin
    (let ((result-flag #f)
          (triples-value -1)
          (tflag #f)
          (pairs-value -1)
          (pflag #f))
      (begin
        (hash-for-each
         (lambda (this-value this-count)
           (begin
             (if (>= this-count 3)
                 (begin
                   (if (equal? tflag #t)
                       (begin
                         (if (< triples-value this-value)
                             (begin
                               (set! pairs-value triples-value)
                               (set! triples-value this-value)
                               (set! pflag #t))
                             (begin
                               (set! pairs-value this-value)
                               (set! pflag #t)
                               )))
                       (begin
                         (set! triples-value this-value)
                         (set! tflag #t)
                         ))
                   ))
             (if (equal? this-count 2)
                 (begin
                   (if (< pairs-value this-value)
                       (begin
                         (set! pairs-value this-value)
                         (set! pflag #t)
                         ))
                   ))
             )) freq-htable)

        (if (and (equal? tflag #t)
                 (equal? tflag pflag))
            (begin
              (let ((this-count
                     (hash-ref sub-htable "count" 0)))
                (begin
                  (hash-set!
                   sub-htable "value" 70)
                  (hash-set!
                   sub-htable "count" (1+ this-count))
                  (hash-set!
                   sub-htable "signature" prime-signature)
                  (hash-set!
                   sub-htable "isflush" #f)
                  (hash-set!
                   sub-htable "triples-value" triples-value)
                  (hash-set!
                   sub-htable "top-pair" pairs-value)
                  (set! result-flag #t)
                  ))
              ))

        result-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-straight-hand
         card-1 card-2 card-3 card-4 card-5
         prime-signature sub-htable)
  (begin
    (let ((str-mask
           (is-straight-mask
            card-1 card-2 card-3 card-4 card-5))
          (result-flag #f))
      (begin
        (if (> str-mask 0)
            (begin
              (let ((this-count
                     (hash-ref sub-htable "count" 0)))
                (begin
                  (hash-set!
                   sub-htable "value" 50)
                  (hash-set!
                   sub-htable "count" (1+ this-count))
                  (hash-set!
                   sub-htable "signature" prime-signature)
                  (hash-set!
                   sub-htable "isflush" #f)
                  (hash-set!
                   sub-htable "mask" str-mask)
                  (hash-set!
                   sub-htable "count" (1+ this-count))
                  (set! result-flag #t)
                  ))
              ))

        result-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-triples-hand
         freq-htable prime-signature sub-htable)
  (begin
    (let ((result-flag #f)
          (triples-value -1)
          (tflag #f)
          (kicker1-value -1)
          (k1flag #f)
          (kicker2-value -1)
          (k2flag #f))
      (begin
        (hash-for-each
         (lambda (this-value this-count)
           (begin
             (if (>= this-count 3)
                 (begin
                   (if (> this-value triples-value)
                       (begin
                         (set! triples-value this-value)
                         (set! tflag #t)
                         ))
                   ))
             (if (< this-count 3)
                 (begin
                   (if (< kicker1-value this-value)
                       (begin
                         (if (<= kicker2-value kicker1-value)
                             (begin
                               (if (> kicker1-value 0)
                                   (begin
                                     (set! kicker2-value kicker1-value)
                                     (set! k2flag #t)
                                     ))
                               (set! kicker1-value this-value)
                               (set! k1flag #t)
                               )))
                       (begin
                         (if (< kicker2-value this-value)
                             (begin
                               (set! kicker2-value this-value)
                               (set! k2flag #t)
                               ))
                         ))
                   ))
             )) freq-htable)

        (if (and (equal? tflag #t)
                 (equal? tflag k1flag)
                 (equal? k1flag k2flag))
            (begin
              (let ((this-count
                     (hash-ref sub-htable "count" 0)))
                (begin
                  (hash-set!
                   sub-htable "value" 40)
                  (hash-set!
                   sub-htable "count" (1+ this-count))
                  (hash-set!
                   sub-htable "signature" prime-signature)
                  (hash-set!
                   sub-htable "isflush" #f)
                  (hash-set!
                   sub-htable "triples-value" triples-value)
                  (hash-set!
                   sub-htable "kicker1" kicker1-value)
                  (hash-set!
                   sub-htable "kicker2" kicker2-value)
                  (set! result-flag #t)
                  ))
              ))

        result-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-two-pairs-hand
         freq-htable prime-signature sub-htable)
  (begin
    (let ((result-flag #f)
          (top-pairs-value -1)
          (tpflag #f)
          (second-pairs-value -1)
          (spflag #f)
          (kicker1-value -1)
          (k1flag #f))
      (begin
        (hash-for-each
         (lambda (this-value this-count)
           (begin
             (if (>= this-count 2)
                 (begin
                   (if (> this-value top-pairs-value)
                       (begin
                         (if (>= top-pairs-value
                                 second-pairs-value)
                             (begin
                               (if (> top-pairs-value 0)
                                   (begin
                                     (set! second-pairs-value
                                           top-pairs-value)
                                     (set! spflag #t)
                                     ))
                               (set! top-pairs-value this-value)
                               (set! tpflag #t))
                             (begin
                               (set! top-pairs-value this-value)
                               (set! tpflag #t)
                               )))
                       (begin
                         (if (> this-value second-pairs-value)
                             (begin
                               (set! second-pairs-value
                                     this-value)
                               (set! spflag #t)
                               ))
                         ))
                   ))
             (if (< this-count 2)
                 (begin
                   (if (> this-value kicker1-value)
                       (begin
                         (set! kicker1-value this-value)
                         (set! k1flag #t)
                         ))
                   ))
             )) freq-htable)

        (if (and (equal? tpflag #t)
                 (equal? tpflag spflag)
                 (equal? tpflag k1flag))
            (begin
              (let ((this-count
                     (hash-ref sub-htable "count" 0)))
                (begin
                  (hash-set!
                   sub-htable "value" 30)
                  (hash-set!
                   sub-htable "count" (1+ this-count))
                  (hash-set!
                   sub-htable "signature" prime-signature)
                  (hash-set!
                   sub-htable "isflush" #f)
                  (hash-set!
                   sub-htable "top-pair" top-pairs-value)
                  (hash-set!
                   sub-htable "second-pair" second-pairs-value)
                  (hash-set!
                   sub-htable "kicker1" kicker1-value)
                  (set! result-flag #t)
                  ))
              ))

        result-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (convert-value-to-bitmask avalue)
  (begin
    (let ((bshift (- avalue 2)))
      (let ((result (ash #b01 bshift)))
        (begin
          result
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (convert-bitmask-to-value amask nth-bit)
  (begin
    (let ((result-value -1)
          (bit-count 0)
          (end-flag #f))
      (begin
        (do ((ii 0 (1+ ii)))
            ((or (>= ii 15)
                 (equal? end-flag #t)))
          (begin
            (let ((bit-set (logbit? ii amask)))
              (begin
                (if (equal? bit-set #t)
                    (begin
                      (set! bit-count (1+ bit-count))
                      (if (>= bit-count nth-bit)
                          (begin
                            (set! result-value (+ ii 2))
                            (set! end-flag #t)
                            ))
                      ))
                ))
            ))
        result-value
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-one-pairs-hand
         freq-htable prime-signature sub-htable)
  (begin
    (let ((result-flag #f)
          (top-pairs-value -1)
          (tpflag #f)
          (kicker-mask 0))
      (begin
        (hash-for-each
         (lambda (this-value this-count)
           (begin
             (if (>= this-count 2)
                 (begin
                   (if (> this-value top-pairs-value)
                       (begin
                         (set! top-pairs-value this-value)
                         (set! tpflag #t)
                         ))
                   ))
             (if (< this-count 2)
                 (begin
                   (let ((kvalue
                          (convert-value-to-bitmask this-value)))
                     (begin
                       (set! kicker-mask
                             (logior kvalue kicker-mask))
                       ))
                   ))
             )) freq-htable)

        (if (equal? tpflag #t)
            (begin
              (let ((this-count
                     (hash-ref sub-htable "count" 0)))
                (begin
                  (hash-set!
                   sub-htable "value" 20)
                  (hash-set!
                   sub-htable "count" (1+ this-count))
                  (hash-set!
                   sub-htable "signature" prime-signature)
                  (hash-set!
                   sub-htable "isflush" #f)
                  (hash-set!
                   sub-htable "top-pair" top-pairs-value)
                  (let ((kicker1-value
                         (convert-bitmask-to-value
                          kicker-mask 3))
                        (kicker2-value
                         (convert-bitmask-to-value
                          kicker-mask 2))
                        (kicker3-value
                         (convert-bitmask-to-value
                          kicker-mask 1)))
                    (begin
                      (hash-set!
                       sub-htable "kicker1" kicker1-value)
                      (hash-set!
                       sub-htable "kicker2" kicker2-value)
                      (hash-set!
                       sub-htable "kicker3" kicker3-value)
                      ))
                  (set! result-flag #t)
                  ))
              ))

        result-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-high-cards-hand
         card-1 card-2 card-3 card-4 card-5
         prime-signature sub-htable)
  (begin
    (let ((result-flag #f)
          (hand-mask 0)
          (card-list
           (list card-1 card-2 card-3 card-4 card-5)))
      (begin
        (for-each
         (lambda (this-card)
           (begin
             (set! hand-mask
                   (logior hand-mask
                           (card-module:card-to-mask this-card)))
             )) card-list)

        (let ((this-count
               (hash-ref sub-htable "count" 0)))
          (begin
            (hash-set!
             sub-htable "value" 10)
            (hash-set!
             sub-htable "count" (1+ this-count))
            (hash-set!
             sub-htable "signature" prime-signature)
            (hash-set!
             sub-htable "isflush" #f)
            (hash-set!
             sub-htable "mask" hand-mask)

            (set! result-flag #t)
            ))

        result-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-nonflush-hand
         card-1 card-2 card-3 card-4 card-5
         prime-signature sub-htable)
  (begin
    (let ((freq-htable (make-hash-table 20))
          (result-flag #f))
      (begin
        (let ((straight-result-flag
               (is-straight-hand
                card-1 card-2 card-3 card-4 card-5
                prime-signature sub-htable)))
          (begin
            (set! result-flag straight-result-flag)
            ))

        ;;; load up frequency count hash table
        (for-each
         (lambda (acard)
           (begin
             (let ((this-value
                    (card-module:card-to-value acard)))
               (let ((this-count
                      (hash-ref freq-htable this-value 0)))
                 (begin
                   (hash-set!
                    freq-htable this-value (1+ this-count))
                   )))
             )) (list card-1 card-2 card-3 card-4 card-5))

        (if (equal? result-flag #f)
            (begin
              (let ((quads-result-flag
                     (is-quads-hand
                      freq-htable prime-signature sub-htable)))
                (begin
                  (set! result-flag quads-result-flag)
                  ))
              ))

        (if (equal? result-flag #f)
            (begin
              (let ((fulls-result-flag
                     (is-fulls-hand
                      freq-htable prime-signature sub-htable)))
                (begin
                  (set! result-flag fulls-result-flag)
                  ))
              ))

        (if (equal? result-flag #f)
            (begin
              (let ((triples-result-flag
                     (is-triples-hand
                      freq-htable prime-signature sub-htable)))
                (begin
                  (set! result-flag triples-result-flag)
                  ))
              ))

        (if (equal? result-flag #f)
            (begin
              (let ((two-pairs-result-flag
                     (is-two-pairs-hand
                      freq-htable prime-signature sub-htable)))
                (begin
                  (set! result-flag two-pairs-result-flag)
                  ))
              ))

        (if (equal? result-flag #f)
            (begin
              (let ((one-pairs-result-flag
                     (is-one-pairs-hand
                      freq-htable prime-signature sub-htable)))
                (begin
                  (set! result-flag one-pairs-result-flag)
                  ))
              ))

        (if (equal? result-flag #f)
            (begin
              (is-high-cards-hand
               card-1 card-2 card-3 card-4 card-5
               prime-signature sub-htable)
              (set! result-flag #t)
              ))

        result-flag
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

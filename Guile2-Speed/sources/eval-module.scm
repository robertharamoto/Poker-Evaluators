;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  evaluate poker hand module functions                 ###
;;;###  for equivalent hands                                 ###
;;;###                                                       ###
;;;###  last updated June 29, 2024                           ###
;;;###                                                       ###
;;;###  created June 28, 2024                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### eval module
(define-module (eval-module)
  #:export
  (run-hand-speed-7cards
   best-5-of-7-hand
   compare-pocket-pairs
   ))

;;;#############################################################
;;;#############################################################

;;;### srfi-1 for fold and delete functions
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format used for advanced formatting
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((card-module)
              :renamer (symbol-prefix-proc 'card-module:)))

(use-modules ((distinct-module)
              :renamer (symbol-prefix-proc 'distinct-module:)))


;;;#############################################################
;;;#############################################################

;;;###  card representation
;;;###    3          2          1          0
;;;###   10987654 32109876 54321098 76543210
;;;###  +--------+--------+--------+--------+
;;;###  |xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
;;;###  +--------+--------+--------+--------+
;;;###
;;;###  p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
;;;###  r = rank of card (deuce=2,trey=3,four=4,five=4,...,ace=14)
;;;###  cdhs = suit of card (bit turned on based on suit of card)
;;;###  b = bit turned on depending on rank of card
;;;###
;;;###  Using such a scheme, here are some bit pattern examples:
;;;###
;;;###  xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
;;;###  00001000 00000000 01001011 00100101    King of Diamonds
;;;###  00000000 00001000 00010011 00000111    Five of Spades
;;;###  00000010 00000000 10001001 00011101    Jack of Clubs

;;;###
;;;###  hand values
;;;###  royal flush 100
;;;###  straight flush 90
;;;###  four-of-a-kind 80
;;;###  full-house 70
;;;###  flush 60
;;;###  straight 50
;;;###  three-of-a-kind 40
;;;###  two-pairs 30
;;;###  one-pair 20
;;;###  high-card 10
;;;###

;;;###
;;;###  data for hash of hash tables
;;;###  hash(prime-signature) -> hash(string) -> value
;;;###
;;;###  "value" - hand values (from 10 to 100)
;;;###  "rank" - hand rank (from 1 to 7,462)
;;;###  "mask" - rank of 5 cards in bit format
;;;###  "count" - number of hands with this prime signature
;;;###  "signature" - prime signature
;;;###  "isflush" - bool flush indicator
;;;###  "quad-value" - card value of 4-of-a-kind
;;;###  "trip-value" - card value of 3-of-a-kind
;;;###  "top-pair" - card value of top pair
;;;###  "second-pair" - card value of second pair
;;;###  "kicker1" - card value of top kicker
;;;###  "kicker2" - card value of second kicker
;;;###  "kicker3" - card value of third kicker
;;;###
;;;###


;;;#############################################################
;;;#############################################################
(define-syntax inner-inner-inner-loop-best-5-macro
  (syntax-rules ()
    ((inner-inner-inner-loop-best-5-macro
      suit-1 suit-2 suit-3 suit-4 suit-5
      signature
      flush-primes-htable
      nonflush-primes-htable
      best-rank best-hand-value
      best-signature best-flush-flag)
     (begin
       (if (not
            (equal?
             (logand
              suit-1 suit-2 suit-3 suit-4 suit-5)
             0))
           (begin
             (let ((sub-htable
                    (hash-ref flush-primes-htable
                              signature #f)))
               (let ((this-rank
                      (hash-ref
                       sub-htable
                       "rank" 0)))
                 (begin
                   (if (> this-rank best-rank)
                       (begin
                         (let ((this-value
                                (hash-ref sub-htable
                                          "value" 0)))
                           (begin
                             (set! best-hand-value
                                   this-value)
                             (set! best-rank this-rank)
                             (set! best-signature
                                   signature)
                             (set! best-flush-flag #t)
                             ))
                         ))
                   ))
               ))
           (begin
             (let ((sub-htable
                    (hash-ref nonflush-primes-htable
                              signature #f)))
               (let ((this-rank
                      (hash-ref sub-htable
                                "rank" 0)))
                 (begin
                   (if (> this-rank best-rank)
                       (begin
                         (let ((this-value
                                (hash-ref sub-htable
                                          "value" 0)))
                           (begin
                             (set! best-hand-value
                                   this-value)
                             (set! best-rank
                                   this-rank)
                             (set! best-signature
                                   signature)
                             (set! best-flush-flag #f)
                             ))
                         ))
                   )))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax inner-inner-loop-best-5-macro
  (syntax-rules ()
    ((inner-inner-loop-best-5-macro
      community-board
      ii-3
      card-1 suit-1 prime-1
      card-2 suit-2 prime-2
      card-3 suit-3 prime-3
      max-4 max-5
      flush-primes-htable
      nonflush-primes-htable
      best-signature best-hand-value
      best-rank best-flush-flag)
     (begin
       (do ((ii-4 (1+ ii-3) (1+ ii-4)))
           ((>= ii-4 max-4))
         (begin
           (let ((card-4
                  (list-ref
                   community-board ii-4)))
             (let ((suit-4
                    (card-module:card-to-suit
                     card-4))
                   (prime-4
                    (card-module:card-to-prime-value
                     card-4)))
               (begin
                 (do ((ii-5 (1+ ii-4) (1+ ii-5)))
                     ((>= ii-5 max-5))
                   (begin
                     (let ((card-5
                            (list-ref
                             community-board ii-5)))
                       (let ((suit-5
                              (card-module:card-to-suit
                               card-5))
                             (prime-5
                              (card-module:card-to-prime-value
                               card-5)))
                         (let ((signature
                                (* prime-1 prime-2 prime-3
                                   prime-4 prime-5)))
                           (begin
                             (inner-inner-inner-loop-best-5-macro
                              suit-1 suit-2 suit-3 suit-4 suit-5
                              signature
                              flush-primes-htable
                              nonflush-primes-htable
                              best-rank best-hand-value
                              best-signature best-flush-flag)
                             ))
                         ))
                     ))
                 )))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (best-5-of-7-hand
         community-board
         flush-primes-htable nonflush-primes-htable)
  (begin
    (let ((max-len (length community-board)))
      (let ((max-1 (- max-len 4))
            (max-2 (- max-len 3))
            (max-3 (- max-len 2))
            (max-4 (- max-len 1))
            (max-5 max-len)
            (best-signature -1)
            (best-hand-value -1)
            (best-rank -1)
            (best-flush-flag #f))
        (begin
          (do ((ii-1 0 (1+ ii-1)))
              ((>= ii-1 max-1))
            (begin
              (let ((card-1 (list-ref community-board ii-1)))
                (let ((suit-1
                       (card-module:card-to-suit
                        card-1))
                      (prime-1
                       (card-module:card-to-prime-value
                        card-1)))
                  (begin
                    (do ((ii-2 (1+ ii-1) (1+ ii-2)))
                        ((>= ii-2 max-2))
                      (begin
                        (let ((card-2
                               (list-ref community-board ii-2)))
                          (let ((suit-2
                                 (card-module:card-to-suit
                                  card-2))
                                (prime-2
                                 (card-module:card-to-prime-value
                                  card-2)))
                            (begin
                              (do ((ii-3 (1+ ii-2) (1+ ii-3)))
                                  ((>= ii-3 max-3))
                                (begin
                                  (let ((card-3
                                         (list-ref
                                          community-board ii-3)))
                                    (let ((suit-3
                                           (card-module:card-to-suit
                                            card-3))
                                          (prime-3
                                           (card-module:card-to-prime-value
                                            card-3)))
                                      (begin
                                        (inner-inner-loop-best-5-macro
                                         community-board
                                         ii-3
                                         card-1 suit-1 prime-1
                                         card-2 suit-2 prime-2
                                         card-3 suit-3 prime-3
                                         max-4 max-5
                                         flush-primes-htable
                                         nonflush-primes-htable
                                         best-signature
                                         best-hand-value best-rank
                                         best-flush-flag)
                                        )))
                                  ))
                              )))
                        ))
                    )))
              ))

          (list best-signature best-hand-value best-rank
                best-flush-flag)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define-syntax inner-7-procs-loop-6-7-macro
  (syntax-rules ()
    ((inner-7-procs-loop-6-7-macro
      ii-5 max-6 max-7
      c1-array c5-board
      flush-primes-htable
      nonflush-primes-htable
      total-count-htable
      rank-value-htable)
     (begin
       (do ((ii-6 (1+ ii-5) (1+ ii-6)))
           ((>= ii-6 max-6))
         (begin
           (let ((card-6 (array-ref c1-array ii-6)))
             (let ((c6-board (cons card-6 c5-board)))
               (begin
                 (do ((ii-7 (1+ ii-6) (1+ ii-7)))
                     ((>= ii-7 max-7))
                   (begin
                     (let ((card-7
                            (array-ref c1-array ii-7)))
                       (let ((community-board (cons card-7 c6-board)))
                         (begin
                           (let ((result-list
                                  (best-5-of-7-hand
                                   community-board
                                   flush-primes-htable
                                   nonflush-primes-htable)))
                             (let ((best-hand-value
                                    (list-ref result-list 1))
                                   (best-rank
                                    (list-ref result-list 2)))
                               (let ((tcount
                                      (hash-ref
                                       total-count-htable
                                       best-hand-value 0)))
                                 (begin
                                   (hash-set! total-count-htable
                                              best-hand-value
                                              (1+ tcount))
                                   (hash-set! rank-value-htable
                                              best-rank
                                              best-hand-value)
                                   )))
                             ))
                         ))
                     ))
                 )))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax inner-7-procs-loop-4-5-macro
  (syntax-rules ()
    ((inner-7-procs-loop-4-5-macro
      ii-3 max-4 max-5 max-6 max-7
      c1-array c3-board
      flush-primes-htable
      nonflush-primes-htable
      total-count-htable
      rank-value-htable)
     (begin
       (do ((ii-4 (1+ ii-3) (1+ ii-4)))
           ((>= ii-4 max-4))
         (begin
           (let ((card-4 (array-ref c1-array ii-4)))
             (let ((c4-board (cons card-4 c3-board)))
               (begin
                 (do ((ii-5 (1+ ii-4) (1+ ii-5)))
                     ((>= ii-5 max-5))
                   (begin
                     (let ((card-5 (array-ref c1-array ii-5)))
                       (let ((c5-board (cons card-5 c4-board)))
                         (begin
                           (inner-7-procs-loop-6-7-macro
                            ii-5 max-6 max-7
                            c1-array c5-board
                            flush-primes-htable
                            nonflush-primes-htable
                            total-count-htable
                            rank-value-htable)
                           )))
                     ))
                 )))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax inner-7-procs-loop-2-3-macro
  (syntax-rules ()
    ((inner-7-procs-loop-2-3-macro
      ii-1 max-2 max-3 max-4 max-5
      max-6 max-7
      c1-array c1-board
      flush-primes-htable
      nonflush-primes-htable
      total-count-htable
      rank-value-htable)
     (begin
       (do ((ii-2 (1+ ii-1) (1+ ii-2)))
           ((>= ii-2 max-2))
         (begin
           (let ((card-2 (array-ref c1-array ii-2)))
             (let ((c2-board (cons card-2 c1-board)))
               (begin
                 (do ((ii-3 (1+ ii-2) (1+ ii-3)))
                     ((>= ii-3 max-3))
                   (begin
                     (let ((card-3 (array-ref c1-array ii-3)))
                       (let ((c3-board (cons card-3 c2-board)))
                         (begin
                           (inner-7-procs-loop-4-5-macro
                            ii-3 max-4 max-5 max-6 max-7
                            c1-array c3-board
                            flush-primes-htable
                            nonflush-primes-htable
                            total-count-htable
                            rank-value-htable)
                           )))
                     ))
                 )))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-7-hands-results
         start-time-secs-1 end-time-secs-1
         total-count-htable rank-value-htable)
  (begin
    (let ((hand-values-list
           (list 100 90 80 70 60 50 40 30 20 10))
          (hand-string-list
           (list "royal flush" "straight-flush"
                 "four-of-a-kind" "full-house"
                 "flush" "straight" "three-of-a-kind"
                 "two-pairs" "one-pair" "high-card"))
          (nseconds
           (inexact->exact
            (truncate
             (- end-time-secs-1 start-time-secs-1))))
          (totals 0)
          (distinct-count 0))
      (let ((max-len (length hand-values-list)))
        (begin
          (do ((ii 0 (1+ ii)))
              ((>= ii max-len))
            (begin
              (let ((hand-value
                     (list-ref hand-values-list ii))
                    (hand-string
                     (list-ref hand-string-list ii)))
                (let ((this-count
                       (hash-ref
                        total-count-htable hand-value 0))
                      (this-distinct 0))
                  (begin
                    (hash-for-each
                     (lambda (rank value)
                       (begin
                         (if (equal? value hand-value)
                             (begin
                               (set! this-distinct
                                     (1+ this-distinct))
                               ))
                         )) rank-value-htable)

                    (display
                     (ice-9-format:format
                      #f "  ~a  :  ~:d  :  ~:d~%"
                      hand-string this-count this-distinct))

                    (set! totals (+ totals this-count))
                    (set! distinct-count
                          (+ distinct-count this-distinct))
                    )))
              ))

          (let ((ips
                 (inexact->exact
                  (truncate
                   (+ (/ totals nseconds) 0.50)))))
            (begin
              (display
               (ice-9-format:format
                #f "  totals  :  ~:d  :  ~:d~%"
                totals distinct-count))
              (display
               (ice-9-format:format
                #f "number of hands = ~:d, number of seconds = ~:d~%"
                totals nseconds))
              (display
               (ice-9-format:format
                #f "7-card hand evaluations per second = ~:d~%"
                ips))
              ))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;;### define evaluation over all community boards
;;;### using do loops
;;;### note - card deck is a list of cards
;;;### and so are pockets-a and pockets-b
(define (run-hand-speed-7cards
         flush-primes-htable nonflush-primes-htable)
  (begin
    (let ((start-time-secs-1 (current-time))
          (card-deck (distinct-module:create-card-deck)))
      (let ((total-count-htable (make-hash-table 20))
            (rank-value-htable (make-hash-table 5000))
            (ncards (length card-deck))
            (c1-array (list->array 1 card-deck)))
        (let ((max-1 (- ncards 6))
              (max-2 (- ncards 5))
              (max-3 (- ncards 4))
              (max-4 (- ncards 3))
              (max-5 (- ncards 2))
              (max-6 (- ncards 1))
              (max-7 ncards))
          (begin
            (do ((ii-1 0 (1+ ii-1)))
                ((>= ii-1 max-1))
              (begin
                (let ((card-1 (array-ref c1-array ii-1)))
                  (let ((c1-board (list card-1)))
                    (begin
                      (if (and (> ii-1 0)
                               (equal? (modulo ii-1 13) 0))
                          (begin
                            (display
                             (format
                              #f "~a~%"
                              (card-module:card-to-string card-1)))
                            (force-output))
                          (begin
                            (display
                             (format
                              #f "~a "
                              (card-module:card-to-string card-1)))
                            ))
                      (force-output)

                      (inner-7-procs-loop-2-3-macro
                       ii-1 max-2 max-3 max-4 max-5
                       max-6 max-7
                       c1-array c1-board
                       flush-primes-htable
                       nonflush-primes-htable
                       total-count-htable
                       rank-value-htable)
                      )))
                ))

            (newline)
            (let ((end-time-secs-1 (current-time)))
              (begin
                (display-7-hands-results
                 start-time-secs-1
                 end-time-secs-1
                 total-count-htable
                 rank-value-htable)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (calc-win-loss-stats
         flush-primes-htable
         nonflush-primes-htable
         card-list-a a-wins a-losses a-ties
         card-list-b b-wins b-losses b-ties)
  (begin
    (let ((next-a-wins a-wins)
          (next-a-losses a-losses)
          (next-a-ties a-ties)
          (next-b-wins b-wins)
          (next-b-losses b-losses)
          (next-b-ties b-ties))
      (begin
        (let ((result-a-list
               (best-5-of-7-hand
                card-list-a
                flush-primes-htable nonflush-primes-htable))
              (result-b-list
               (best-5-of-7-hand
                card-list-b
                flush-primes-htable nonflush-primes-htable)))
          (let ((best-a-hand-value
                 (list-ref result-a-list 1))
                (best-a-rank
                 (list-ref result-a-list 2))
                (best-b-hand-value
                 (list-ref result-b-list 1))
                (best-b-rank
                 (list-ref result-b-list 2)))
            (begin
              (cond
               ((> best-a-hand-value best-b-hand-value)
                (begin
                  (set! next-a-wins (1+ next-a-wins))
                  (set! next-b-losses (1+ next-b-losses))
                  ))
               ((< best-a-hand-value best-b-hand-value)
                (begin
                  (set! next-b-wins (1+ next-b-wins))
                  (set! next-a-losses (1+ next-a-losses))
                  ))
               (else
                (begin
                  ;;; hand-a type the same as hand-b type
                  (cond
                   ((> best-a-rank best-b-rank)
                    (begin
                      (set! next-a-wins (1+ next-a-wins))
                      (set! next-b-losses (1+ next-b-losses))
                      ))
                   ((equal? best-a-rank best-b-rank)
                    (begin
                      (set! next-a-ties (1+ next-a-ties))
                      (set! next-b-ties (1+ next-b-ties))
                      ))
                   (else
                    (begin
                      (set! next-a-losses (1+ next-a-losses))
                      (set! next-b-wins (1+ next-b-wins))
                      )))
                  )))

              (list next-a-wins next-a-losses next-a-ties
                    next-b-wins next-b-losses next-b-ties)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax inner-comparison-4-5-macro
  (syntax-rules ()
    ((inner-comparison-4-5-macro
      ii-3 max-4 max-5 cc-array
      pockets-a pockets-b
      c3-board
      flush-primes-htable
      nonflush-primes-htable
      ntotals
      a-wins a-losses a-ties
      b-wins b-losses b-ties)
     (begin
       (do ((ii-4 (1+ ii-3) (1+ ii-4)))
           ((>= ii-4 max-4))
         (begin
           (let ((card-4 (array-ref cc-array ii-4)))
             (let ((c4-board (cons card-4 c3-board)))
               (begin
                 (do ((ii-5 (1+ ii-4) (1+ ii-5)))
                     ((>= ii-5 max-5))
                   (begin
                     (let ((card-5 (array-ref cc-array ii-5)))
                       (let ((community-board (cons card-5 c4-board)))
                         (let ((card-list-a
                                (append pockets-a community-board))
                               (card-list-b
                                (append pockets-b community-board)))
                           (let ((wl-list
                                  (calc-win-loss-stats
                                   flush-primes-htable
                                   nonflush-primes-htable
                                   card-list-a a-wins a-losses a-ties
                                   card-list-b b-wins b-losses b-ties)))
                             (begin
                               (set! ntotals (1+ ntotals))

                               (set! a-wins (list-ref wl-list 0))
                               (set! a-losses (list-ref wl-list 1))
                               (set! a-ties (list-ref wl-list 2))

                               (set! b-wins (list-ref wl-list 3))
                               (set! b-losses (list-ref wl-list 4))
                               (set! b-ties (list-ref wl-list 5))
                               )))
                         ))
                     ))
                 )))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax inner-comparison-2-3-macro
  (syntax-rules ()
    ((inner-comparison-2-3-macro
      ii-1 max-2 max-3 max-4 max-5
      cc-array
      pockets-a pockets-b
      c1-board
      flush-primes-htable
      nonflush-primes-htable
      ntotals
      a-wins a-losses a-ties
      b-wins b-losses b-ties)
     (begin
       (do ((ii-2 (1+ ii-1) (1+ ii-2)))
           ((>= ii-2 max-2))
         (begin
           (let ((card-2 (array-ref cc-array ii-2)))
             (let ((c2-board (cons card-2 c1-board)))
               (begin
                 (do ((ii-3 (1+ ii-2) (1+ ii-3)))
                     ((>= ii-3 max-3))
                   (begin
                     (let ((card-3 (array-ref cc-array ii-3)))
                       (let ((c3-board (cons card-3 c2-board)))
                         (begin
                           (inner-comparison-4-5-macro
                            ii-3 max-4 max-5 cc-array
                            pockets-a pockets-b
                            c3-board
                            flush-primes-htable
                            nonflush-primes-htable
                            ntotals
                            a-wins a-losses a-ties
                            b-wins b-losses b-ties)
                           )))
                     ))
                 )))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-pocket-pairs-results
         start-time-secs-1 end-time-secs-1
         pockets-a pockets-b
         ntotals
         a-wins a-losses a-ties
         b-wins b-losses b-ties)
  (begin
    (let ((nseconds
           (inexact->exact
            (truncate
             (- end-time-secs-1 start-time-secs-1))))
          (a-string
           (card-module:card-list-to-string pockets-a))
          (b-string
           (card-module:card-list-to-string pockets-b)))
      (let ((a-wins-pcnt
             (utils-module:round-float
              (* 100.0 (/ a-wins ntotals)) 2.0))
            (a-losses-pcnt
             (utils-module:round-float
              (* 100.0 (/ a-losses ntotals)) 2.0))
            (a-ties-pcnt
             (utils-module:round-float
              (* 100.0 (/ a-ties ntotals)) 2.0))
            (b-wins-pcnt
             (utils-module:round-float
              (* 100.0 (/ b-wins ntotals)) 2.0))
            (b-losses-pcnt
             (utils-module:round-float
              (* 100.0 (/ b-losses ntotals)) 2.0))
            (b-ties-pcnt
             (utils-module:round-float
              (* 100.0 (/ b-ties ntotals)) 2.0))
            (a-totals (+ a-wins a-losses a-ties))
            (b-totals (+ b-wins b-losses b-ties))
            (a-totals-pcnt
             (utils-module:round-float
              (* 100.0 (/ (+ a-wins a-losses a-ties) ntotals))
              2.0))
            (b-totals-pcnt
             (utils-module:round-float
              (* 100.0 (/ (+ b-wins b-losses b-ties) ntotals))
              2.0)))
        (begin
          (display
           (format
            #f "Stats for pockets ~a,  " a-string))
          (display
           (format
            #f "Stats for pockets ~a~%" b-string))
          (display
           (ice-9-format:format
            #f "Number of wins ~:d (~4,2f%),  ~:d (~4,2f%)~%"
            a-wins a-wins-pcnt
            b-wins b-wins-pcnt))
          (display
           (ice-9-format:format
            #f "Number of losses ~:d (~4,2f%),  ~:d (~4,2f%)~%"
            a-losses a-losses-pcnt
            b-losses b-losses-pcnt))
          (display
           (ice-9-format:format
            #f "Number of ties ~:d (~4,2f%),  ~:d (~4,2f%)~%"
            a-ties a-ties-pcnt
            b-ties b-ties-pcnt))
          (display
           (ice-9-format:format
            #f "Totals ~:d (~4,2f%),  ~:d (~4,2f%)~%"
            a-totals a-totals-pcnt
            b-totals b-totals-pcnt))

          (let ((ips
                 (inexact->exact
                  (truncate
                   (+ (/ ntotals nseconds) 0.50)))))
            (begin
              (display
               (ice-9-format:format
                #f "number of hands = ~:d, number of seconds = ~:d~%"
                ntotals nseconds))
              (display
               (ice-9-format:format
                #f "7-card hand evaluations per second = ~:d~%"
                ips))
              ))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;;### define evaluation over all community boards
;;;### using do loops
;;;### note - card deck is a list of cards
;;;### and so are pockets-a and pockets-b
(define (compare-pocket-pairs
         pockets-a pockets-b
         flush-primes-htable nonflush-primes-htable)
  (define (local-extract-pockets-from-deck
           pockets-a pockets-b card-deck)
    (begin
      (let ((c1
             (srfi-1:delete
              (list-ref pockets-a 0)
              card-deck card-module:card1-identical-card2?)))
        (let ((c2
               (srfi-1:delete
                (list-ref pockets-a 1)
                c1 card-module:card1-identical-card2?)))
          (let ((c3
                 (srfi-1:delete
                  (list-ref pockets-b 0)
                  c2 card-module:card1-identical-card2?)))
            (let ((c4
                   (srfi-1:delete
                    (list-ref pockets-b 1)
                    c3 card-module:card1-identical-card2?)))
              (begin
                c4
                )))
          ))
      ))
  (begin
    (let ((card-deck (distinct-module:create-card-deck))
          (start-time-secs-1 (current-time)))
      (let ((cc-deck
             (local-extract-pockets-from-deck
              pockets-a pockets-b card-deck))
            (a-wins 0)
            (a-losses 0)
            (a-ties 0)
            (b-wins 0)
            (b-losses 0)
            (b-ties 0)
            (ntotals 0))
        (let ((ncards (length cc-deck))
              (c1-array (list->array 1 cc-deck)))
          (let ((max-1 (- ncards 4))
                (max-2 (- ncards 3))
                (max-3 (- ncards 2))
                (max-4 (- ncards 1))
                (max-5 ncards))
            (begin
              (do ((ii-1 0 (1+ ii-1)))
                  ((>= ii-1 max-1))
                (begin
                  (let ((card-1 (array-ref c1-array ii-1)))
                    (let ((c1-board (list card-1)))
                      (begin
                        (if (and (> ii-1 0)
                                 (equal? (modulo ii-1 13) 0))
                            (begin
                              (display
                               (format
                                #f "~a~%"
                                (card-module:card-to-string card-1)))
                              (force-output))
                            (begin
                              (display
                               (format
                                #f "~a "
                                (card-module:card-to-string card-1)))
                              ))
                        (force-output)

                        (inner-comparison-2-3-macro
                         ii-1 max-2 max-3 max-4 max-5
                         c1-array
                         pockets-a pockets-b
                         c1-board
                         flush-primes-htable
                         nonflush-primes-htable
                         ntotals
                         a-wins a-losses a-ties
                         b-wins b-losses b-ties)
                        )))
                  ))

              (newline)
              (let ((end-time-secs-1 (current-time)))
                (begin
                  (display-pocket-pairs-results
                   start-time-secs-1 end-time-secs-1
                   pockets-a pockets-b
                   ntotals
                   a-wins a-losses a-ties
                   b-wins b-losses b-ties)
                  ))
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

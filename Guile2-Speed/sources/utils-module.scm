;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  utils-module - standard utilities                    ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 4, 2022                                  ###
;;;###                                                       ###
;;;###  updated February 27, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start utils modules
(define-module (utils-module)
  #:export (commafy-number
            round-float
            sequence-list-to-string
            number-list-to-string
            remove-commas-from-string
            get-processor-name
            get-basename)
  #:duplicates (replace last))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;#############################################################
;;;#############################################################
;;;### regex used for regex
(use-modules ((ice-9 regex)
              :renamer (symbol-prefix-proc 'ice-9-regex:)))

;;;### ice-9 format to automatically commafy using format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### ice-9 rdelim for read-line
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### ice-9 popen to open-pipe
(use-modules ((ice-9 popen)
              :renamer (symbol-prefix-proc 'ice-9-popen:)))

;;;#############################################################
;;;#############################################################
;;;### exported functions

;;;#############################################################
;;;#############################################################
(define (commafy-number num)
  (begin
    (let ((int1 (inexact->exact (truncate num))))
      (begin
        (if (= num int1)
            (begin
              (ice-9-format:format
               #f "~:d" int1))
            (begin
              (let ((str
                     (ice-9-format:format #f "~8,6f" num)))
                (let ((sresult
                       (substring
                        str
                        (or (string-index str #\.)
                            (string-length str)))))
                  (begin
                    (string-append
                     (ice-9-format:format #f "~:d" int1)
                     sresult)
                    )))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (round-float fnum decimals)
  (begin
    (if (and
         (number? fnum)
         (number? decimals))
        (begin
          (let ((factor
                 (truncate
                  (+ (expt 10.0 decimals) 0.500))))
            (let ((inv-factor
                   (expt 10.0 (* -1.0 decimals))))
              (begin
                (if (>= fnum 0.0)
                    (begin
                      (let ((tresult
                             (* inv-factor
                                (inexact->exact
                                 (truncate
                                  (+ (* factor fnum) 0.500))))
                             ))
                        (begin
                          tresult
                          )))
                    (begin
                      (let ((tresult
                             (* inv-factor
                                (truncate
                                 (- (* factor fnum) 0.50)))))
                        (begin
                          tresult
                          ))
                      ))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (sequence-list-to-string seq-list)
  (begin
    (if (list? seq-list)
        (begin
          (let ((this-string
                 (string-join
                  (map
                   (lambda (anum)
                     (begin
                       (commafy-number anum)
                       )) seq-list)
                  " -> ")))
            (begin
              (format #f "[~a]" this-string)
              )))
        (begin
          (format #f "[~a]" seq-list)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (number-list-to-string num-list)
  (begin
    (if (list? num-list)
        (begin
          (let ((this-string
                 (string-join
                  (map
                   (lambda (anum)
                     (begin
                       (commafy-number anum)
                       )) num-list)
                  " ; ")))
            (begin
              (format #f "[~a]" this-string)
              )))
        (begin
          (format #f "[~a]" num-list)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (remove-commas-from-string this-string)
  (begin
    (if (not (string? this-string))
        (begin
          this-string)
        (begin
          (let ((new-string
                 (ice-9-regex:regexp-substitute/global
                  #f "," this-string 'pre "" 'post)))
            (begin
              new-string
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (get-processor-name)
  (begin
    (let ((ipipe
           (ice-9-popen:open-input-pipe
            "grep -i 'model name' /proc/cpuinfo | head --lines=1")))
      (let ((line (ice-9-rdelim:read-line ipipe)))
        (begin
          (ice-9-popen:close-pipe ipipe)
          (let ((pname
                 (string-trim-both
                  (list-ref
                   (string-split line #\:)
                   1))))
            (begin
              pname
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (get-basename fname)
  (begin
    (if (string? fname)
        (begin
          (car
           (last-pair
            (string-split fname #\/))))
        (begin
          ""
          ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

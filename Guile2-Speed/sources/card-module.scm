;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  cards module functions                               ###
;;;###                                                       ###
;;;###  last updated June 23, 2024                           ###
;;;###                                                       ###
;;;###  created June 22, 2024                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;###  uses bitmask representation from suffe.cool
;;;###  http://suffe.cool/poker/evaluator.html
;;;###

;;;#############################################################
;;;#############################################################
;;;### start card module
(define-module (card-module)
  #:export (string-to-card
            card-to-string
            card-list-to-string

            card1-greater-card2?
            card1-equal-card2?
            card1-identical-card2?

            card-to-value
            card-to-prime-value
            card-to-mask

            card-to-suit
            card-to-suit-string
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### srfi-1 for map functions
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))


;;;#############################################################
;;;#############################################################
;;;###
;;;###  +--------+--------+--------+--------+
;;;###  |xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
;;;###  +--------+--------+--------+--------+
;;;###
;;;###  p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
;;;###  r = rank of card (deuce=2,trey=3,four=4,five=4,...,ace=14)
;;;###  cdhs = suit of card (bit turned on based on suit of card)
;;;###  b = bit turned on depending on rank of card
;;;###
;;;###  Using such a scheme, here are some bit pattern examples:
;;;###
;;;###  xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
;;;###  00001000 00000000 01001011 00100101    King of Diamonds
;;;###  00000000 00001000 00010011 00000111    Five of Spades
;;;###  00000010 00000000 10001001 00011101    Jack of Clubs


;;;#############################################################
;;;#############################################################
;;;  define bit shifts
;;;###  (define-public straight-mask-shift 16)
;;;###  (define-public suit-shift 12)
;;;###  (define-public value-shift 8)

;;;#############################################################
;;;#############################################################
;;;### define functions

;;;#############################################################
;;;#############################################################
(define-syntax string-value-to-card-macro
  (syntax-rules ()
    ((string-value-to-card-macro
      anum
      amask straight-mask-shift
      suit-mask suit-mask-shift
      avalue value-shift
      aprime)
     (begin
       (set! anum
             (logior
              (ash amask straight-mask-shift)
              (ash suit-mask suit-mask-shift)
              (ash avalue value-shift)
              aprime))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (string-to-card card-string)
  (begin
    (let ((anum 0)
          (straight-mask-shift 16)
          (suit-shift 12)
          (value-shift 8)
          (suit-mask 0)
          (spades-mask 1)
          (hearts-mask 2)
          (diamonds-mask 4)
          (clubs-mask 8))
      (begin
        (cond
         ((not
           (equal?
            (string-contains-ci card-string "s") #f))
          (begin
            (set! suit-mask spades-mask)
            ))
         ((not
           (equal?
            (string-contains-ci card-string "c") #f))
          (begin
            (set! suit-mask clubs-mask)
            ))
         ((not
           (equal?
            (string-contains-ci card-string "d") #f))
          (begin
            (set! suit-mask diamonds-mask)
            ))
         ((not
           (equal?
            (string-contains-ci card-string "h") #f))
          (begin
            (set! suit-mask hearts-mask)
            )))

        (cond
         ((string-contains-ci card-string "2")
          (begin
            (let ((two-mask #x1)
                  (two-value 2)
                  (two-prime 2))
              (begin
                (string-value-to-card-macro
                 anum
                 two-mask straight-mask-shift
                 suit-mask suit-shift
                 two-value value-shift
                 two-prime)
                anum
                ))
            ))
         ((string-contains-ci card-string "3")
          (begin
            (let ((three-mask #x2)
                  (three-value 3)
                  (three-prime 3))
              (begin
                (string-value-to-card-macro
                 anum
                 three-mask straight-mask-shift
                 suit-mask suit-shift
                 three-value value-shift
                 three-prime)
                anum
                ))
            ))
         ((string-contains-ci card-string "4")
          (begin
            (let ((four-mask #x4)
                  (four-value 4)
                  (four-prime 5))
              (begin
                (string-value-to-card-macro
                 anum
                 four-mask straight-mask-shift
                 suit-mask suit-shift
                 four-value value-shift
                 four-prime)
                anum
                ))
            ))
         ((string-contains-ci card-string "5")
          (begin
            (let ((five-mask #x8)
                  (five-value 5)
                  (five-prime 7))
              (begin
                (string-value-to-card-macro
                 anum
                 five-mask straight-mask-shift
                 suit-mask suit-shift
                 five-value value-shift
                 five-prime)
                anum
                ))
            ))
         ((string-contains-ci card-string "6")
          (begin
            (let ((six-mask #x10)
                  (six-value 6)
                  (six-prime 11))
              (begin
                (string-value-to-card-macro
                 anum
                 six-mask straight-mask-shift
                 suit-mask suit-shift
                 six-value value-shift
                 six-prime)
                anum
                ))
            ))
         ((string-contains-ci card-string "7")
          (begin
            (let ((seven-mask #x20)
                  (seven-value 7)
                  (seven-prime 13))
              (begin
                (string-value-to-card-macro
                 anum
                 seven-mask straight-mask-shift
                 suit-mask suit-shift
                 seven-value value-shift
                 seven-prime)
                anum
                ))
            ))
         ((string-contains-ci card-string "8")
          (begin
            (let ((eight-mask #x40)
                  (eight-value 8)
                  (eight-prime 17))
              (begin
                (string-value-to-card-macro
                 anum
                 eight-mask straight-mask-shift
                 suit-mask suit-shift
                 eight-value value-shift
                 eight-prime)
                anum
                ))
            ))
         ((string-contains-ci card-string "9")
          (begin
            (let ((nine-mask #x80)
                  (nine-value 9)
                  (nine-prime 19))
              (begin
                (string-value-to-card-macro
                 anum
                 nine-mask straight-mask-shift
                 suit-mask suit-shift
                 nine-value value-shift
                 nine-prime)
                anum
                ))
            ))
         ((string-contains-ci card-string "t")
          (begin
            (let ((ten-mask #x100)
                  (ten-value 10)
                  (ten-prime 23))
              (begin
                (string-value-to-card-macro
                 anum
                 ten-mask straight-mask-shift
                 suit-mask suit-shift
                 ten-value value-shift
                 ten-prime)
                anum
                ))
            ))
         ((string-contains-ci card-string "j")
          (begin
            (let ((jack-mask #x200)
                  (jack-value 11)
                  (jack-prime 29))
              (begin
                (string-value-to-card-macro
                 anum
                 jack-mask straight-mask-shift
                 suit-mask suit-shift
                 jack-value value-shift
                 jack-prime)
                anum
                ))
            ))
         ((string-contains-ci card-string "q")
          (begin
            (let ((queen-mask #x400)
                  (queen-value 12)
                  (queen-prime 31))
              (begin
                (string-value-to-card-macro
                 anum
                 queen-mask straight-mask-shift
                 suit-mask suit-shift
                 queen-value value-shift
                 queen-prime)
                anum
                ))
            ))
         ((string-contains-ci card-string "k")
          (begin
            (let ((king-mask #x800)
                  (king-value 13)
                  (king-prime 37))
              (begin
                (string-value-to-card-macro
                 anum
                 king-mask straight-mask-shift
                 suit-mask suit-shift
                 king-value value-shift
                 king-prime)
                anum
                ))
            ))
         ((string-contains-ci card-string "a")
          (begin
            (let ((ace-mask #x1000)
                  (ace-value 14)
                  (ace-prime 41))
              (begin
                (string-value-to-card-macro
                 anum
                 ace-mask straight-mask-shift
                 suit-mask suit-shift
                 ace-value value-shift
                 ace-prime)
                anum
                ))
            ))
         (else
          (begin
            (display
             (format #f "string-value-to-card(~a) error~%"
                     card-string))
            -1
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (card-to-string a-card)
  (begin
    (let ((a-card-value
           (card-to-value a-card))
          (this-suit
           (card-to-suit-string a-card)))
      (begin
        (cond
         ((< a-card-value 10)
          (begin
            (format
             #f "~a~a"
             a-card-value this-suit)
            ))
         ((equal? a-card-value 10)
          (begin
            (format #f "~a~a"
                    "t" this-suit)
            ))
         ((equal? a-card-value 11)
          (begin
            (format #f "~a~a"
                    "j" this-suit)
            ))
         ((equal? a-card-value 12)
          (begin
            (format #f "~a~a"
                    "q" this-suit)
            ))
         ((equal? a-card-value 13)
          (begin
            (format #f "~a~a"
                    "k" this-suit)
            ))
         ((equal? a-card-value 14)
          (begin
            (format #f "~a~a"
                    "a" this-suit)
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (card-list-to-string card-list)
  (begin
    (let ((result-string
           (string-join
            (map
             (lambda (acard)
               (begin
                 (card-to-string acard)
                 )) card-list)
            ", ")))
      (begin
        (format #f "[~a]" (string-trim result-string))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (card1-greater-card2? c1 c2)
  (begin
    (let ((prime-mask #x3F))
      (let ((prime-1 (logand c1 prime-mask))
            (prime-2 (logand c2 prime-mask)))
        (begin
          (> prime-1 prime-2)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (card1-equal-card2? c1 c2)
  (begin
    (let ((prime-mask #x3F))
      (let ((prime-1 (logand c1 prime-mask))
            (prime-2 (logand c2 prime-mask)))
        (begin
          (= prime-1 prime-2)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (card1-identical-card2? c1 c2)
  (begin
    (equal? c1 c2)
    ))

;;;#############################################################
;;;#############################################################
(define (card-to-value a-card)
  (begin
    (let ((value-mask #x0F)
          (value-shift -8))
      (begin
        (logand value-mask
                (ash a-card value-shift))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (card-to-prime-value a-card)
  (begin
    (let ((prime-mask #x3F))
      (begin
        (logand prime-mask a-card)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (card-to-mask a-card)
  (begin
    (let ((value-mask #x1FFF)
          (value-shift -16))
      (begin
        (logand value-mask
                (ash a-card value-shift))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (card-to-suit-string a-card)
  (begin
    (let ((suit-mask #x0F)
          (suit-shift 12))
      (let ((suit-value
             (logand suit-mask
                     (ash a-card (* -1 suit-shift)))))
        (begin
          (cond
           ((equal? suit-value 1)
            (begin
              "s"
              ))
           ((equal? suit-value 2)
            (begin
              "h"
              ))
           ((equal? suit-value 4)
            (begin
              "d"
              ))
           ((equal? suit-value 8)
            (begin
              "c"
              ))
           (else
            (begin
              "error"
              )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (card-to-suit a-card)
  (begin
    (let ((suit-mask #x0F)
          (suit-shift -12))
      (let ((suit-value
             (logand suit-mask
                     (ash a-card suit-shift))))
        (begin
          suit-value
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

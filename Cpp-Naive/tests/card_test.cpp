/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TCardTest implementation                              ###
  ###                                                        ###
  ###  last updated April 29, 2024                           ###
  ###    changed definitions of bitmask                      ###
  ###    fixed updated cpplint.py errors                     ###
  ###                                                        ###
  ###  last updated July 17, 2022                            ###
  ###                                                        ###
  ###  updated January 21, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/card_test.h"

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cctype>       // std::tolower

#include <algorithm>
#include <vector>
#include <string>

#include "sources/card.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::TCardTest);

namespace handtest {

// #############################################################
// #############################################################
TCardTest::TCardTest() {
}

// #############################################################
// #############################################################
void TCardTest::setUp() {
}

// #############################################################
// #############################################################
void TCardTest::tearDown() {
}

// #############################################################
// #############################################################
void TCardTest::TestConstructor() {
  std::string error_message("TCardTest::TestConstructor:");
  cardlib::CCard tmp1_ah("ah"), tmp2_ah("ah");
  cardlib::CCard tmp_as("as"), tmp_kh("kh"), tmp_kd("kd");

  CPPUNIT_ASSERT_EQUAL_MESSAGE(
      error_message, tmp1_ah.CardToString(),
      tmp2_ah.CardToString());

  CPPUNIT_ASSERT_MESSAGE(
      error_message,
      (tmp1_ah.CardToString() != tmp_as.CardToString()));

  CPPUNIT_ASSERT_MESSAGE(
      error_message, !(tmp1_ah == tmp_kh));

  CPPUNIT_ASSERT_MESSAGE(
      error_message, !(tmp1_ah == tmp_kd));

  // test copy constructor
  cardlib::CCard tmp3_ah(tmp1_ah);
  CPPUNIT_ASSERT_EQUAL_MESSAGE(
      error_message,
      tmp1_ah.CardToString(),
      tmp3_ah.CardToString());

  cardlib::CCard tmp3_kd(tmp_kd);
  CPPUNIT_ASSERT_EQUAL_MESSAGE(
      error_message,
      tmp_kd.CardToString(),
      tmp3_kd.CardToString());

  return;
}

// #############################################################
// #############################################################
void TCardTest::TestSet1() {
  std::string error_message("TCardTest::TestSet1:");
  cardlib::CCard tmp1_ah("ah"), tmp2_ah("ah");
  cardlib::CCard tmp_as("as"), tmp_kh("kh");

  CPPUNIT_ASSERT_MESSAGE(
      error_message, (tmp1_ah == tmp2_ah));

  tmp2_ah.Set("as");
  CPPUNIT_ASSERT_MESSAGE(
      error_message, (tmp1_ah != tmp2_ah));

  CPPUNIT_ASSERT_MESSAGE(
      error_message, (tmp_as == tmp2_ah));

  tmp2_ah.Set(static_cast<std::string>("k"),
              static_cast<std::string>("h"));
  CPPUNIT_ASSERT_MESSAGE(
      error_message, (!(tmp_as == tmp2_ah)));

  CPPUNIT_ASSERT_MESSAGE(
      error_message, (tmp2_ah == tmp_kh));

  return;
}

// #############################################################
// #############################################################
void TCardTest::TestGet1() {
  struct atest_struct {
    std::string input_string;
    std::string shouldbe_long_name, shouldbe_short_name;
    std::string shouldbe_long_suit, shouldbe_short_suit;
  };
  std::vector<atest_struct> test_vector = {
    { "ah", "ace", "a", "hearts", "h" },
    { "kd", "king", "k", "diamonds", "d" },
    { "qc", "queen", "q", "clubs", "c" },
    { "js", "jack", "j", "spades", "s" },
    { "th", "ten", "t", "hearts", "h" },
    { "9d", "nine", "9", "diamonds", "d" },
    { "8c", "eight", "8", "clubs", "c" },
    { "7s", "seven", "7", "spades", "s" },
    { "6h", "six", "6", "hearts", "h" },
    { "5d", "five", "5", "diamonds", "d" },
    { "4c", "four", "4", "clubs", "c" },
    { "3s", "three", "3", "spades", "s" },
    { "2h", "two", "2", "hearts", "h" },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  cardlib::CCard tmp_card;
  std::string tmp_string;
  std::string shouldbe_long_name, result_long_name;
  std::string shouldbe_short_name, result_short_name;
  std::string shouldbe_long_suit, result_long_suit;
  std::string shouldbe_short_suit, result_short_suit;
  std::string sub_name =
      static_cast<std::string>("TCardTest1::")
      + static_cast<std::string>("TestGet1:");
  std::string err_start, error_message;

  test_label_index = 0;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    tmp_string = va_cit->input_string;
    shouldbe_long_name = va_cit->shouldbe_long_name;
    shouldbe_short_name = va_cit->shouldbe_short_name;
    shouldbe_long_suit = va_cit->shouldbe_long_suit;
    shouldbe_short_suit = va_cit->shouldbe_short_suit;

    tmp_card.Set(tmp_string);
    result_long_name = tmp_card.GetValueName();
    result_short_name = tmp_card.GetValueString();
    result_long_suit = tmp_card.GetSuitName();
    result_short_suit = tmp_card.GetSuitString();

    err_start = sub_name
        + static_cast<std::string>(" : (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), card  = ")
        + tmp_card.CardToString()
        + static_cast<std::string>(" : ");

    error_message = err_start
        + static_cast<std::string>("shouldbe long name = ")
        + shouldbe_long_name
        + static_cast<std::string>(", result long name = ")
        + result_long_name;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_long_name, result_long_name);

    error_message = err_start
        + static_cast<std::string>("shouldbe short name = ")
        + shouldbe_short_name
        + static_cast<std::string>(", result short name = ")
        + result_short_name;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_short_name, result_short_name);

    error_message = err_start
        + static_cast<std::string>("shouldbe long suit = ")
        + shouldbe_long_suit
        + static_cast<std::string>(", result long suit = ")
        + result_long_suit;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_long_suit, result_long_suit);

    error_message = err_start
        + static_cast<std::string>("shouldbe short suit = ")
        + shouldbe_short_suit
        + static_cast<std::string>(", result short suit = ")
        + result_short_suit;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_short_suit, result_short_suit);

    ++test_label_index;
  }

  return;
}
/*
Single card bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00010000 00000000 00101110 00101001    Ace of Hearts
//  0x10    00         2E      29
// 00001000 00000000 01001101 00100101    King of Diamonds
//  0x08    00        4D       25
// 00000100 00000000 10001100 00011111    Queen of Clubs
//  0x04    00       8C       1F
// 00000010 00000000 00011011 00011101    Jack of Spades
//  0x02    00       1B       1D
// 00000001 00000000 00101010 00010111    Ten of Hearts
//  0x01    00       2A       17
// 00000000 10000000 01001001 00010011    Nine of Diamonds
//  0x00    80       49       13
// 00000000 01000000 10001000 00010001    Eight of Clubs
//  0x00    40        88      11
// 00000000 00100000 00010111 00001101    Seven of Spades
//  0x00    20        17      0D
// 00000000 00010000 00100110 00001011    Six of Hearts
//  0x00    10        26      0B
// 00000000 00001000 01000101 00000111    Five of Diamonds
//  0x00    08        45      07
// 00000000 00000100 10000100 00000101    Four of Clubs
//  0x00    04       84       05
// 00000000 00000010 00010011 00000011    Three of Spades
//  0x00    02       13       03
// 00000000 00000001 00100010 00000010    Two of Hearts
//  0x00    01       22       02
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
*/

// #############################################################
// #############################################################
void TCardTest::TestGetMask1() {
  struct atest_struct {
    std::string input_string;
    int64_t shouldbe_mask;
    int64_t card_order, card_suit;
    int64_t card_value, card_prime;
  };
  //  3          2          1
  // 10987654 32109876 54321098 76543210
  // xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
  std::vector<atest_struct> test_vector = {
    { "ah", 0x10002E29, 0x1000, 0x02, 0x0E, 0x29, },
    { "kd", 0x08004D25, 0x0800, 0x04, 0x0D, 0x25, },
    { "qc", 0x04008C1F, 0x0400, 0x08, 0x0C, 0x1F, },
    { "js", 0x02001B1D, 0x0200, 0x01, 0x0B, 0x1D, },
    { "th", 0x01002A17, 0x0100, 0x02, 0x0A, 0x17, },
    { "9d", 0x00804913, 0x0080, 0x04, 0x09, 0x13, },
    { "8c", 0x00408811, 0x0040, 0x08, 0x08, 0x11, },
    { "7s", 0x0020170D, 0x0020, 0x01, 0x07, 0x0D, },
    { "6h", 0x0010260B, 0x0010, 0x02, 0x06, 0x0B, },
    { "5d", 0x00084507, 0x0008, 0x04, 0x05, 0x07, },
    { "4c", 0x00048405, 0x0004, 0x08, 0x04, 0x05, },
    { "3s", 0x00021303, 0x0002, 0x01, 0x03, 0x03, },
    { "2h", 0x00012202, 0x0001, 0x02, 0x02, 0x02, },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  cardlib::CCard tmp_card;
  int64_t shouldbe_mask, result_mask;
  int64_t shouldbe_order, result_order;
  int64_t shouldbe_suit, result_suit;
  int64_t shouldbe_value, result_value;
  int64_t shouldbe_prime, result_prime;
  const int64_t kBufSize { 100L };
  char buffer[kBufSize];
  std::string shouldbe_string, result_string;
  std::string tmp_string;
  std::string sub_name =
      static_cast<std::string>("TCardTest1::")
      + static_cast<std::string>("TestGetMask1:");
  std::string err_start, error_message;

  test_label_index = 0;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    tmp_string = va_cit->input_string;
    shouldbe_mask = va_cit->shouldbe_mask;
    shouldbe_order = va_cit->card_order;
    shouldbe_suit = va_cit->card_suit;
    shouldbe_value = va_cit->card_value;
    shouldbe_prime = va_cit->card_prime;

    tmp_card.Set(tmp_string);

    result_mask = tmp_card.GetCardMask();
    result_order = tmp_card.GetCardOrder();
    result_suit =
        static_cast<int64_t>(tmp_card.GetCardSuitMask());
    result_value =
        static_cast<int64_t>(tmp_card.GetCardValue());
    result_prime = tmp_card.GetCardPrimeValue();

    err_start = sub_name
        + static_cast<std::string>(" : (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), card  = ")
        + tmp_card.CardToString()
        + static_cast<std::string>(" : ");

    snprintf(buffer, kBufSize, "%#x",
             static_cast<int>(shouldbe_mask));
    shouldbe_string = buffer;
    snprintf(buffer, kBufSize, "%#x",
             static_cast<int>(result_mask));
    result_string = buffer;

    error_message = err_start
        + static_cast<std::string>("shouldbe mask = ")
        + shouldbe_string
        + static_cast<std::string>(", result mask = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_mask, result_mask);

    // #########################################################
    snprintf(buffer, kBufSize, "%#x",
             static_cast<int>(shouldbe_order));
    shouldbe_string = buffer;
    snprintf(buffer, kBufSize, "%#x",
             static_cast<int>(result_order));
    result_string = buffer;

    error_message = err_start
        + static_cast<std::string>("shouldbe order = ")
        + shouldbe_string
        + static_cast<std::string>(", result order = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_order, result_order);

    // #########################################################
    snprintf(buffer, kBufSize, "%#x",
             static_cast<int>(shouldbe_suit));
    shouldbe_string = buffer;
    snprintf(buffer, kBufSize, "%#x",
             static_cast<int>(result_suit));
    result_string = buffer;

    error_message = err_start
        + static_cast<std::string>("shouldbe suit = ")
        + shouldbe_string
        + static_cast<std::string>(", result suit = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_suit, result_suit);

    // #########################################################
    snprintf(buffer, kBufSize, "%#x",
             static_cast<int>(shouldbe_value));
    shouldbe_string = buffer;
    snprintf(buffer, kBufSize, "%#x",
             static_cast<int>(result_value));
    result_string = buffer;

    error_message = err_start
        + static_cast<std::string>("shouldbe value = ")
        + shouldbe_string
        + static_cast<std::string>(", result value = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_value, result_value);

    // #########################################################
    snprintf(buffer, kBufSize, "%ld", shouldbe_prime);
    shouldbe_string = buffer;
    snprintf(buffer, kBufSize, "%ld", result_prime);
    result_string = buffer;

    error_message = err_start
        + static_cast<std::string>("shouldbe prime = ")
        + shouldbe_string
        + static_cast<std::string>(", result prime = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_prime, result_prime);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TCardTest::TestToLower1() {
  struct atest_struct {
    std::string input_string;
    std::string shouldbe_string;
  };
  std::vector<atest_struct> test_vector = {
    { "UPPERCASE", "uppercase" },
    { "UpperCase", "uppercase" },
    { "UPperCasE", "uppercase" },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::string sub_name =
      static_cast<std::string>("TCardTest1::")
      + static_cast<std::string>("TestToLower1:");
  std::string error_message;
  cardlib::CCard ctmp1;
  std::string result_string, shouldbe_string;

  test_label_index = 0;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    result_string = va_cit->input_string;
    shouldbe_string = va_cit->shouldbe_string;

    error_message = sub_name
        + static_cast<std::string>(" : (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), input string  = ")
        + result_string
        + static_cast<std::string>(" , shouldbe string = ")
        + shouldbe_string;

    ctmp1.ToLower(&result_string);
    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TCardTest::TestEqual1() {
  struct atest_struct {
    cardlib::CCard card1, card2;
    bool shouldbe1, shouldbe2, shouldbe3;
  };
  std::vector<atest_struct> test_vector = {
    { cardlib::CCard("ah"), cardlib::CCard("ah"),
      true, true, false },
    { cardlib::CCard("ah"), cardlib::CCard("as"),
      false, true, false },
    { cardlib::CCard("ah"), cardlib::CCard("kh"),
      false, false, true },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  cardlib::CCard cc1, cc2;
  bool bresult, bshouldbe;
  std::string sub_name("TCardTest::TestEqual1:");
  std::string error_message, err_start;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    cc1 = va_cit->card1;
    cc2 = va_cit->card2;

    // test is_identical
    bshouldbe = va_cit->shouldbe1;
    bresult = cc1.IsIdentical(&cc2);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card 1 = ")
        + cc1.CardToString()
        + static_cast<std::string>(", card 2 = ")
        + cc2.CardToString();

    error_message =
        err_start
        + static_cast<std::string>(", IsIdentical shouldbe = ")
        + static_cast<std::string>(
            (bshouldbe ? "true" : "false"))
        + static_cast<std::string>(", result = ")
        + static_cast<std::string>(
            (bresult ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, bshouldbe, bresult);

    // test equal
    bshouldbe = va_cit->shouldbe2;
    bresult = cc1.ValueEqual(&cc2);
    error_message =
        err_start
        + static_cast<std::string>(", ValueEqual shouldbe = ")
        + static_cast<std::string>(
            (bshouldbe ? "true" : "false"))
        + static_cast<std::string>(", result = ")
        + static_cast<std::string>(
            (bresult ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, bshouldbe, bresult);

    // test Greater
    bshouldbe = va_cit->shouldbe3;
    bresult = cc1.Greater(cc2);
    error_message =
        err_start
        + static_cast<std::string>(", Greater shouldbe = ")
        + static_cast<std::string>(
            (bshouldbe ? "true" : "false"))
        + static_cast<std::string>(", result = ")
        + static_cast<std::string>(
            (bresult ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, bshouldbe, bresult);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TCardTest::TestOperators1() {
  struct atest_struct {
    cardlib::CCard card1, card2;
    bool shouldbe, btest;
    std::string err_1;
  };
  cardlib::CCard tmp_ah("ah"), tmp_as("as");
  cardlib::CCard tmp_kh("kh"), tmp_ks("ks");
  cardlib::CCard tmp_qc("qc");
  std::vector<atest_struct> test_vector = {
    { tmp_ah, tmp_ah, true, (tmp_ah == tmp_ah),
      "test ==" },
    { tmp_ah, tmp_as, false, (tmp_ah == tmp_as),
      "test ==" },
    { tmp_ah, tmp_kh, true, (tmp_ah != tmp_kh),
      "test !=" },
    { tmp_ah, tmp_as, true, (tmp_ah != tmp_as),
      "test !=" },
    { tmp_ah, tmp_ah, false, (tmp_ah != tmp_ah),
      "test !=" },
    { tmp_ah, tmp_as, true, (tmp_ah >= tmp_as),
      "test >=" },
    { tmp_ah, tmp_kh, true, (tmp_ah >= tmp_kh),
      "test >=" },
    { tmp_kh, tmp_qc, true, (tmp_kh >= tmp_qc),
      "test >=" },
    { tmp_as, tmp_ah, true, (tmp_as >= tmp_ah),
      "test >=" },
    { tmp_qc, tmp_kh, false, (tmp_qc >= tmp_kh),
      "test >=" },
    { tmp_ah, tmp_as, false, (tmp_ah > tmp_as),
      "test >" },
    { tmp_ah, tmp_kh, true, (tmp_ah > tmp_kh),
      "test >" },
    { tmp_kh, tmp_qc, true, (tmp_kh > tmp_qc),
      "test >" },
    { tmp_kh, tmp_as, false, (tmp_kh > tmp_as),
      "test >" },
    { tmp_qc, tmp_kh, false, (tmp_qc > tmp_kh),
      "test >" },
    { tmp_ah, tmp_as, false, (tmp_ah < tmp_as),
      "test <" },
    { tmp_ah, tmp_kh, false, (tmp_ah < tmp_kh),
      "test <" },
    { tmp_kh, tmp_qc, false, (tmp_kh < tmp_qc),
      "test <" },
    { tmp_kh, tmp_as, true, (tmp_kh < tmp_as),
      "test <" },
    { tmp_qc, tmp_kh, true, (tmp_qc < tmp_kh),
      "test <" },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  cardlib::CCard cc1, cc2;
  bool bshouldbe, bresult;
  std::string sdesc;
  std::string sub_name("TCardTest::TestOperators1:");
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    cc1 = va_cit->card1;
    cc2 = va_cit->card2;

    bshouldbe = va_cit->shouldbe;
    bresult = va_cit->btest;
    sdesc = va_cit->err_1;

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") operator ")
        + sdesc
        + static_cast<std::string>(", card 1 = ")
        + cc1.CardToString()
        + static_cast<std::string>(", card 2 = ")
        + cc2.CardToString()
        + static_cast<std::string>(", shouldbe = ")
        + static_cast<std::string>(
            (bshouldbe ? "true" : "false"))
        + static_cast<std::string>(", result = ")
        + static_cast<std::string>(
            (bresult ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, bshouldbe, bresult);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TCardTest::TestStringToCard1() {
  struct atest_struct {
    cardlib::CCard card;
    std::string svalue, ssuit;
    std::string svaluename, ssuitname;
  };
  cardlib::CCard tmp_ah("ah"), tmp_as("as");
  cardlib::CCard tmp_kh("kh"), tmp_kd("kd");
  cardlib::CCard tmp_qc("qc"), tmp_js("js");
  std::vector<atest_struct> test_vector = {
    { tmp_ah, "a", "h", "ace", "hearts" },
    { tmp_kd, "k", "d", "king", "diamonds" },
    { tmp_qc, "q", "c", "queen", "clubs" },
    { tmp_js, "j", "s", "jack", "spades" },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  cardlib::CCard acard;
  std::string shouldbe_value, shouldbe_suit;
  std::string shouldbe_value_name, shouldbe_suit_name;
  std::string result_string;
  std::string sub_name("TCardTest::TestStringToCard1:");
  std::string error_message, err_start;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    acard = va_cit->card;
    shouldbe_value = va_cit->svalue;
    shouldbe_suit = va_cit->ssuit;
    shouldbe_value_name = va_cit->svaluename;
    shouldbe_suit_name = va_cit->ssuitname;

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") card = ")
        + acard.CardToString();

    result_string = acard.GetValueString();
    error_message =
        err_start
        + static_cast<std::string>(", shouldbe value = ")
        + shouldbe_value
        + static_cast<std::string>(", result value = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_value, result_string);

    result_string = acard.GetSuitString();
    error_message =
        err_start
        + static_cast<std::string>(", shouldbe suit = ")
        + shouldbe_suit
        + static_cast<std::string>(", result suit = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_suit, result_string);

    result_string = acard.GetValueName();
    error_message =
        err_start
        + static_cast<std::string>(", shouldbe value name = ")
        + shouldbe_value_name
        + static_cast<std::string>(", result value name = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_value_name, result_string);

    result_string = acard.GetSuitName();
    error_message =
        err_start
        + static_cast<std::string>(", shouldbe suit name = ")
        + shouldbe_suit_name
        + static_cast<std::string>(", result value = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_suit_name, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TCardTest::TestCardToString1() {
  struct atest_struct {
    std::string ss1;
  };
  std::vector<atest_struct> test_vector = {
    { "ah" }, { "kd" }, { "qc" }, { "js" },
    { "th" }, { "9d" }, { "8c" }, { "7s" },
    { "6h" }, { "5d" }, { "4c" }, { "3s" },
    { "2h" }, };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  cardlib::CCard cc1;
  std::string sresult, sshouldbe;
  std::string sub_name("TCardTest::TestCardToString1:");
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    sshouldbe = va_cit->ss1;

    cc1.Set(sshouldbe);
    sresult = cc1.CardToString();

    cc1.ToLower(&sresult);
    cc1.ToLower(&sshouldbe);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") shouldbe ")
        + sshouldbe
        + static_cast<std::string>(", result = ")
        + sresult;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, sshouldbe, sresult);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

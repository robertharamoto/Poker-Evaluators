#ifndef TESTS_FOUR_FUNCTIONS_TEST_H_
#define TESTS_FOUR_FUNCTIONS_TEST_H_

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TFourFunctionsTest1 definition                        ###
  ###    namespace HandTest                                  ###
  ###                                                        ###
  ###  last updated April 15, 2024                           ###
  ###    new card mask definition                            ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 18, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cppunit/extensions/HelperMacros.h>

#include <algorithm>
#include <string>
#include <vector>

#include "sources/card.h"

namespace handtest {

class TFourFunctionsTest1 : public CppUnit::TestFixture  {
  CPPUNIT_TEST_SUITE(TFourFunctionsTest1);

  CPPUNIT_TEST(TestIsFour1);

  CPPUNIT_TEST(TestQuadsGreaterQuads1);
  CPPUNIT_TEST(TestQuadsLessThanQuads1);
  CPPUNIT_TEST(TestQuadsEqualQuads1);

  CPPUNIT_TEST_SUITE_END();

 public:
  TFourFunctionsTest1();
  ~TFourFunctionsTest1();

  void setUp();
  void tearDown();

  void TestIsFour1();

  void TestQuadsGreaterQuads1();
  void TestQuadsLessThanQuads1();
  void TestQuadsEqualQuads1();
};  // class TFourFunctionsTest1

}  // namespace handtest

#endif  // TESTS_FOUR_FUNCTIONS_TEST_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

/*

// Copyright 2024 robert haramoto

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  THighCardFunctionsTest1 implementation                ###
  ###    namespace HandTest                                  ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###    new card mask definition                            ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 19, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/high_card_functions_test.h"

#include <cppunit/SourceLine.h>

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cctype>       // std::tolower

#include <algorithm>
#include <string>
#include <vector>
#include <map>

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/high_card_functions.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::THighCardFunctionsTest1);

namespace handtest {

// #############################################################
// #############################################################
THighCardFunctionsTest1::THighCardFunctionsTest1() {
}

// #############################################################
// #############################################################
THighCardFunctionsTest1::~THighCardFunctionsTest1() {
}

// #############################################################
// #############################################################
void THighCardFunctionsTest1::setUp() {
}

// #############################################################
// #############################################################
void THighCardFunctionsTest1::tearDown() {
}

// #############################################################
// #############################################################
void THighCardFunctionsTest1::TestForm5CardHand1() {
  cardlib::CCard card_obj;
  cardlib::CHighCardFunctions highf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    int64_t hand_order;
    int64_t prime7_signature;
    int64_t prime5_signature;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("tc"), cardlib::CCard("5c"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("5c").GetCardOrder()
        | cardlib::CCard("as").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
        | cardlib::CCard("7d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpFive
      },
      cardlib::CHand::khand_value::kHighCard,
    },
    { { cardlib::CCard("tc"), cardlib::CCard("qc"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("7d"), },
      { cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("qc").GetCardOrder()
        | cardlib::CCard("as").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
        | cardlib::CCard("7d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kHighCard,
    },
    { { cardlib::CCard("8h"), cardlib::CCard("tc"),
        cardlib::CCard("qc"), cardlib::CCard("as"),
        cardlib::CCard("jd"),
      },
      { cardlib::CCard("8h").GetCardOrder()
        | cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("qc").GetCardOrder()
        | cardlib::CCard("as").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpEight
      },
      cardlib::CHand::khand_value::kHighCard,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_vector;
  int64_t shouldbe_order, result_order;
  int64_t shouldbe7_signature, result7_signature;
  int64_t shouldbe5_signature, result5_signature;
  cardlib::CHand::khand_value
      shouldbe_handvalue, result_handvalue;
  std::string sub_name = {
    static_cast<std::string>("THighCardFunctionsTest1::")
    + static_cast<std::string>("TestForm5CardHand1:") };
  std::string error_message, err_start, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->input_vector;
    shouldbe_order = va_cit->hand_order;
    shouldbe7_signature = va_cit->prime7_signature;
    shouldbe5_signature = va_cit->prime5_signature;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_vector);

    highf_obj.Form5CardHand(
        &input_vector, &result_order,
        &result7_signature, &result5_signature,
        &result_handvalue);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("hand order shouldbe = ")
        + std::to_string(shouldbe_order)
        + static_cast<std::string>(", result order = ")
        + std::to_string(result_order);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_order, result_order);

    error_message =
        err_start
        + static_cast<std::string>("7-card signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result signature = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-card signature shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result signature = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("shouldbe_handvalue = ")
        + std::to_string(shouldbe_handvalue)
        + static_cast<std::string>(", result_handvalue = ")
        + std::to_string(result_handvalue);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THighCardFunctionsTest1::TestForm7CardHand1() {
  cardlib::CCard card_obj;
  cardlib::CHighCardFunctions highf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    int64_t hand_order;
    int64_t prime7_signature;
    int64_t prime5_signature;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("tc"), cardlib::CCard("2h"),
        cardlib::CCard("5c"), cardlib::CCard("6d"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
        | cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("7d").GetCardOrder()
        | cardlib::CCard("6d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
      },
      cardlib::CHand::khand_value::kHighCard,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("qc"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as").GetCardOrder()
        | cardlib::CCard("qc").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
        | cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("7d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kHighCard,
    },
    { { cardlib::CCard("as"), cardlib::CCard("qc"),
        cardlib::CCard("4h"), cardlib::CCard("2s"),
        cardlib::CCard("jd"), cardlib::CCard("tc"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as").GetCardOrder()
        | cardlib::CCard("qc").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
        | cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("7d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kHighCard,
    },
    { { cardlib::CCard("8h"), cardlib::CCard("tc"),
        cardlib::CCard("qc"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as").GetCardOrder()
        | cardlib::CCard("qc").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
        | cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("8h").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpEight
      },
      cardlib::CHand::khand_value::kHighCard,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_vector;
  int64_t shouldbe_order, result_order;
  int64_t shouldbe7_signature, result7_signature;
  int64_t shouldbe5_signature, result5_signature;
  cardlib::CHand::khand_value
      shouldbe_handvalue, result_handvalue;
  std::string sub_name = {
    static_cast<std::string>("THighCardFunctionsTest1::")
    + static_cast<std::string>("TestForm7CardHand1:") };
  std::string error_message, err_start, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->input_vector;
    shouldbe_order = va_cit->hand_order;
    shouldbe7_signature = va_cit->prime7_signature;
    shouldbe5_signature = va_cit->prime5_signature;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_vector);

    highf_obj.Form7CardHand(
        &input_vector, &result_order,
        &result7_signature, &result5_signature,
        &result_handvalue);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("hand order shouldbe = ")
        + std::to_string(shouldbe_order)
        + static_cast<std::string>(", result order = ")
        + std::to_string(result_order);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_order, result_order);

    error_message =
        err_start
        + static_cast<std::string>("7-card signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result signature = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-card signature shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result signature = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("shouldbe_handvalue = ")
        + std::to_string(shouldbe_handvalue)
        + static_cast<std::string>(", result_handvalue = ")
        + std::to_string(result_handvalue);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THighCardFunctionsTest1::TestIsHighCard1() {
  cardlib::CCard card_obj;
  cardlib::CHighCardFunctions highf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    int64_t hand_order;
    int64_t prime7_signature;
    int64_t prime5_signature;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("tc"), cardlib::CCard("2h"),
        cardlib::CCard("5c"), cardlib::CCard("6d"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
        | cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("7d").GetCardOrder()
        | cardlib::CCard("6d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
      },
      cardlib::CHand::khand_value::kHighCard,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("qc"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as").GetCardOrder()
        | cardlib::CCard("qc").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
        | cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("7d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kHighCard,
    },
    { { cardlib::CCard("as"), cardlib::CCard("qc"),
        cardlib::CCard("4h"), cardlib::CCard("2s"),
        cardlib::CCard("jd"), cardlib::CCard("tc"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as").GetCardOrder()
        | cardlib::CCard("qc").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
        | cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("7d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kHighCard,
    },
    { { cardlib::CCard("8h"), cardlib::CCard("tc"),
        cardlib::CCard("qc"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as").GetCardOrder()
        | cardlib::CCard("qc").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
        | cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("8h").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpEight
      },
      cardlib::CHand::khand_value::kHighCard,
    },
    { { cardlib::CCard("8h"), cardlib::CCard("tc"),
        cardlib::CCard("qc"), cardlib::CCard("as"),
        cardlib::CCard("jd"),
      },
      { cardlib::CCard("as").GetCardOrder()
        | cardlib::CCard("qc").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
        | cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("8h").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpEight
      },
      cardlib::CHand::khand_value::kHighCard,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_vector;
  int64_t shouldbe_order, result_order;
  int64_t shouldbe7_signature, result7_signature;
  int64_t shouldbe5_signature, result5_signature;
  cardlib::CHand::khand_value
      shouldbe_handvalue, result_handvalue;
  std::string sub_name = {
    static_cast<std::string>("THighCardFunctionsTest1::")
    + static_cast<std::string>("TestIsHighCard1:") };
  std::string error_message, err_start, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->input_vector;
    shouldbe_order = va_cit->hand_order;
    shouldbe7_signature = va_cit->prime7_signature;
    shouldbe5_signature = va_cit->prime5_signature;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_vector);

    highf_obj.IsHighCard(
        &input_vector, &result_order,
        &result7_signature, &result5_signature,
        &result_handvalue);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("hand order shouldbe = ")
        + std::to_string(shouldbe_order)
        + static_cast<std::string>(", result order = ")
        + std::to_string(result_order);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_order, result_order);

    error_message =
        err_start
        + static_cast<std::string>("7-card signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result signature = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-card signature shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result signature = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("shouldbe_handvalue = ")
        + std::to_string(shouldbe_handvalue)
        + static_cast<std::string>(", result_handvalue = ")
        + std::to_string(result_handvalue);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THighCardFunctionsTest1::TestHighCardsGreaterHighCards1() {
  cardlib::CCard card_obj;
  cardlib::CHighCardFunctions hcf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> hc1_vector, hc2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("ks"), cardlib::CCard("jd"),
        cardlib::CCard("tc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true,
    },
    // 2nd high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("tc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true,
    },
    // 3rd high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("tc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true,
    },
    // 4th high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("jc"), cardlib::CCard("6d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true,
    },
    // 5th high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("jc"), cardlib::CCard("7d"),
        cardlib::CCard("4c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true,
    },
    // high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("ks"), cardlib::CCard("jd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("tc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false,
    },
    // 2nd high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("tc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false,
    },
    // 3rd high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("jc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false,
    },
    // 4th high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("6d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("jc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false,
    },
    // 5th high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("jc"), cardlib::CCard("7d"),
        cardlib::CCard("6c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> hc1_vector, hc2_vector;
  cardlib::CHand hand1, hand2;
  bool bshouldbe, bresult;
  std::string sub_name = {
    static_cast<std::string>("THighCardFunctionsTest1::")
    + static_cast<std::string>("TestHighCardsGreaterHighCards1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    hc1_vector = va_cit->hc1_vector;
    hc2_vector = va_cit->hc2_vector;
    bshouldbe = va_cit->bshouldbe;

    hand1.Calculate(&hc1_vector);

    hand2.Calculate(&hc2_vector);

    bresult =
        hcf_obj.HighCardsGreaterHighCards(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand1 vec = ")
        + card_obj.CardVectorToString(&hc1_vector)
        + static_cast<std::string>(" : hand2 vec = ")
        + card_obj.CardVectorToString(&hc2_vector)
        + static_cast<std::string>(" : shouldbe = ")
        + (bshouldbe ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (bresult ? "true" : "false");


    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, bshouldbe, bresult);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THighCardFunctionsTest1::TestHighCardsLessThanHighCards1() {
  cardlib::CCard card_obj;
  cardlib::CHighCardFunctions hcf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> hc1_vector, hc2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("ks"), cardlib::CCard("jd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("tc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true,
    },
    // 2nd high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("tc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true,
    },
    // 3rd high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("jc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true,
    },
    // 4th high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("6d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("jc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true,
    },
    // 5th high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("jc"), cardlib::CCard("7d"),
        cardlib::CCard("4c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false,
    },
    // 6th card differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("tc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("4h"),
        cardlib::CCard("2h"),
      },
      false,
    },
    // 7th high card differs
    { { cardlib::CCard("3h"), cardlib::CCard("tc"),
        cardlib::CCard("7c"), cardlib::CCard("5h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("9d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("tc"), cardlib::CCard("9d"),
        cardlib::CCard("7c"), cardlib::CCard("5h"),
        cardlib::CCard("2h"),
      },
      false,
    },
    // 1st high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("ks"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("jc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("th"),
        cardlib::CCard("2h"),
      },
      true,
    },
    // 2nd high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("6d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("kd"),
        cardlib::CCard("jc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true,
    },
    // 5th high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("jc"), cardlib::CCard("7d"),
        cardlib::CCard("6c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true, },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> hc1_vector, hc2_vector;
  cardlib::CHand hand1, hand2;
  bool bshouldbe, bresult;
  std::string sub_name = {
    static_cast<std::string>("THighCardFunctionsTest1::")
    + static_cast<std::string>("TestHighCardsLessThanHighCards1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    hc1_vector = va_cit->hc1_vector;
    hc2_vector = va_cit->hc2_vector;
    bshouldbe = va_cit->bshouldbe;

    hand1.Calculate(&hc1_vector);

    hand2.Calculate(&hc2_vector);

    bresult =
        hcf_obj.HighCardsLessThanHighCards(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand1 vec = ")
        + card_obj.CardVectorToString(&hc1_vector)
        + static_cast<std::string>(" : hand2 vec = ")
        + card_obj.CardVectorToString(&hc2_vector)
        + static_cast<std::string>(" : shouldbe = ")
        + (bshouldbe ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (bresult ? "true" : "false");


    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, bshouldbe, bresult);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THighCardFunctionsTest1::TestHighCardsEqualHighCards1() {
  cardlib::CCard card_obj;
  cardlib::CHighCardFunctions hcf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> hc1_vector, hc2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // 1st high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("ks"), cardlib::CCard("jd"),
        cardlib::CCard("tc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false,
    },
    // 2nd high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("tc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false,
    },
    // 3rd high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("tc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false,
    },
    // 4th high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("jc"), cardlib::CCard("6d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false,
    },
    // 5th high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("jc"), cardlib::CCard("7d"),
        cardlib::CCard("4c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false,
    },
    // 6th card differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("7h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("qd"),
      },
      { cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("tc"), cardlib::CCard("qd"),
        cardlib::CCard("7c"), cardlib::CCard("4h"),
        cardlib::CCard("2h"),
      },
      true,
    },
    // 7th high card differs
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("4h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("tc"), cardlib::CCard("7d"),
        cardlib::CCard("5c"), cardlib::CCard("jh"),
        cardlib::CCard("3h"),
      },
      true,
    },
    // 5th high card differs
    { { cardlib::CCard("2h"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("7d"),
      },
      { cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("jc"), cardlib::CCard("7d"),
        cardlib::CCard("6c"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> hc1_vector, hc2_vector;
  cardlib::CHand hand1, hand2;
  bool bshouldbe, bresult;
  std::string sub_name = {
    static_cast<std::string>("THighCardFunctionsTest1::")
    + static_cast<std::string>("TestHighCardsEqualHighCards1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    hc1_vector = va_cit->hc1_vector;
    hc2_vector = va_cit->hc2_vector;
    bshouldbe = va_cit->bshouldbe;

    hand1.Calculate(&hc1_vector);

    hand2.Calculate(&hc2_vector);

    bresult =
        hcf_obj.HighCardsEqualHighCards(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand1 vec = ")
        + card_obj.CardVectorToString(&hc1_vector)
        + static_cast<std::string>(" : hand2 vec = ")
        + card_obj.CardVectorToString(&hc2_vector)
        + static_cast<std::string>(" : shouldbe = ")
        + (bshouldbe ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (bresult ? "true" : "false");


    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, bshouldbe, bresult);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

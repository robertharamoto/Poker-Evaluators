#ifndef TESTS_FAST_CALC_TEST_H_
#define TESTS_FAST_CALC_TEST_H_

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TFastCalcTest definition                              ###
  ###  namespace HandTest                                    ###
  ###                                                        ###
  ###  last updated April 20, 2024                           ###
  ###    changed definitions of bitmask                      ###
  ###                                                        ###
  ###  created April 20, 2024                                ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cppunit/extensions/HelperMacros.h>

namespace handtest {

class TFastCalcTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(TFastCalcTest);

  CPPUNIT_TEST(TestMakeEquivalentRankings1);
  CPPUNIT_TEST(TestLoadMaskMap5);
  CPPUNIT_TEST(TestLoadMaskMap7);
  CPPUNIT_TEST(TestSubtotalValueRankCountstructs);

  CPPUNIT_TEST_SUITE_END();

 public:
  TFastCalcTest();
  ~TFastCalcTest();

  void setUp();
  void tearDown();

  void TestMakeEquivalentRankings1();

  void TestLoadMaskMap5();
  void TestLoadMaskMap7();

  void TestSubtotalValueRankCountstructs();
};  // class TFastCalcTest

}  // namespace handtest

#endif  // TESTS_FAST_CALC_TEST_H_

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

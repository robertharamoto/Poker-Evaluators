#ifndef TESTS_ASSERT_FUNCTIONS_H_
#define TESTS_ASSERT_FUNCTIONS_H_

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CAssertFunctionsTest1 definition                      ###
  ###    namespace HandTest                                  ###
  ###    a function object                                   ###
  ###                                                        ###
  ###  last updated April 2, 2024                            ###
  ###    new card mask definition                            ###
  ###                                                        ###
  ###  created March 21, 2024                                ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cppunit/extensions/HelperMacros.h>

#include <algorithm>
#include <string>
#include <vector>

#include "sources/card.h"
#include "sources/hand.h"

namespace handtest {

class CAssertFunctions {
 public:
  CAssertFunctions();
  ~CAssertFunctions();

  // do not allow copying nor moving
  CAssertFunctions(const CAssertFunctions&) = delete;
  CAssertFunctions& operator=(const CAssertFunctions&) = delete;
  CAssertFunctions(CAssertFunctions&&) = delete;
  CAssertFunctions& operator=(CAssertFunctions&&) = delete;

  void AssertPrimesHelper(
      const std::string *err_start,
      const int64_t shouldbe_prime_product,
      const int64_t result_prime_product,
      const int64_t shouldbe_straight_mask,
      const int64_t result_straight_mask) const;

  void AssertCardVectorsEqual(
      const std::string *err_1,
      const std::vector<cardlib::CCard> *shouldbe_vec,
      const std::vector<cardlib::CCard> *result_vec) const;

  void AssertOrderedCardVectorsEqual(
      const std::string *err_1,
      const std::vector<cardlib::CCard> *shouldbe_vec,
      const std::vector<cardlib::CCard> *result_vec) const;

  void AssertOrderedInt64VectorsEqual(
      const std::string *err_1,
      const std::vector<int64_t> *shouldbe_vec,
      const std::vector<int64_t> *result_vec) const;

  void Assert1Greater2(
      const std::string *err_1,
      const cardlib::CHand *hand1_obj,
      const cardlib::CHand *hand2_obj,
      const bool shouldbe_bool1,
      const bool shouldbe_bool2,
      const bool result_bool1,
      const bool result_bool2) const;
};  // class CAssertFunctions

}  // namespace handtest

#endif  // TESTS_ASSERT_FUNCTIONS_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

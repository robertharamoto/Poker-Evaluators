/*

// Copyright 2024 robert haramoto

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TTwoPairFunctionsTest1 implementation                 ###
  ###    namespace HandTest                                  ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###    new card mask definition                            ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 19, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/two_pair_functions_test.h"

#include <cppunit/SourceLine.h>

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cctype>       // std::tolower

#include <algorithm>
#include <string>
#include <vector>
#include <map>

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/two_pair_functions.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::TTwoPairFunctionsTest1);

namespace handtest {

// #############################################################
// #############################################################
TTwoPairFunctionsTest1::TTwoPairFunctionsTest1() {
}

// #############################################################
// #############################################################
TTwoPairFunctionsTest1::~TTwoPairFunctionsTest1() {
}

// #############################################################
// #############################################################
void TTwoPairFunctionsTest1::setUp() {
}

// #############################################################
// #############################################################
void TTwoPairFunctionsTest1::tearDown() {
}

// #############################################################
// #############################################################
void TTwoPairFunctionsTest1::TestFormTwoPair1() {
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CTwoPairFunctions twof_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    cardlib::CCard::kcard_value toppair;
    cardlib::CCard::kcard_value secondpair;
    cardlib::CCard::kcard_value kicker1;
    int64_t signature7;
    int64_t signature5;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("3s"),
      },
      cardlib::CCard::kcard_value::kThree,
      cardlib::CCard::kcard_value::kTwo,
      cardlib::CCard::kcard_value::kNine,
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpThree },
      { cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpNine
      },
      cardlib::CHand::khand_value::kTwoPair,
    },
    { { cardlib::CCard("th"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("td"),
        cardlib::CCard("3d"), cardlib::CCard("9h"),
        cardlib::CCard("7s"),
      },
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kThree,
      cardlib::CCard::kcard_value::kNine,
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpNine
      },
      cardlib::CHand::khand_value::kTwoPair,
    },
    { { cardlib::CCard("th"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("td"),
        cardlib::CCard("jd"), cardlib::CCard("jh"),
        cardlib::CCard("7s"),
      },
      cardlib::CCard::kcard_value::kJack,
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kSeven,
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kTwoPair,
    },
    { { cardlib::CCard("th"), cardlib::CCard("3h"),
        cardlib::CCard("ac"), cardlib::CCard("td"),
        cardlib::CCard("jd"), cardlib::CCard("jh"),
        cardlib::CCard("7s"),
      },
      cardlib::CCard::kcard_value::kJack,
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kAce,
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpAce
      },
      cardlib::CHand::khand_value::kTwoPair,
    },
    { { cardlib::CCard("th"), cardlib::CCard("3h"),
        cardlib::CCard("ac"), cardlib::CCard("td"),
        cardlib::CCard("jd"), cardlib::CCard("jh"),
        cardlib::CCard("3s"),
      },
      cardlib::CCard::kcard_value::kJack,
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kAce,
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpThree
      },
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpAce
      },
      cardlib::CHand::khand_value::kTwoPair,
    },
    { { cardlib::CCard("th"), cardlib::CCard("7h"),
        cardlib::CCard("4c"), cardlib::CCard("td"),
        cardlib::CCard("jd"), cardlib::CCard("jh"),
        cardlib::CCard("7s"),
      },
      cardlib::CCard::kcard_value::kJack,
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kSeven,
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kTwoPair,
    },
    { { cardlib::CCard("kh"), cardlib::CCard("3h"),
        cardlib::CCard("ac"), cardlib::CCard("td"),
        cardlib::CCard("2d"), cardlib::CCard("jh"),
        cardlib::CCard("js"),
      },
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      -1L, -1L,
      cardlib::CHand::khand_value::kHandValueNull,
    },
    { { cardlib::CCard("kh"), cardlib::CCard("3h"),
        cardlib::CCard("ac"), cardlib::CCard("td"),
        cardlib::CCard("2d"), cardlib::CCard("jh"),
        cardlib::CCard("7s"),
      },
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      -1L, -1L,
      cardlib::CHand::khand_value::kHandValueNull,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  std::map<cardlib::CCard::kcard_value, int64_t>
      value_count_map;
  cardlib::CCard::kcard_value
      shouldbe_toppair, shouldbe_secondpair;
  cardlib::CCard::kcard_value
      result_toppair, result_secondpair;
  cardlib::CCard::kcard_value
      shouldbe_kicker, result_kicker;
  int64_t shouldbe7_signature, shouldbe5_signature;
  int64_t result7_signature, result5_signature;
  cardlib::CHand::khand_value
      shouldbe_handvalue, result_handvalue;
  std::string sub_name = {
    static_cast<std::string>("TTwoPairFunctionsTest1::")
    + static_cast<std::string>("TestFormTwoPair1:") };
  std::string error_message, err_start, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    shouldbe_toppair = va_cit->toppair;
    shouldbe_secondpair = va_cit->secondpair;
    shouldbe_kicker = va_cit->kicker1;
    shouldbe7_signature = va_cit->signature7;
    shouldbe5_signature = va_cit->signature5;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    hand_obj.PopulateNthOfAKindMap(
        &input_card_vector, &value_count_map);

    twof_obj.FormTwoPair(
        &input_card_vector, &value_count_map,
        &result_toppair, &result_secondpair, &result_kicker,
        &result7_signature, &result5_signature,
        &result_handvalue);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("top pair shouldbe = ")
        + std::to_string(shouldbe_toppair)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_toppair);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_toppair, result_toppair);

    error_message =
        err_start
        + static_cast<std::string>("second pair shouldbe = ")
        + std::to_string(shouldbe_secondpair)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_secondpair);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_secondpair, result_secondpair);

    error_message =
        err_start
        + static_cast<std::string>("kicker shouldbe = ")
        + std::to_string(shouldbe_kicker)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_kicker);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_kicker, result_kicker);

    error_message =
        err_start
        + static_cast<std::string>("7-card signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-card signature shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("hand value shouldbe = ")
        + std::to_string(shouldbe_handvalue)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_handvalue);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TTwoPairFunctionsTest1::TestIsTwoPair1() {
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CTwoPairFunctions tpf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    cardlib::CCard::kcard_value toppair;
    cardlib::CCard::kcard_value secondpair;
    cardlib::CCard::kcard_value kicker1;
    int64_t signature7;
    int64_t signature5;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("3s"),
      },
      cardlib::CCard::kcard_value::kThree,
      cardlib::CCard::kcard_value::kTwo,
      cardlib::CCard::kcard_value::kNine,
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpThree
      },
      { cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpNine
      },
      cardlib::CHand::khand_value::kTwoPair,
    },
    { { cardlib::CCard("th"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("td"),
        cardlib::CCard("3d"), cardlib::CCard("9h"),
        cardlib::CCard("7s"),
      },
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kThree,
      cardlib::CCard::kcard_value::kNine,
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpNine
      },
      cardlib::CHand::khand_value::kTwoPair,
    },
    { { cardlib::CCard("th"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("td"),
        cardlib::CCard("jd"), cardlib::CCard("jh"),
        cardlib::CCard("7s"),
      },
      cardlib::CCard::kcard_value::kJack,
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kSeven,
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kTwoPair,
    },
    { { cardlib::CCard("th"), cardlib::CCard("3h"),
        cardlib::CCard("ac"), cardlib::CCard("td"),
        cardlib::CCard("jd"), cardlib::CCard("jh"),
        cardlib::CCard("7s"),
      },
      cardlib::CCard::kcard_value::kJack,
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kAce,
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpAce
      },
      cardlib::CHand::khand_value::kTwoPair,
    },
    { { cardlib::CCard("th"), cardlib::CCard("3h"),
        cardlib::CCard("ac"), cardlib::CCard("td"),
        cardlib::CCard("jd"), cardlib::CCard("jh"),
        cardlib::CCard("3s"),
      },
      cardlib::CCard::kcard_value::kJack,
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kAce,
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpThree
      },
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpAce
      },
      cardlib::CHand::khand_value::kTwoPair,
    },
    { { cardlib::CCard("th"), cardlib::CCard("7h"),
        cardlib::CCard("4c"), cardlib::CCard("td"),
        cardlib::CCard("jd"), cardlib::CCard("jh"),
        cardlib::CCard("7s"),
      },
      cardlib::CCard::kcard_value::kJack,
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kSeven,
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kTwoPair,
    },
    { { cardlib::CCard("kh"), cardlib::CCard("3h"),
        cardlib::CCard("ac"), cardlib::CCard("td"),
        cardlib::CCard("2d"), cardlib::CCard("jh"),
        cardlib::CCard("js"),
      },
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      -1L, -1L,
      cardlib::CHand::khand_value::kHandValueNull
    },
    { { cardlib::CCard("kh"), cardlib::CCard("3h"),
        cardlib::CCard("ac"), cardlib::CCard("td"),
        cardlib::CCard("2d"), cardlib::CCard("jh"),
        cardlib::CCard("7s"),
      },
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      -1L, -1L,
      cardlib::CHand::khand_value::kHandValueNull
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  std::map<cardlib::CCard::kcard_value, int64_t>
      value_count_map;
  cardlib::CCard::kcard_value
      shouldbe_toppair, shouldbe_secondpair;
  cardlib::CCard::kcard_value
      result_toppair, result_secondpair;
  cardlib::CCard::kcard_value
      shouldbe_kicker, result_kicker;
  int64_t shouldbe7_signature, shouldbe5_signature;
  int64_t result7_signature, result5_signature;
  cardlib::CHand::khand_value
      shouldbe_handvalue, result_handvalue;
  std::string sub_name = {
    static_cast<std::string>("TTwoPairFunctionsTest1::")
    + static_cast<std::string>("TestIsTwoPair1:") };
  std::string error_message, err_start, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    shouldbe_toppair = va_cit->toppair;
    shouldbe_secondpair = va_cit->secondpair;
    shouldbe_kicker = va_cit->kicker1;
    shouldbe7_signature = va_cit->signature7;
    shouldbe5_signature = va_cit->signature5;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    value_count_map.clear();
    hand_obj.PopulateNthOfAKindMap(&input_card_vector,
                                   &value_count_map);

    tpf_obj.IsTwoPair(
        &input_card_vector, &value_count_map,
        &result_toppair, &result_secondpair, &result_kicker,
        &result7_signature, &result5_signature,
        &result_handvalue);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("top pair shouldbe = ")
        + std::to_string(shouldbe_toppair)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_toppair);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_toppair, result_toppair);

    error_message =
        err_start
        + static_cast<std::string>("second pair shouldbe = ")
        + std::to_string(shouldbe_secondpair)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_secondpair);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_secondpair, result_secondpair);

    error_message =
        err_start
        + static_cast<std::string>("kicker shouldbe = ")
        + std::to_string(shouldbe_kicker)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_kicker);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_kicker, result_kicker);

    error_message =
        err_start
        + static_cast<std::string>("7-card signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-card signature shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("hand value shouldbe = ")
        + std::to_string(shouldbe_handvalue)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_handvalue);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TTwoPairFunctionsTest1::TestTwoPairGreaterTwoPair1() {
  cardlib::CCard card_obj;
  cardlib::CTwoPairFunctions twof_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // top-pair is greater
    { { cardlib::CCard("2h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("4s"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("3s"),
      },
      true,
    },
    // second-pair is greater
    { { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("4s"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("4s"),
      },
      true,
    },
    // kicker is greater
    { { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("qh"),
        cardlib::CCard("4s"),
      },
      { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("4s"),
      },
      true,
    },
    // identical
    { { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("qh"),
        cardlib::CCard("4s"),
      },
      { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("qh"),
        cardlib::CCard("4s"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TTwoPairFunctionsTest1::")
    + static_cast<std::string>("TestTwoPairGreaterTwoPair1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        twof_obj.TwoPairGreaterTwoPair(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TTwoPairFunctionsTest1::TestTwoPairLessThanTwoPair1() {
  cardlib::CCard card_obj;
  cardlib::CTwoPairFunctions twof_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // top-pair is lesser
    { { cardlib::CCard("2h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("4s"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("th"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("ts"),
      },
      true,
    },
    // second-pair is lesser
    { { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("7s"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("4d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("7s"),
      },
      true,
    },
    // kicker is lesser
    { { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("th"),
        cardlib::CCard("4s"),
      },
      { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("qh"),
        cardlib::CCard("4s"),
      },
      true,
    },
    // identical
    { { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("qh"),
        cardlib::CCard("4s"),
      },
      { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("qh"),
        cardlib::CCard("4s"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TTwoPairFunctionsTest1::")
    + static_cast<std::string>("TestTwoPairLessThanTwoPair1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        twof_obj.TwoPairLessThanTwoPair(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TTwoPairFunctionsTest1::TestTwoPairEqualTwoPair1() {
  cardlib::CCard card_obj;
  cardlib::CTwoPairFunctions twof_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // top-pair is greater
    { { cardlib::CCard("2h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("4s"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("3s"),
      },
      false,
    },
    // second-pair is greater
    { { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("4s"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("4s"),
      },
      false,
    },
    // kicker is greater
    { { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("qh"),
        cardlib::CCard("4s"),
      },
      { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("4s"),
      },
      false,
    },
    // identical
    { { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("qh"),
        cardlib::CCard("4s"),
      },
      { cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("qh"),
        cardlib::CCard("4s"),
      },
      true,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TTwoPairFunctionsTest1::")
    + static_cast<std::string>("TestTwoPairEqualTwoPair1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        twof_obj.TwoPairEqualTwoPair(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

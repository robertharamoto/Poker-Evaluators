/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  THandTest implementation                              ###
  ###    namespace handtest                                  ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###    split into multiple files                           ###
  ###    new bitmask definition for cards                    ###
  ###    fixed updated cpplint.py errors                     ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/hand_test.h"

#include <cstdint>

#include <string>
#include <vector>
#include <algorithm>
#include <cctype>       // std::tolower

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/utils.h"

namespace handtest {

CPPUNIT_TEST_SUITE_REGISTRATION(THandTest);

// #############################################################
// #############################################################
THandTest::THandTest() {
}

// #############################################################
// #############################################################
THandTest::~THandTest() {
}

// #############################################################
// #############################################################
void THandTest::setUp() {
}

// #############################################################
// #############################################################
void THandTest::tearDown() {
}

// #############################################################
// #############################################################
void THandTest::TestConstructor() {
  cardlib::CHand tmp_hand;
  std::string error_message =
      static_cast<std::string>("THandTest::TestConstructor:");

  CPPUNIT_ASSERT_EQUAL_MESSAGE(
      error_message, 0L, tmp_hand.GetPrime7Signature());

  CPPUNIT_ASSERT_EQUAL_MESSAGE(
      error_message, 0L, tmp_hand.GetPrime5Signature());

  CPPUNIT_ASSERT_EQUAL_MESSAGE(
      error_message,
      static_cast<std::string>(""),
      tmp_hand.HandName());

  return;
}

// #############################################################
// #############################################################
void THandTest::TestCalculate1() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand_obj;
  cardlib::CCard card_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> cvec;
    std::string shouldbe_name;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("ah"), cardlib::CCard("th"),
        cardlib::CCard("kh"), cardlib::CCard("jh"),
        cardlib::CCard("qh"), cardlib::CCard("as"),
        cardlib::CCard("jd"),
      },
      "royal-flush",
    },
    { { cardlib::CCard("ad"), cardlib::CCard("th"),
        cardlib::CCard("kh"), cardlib::CCard("jh"),
        cardlib::CCard("qh"), cardlib::CCard("as"),
        cardlib::CCard("9h"),
      },
      "straight-flush",
    },
    { { cardlib::CCard("ad"), cardlib::CCard("7d"),
        cardlib::CCard("3d"), cardlib::CCard("2d"),
        cardlib::CCard("6d"), cardlib::CCard("4d"),
        cardlib::CCard("5d"),
      },
      "straight-flush",
    },
    { { cardlib::CCard("ad"), cardlib::CCard("7h"),
        cardlib::CCard("3d"), cardlib::CCard("2d"),
        cardlib::CCard("6d"), cardlib::CCard("4d"),
        cardlib::CCard("5d"),
      },
      "straight-flush",
    },
    { { cardlib::CCard("ad"), cardlib::CCard("th"),
        cardlib::CCard("3d"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("4d"),
        cardlib::CCard("5d"),
      },
      "straight-flush",
    },
    { { cardlib::CCard("ad"), cardlib::CCard("th"),
        cardlib::CCard("ah"), cardlib::CCard("ac"),
        cardlib::CCard("jh"), cardlib::CCard("4d"),
        cardlib::CCard("as"),
      },
      "four-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("th"),
        cardlib::CCard("ah"), cardlib::CCard("ac"),
        cardlib::CCard("tc"), cardlib::CCard("ad"),
        cardlib::CCard("as"),
      },
      "four-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("2h"),
        cardlib::CCard("ah"), cardlib::CCard("2c"),
        cardlib::CCard("tc"), cardlib::CCard("2d"),
        cardlib::CCard("2s"),
      },
      "four-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("ad"),
        cardlib::CCard("ah"), cardlib::CCard("2c"),
        cardlib::CCard("kc"), cardlib::CCard("kd"),
        cardlib::CCard("as"),
      },
      "full-house",
    },
    { { cardlib::CCard("td"), cardlib::CCard("th"),
        cardlib::CCard("ah"), cardlib::CCard("kh"),
        cardlib::CCard("kc"), cardlib::CCard("kd"),
        cardlib::CCard("as"),
      },
      "full-house",
    },
    { { cardlib::CCard("td"), cardlib::CCard("th"),
        cardlib::CCard("ah"), cardlib::CCard("kc"),
        cardlib::CCard("kd"), cardlib::CCard("ts"),
        cardlib::CCard("ks"),
      },
      "full-house",
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> cvec1;
  std::string shouldbe_name;
  std::string result_name;
  std::string sub_name("THandTest(1)::TestCalculate1:");
  std::string err_start, error_message;
  std::string hand_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    cvec1 = va_cit->cvec;

    shouldbe_name = va_cit->shouldbe_name;

    hand_obj.Clear();
    hand_obj.Calculate(&cvec1);
    hand_string = hand_obj.GetCardsString();

    result_name = hand_obj.HandName();

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + hand_string
        + static_cast<std::string>(" ")
        + card_obj.CardVectorToString(&cvec1)
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("shouldbe = ")
        + shouldbe_name
        + static_cast<std::string>(", result = ")
        + result_name;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_name, result_name);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THandTest::TestCalculate2() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand_obj;
  cardlib::CCard card_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    std::string shouldbe_name;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("td"), cardlib::CCard("th"),
        cardlib::CCard("9d"), cardlib::CCard("ad"),
        cardlib::CCard("2d"), cardlib::CCard("kd"),
        cardlib::CCard("3d"),
      },
      "flush",
    },
    { { cardlib::CCard("td"), cardlib::CCard("th"),
        cardlib::CCard("9d"), cardlib::CCard("2c"),
        cardlib::CCard("2d"), cardlib::CCard("kd"),
        cardlib::CCard("3d"),
      },
      "flush",
    },
    { { cardlib::CCard("td"), cardlib::CCard("qd"),
        cardlib::CCard("9d"), cardlib::CCard("2c"),
        cardlib::CCard("2d"), cardlib::CCard("qh"),
        cardlib::CCard("5d"),
      },
      "flush",
    },
    { { cardlib::CCard("td"), cardlib::CCard("th"),
        cardlib::CCard("9d"), cardlib::CCard("ac"),
        cardlib::CCard("2d"), cardlib::CCard("jd"),
        cardlib::CCard("5d"),
      },
      "flush",
    },
    { { cardlib::CCard("td"), cardlib::CCard("th"),
        cardlib::CCard("9d"), cardlib::CCard("2c"),
        cardlib::CCard("2d"), cardlib::CCard("8d"),
        cardlib::CCard("5d"),
      },
      "flush",
    },
    { { cardlib::CCard("3d"), cardlib::CCard("th"),
        cardlib::CCard("9d"), cardlib::CCard("2c"),
        cardlib::CCard("2d"), cardlib::CCard("8d"),
        cardlib::CCard("5d"),
      },
      "flush",
    },
    { { cardlib::CCard("6d"), cardlib::CCard("th"),
        cardlib::CCard("5d"), cardlib::CCard("2c"),
        cardlib::CCard("2d"), cardlib::CCard("8d"),
        cardlib::CCard("3d"),
      },
      "flush",
    },
    { { cardlib::CCard("6d"), cardlib::CCard("th"),
        cardlib::CCard("5d"), cardlib::CCard("2c"),
        cardlib::CCard("2d"), cardlib::CCard("7d"),
        cardlib::CCard("3d"),
      },
      "flush",
    },
    { { cardlib::CCard("2d"), cardlib::CCard("th"),
        cardlib::CCard("5d"), cardlib::CCard("2c"),
        cardlib::CCard("4d"), cardlib::CCard("7d"),
        cardlib::CCard("3d"),
      },
      "flush",
    },
    { { cardlib::CCard("td"), cardlib::CCard("jh"),
        cardlib::CCard("9d"), cardlib::CCard("qc"),
        cardlib::CCard("8c"), cardlib::CCard("ad"),
        cardlib::CCard("ks"),
      },
      "straight",
    },
    { { cardlib::CCard("td"), cardlib::CCard("jh"),
        cardlib::CCard("as"), cardlib::CCard("qc"),
        cardlib::CCard("ac"), cardlib::CCard("ad"),
        cardlib::CCard("ks"),
      },
      "straight",
    },
    { { cardlib::CCard("td"), cardlib::CCard("jh"),
        cardlib::CCard("9d"), cardlib::CCard("2c"),
        cardlib::CCard("8c"), cardlib::CCard("kd"),
        cardlib::CCard("qs"),
      },
      "straight",
    },
    { { cardlib::CCard("td"), cardlib::CCard("jh"),
        cardlib::CCard("9d"), cardlib::CCard("qc"),
        cardlib::CCard("8c"), cardlib::CCard("ad"),
        cardlib::CCard("7s"),
      },
      "straight",
    },
    { { cardlib::CCard("td"), cardlib::CCard("jh"),
        cardlib::CCard("9d"), cardlib::CCard("2c"),
        cardlib::CCard("8c"), cardlib::CCard("ad"),
        cardlib::CCard("7s"),
      },
      "straight",
    },
    { { cardlib::CCard("td"), cardlib::CCard("6h"),
        cardlib::CCard("9d"), cardlib::CCard("2c"),
        cardlib::CCard("8c"), cardlib::CCard("ad"),
        cardlib::CCard("7s"),
      },
      "straight",
    },
    { { cardlib::CCard("6d"), cardlib::CCard("jh"),
        cardlib::CCard("9d"), cardlib::CCard("4c"),
        cardlib::CCard("8c"), cardlib::CCard("5d"),
        cardlib::CCard("7s"),
      },
      "straight",
    },
    { { cardlib::CCard("5d"), cardlib::CCard("jh"),
        cardlib::CCard("6d"), cardlib::CCard("4c"),
        cardlib::CCard("8c"), cardlib::CCard("3d"),
        cardlib::CCard("7s"),
      },
      "straight",
    },
    { { cardlib::CCard("4d"), cardlib::CCard("5h"),
        cardlib::CCard("9d"), cardlib::CCard("3c"),
        cardlib::CCard("6c"), cardlib::CCard("ad"),
        cardlib::CCard("7s"),
      },
      "straight",
    },
    { { cardlib::CCard("6d"), cardlib::CCard("jh"),
        cardlib::CCard("5d"), cardlib::CCard("2c"),
        cardlib::CCard("4c"), cardlib::CCard("3d"),
        cardlib::CCard("as"),
      },
      "straight",
    },
    { { cardlib::CCard("5d"), cardlib::CCard("jh"),
        cardlib::CCard("9d"), cardlib::CCard("2c"),
        cardlib::CCard("3c"), cardlib::CCard("ad"),
        cardlib::CCard("4s"),
      },
      "straight",
    },
    { { cardlib::CCard("7d"), cardlib::CCard("jh"),
        cardlib::CCard("9d"), cardlib::CCard("2c"),
        cardlib::CCard("3c"), cardlib::CCard("ad"),
        cardlib::CCard("4s"),
      },
      "high-cards",
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector;
  std::string shouldbe_name, result_name;
  std::string sub_name("THandTest(1)::TestCalculate2:");
  std::string error_message;
  std::string hand_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;

    shouldbe_name = va_cit->shouldbe_name;

    hand_obj.Clear();
    hand_obj.Calculate(&card_vector);

    hand_string = hand_obj.GetCardsString();

    result_name = hand_obj.HandName();

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + card_obj.CardVectorToString(&card_vector)
        + static_cast<std::string>(" ")
        + hand_string
        + static_cast<std::string>(" : ")
        + static_cast<std::string>("shouldbe = ")
        + shouldbe_name
        + static_cast<std::string>(", result = ")
        + result_name;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_name, result_name);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THandTest::TestCalculate3() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand_obj;
  cardlib::CCard card_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    std::string shouldbe_name;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("td"), cardlib::CCard("4h"),
        cardlib::CCard("9d"), cardlib::CCard("ac"),
        cardlib::CCard("8c"), cardlib::CCard("ad"),
        cardlib::CCard("as"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("4h"),
        cardlib::CCard("kd"), cardlib::CCard("kc"),
        cardlib::CCard("8c"), cardlib::CCard("kh"),
        cardlib::CCard("7s"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("qd"), cardlib::CCard("th"),
        cardlib::CCard("9d"), cardlib::CCard("kc"),
        cardlib::CCard("qc"), cardlib::CCard("4d"),
        cardlib::CCard("qs"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("4h"),
        cardlib::CCard("9d"), cardlib::CCard("ac"),
        cardlib::CCard("jc"), cardlib::CCard("jd"),
        cardlib::CCard("js"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("4h"),
        cardlib::CCard("9d"), cardlib::CCard("tc"),
        cardlib::CCard("8c"), cardlib::CCard("th"),
        cardlib::CCard("7s"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("9h"),
        cardlib::CCard("9d"), cardlib::CCard("4c"),
        cardlib::CCard("8c"), cardlib::CCard("9s"),
        cardlib::CCard("7s"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("8h"),
        cardlib::CCard("9d"), cardlib::CCard("jc"),
        cardlib::CCard("8c"), cardlib::CCard("4d"),
        cardlib::CCard("8s"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("7d"), cardlib::CCard("4h"),
        cardlib::CCard("9d"), cardlib::CCard("7c"),
        cardlib::CCard("8c"), cardlib::CCard("ad"),
        cardlib::CCard("7h"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("6h"),
        cardlib::CCard("9d"), cardlib::CCard("6c"),
        cardlib::CCard("kc"), cardlib::CCard("6d"),
        cardlib::CCard("7s"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("4h"),
        cardlib::CCard("5d"), cardlib::CCard("5c"),
        cardlib::CCard("5h"), cardlib::CCard("ad"),
        cardlib::CCard("7s"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("4h"),
        cardlib::CCard("9d"), cardlib::CCard("4c"),
        cardlib::CCard("8h"), cardlib::CCard("4d"),
        cardlib::CCard("7s"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("3h"),
        cardlib::CCard("9d"), cardlib::CCard("3c"),
        cardlib::CCard("8c"), cardlib::CCard("3d"),
        cardlib::CCard("7s"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("2h"),
        cardlib::CCard("9d"), cardlib::CCard("2c"),
        cardlib::CCard("8c"), cardlib::CCard("7d"),
        cardlib::CCard("2s"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("5d"), cardlib::CCard("4h"),
        cardlib::CCard("4d"), cardlib::CCard("3c"),
        cardlib::CCard("3h"), cardlib::CCard("5c"),
        cardlib::CCard("7s"),
      },
      "two-pairs",
    },
    { { cardlib::CCard("8d"), cardlib::CCard("8h"),
        cardlib::CCard("4d"), cardlib::CCard("jc"),
        cardlib::CCard("jh"), cardlib::CCard("5c"),
        cardlib::CCard("4s"),
      },
      "two-pairs",
    },
    { { cardlib::CCard("5d"), cardlib::CCard("ah"),
        cardlib::CCard("4d"), cardlib::CCard("kc"),
        cardlib::CCard("3c"), cardlib::CCard("5c"),
        cardlib::CCard("7s"),
      },
      "one-pair",
    },
    { { cardlib::CCard("5d"), cardlib::CCard("qh"),
        cardlib::CCard("4d"), cardlib::CCard("tc"),
        cardlib::CCard("3c"), cardlib::CCard("2c"),
        cardlib::CCard("7s"),
      },
      "high-cards",
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector;
  std::string shouldbe_name, result_name;
  std::string sub_name("THandTest(1)::TestCalculate3:");
  std::string error_message;
  std::string hand_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;

    shouldbe_name = va_cit->shouldbe_name;

    hand_obj.Clear();
    hand_obj.Calculate(&card_vector);

    hand_string = hand_obj.GetCardsString();

    result_name = hand_obj.HandName();

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + card_obj.CardVectorToString(&card_vector)
        + static_cast<std::string>(", ")
        + hand_string
        + static_cast<std::string>(" : ")
        + static_cast<std::string>("shouldbe = ")
        + shouldbe_name
        + static_cast<std::string>(", result = ")
        + result_name;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_name, result_name);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

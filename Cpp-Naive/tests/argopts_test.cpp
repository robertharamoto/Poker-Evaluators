/*

// Copyright 2024 <robert haramoto>

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  argument options test class implementation            ###
  ###                                                        ###
  ###  last updated May 2, 2024                              ###
  ###                                                        ###
  ###  last updated July 16, 2022                            ###
  ###                                                        ###
  ###  updated February 5, 2020                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/argopts_test.h"

#include <vector>
#include <map>
#include <string>
#include <cstring>

#include "sources/argopts.h"

namespace test_mylib {

CPPUNIT_TEST_SUITE_REGISTRATION(TArgOptsTest);

// #############################################################
// #############################################################
TArgOptsTest::TArgOptsTest() {
}

// #############################################################
// #############################################################
TArgOptsTest::~TArgOptsTest() {
}

// #############################################################
// #############################################################
void TArgOptsTest::setUp() {
}

// #############################################################
// #############################################################
void TArgOptsTest::tearDown() {
}

// #############################################################
// #############################################################
void TArgOptsTest::TestLowerCase1() {
  mylib::CArgOpts argopts_obj;
  struct atest_struct {
    std::string tstring, shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { "--h", "--h" },  { "-H", "-h" }, { "--help", "--help" },
    { "--Help", "--help" }, { "--HELP", "--help" },
    { "--Version", "--version" }, { "--VERSION", "--version" }
  };
  int64_t test_label_index;
  std::string shouldbe_string, result_string;
  std::vector<atest_struct>::const_iterator va_cit;
  std::string sub_name = {
    static_cast<std::string>("TArgOptsTest::TestLowerCase1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    result_string = va_cit->tstring;
    shouldbe_string = va_cit->shouldbe;
    argopts_obj.LowerCase(&result_string);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), test = ")
        + va_cit->tstring
        + static_cast<std::string>(", shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TArgOptsTest::TestOptionsEntered1() {
  mylib::CArgOpts argopts_obj;
  int argc = 1;
  const char *argv[] = { "program name" };
  struct atest_struct {
    std::string input_string;
    bool shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { "--help", false }, { "-h", false },
    { "--version", false },
    { "-v", false }, { "--invalid", false }
  };
  int64_t test_label_index;
  bool bresult, bshouldbe;
  std::string input_string;
  std::string version = "2024-05-02";
  std::vector<atest_struct>::const_iterator va_cit;
  std::string sub_name = {
    static_cast<std::string>("TArgOptsTest::")
    + static_cast<std::string>("TestOptionsEntered1:") };
  std::string tmp_string;
  std::string error_message;

  // check ArgOpts with no options
  tmp_string = static_cast<std::string>("test");
  argopts_obj.SetName(&tmp_string);
  argopts_obj.SetVersion(&version);

  // everything should be setup by setup()
  argopts_obj.ProcessOptions(argc, argv);
  argopts_obj.Clear();

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_string = va_cit->input_string;
    bshouldbe = va_cit->shouldbe;

    bresult = argopts_obj.OptionEntered(&input_string);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), input = ")
        + input_string
        + static_cast<std::string>(", shouldbe = ")
        + static_cast<std::string>(
            (bshouldbe ? "true" : "false"))
        + static_cast<std::string>(", result = ")
        + static_cast<std::string>(
            (bresult ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, bshouldbe, bresult);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TArgOptsTest::TestOptionsEntered2() {
  mylib::CArgOpts argopts_obj;
  int argc = 2;
  const char *argv[] = { "program name", "--help" };
  struct atest_struct {
    std::string input_string;
    bool shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { "--help", true }, { "-h", true }, { "--version", false },
    { "-v", false }, { "--invalid", false }
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  bool bresult, bshouldbe;
  std::string input_string;
  std::string version = "2024-05-02";
  std::string sub_name = {
    static_cast<std::string>("TArgOptsTest::")
    + static_cast<std::string>("TestOptionsEntered2:") };
  std::string tmp_string, opt1, opt2, opt3;
  std::string error_message;

  // check ArgOpts with no options
  tmp_string = static_cast<std::string>("test");
  argopts_obj.SetName(&tmp_string);
  argopts_obj.SetVersion(&version);

  opt1 = static_cast<std::string>("-h");
  opt2 = static_cast<std::string>("--help");
  opt3 = static_cast<std::string>("display this message");
  argopts_obj.AddValidOptionNoArgument(
      &opt1, &opt2, &opt3);

  opt1 = static_cast<std::string>("-v");
  opt2 = static_cast<std::string>("--version");
  opt3 = static_cast<std::string>("display version");
  argopts_obj.AddValidOptionNoArgument(
      &opt1, &opt2, &opt3);

  // everything should be setup by setup()
  argopts_obj.ProcessOptions(argc, argv);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_string = va_cit->input_string;
    bshouldbe = va_cit->shouldbe;

    bresult = argopts_obj.OptionEntered(&input_string);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), input = ")
        + input_string
        + static_cast<std::string>(", shouldbe = ")
        + static_cast<std::string>(
            (bshouldbe ? "true" : "false"))
        + static_cast<std::string>(", result = ")
        + static_cast<std::string>(
            (bresult ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, bshouldbe, bresult);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TArgOptsTest::TestOptionsEntered3() {
  mylib::CArgOpts argopts_obj;
  int argc = 3;
  const char *argv[] =
      { "program name", "--help", "--60minutes" };
  struct atest_struct {
    std::string input_string;
    bool shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { "--help", true }, { "-h", true },
    { "--version", false }, { "-v", false },
    { "--60minutes", true }, { "-60", true },
    { "--invalid", false }
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  bool bresult, bshouldbe;
  std::string input_string;
  std::string version = "2024-05-02";
  std::string sub_name = {
    static_cast<std::string>("TArgOptsTest::")
    + static_cast<std::string>("TestOptionsEntered3:") };
  std::string tmp_string, opt1, opt2, opt3;
  std::string error_message;

  // check ArgOpts with no options
  tmp_string = static_cast<std::string>("test");
  argopts_obj.SetName(&tmp_string);
  argopts_obj.SetVersion(&version);

  opt1 = static_cast<std::string>("-h");
  opt2 = static_cast<std::string>("--help");
  opt3 = static_cast<std::string>("display this message");
  argopts_obj.AddValidOptionNoArgument(
      &opt1, &opt2, &opt3);

  opt1 = static_cast<std::string>("-v");
  opt2 = static_cast<std::string>("--version");
  opt3 = static_cast<std::string>("display version");
  argopts_obj.AddValidOptionNoArgument(
      &opt1, &opt2, &opt3);

  opt1 = static_cast<std::string>("-60");
  opt2 = static_cast<std::string>("--60minutes");
  opt3 = static_cast<std::string>("time for 60 minutes");
  argopts_obj.AddValidOptionNoArgument(
      &opt1, &opt2, &opt3);

  // everything should be setup by setup()
  argopts_obj.ProcessOptions(argc, argv);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_string = va_cit->input_string;
    bshouldbe = va_cit->shouldbe;

    bresult = argopts_obj.OptionEntered(&input_string);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), input = ")
        + input_string
        + static_cast<std::string>(", shouldbe = ")
        + static_cast<std::string>(
            (bshouldbe ? "true" : "false"))
        + static_cast<std::string>(", result = ")
        + static_cast<std::string>(
            (bresult ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, bshouldbe, bresult);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TArgOptsTest::TestOptionsEntered4() {
  mylib::CArgOpts argopts_obj;
  int argc = 3;
  const char *argv[] =
      { "program name", "--version",
        "-60", "--invalidoption" };
  struct atest_struct {
    std::string input_string;
    bool shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { "--help", false }, { "-h", false },
    { "--version", true }, { "-v", true },
    { "--60minutes", true }, { "-60", true },
    { "--invalidoption", false }
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  bool bresult, bshouldbe;
  std::string input_string;
  std::string version = "2024-05-02";
  std::string sub_name = {
    static_cast<std::string>("TArgOptsTest::")
    + static_cast<std::string>("TestOptionsEntered4:") };
  std::string tmp_string, opt1, opt2, opt3;
  std::string error_message;

  // check ArgOpts with no options
  tmp_string = static_cast<std::string>("test");
  argopts_obj.SetName(&tmp_string);
  argopts_obj.SetVersion(&version);

  opt1 = static_cast<std::string>("-h");
  opt2 = static_cast<std::string>("--help");
  opt3 = static_cast<std::string>("display this message");
  argopts_obj.AddValidOptionNoArgument(
      &opt1, &opt2, &opt3);

  opt1 = static_cast<std::string>("-v");
  opt2 = static_cast<std::string>("--version");
  opt3 = static_cast<std::string>("display version");
  argopts_obj.AddValidOptionNoArgument(
      &opt1, &opt2, &opt3);

  opt1 = static_cast<std::string>("-60");
  opt2 = static_cast<std::string>("--60minutes");
  opt3 = static_cast<std::string>("time for 60 minutes");
  argopts_obj.AddValidOptionNoArgument(
      &opt1, &opt2, &opt3);

  // everything should be setup by setup()
  argopts_obj.ProcessOptions(argc, argv);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_string = va_cit->input_string;
    bshouldbe = va_cit->shouldbe;

    bresult = argopts_obj.OptionEntered(&input_string);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), input = ")
        + input_string
        + static_cast<std::string>(", shouldbe = ")
        + static_cast<std::string>(
            (bshouldbe ? "true" : "false"))
        + static_cast<std::string>(", result = ")
        + static_cast<std::string>(
            (bresult ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, bshouldbe, bresult);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TArgOptsTest::TestParseArgToOptFlag1() {
  mylib::CArgOpts argopts_obj;
  struct atest_struct {
    std::string input_option, possible1, possible2;
    std::string shouldbe_opt_flag, shouldbe_opt_arg;
  };
  std::vector<atest_struct> test_vector = {
    { "--help", "", "", "--help", "" },
    { "-h", "", "", "-h", "" },
    { "--max=999", "", "", "--max", "999" },
    { "-m=", "11", "", "-m", "11" },
    { "-m=77", "", "", "-m", "77" },
    { "--max", "=", "44", "--max", "44" },
    { "-m", "=22", "", "-m", "22" },
    { "--max", "33", "", "--max", "33" },
    { "--max", "55", "", "--max", "55" },
    { "-m", "", "", "-m", "" },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::string input_option_string, input_possible1_string;
  std::string input_possible2_string;
  std::string shouldbe_opt_flag, result_opt_flag;
  std::string shouldbe_opt_arg, result_opt_arg;
  std::string version = "2024-05-02";
  std::string sub_name = {
    static_cast<std::string>("TArgOptsTest::")
    + static_cast<std::string>("TestParseArgToOptFlag1:") };
  std::string tmp_string, opt1, opt2, opt3, opt4, opt5;
  std::string error_message, err_start;

  // check ArgOpts with no options
  tmp_string = static_cast<std::string>("test");
  argopts_obj.SetName(&tmp_string);
  argopts_obj.SetVersion(&version);

  opt1 = static_cast<std::string>("-h");
  opt2 = static_cast<std::string>("--help");
  opt3 = static_cast<std::string>("display this message");
  argopts_obj.AddValidOptionNoArgument(
      &opt1, &opt2, &opt3);

  opt1 = static_cast<std::string>("-v");
  opt2 = static_cast<std::string>("--version");
  opt3 = static_cast<std::string>("display version");
  argopts_obj.AddValidOptionNoArgument(
      &opt1, &opt2, &opt3);

  opt1 = static_cast<std::string>("-m");
  opt2 = static_cast<std::string>("--max");
  opt3 = static_cast<std::string>("max value");
  opt4 = static_cast<std::string>("88");
  opt5 = static_cast<std::string>("");
  argopts_obj.AddValidOptionWithArgument(
      &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

  opt1 = static_cast<std::string>("-p");
  opt2 = static_cast<std::string>("--pockets1");
  opt3 = static_cast<std::string>("pocket pairs");
  opt4 = static_cast<std::string>("as ac");
  opt5 = static_cast<std::string>("");
  argopts_obj.AddValidOptionWithArgument(
      &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

  opt1 = static_cast<std::string>("-q");
  opt2 = static_cast<std::string>("--pockets2");
  opt3 = static_cast<std::string>("pocket pairs");
  opt4 = static_cast<std::string>("4d 5d");
  opt5 = static_cast<std::string>("");
  argopts_obj.AddValidOptionWithArgument(
      &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_option_string = va_cit->input_option;
    input_possible1_string = va_cit->possible1;
    input_possible2_string = va_cit->possible2;

    shouldbe_opt_flag = va_cit->shouldbe_opt_flag;
    shouldbe_opt_arg = va_cit->shouldbe_opt_arg;

    argopts_obj.ParseArgToOptFlag(
        &input_option_string,
        &input_possible1_string,
        &input_possible2_string,
        &result_opt_flag,
        &result_opt_arg);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), input = ")
        + input_option_string
        + static_cast<std::string>(", possible1 = ")
        + input_possible1_string
        + static_cast<std::string>(", possible2 = ")
        + input_possible2_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("shouldbe opt flag = ")
        + shouldbe_opt_flag
        + static_cast<std::string>(", result opt flag = ")
        + result_opt_flag;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_opt_flag, result_opt_flag);

    error_message =
        err_start
        + static_cast<std::string>(", shouldbe opt arg = ")
        + shouldbe_opt_arg
        + static_cast<std::string>(", result opt arg = ")
        + result_opt_arg;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_opt_arg, result_opt_arg);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TArgOptsTest::TestParsePocketsOption1() {
  mylib::CArgOpts argopts_obj;
  struct atest_struct {
    std::string input_option, possible1, possible2, possible3;
    std::string shouldbe_opt_flag, shouldbe_opt_arg;
  };
  std::vector<atest_struct> test_vector = {
    { "-p", "as", "ad", "", "-p", "as ad" },
    { "-q", "as", "ad", "", "-q", "as ad" },
    { "-p", "=", "as", "ad", "-p", "as ad" },
    { "-q=", "as", "ad", "", "-q", "as ad" },
    { "--pockets1", "=", "as", "ad", "--pockets1", "as ad" },
    { "--pockets2", "=", "4d", "5d", "--pockets2", "4d 5d" },
    { "--pockets1", "as", "ad", "", "--pockets1", "as ad" },
    { "--pockets2", "4d", "5d", "", "--pockets2", "4d 5d" },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::string input_option_string, input_possible1_string;
  std::string input_possible2_string, input_possible3_string;
  std::string shouldbe_opt_flag, result_opt_flag;
  std::string shouldbe_opt_arg, result_opt_arg;
  std::string version = "2024-05-02";
  std::string sub_name = {
    static_cast<std::string>("TArgOptsTest::")
    + static_cast<std::string>("TestParsePocketsOption1:") };
  std::string tmp_string, opt1, opt2, opt3, opt4, opt5;
  std::string error_message, err_start;

  // check ArgOpts with no options
  tmp_string = static_cast<std::string>("test");
  argopts_obj.SetName(&tmp_string);
  argopts_obj.SetVersion(&version);

  opt1 = static_cast<std::string>("-h");
  opt2 = static_cast<std::string>("--help");
  opt3 = static_cast<std::string>("display this message");
  argopts_obj.AddValidOptionNoArgument(
      &opt1, &opt2, &opt3);

  opt1 = static_cast<std::string>("-v");
  opt2 = static_cast<std::string>("--version");
  opt3 = static_cast<std::string>("display version");
  argopts_obj.AddValidOptionNoArgument(
      &opt1, &opt2, &opt3);

  opt1 = static_cast<std::string>("-m");
  opt2 = static_cast<std::string>("--max");
  opt3 = static_cast<std::string>("max value");
  opt4 = static_cast<std::string>("88");
  opt5 = static_cast<std::string>("");
  argopts_obj.AddValidOptionWithArgument(
      &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

  opt1 = static_cast<std::string>("-p");
  opt2 = static_cast<std::string>("--pockets1");
  opt3 = static_cast<std::string>("pocket pairs");
  opt4 = static_cast<std::string>("as ac");
  opt5 = static_cast<std::string>("");
  argopts_obj.AddValidOptionWithArgument(
      &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

  opt1 = static_cast<std::string>("-q");
  opt2 = static_cast<std::string>("--pockets2");
  opt3 = static_cast<std::string>("pocket pairs");
  opt4 = static_cast<std::string>("4d 5d");
  opt5 = static_cast<std::string>("");
  argopts_obj.AddValidOptionWithArgument(
      &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_option_string = va_cit->input_option;
    input_possible1_string = va_cit->possible1;
    input_possible2_string = va_cit->possible2;
    input_possible3_string = va_cit->possible3;

    shouldbe_opt_flag = va_cit->shouldbe_opt_flag;
    shouldbe_opt_arg = va_cit->shouldbe_opt_arg;

    argopts_obj.ParsePocketsOption(
        &input_option_string,
        &input_possible1_string,
        &input_possible2_string,
        &input_possible3_string,
        &result_opt_flag,
        &result_opt_arg);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), input = ")
        + input_option_string
        + static_cast<std::string>(", possible1 = ")
        + input_possible1_string
        + static_cast<std::string>(", possible2 = ")
        + input_possible2_string
        + static_cast<std::string>(", possible3 = ")
        + input_possible3_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("shouldbe opt flag = ")
        + shouldbe_opt_flag
        + static_cast<std::string>(", result opt flag = ")
        + result_opt_flag;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_opt_flag, result_opt_flag);

    error_message =
        err_start
        + static_cast<std::string>(", shouldbe opt arg = ")
        + shouldbe_opt_arg
        + static_cast<std::string>(", result opt arg = ")
        + result_opt_arg;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_opt_arg, result_opt_arg);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TArgOptsTest::TestGetOptionArgument1() {
  mylib::CArgOpts argopts_obj;
  struct atest_struct {
    std::vector<std::string> argvs;
    std::vector<std::vector<std::string> >
    input_vector_vector;
  };
  std::vector<atest_struct> test_vector = {
    { { "program name", "-m=60" },
      { { "--max", "60"},
        { "-m", "60" },
        { "--version", "" },
        { "-v", "" },
        { "--60minutes", "" },
        { "-60", "" },
      },
    },
    { { "program name", "-m=", "50" },
      { { "--max", "50" },
        { "-m", "50" },
        { "--version", "" },
        { "-v", "" },
        { "--60minutes", "" },
        { "-60", "" },
      }
    },
    { { "program name", "--max=", "55" },
      { { "--max", "55" },
        { "-m", "55" },
        { "--version", "" },
        { "-v", "" },
        { "--60minutes", "" },
        { "-60", "" },
      }
    },
    { { "program name", "--max", "=", "44" },
      { { "--max", "44" },
        { "-m", "44" },
        { "--version", "" },
        { "-v", "" },
        { "--60minutes", "" },
        { "-60", "" },
      }
    },
    { { "program name", "-m", "=33" },
      { { "--max", "33" },
        { "-m", "33" },
        { "--version", "" },
        { "-v", "" },
        { "--60minutes", "" },
        { "-60", "" },
      }
    },
    { { "program name", "--max", "="},
      { { "--max", "1234" },
        { "-m", "1234" },
        { "--version", "" },
        { "-v", "" },
        { "--60minutes", "" },
        { "-60", "" },
      }
    },
    { { "program name", "-m" },
      { { "--max", "1234" },
        { "-m", "1234" },
        { "--version", "" },
        { "-v", "" },
        { "--60minutes", "" },
        { "-60", "" },
      }
    },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  int arg_size, ntrials;
  std::vector<std::string> argv_vector;
  std::vector<std::vector<std::string> > input_vector_vector;
  std::string input_string;
  std::string shouldbe_string, result_string;
  std::string version = "2024-04-28";
  std::string sub_name = {
    static_cast<std::string>("TArgOptsTest::")
    + static_cast<std::string>("TestGetOptionArgument1:") };
  std::string err_start, error_message;
  std::string tmp_string, opt1, opt2, opt3, opt4, opt5;
  std::string input_arg_string;
  char **ppchar;

  // check argopts_obj with no options
  tmp_string = static_cast<std::string>("test");
  argopts_obj.SetName(&tmp_string);
  argopts_obj.SetVersion(&version);

  opt1 = static_cast<std::string>("-h");
  opt2 = static_cast<std::string>("--help");
  opt3 = static_cast<std::string>("display this message");
  argopts_obj.AddValidOptionNoArgument(
      &opt1, &opt2, &opt3);

  opt1 = static_cast<std::string>("-v");
  opt2 = static_cast<std::string>("--version");
  opt3 = static_cast<std::string>("display version");
  argopts_obj.AddValidOptionNoArgument(
      &opt1, &opt2, &opt3);

  opt1 = static_cast<std::string>("-m");
  opt2 = static_cast<std::string>("--max");
  opt3 = static_cast<std::string>("max number of primes");
  opt4 = static_cast<std::string>("1234");
  opt5 = static_cast<std::string>("5678");
  argopts_obj.AddValidOptionWithArgument(
      &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

  test_label_index = 0L;
  // loop over different tests
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    // populate argv
    argv_vector = va_cit->argvs;
    input_vector_vector = va_cit->input_vector_vector;

    input_arg_string = "";

    // insert arguments into argopts_obj
    arg_size = static_cast<int>(argv_vector.size());
    ppchar = reinterpret_cast<char **>(
        malloc(arg_size * sizeof(char *)));
    for (int ii = 0; ii < arg_size; ++ii) {
      input_string = argv_vector[ii];
      ppchar[ii] = reinterpret_cast<char *>(
          malloc(input_string.size() + 2));
      strncpy(ppchar[ii], input_string.c_str(),
              input_string.size()+1);
      input_arg_string +=
          static_cast<std::string>("argv[")
          + std::to_string(ii)
          + static_cast<std::string>("]=")
          + input_string
          + static_cast<std::string>(", ");
    }

    argopts_obj.ProcessOptions(
        arg_size, const_cast<const char **>(ppchar));

    for (int ii = 0; ii < arg_size; ++ii) {
      free(ppchar[ii]);
    }
    free(ppchar);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), input args = ")
        + input_arg_string
        + static_cast<std::string>(" : ");

    // loop over different options
    ntrials = static_cast<int>(input_vector_vector.size());

    for (int ii = 0; ii < ntrials; ++ii) {
      input_string = input_vector_vector[ii][0];
      shouldbe_string = input_vector_vector[ii][1];
      result_string =
          argopts_obj.GetOptionArgument(&input_string);

      error_message =
          err_start
          + static_cast<std::string>(" (")
          + std::to_string(ii)
          + static_cast<std::string>(") ")
          + static_cast<std::string>(", shouldbe = ")
          + shouldbe_string
          + static_cast<std::string>(", result = ")
          + result_string;

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message, shouldbe_string, result_string);
    }

    ++test_label_index;
  }

  return;
}

}  // namespace test_mylib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

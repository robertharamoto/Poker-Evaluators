#ifndef TESTS_ARGOPTS_TEST_H_
#define TESTS_ARGOPTS_TEST_H_
/*

// Copyright 2024 robert haramoto

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  argument options test class declaration               ###
  ###                                                        ###
  ###  last updated May 2, 2024                              ###
  ###                                                        ###
  ###  last updated July 31, 2022                            ###
  ###                                                        ###
  ###  updated February 5, 2020                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/SourceLine.h>

#include "sources/argopts.h"

namespace test_mylib {

class TArgOptsTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(TArgOptsTest);

  CPPUNIT_TEST(TestLowerCase1);
  CPPUNIT_TEST(TestOptionsEntered1);
  CPPUNIT_TEST(TestOptionsEntered2);
  CPPUNIT_TEST(TestOptionsEntered3);
  CPPUNIT_TEST(TestOptionsEntered4);

  CPPUNIT_TEST(TestParseArgToOptFlag1);
  CPPUNIT_TEST(TestParsePocketsOption1);
  CPPUNIT_TEST(TestGetOptionArgument1);

  CPPUNIT_TEST_SUITE_END();

 public:
  TArgOptsTest();
  ~TArgOptsTest();

  // do not allow copying nor moving
  TArgOptsTest(const TArgOptsTest&) = delete;
  TArgOptsTest& operator=(const TArgOptsTest&) = delete;
  TArgOptsTest(TArgOptsTest&&) = delete;
  TArgOptsTest& operator=(TArgOptsTest&&) = delete;

  void setUp();
  void tearDown();

  void TestLowerCase1();

  void TestOptionsEntered1();
  void TestOptionsEntered2();
  void TestOptionsEntered3();
  void TestOptionsEntered4();

  void TestParseArgToOptFlag1();
  void TestParsePocketsOption1();
  void TestGetOptionArgument1();
};  // class TArgOptsTest

}  // namespace test_mylib

#endif  // TESTS_ARGOPTS_TEST_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

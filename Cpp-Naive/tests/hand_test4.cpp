/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  THandTest implementation                              ###
  ###    namespace handtest                                  ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###    split into multiple files                           ###
  ###    new bitmask definition for cards                    ###
  ###    fixed updated cpplint.py errors                     ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/hand_test.h"

#include <cstdint>

#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <cctype>       // std::tolower

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/utils.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::THandTest);

namespace handtest {

// #############################################################
// #############################################################
void THandTest::TestHand1EqualHand2Various1() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand1_obj, hand2_obj;
  cardlib::CCard card_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector1, card_vector2;
    bool shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // royal flush versus royal flush
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("ah"),
        cardlib::CCard("9h"),
      },
      { cardlib::CCard("ah"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("8h"),
        cardlib::CCard("9h"),
      },
      true,
    },
    // royal flush versus king-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("ah"),
        cardlib::CCard("9h"),
      },
      { cardlib::CCard("3h"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("8h"),
        cardlib::CCard("9h"),
      },
      false,
    },
    // six-high straight flush versus six-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("2d"),
        cardlib::CCard("3d"), cardlib::CCard("6d"),
        cardlib::CCard("th"), cardlib::CCard("5d"),
        cardlib::CCard("4d"),
      },
      { cardlib::CCard("3d"), cardlib::CCard("kh"),
        cardlib::CCard("6d"), cardlib::CCard("2d"),
        cardlib::CCard("4d"), cardlib::CCard("5d"),
        cardlib::CCard("9h"),
      },
      true,
    },
    // six-high straight flush versus seven-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("2d"),
        cardlib::CCard("3d"), cardlib::CCard("6d"),
        cardlib::CCard("th"), cardlib::CCard("5d"),
        cardlib::CCard("4d"),
      },
      { cardlib::CCard("3d"), cardlib::CCard("7d"),
        cardlib::CCard("6d"), cardlib::CCard("2d"),
        cardlib::CCard("4d"), cardlib::CCard("5d"),
        cardlib::CCard("9h"),
      },
      false,
    },
    // six-high straight flush versus five-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("2d"),
        cardlib::CCard("3d"), cardlib::CCard("6d"),
        cardlib::CCard("th"), cardlib::CCard("5d"),
        cardlib::CCard("4d"),
      },
      { cardlib::CCard("3d"), cardlib::CCard("7d"),
        cardlib::CCard("ad"), cardlib::CCard("2d"),
        cardlib::CCard("4d"), cardlib::CCard("5d"),
        cardlib::CCard("9h"),
      },
      false,
    },
    // five-high straight flush versus five-high straight flush
    { { cardlib::CCard("7c"), cardlib::CCard("2c"),
        cardlib::CCard("3c"), cardlib::CCard("6d"),
        cardlib::CCard("ac"), cardlib::CCard("5c"),
        cardlib::CCard("4c"),
      },
      { cardlib::CCard("3s"), cardlib::CCard("7d"),
        cardlib::CCard("2s"), cardlib::CCard("as"),
        cardlib::CCard("4s"), cardlib::CCard("5s"),
        cardlib::CCard("6h"),
      },
      true,
    },
    // quad aces, jack kicker versus quad aces, jack kicker
    { { cardlib::CCard("ac"), cardlib::CCard("as"),
        cardlib::CCard("ad"), cardlib::CCard("ah"),
        cardlib::CCard("jc"), cardlib::CCard("5c"),
        cardlib::CCard("4c"),
      },
      { cardlib::CCard("3s"), cardlib::CCard("jd"),
        cardlib::CCard("as"), cardlib::CCard("ad"),
        cardlib::CCard("4s"), cardlib::CCard("ac"),
        cardlib::CCard("ah"),
      },
      true,
    },
    // quad aces, jack kicker versus quad aces, king kicker
    { { cardlib::CCard("ac"), cardlib::CCard("as"),
        cardlib::CCard("ad"), cardlib::CCard("ah"),
        cardlib::CCard("jc"), cardlib::CCard("5c"),
        cardlib::CCard("4c"),
      },
      { cardlib::CCard("3s"), cardlib::CCard("jd"),
        cardlib::CCard("as"), cardlib::CCard("ad"),
        cardlib::CCard("ks"), cardlib::CCard("ac"),
        cardlib::CCard("ah"),
      },
      false,
    },
    // aces full of jacks versus aces full of jacks
    { { cardlib::CCard("ac"), cardlib::CCard("js"),
        cardlib::CCard("ad"), cardlib::CCard("ah"),
        cardlib::CCard("jc"), cardlib::CCard("5c"),
        cardlib::CCard("4h"),
      },
      { cardlib::CCard("jh"), cardlib::CCard("jd"),
        cardlib::CCard("as"), cardlib::CCard("ad"),
        cardlib::CCard("ks"), cardlib::CCard("ac"),
        cardlib::CCard("2h"),
      },
      true,
    },
    // aces full of jacks versus aces full of tens
    { { cardlib::CCard("ac"), cardlib::CCard("js"),
        cardlib::CCard("ad"), cardlib::CCard("ah"),
        cardlib::CCard("jc"), cardlib::CCard("5c"),
        cardlib::CCard("4h"),
      },
      { cardlib::CCard("th"), cardlib::CCard("td"),
        cardlib::CCard("as"), cardlib::CCard("ad"),
        cardlib::CCard("ks"), cardlib::CCard("ac"),
        cardlib::CCard("2h"),
      },
      false,
    },
    // ace-high flush versus ace-high flush
    { { cardlib::CCard("as"), cardlib::CCard("js"),
        cardlib::CCard("ad"), cardlib::CCard("ts"),
        cardlib::CCard("jc"), cardlib::CCard("5s"),
        cardlib::CCard("4s"),
      },
      { cardlib::CCard("ah"), cardlib::CCard("jh"),
        cardlib::CCard("as"), cardlib::CCard("th"),
        cardlib::CCard("5h"), cardlib::CCard("4h"),
        cardlib::CCard("2h"),
      },
      true,
    },
    // ace-high flush versus ace-high flush
    { { cardlib::CCard("as"), cardlib::CCard("js"),
        cardlib::CCard("ad"), cardlib::CCard("ts"),
        cardlib::CCard("jc"), cardlib::CCard("5s"),
        cardlib::CCard("4s"),
      },
      { cardlib::CCard("ah"), cardlib::CCard("qh"),
        cardlib::CCard("as"), cardlib::CCard("th"),
        cardlib::CCard("5h"), cardlib::CCard("4h"),
        cardlib::CCard("2h"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector1, card_vector2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("THandTest(4)::")
    + static_cast<std::string>("TestHand1EqualHand2Various1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector1 = va_cit->card_vector1;
    card_vector2 = va_cit->card_vector2;

    shouldbe_bool = va_cit->shouldbe;

    hand1_obj.Clear();
    hand1_obj.Calculate(&card_vector1);

    hand2_obj.Clear();
    hand2_obj.Calculate(&card_vector2);

    result_bool = hand1_obj.Hand1EqualHand2(&hand2_obj);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(")  for ")
        + hand1_obj.HandName()
        + static_cast<std::string>(" ")
        + card_obj.CardVectorToString(&card_vector1)
        + static_cast<std::string>(" versus ")
        + hand2_obj.HandName()
        + static_cast<std::string>(" ")
        + card_obj.CardVectorToString(&card_vector2)
        + static_cast<std::string>(" : shouldbe = ")
        + static_cast<std::string>(
            (shouldbe_bool ? "true" : "false"))
        + static_cast<std::string>(", result = ")
        + static_cast<std::string>(
            (result_bool ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THandTest::TestHand1EqualHand2Various2() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand1_obj, hand2_obj;
  cardlib::CCard card_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector1, card_vector2;
    bool shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // ace-high straight versus ace-high straight
    { { cardlib::CCard("7h"), cardlib::CCard("kd"),
        cardlib::CCard("qs"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("ac"),
        cardlib::CCard("9d"), },
      { cardlib::CCard("ad"), cardlib::CCard("kh"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("td"), cardlib::CCard("8s"),
        cardlib::CCard("9s"),
      },
      true,
    },
    // ace-high straight versus king high straight
    { { cardlib::CCard("7h"), cardlib::CCard("kd"),
        cardlib::CCard("qs"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("ac"),
        cardlib::CCard("9d"),
      },
      { cardlib::CCard("2d"), cardlib::CCard("kh"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("td"), cardlib::CCard("8s"),
        cardlib::CCard("9s"),
      },
      false,
    },
    // triple aces versus triple aces
    { { cardlib::CCard("7h"), cardlib::CCard("ad"),
        cardlib::CCard("qs"), cardlib::CCard("ah"),
        cardlib::CCard("th"), cardlib::CCard("ac"),
        cardlib::CCard("9d"),
      },
      { cardlib::CCard("td"), cardlib::CCard("ad"),
        cardlib::CCard("qc"), cardlib::CCard("ah"),
        cardlib::CCard("2d"), cardlib::CCard("as"),
        cardlib::CCard("9s"),
      },
      true,
    },
    // triple aces versus triple kings
    { { cardlib::CCard("ah"), cardlib::CCard("ad"),
        cardlib::CCard("4c"), cardlib::CCard("as"),
        cardlib::CCard("8h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      { cardlib::CCard("ah"), cardlib::CCard("kd"),
        cardlib::CCard("kc"), cardlib::CCard("ks"),
        cardlib::CCard("3h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      false,
    },
    // two pair versus two pairs
    { { cardlib::CCard("ah"), cardlib::CCard("4d"),
        cardlib::CCard("4c"), cardlib::CCard("as"),
        cardlib::CCard("8h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      { cardlib::CCard("ah"), cardlib::CCard("4d"),
        cardlib::CCard("4c"), cardlib::CCard("as"),
        cardlib::CCard("3h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      true,
    },
    // two pairs versus two pairs
    { { cardlib::CCard("ah"), cardlib::CCard("4d"),
        cardlib::CCard("4c"), cardlib::CCard("as"),
        cardlib::CCard("8h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      { cardlib::CCard("ah"), cardlib::CCard("2d"),
        cardlib::CCard("3c"), cardlib::CCard("as"),
        cardlib::CCard("3h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      false,
    },
    // two pairs versus two pairs
    { { cardlib::CCard("ah"), cardlib::CCard("4d"),
        cardlib::CCard("4c"), cardlib::CCard("as"),
        cardlib::CCard("8h"), cardlib::CCard("td"),
        cardlib::CCard("qc"),
      },
      { cardlib::CCard("ah"), cardlib::CCard("4d"),
        cardlib::CCard("4c"), cardlib::CCard("as"),
        cardlib::CCard("3h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      false,
    },
    // one pair versus  one pair
    { { cardlib::CCard("ah"), cardlib::CCard("2d"),
        cardlib::CCard("4c"), cardlib::CCard("as"),
        cardlib::CCard("8h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      { cardlib::CCard("ah"), cardlib::CCard("2d"),
        cardlib::CCard("5c"), cardlib::CCard("as"),
        cardlib::CCard("8h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      true,
    },
    // one pair versus one pair
    { { cardlib::CCard("ah"), cardlib::CCard("2d"),
        cardlib::CCard("4c"), cardlib::CCard("as"),
        cardlib::CCard("8h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      { cardlib::CCard("ah"), cardlib::CCard("2d"),
        cardlib::CCard("5c"), cardlib::CCard("as"),
        cardlib::CCard("7h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      false,
    },
    // high card versus high card
    { { cardlib::CCard("ah"), cardlib::CCard("2d"),
        cardlib::CCard("4c"), cardlib::CCard("6s"),
        cardlib::CCard("8h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      { cardlib::CCard("ah"), cardlib::CCard("2d"),
        cardlib::CCard("5c"), cardlib::CCard("6s"),
        cardlib::CCard("8h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      true,
    },
    // high card versus high card
    { { cardlib::CCard("ah"), cardlib::CCard("2d"),
        cardlib::CCard("4c"), cardlib::CCard("6s"),
        cardlib::CCard("8h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      { cardlib::CCard("ah"), cardlib::CCard("2d"),
        cardlib::CCard("5c"), cardlib::CCard("7s"),
        cardlib::CCard("8h"), cardlib::CCard("td"),
        cardlib::CCard("jc"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector1, card_vector2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("THandTest(4)::")
    + static_cast<std::string>("TestHand1EqualHand2Various2:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector1 = va_cit->card_vector1;
    card_vector2 = va_cit->card_vector2;

    shouldbe_bool = va_cit->shouldbe;

    hand1_obj.Clear();
    hand1_obj.Calculate(&card_vector1);

    hand2_obj.Clear();
    hand2_obj.Calculate(&card_vector2);

    result_bool = hand1_obj.Hand1EqualHand2(&hand2_obj);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(")  for ")
        + hand1_obj.HandName()
        + static_cast<std::string>(" ")
        + card_obj.CardVectorToString(&card_vector1)
        + static_cast<std::string>(" versus ")
        + hand2_obj.HandName()
        + static_cast<std::string>(" ")
        + card_obj.CardVectorToString(&card_vector2)
        + static_cast<std::string>(" : shouldbe = ")
        + static_cast<std::string>(
            (shouldbe_bool ? "true" : "false"))
        + static_cast<std::string>(", result = ")
        + static_cast<std::string>(
            (result_bool ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THandTest::TestPopulateNthOfAKindMap1() {
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  std::vector<cardlib::CCard> cvector = {
    cardlib::CCard("ah"), cardlib::CCard("ac"),
    cardlib::CCard("kh"), cardlib::CCard("kd"),
    cardlib::CCard("kc"), cardlib::CCard("qc"),
    cardlib::CCard("js") };
  struct atest_struct {
    cardlib::CCard::kcard_value value;
    int64_t shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { cardlib::CCard::kcard_value::kAce, 2 },
    { cardlib::CCard::kcard_value::kKing, 3 },
    { cardlib::CCard::kcard_value::kQueen, 1 },
    { cardlib::CCard::kcard_value::kJack, 1 },
    { cardlib::CCard::kcard_value::kTen, 0 },
    { cardlib::CCard::kcard_value::kNine, 0 }, };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::map<cardlib::CCard::kcard_value, int64_t> output_map;
  cardlib::CCard::kcard_value card_value;
  int64_t shouldbe_count, result_count;
  std::string sub_name = {
    static_cast<std::string>("THandTest(4)::")
    + static_cast<std::string>("TestPopulateNthOfAKind1:") };
  std::string error_message, svector_string;

  hand_obj.PopulateNthOfAKindMap(&cvector, &output_map);

  svector_string = card_obj.CardVectorToString(&cvector);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_value = va_cit->value;
    shouldbe_count = va_cit->shouldbe;

    result_count = output_map[card_value];

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : value = ")
        + std::to_string(card_value)
        + static_cast<std::string>(", count shouldbe = ")
        + std::to_string(shouldbe_count)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_count);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_count, result_count);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

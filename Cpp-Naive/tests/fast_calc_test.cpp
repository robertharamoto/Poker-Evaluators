/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TFastCalcTest implementation                          ###
  ###  namespace handtest                                    ###
  ###                                                        ###
  ###  last updated April 22, 2024                           ###
  ###    changed definitions of bitmask                      ###
  ###                                                        ###
  ###  created April 20, 2024                                ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/fast_calc_test.h"

#include <cstdlib>
#include <cstdint>
#include <cstdio>

#include <map>
#include <vector>
#include <algorithm>
#include <string>

#include "sources/fast_calc.h"
#include "sources/card.h"
#include "sources/hand.h"
#include "sources/utils.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::TFastCalcTest);

namespace handtest {

// #############################################################
// #############################################################
TFastCalcTest::TFastCalcTest() {
}

// #############################################################
// #############################################################
TFastCalcTest::~TFastCalcTest() {
}

// #############################################################
// #############################################################
void TFastCalcTest::setUp() {
}

// #############################################################
// #############################################################
void TFastCalcTest::tearDown() {
}

// #############################################################
// #############################################################
void TFastCalcTest::TestMakeEquivalentRankings1() {
  mylib::CUtils utils_obj;
  cardlib::CFastCalc fastcalc_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card1_vector;
    cardlib::CHand::khand_value hand1_khv;
    std::vector<cardlib::CCard> card2_vector;
    cardlib::CHand::khand_value hand2_khv;
    std::vector<cardlib::CCard> card3_vector;
    cardlib::CHand::khand_value hand3_khv;
    std::vector<cardlib::CCard> card4_vector;
    cardlib::CHand::khand_value hand4_khv;
    std::vector<cardlib::CCard> card5_vector;
    cardlib::CHand::khand_value hand5_khv;

    std::vector<cardlib::CCard> shouldbe_card1_vector;
    cardlib::CHand::khand_value shouldbe_hand1_khv;
    int64_t rank1;
    std::vector<cardlib::CCard> shouldbe_card2_vector;
    cardlib::CHand::khand_value shouldbe_hand2_khv;
    int64_t rank2;
    std::vector<cardlib::CCard> shouldbe_card3_vector;
    cardlib::CHand::khand_value shouldbe_hand3_khv;
    int64_t rank3;
    std::vector<cardlib::CCard> shouldbe_card4_vector;
    cardlib::CHand::khand_value shouldbe_hand4_khv;
    int64_t rank4;
  };
  std::vector<struct atest_struct> test_vector = {
    {
      { cardlib::CCard("ts"), cardlib::CCard("as"),
        cardlib::CCard("qs"), cardlib::CCard("ks"),
        cardlib::CCard("js"), },
      cardlib::CHand::khand_value::kRoyalFlush,
      { cardlib::CCard("jh"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("ah"),
        cardlib::CCard("th"), },
      cardlib::CHand::khand_value::kRoyalFlush,
      { cardlib::CCard("ts"), cardlib::CCard("as"),
        cardlib::CCard("2s"), cardlib::CCard("ks"),
        cardlib::CCard("js"), },
      cardlib::CHand::khand_value::kFlush,
      { cardlib::CCard("tc"), cardlib::CCard("ac"),
        cardlib::CCard("qc"), cardlib::CCard("3c"),
        cardlib::CCard("jc"), },
      cardlib::CHand::khand_value::kFlush,
      { cardlib::CCard("ts"), cardlib::CCard("ad"),
        cardlib::CCard("th"), cardlib::CCard("3c"),
        cardlib::CCard("td"), },
      cardlib::CHand::khand_value::kTriples,

      { cardlib::CCard("ts"), cardlib::CCard("as"),
        cardlib::CCard("qs"), cardlib::CCard("ks"),
        cardlib::CCard("js"), },
      cardlib::CHand::khand_value::kRoyalFlush,
      4,
      { cardlib::CCard("ts"), cardlib::CCard("as"),
        cardlib::CCard("2s"), cardlib::CCard("ks"),
        cardlib::CCard("js"), },
      cardlib::CHand::khand_value::kFlush,
      3,
      { cardlib::CCard("tc"), cardlib::CCard("ac"),
        cardlib::CCard("qc"), cardlib::CCard("3c"),
        cardlib::CCard("jc"), },
      cardlib::CHand::khand_value::kFlush,
      2,
      { cardlib::CCard("ts"), cardlib::CCard("ad"),
        cardlib::CCard("th"), cardlib::CCard("3c"),
        cardlib::CCard("td"), },
      cardlib::CHand::khand_value::kTriples,
      1,
    },
  };
  std::vector<struct atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  std::map<cardlib::CHand, int64_t> input_hand_count_map;
  std::map<cardlib::CHand, int64_t> shouldbe_hand_rank_map;
  std::map<cardlib::CHand, int64_t> result_hand_rank_map;
  std::map<cardlib::CHand, int64_t>::const_iterator mhi_cit;
  cardlib::CHand hand_obj;
  cardlib::CHand::khand_value this_khv;
  int64_t shouldbe_rank, result_rank;
  int64_t shouldbe_msize, result_msize;
  std::string shouldbe_handname, result_handname;
  std::string sub_name = {
    static_cast<std::string>("TFastCalcTest::")
    + static_cast<std::string>("TestMakeEquivalentRankings1:") };
  std::string error_message, err_start;

  // begin the comparison
  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    // hand 1
    input_card_vector = va_cit->card1_vector;
    this_khv = va_cit->hand1_khv;
    hand_obj.Calculate(&input_card_vector);
    input_hand_count_map[hand_obj] = 1L;
    // hand 2
    input_card_vector = va_cit->card2_vector;
    this_khv = va_cit->hand2_khv;
    hand_obj.Calculate(&input_card_vector);
    input_hand_count_map[hand_obj] = 2L;
    // hand 3
    input_card_vector = va_cit->card3_vector;
    this_khv = va_cit->hand3_khv;
    hand_obj.Calculate(&input_card_vector);
    input_hand_count_map[hand_obj] = 3L;
    // hand 4
    input_card_vector = va_cit->card4_vector;
    this_khv = va_cit->hand4_khv;
    hand_obj.Calculate(&input_card_vector);
    input_hand_count_map[hand_obj] = 4L;
    // hand 5
    input_card_vector = va_cit->card5_vector;
    this_khv = va_cit->hand5_khv;
    hand_obj.Calculate(&input_card_vector);
    input_hand_count_map[hand_obj] = 5L;

    // setup shouldbe_hand_rank_map
    // shouldbe hand 1
    input_card_vector = va_cit->shouldbe_card1_vector;
    this_khv = va_cit->shouldbe_hand1_khv;
    hand_obj.Calculate(&input_card_vector);
    shouldbe_hand_rank_map[hand_obj] = va_cit->rank1;
    // shouldbe hand 2
    input_card_vector = va_cit->shouldbe_card2_vector;
    this_khv = va_cit->shouldbe_hand2_khv;
    hand_obj.Calculate(&input_card_vector);
    shouldbe_hand_rank_map[hand_obj] = va_cit->rank2;
    // shouldbe hand 3
    input_card_vector = va_cit->shouldbe_card3_vector;
    this_khv = va_cit->shouldbe_hand3_khv;
    hand_obj.Calculate(&input_card_vector);
    shouldbe_hand_rank_map[hand_obj] = va_cit->rank3;
    // shouldbe hand 4
    input_card_vector = va_cit->shouldbe_card4_vector;
    this_khv = va_cit->shouldbe_hand4_khv;
    hand_obj.Calculate(&input_card_vector);
    shouldbe_hand_rank_map[hand_obj] = va_cit->rank4;

    fastcalc_obj.MakeEquivalentRankings(
        &input_hand_count_map, &result_hand_rank_map);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : ");

    shouldbe_msize =
        static_cast<int64_t>(shouldbe_hand_rank_map.size());
    result_msize =
        static_cast<int64_t>(result_hand_rank_map.size());

    error_message =
        err_start
        + static_cast<std::string>("shouldbe hand_rank_map size = ")
        + std::to_string(shouldbe_msize)
        + static_cast<std::string>(", result size = ")
        + std::to_string(result_msize);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_msize, result_msize);

    for (mhi_cit = shouldbe_hand_rank_map.cbegin();
         mhi_cit != shouldbe_hand_rank_map.cend(); ++mhi_cit) {
      hand_obj = mhi_cit->first;
      shouldbe_rank = mhi_cit->second;
      shouldbe_handname = hand_obj.HandToString();

      this_khv = hand_obj.GetHandValue();

      result_rank = result_hand_rank_map[hand_obj];
      result_handname = hand_obj.HandToString();

      error_message =
          err_start
          + static_cast<std::string>("shouldbe hand value = ")
          + std::to_string(static_cast<int64_t>(this_khv))
          + static_cast<std::string>(", shouldbe hand = ")
          + shouldbe_handname
          + static_cast<std::string>(", result hand = ")
          + result_handname;

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message, shouldbe_handname, result_handname);

      error_message =
          err_start
          + static_cast<std::string>("shouldbe hand = ")
          + shouldbe_handname
          + static_cast<std::string>(", shouldbe hand rank = ")
          + std::to_string(static_cast<int64_t>(shouldbe_rank))
          + static_cast<std::string>(", result hand rank = ")
          + std::to_string(static_cast<int64_t>(result_rank));

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message, shouldbe_rank, result_rank);
    }

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFastCalcTest::TestLoadMaskMap5() {
  mylib::CUtils utils_obj;
  cardlib::CFastCalc fastcalc_obj;
  cardlib::CHand hand_obj;
  struct atest_struct {
    cardlib::CHand::khand_value hand_khv;
    int64_t distinct_hands;
    int64_t frequency;
  };
  std::vector<struct atest_struct> test_vector = {
    { cardlib::CHand::khand_value::kRoyalFlush,
      1L, 4L },
    { cardlib::CHand::khand_value::kStraightFlush,
      9L, 36L },
    { cardlib::CHand::khand_value::kQuads,
      156L, 624L },
    { cardlib::CHand::khand_value::kFullHouse,
      156L, 3744L },
    { cardlib::CHand::khand_value::kFlush,
      1277L, 5108L },
    { cardlib::CHand::khand_value::kStraight,
      10L, 10200L },
    { cardlib::CHand::khand_value::kTriples,
      858L, 54912L },
    { cardlib::CHand::khand_value::kTwoPair,
      858L, 123552L },
    { cardlib::CHand::khand_value::kOnePair,
      2860L, 1098240L },
    { cardlib::CHand::khand_value::kHighCard,
      1277L, 1302540L },
    {cardlib::CHand::khand_value::kHandValueNull,
     7462L, 2598960L },
  };
  std::vector<struct atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::map<cardlib::CHand, int64_t> hand_count_map;
  std::map<cardlib::CHand::khand_value, int64_t>
      subtotal_hand_count_map, subtotal_distinct_count_map;
  std::map<int64_t, int64_t>::const_iterator mii_cit;
  std::vector<cardlib::CHand::khand_value> key_khv_vector;
  cardlib::CHand::khand_value this_khv;
  int64_t shouldbe_distinct_count(0L), shouldbe_hand_count(0L);
  int64_t result_distinct_count(0L), result_hand_count(0L);
  int64_t total_distinct_count, total_hand_count;
  std::string sname;
  std::string sub_name = {
    static_cast<std::string>("TFastCalcTest::")
    + static_cast<std::string>("TestLoadMaskMap5:") };
  std::string error_message, err_start;

  // load up data
  hand_count_map.clear();
  fastcalc_obj.LoadMaskMap5Cards(&hand_count_map);

  key_khv_vector.clear();
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    this_khv = va_cit->hand_khv;
    key_khv_vector.push_back(this_khv);
  }

  fastcalc_obj.SubtotalCardHandCounts(
      &key_khv_vector, &hand_count_map,
      &subtotal_hand_count_map,
      &subtotal_distinct_count_map,
      &total_hand_count,
      &total_distinct_count);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    this_khv = va_cit->hand_khv;
    shouldbe_distinct_count = va_cit->distinct_hands;
    shouldbe_hand_count = va_cit->frequency;

    hand_obj.SetHandName(this_khv);
    sname = hand_obj.HandName();

    if (this_khv != cardlib::CHand::khand_value::kHandValueNull) {
      // subtotal the equivalent hand counts
      result_distinct_count = subtotal_distinct_count_map[this_khv];
      result_hand_count = subtotal_hand_count_map[this_khv];
    } else {
      sname = static_cast<std::string>("total");
      result_distinct_count = total_distinct_count;
      result_hand_count = total_hand_count;
    }

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") hand = ")
        + sname
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>(", shouldbe equivalent hands = ")
        + std::to_string(shouldbe_distinct_count)
        + static_cast<std::string>(", result equivalent hands = ")
        + std::to_string(result_distinct_count);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_distinct_count, result_distinct_count);

    error_message =
        err_start
        + static_cast<std::string>(", shouldbe hand count = ")
        + std::to_string(shouldbe_hand_count)
        + static_cast<std::string>(", result hand count = ")
        + std::to_string(result_hand_count);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_hand_count, result_hand_count);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFastCalcTest::TestSubtotalValueRankCountstructs() {
  mylib::CUtils utils_obj;
  cardlib::CFastCalc fastcalc_obj;
  cardlib::CHand hand_obj;
  struct atest_struct {
    cardlib::CHand::khand_value hand_khv;
    int64_t distinct_hands;
    int64_t frequency;
  };
  std::vector<struct atest_struct> test_vector = {
    { cardlib::CHand::khand_value::kRoyalFlush,
      1L, 4L },
    { cardlib::CHand::khand_value::kStraightFlush,
      9L, 36L },
    { cardlib::CHand::khand_value::kQuads,
      156L, 624L },
    { cardlib::CHand::khand_value::kFullHouse,
      156L, 3744L },
    { cardlib::CHand::khand_value::kFlush,
      1277L, 5108L },
    { cardlib::CHand::khand_value::kStraight,
      10L, 10200L },
    { cardlib::CHand::khand_value::kTriples,
      858L, 54912L },
    { cardlib::CHand::khand_value::kTwoPair,
      858L, 123552L },
    { cardlib::CHand::khand_value::kOnePair,
      2860L, 1098240L },
    { cardlib::CHand::khand_value::kHighCard,
      1277L, 1302540L },
    {cardlib::CHand::khand_value::kHandValueNull,
     7462L, 2598960L },
  };
  std::vector<struct atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::map<cardlib::CHand, int64_t> hand_count_map, hand_rank_map;
  std::map<cardlib::CHand::khand_value, int64_t>
      subtotal_hand_count_map, subtotal_distinct_count_map;
  std::map<int64_t, int64_t>::const_iterator mii_cit;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      flushes_vrc_map, nonflushes_vrc_map;
  std::vector<cardlib::CHand::khand_value> key_khv_vector;
  cardlib::CHand::khand_value this_khv;
  int64_t shouldbe_distinct_count(0L), shouldbe_hand_count(0L);
  int64_t result_distinct_count(0L), result_hand_count(0L);
  int64_t total_distinct_count, total_hand_count;
  std::string sname;
  std::string sub_name = {
    static_cast<std::string>("TFastCalcTest::")
    + static_cast<std::string>("SubtotalValueRankCountstructs:") };
  std::string error_message, err_start;

  // load up data
  hand_count_map.clear();
  fastcalc_obj.LoadMaskMap5Cards(&hand_count_map);
  fastcalc_obj.MakeEquivalentRankings(
      &hand_count_map, &hand_rank_map);
  fastcalc_obj.CombineMaps(
      &hand_count_map, &hand_rank_map,
      &flushes_vrc_map, &nonflushes_vrc_map);

  key_khv_vector.clear();
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    this_khv = va_cit->hand_khv;
    key_khv_vector.push_back(this_khv);
  }

  fastcalc_obj.SubtotalValueRankCountstructs(
      &key_khv_vector,
      &flushes_vrc_map,
      &nonflushes_vrc_map,
      &subtotal_hand_count_map,
      &subtotal_distinct_count_map,
      &total_hand_count,
      &total_distinct_count);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    this_khv = va_cit->hand_khv;
    shouldbe_distinct_count = va_cit->distinct_hands;
    shouldbe_hand_count = va_cit->frequency;

    hand_obj.SetHandName(this_khv);
    sname = hand_obj.HandName();

    if (this_khv != cardlib::CHand::khand_value::kHandValueNull) {
      // subtotal the equivalent hand counts
      result_distinct_count = subtotal_distinct_count_map[this_khv];
      result_hand_count = subtotal_hand_count_map[this_khv];
    } else {
      sname = static_cast<std::string>("total");
      result_distinct_count = total_distinct_count;
      result_hand_count = total_hand_count;
    }

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") hand = ")
        + sname
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>(", shouldbe equivalent hands = ")
        + std::to_string(shouldbe_distinct_count)
        + static_cast<std::string>(", result equivalent hands = ")
        + std::to_string(result_distinct_count);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_distinct_count, result_distinct_count);

    error_message =
        err_start
        + static_cast<std::string>(", shouldbe hand count = ")
        + std::to_string(shouldbe_hand_count)
        + static_cast<std::string>(", result hand count = ")
        + std::to_string(result_hand_count);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_hand_count, result_hand_count);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFastCalcTest::TestLoadMaskMap7() {
  mylib::CUtils utils_obj;
  cardlib::CFastCalc fastcalc_obj;
  cardlib::CHand hand_obj;
  struct atest_struct {
    cardlib::CHand::khand_value hand_khv;
    int64_t distinct_hands;
    int64_t frequency;
  };
  std::vector<struct atest_struct> test_vector = {
    { cardlib::CHand::khand_value::kRoyalFlush,
      1L, 4324L },
    { cardlib::CHand::khand_value::kStraightFlush,
      9L, 37260L },
    { cardlib::CHand::khand_value::kQuads,
      156L, 224848L },
    { cardlib::CHand::khand_value::kFullHouse,
      156L, 3473184L },
    { cardlib::CHand::khand_value::kFlush,
      1277L, 4047644L },
    { cardlib::CHand::khand_value::kStraight,
      10L, 6180020L },
    { cardlib::CHand::khand_value::kTriples,
      575L, 6461620L },
    { cardlib::CHand::khand_value::kTwoPair,
      763L, 31433400L },
    { cardlib::CHand::khand_value::kOnePair,
      1470L, 58627800L },
    { cardlib::CHand::khand_value::kHighCard,
      407L, 23294460L },
    {cardlib::CHand::khand_value::kHandValueNull,
     4824L, 133784560L },
  };
  std::vector<struct atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::map<cardlib::CHand, int64_t> hand_count_map;
  std::map<cardlib::CHand::khand_value, int64_t>
      subtotal_hand_count_map, subtotal_distinct_count_map;
  std::map<int64_t, int64_t>::const_iterator mii_cit;
  std::vector<cardlib::CHand::khand_value> key_khv_vector;
  cardlib::CHand::khand_value this_khv;
  int64_t shouldbe_distinct_count(0L), shouldbe_hand_count(0L);
  int64_t result_distinct_count(0L), result_hand_count(0L);
  int64_t total_distinct_count, total_hand_count;
  std::string sname;
  std::string sub_name = {
    static_cast<std::string>("TFastCalcTest::")
    + static_cast<std::string>("TestLoadMaskMap7:") };
  std::string error_message, err_start;

  hand_count_map.clear();

  // load up data
  fastcalc_obj.LoadMaskMap7Cards(&hand_count_map);

  key_khv_vector.clear();
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    this_khv = va_cit->hand_khv;
    key_khv_vector.push_back(this_khv);
  }

  fastcalc_obj.SubtotalCardHandCounts(
      &key_khv_vector, &hand_count_map,
      &subtotal_hand_count_map,
      &subtotal_distinct_count_map,
      &total_hand_count,
      &total_distinct_count);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    this_khv = va_cit->hand_khv;
    shouldbe_distinct_count = va_cit->distinct_hands;
    shouldbe_hand_count = va_cit->frequency;

    hand_obj.SetHandName(this_khv);
    sname = hand_obj.HandName();

    if (this_khv != cardlib::CHand::khand_value::kHandValueNull) {
      // subtotal the equivalent hand counts
      result_distinct_count = subtotal_distinct_count_map[this_khv];
      result_hand_count = subtotal_hand_count_map[this_khv];
    } else {
      sname = static_cast<std::string>("total");
      result_distinct_count = total_distinct_count;
      result_hand_count = total_hand_count;
    }

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") hand = ")
        + sname
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>(", shouldbe equivalent hands = ")
        + std::to_string(shouldbe_distinct_count)
        + static_cast<std::string>(", result equivalent hands = ")
        + std::to_string(result_distinct_count);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_distinct_count, result_distinct_count);

    error_message =
        err_start
        + static_cast<std::string>(", shouldbe hand count = ")
        + std::to_string(shouldbe_hand_count)
        + static_cast<std::string>(", result hand count = ")
        + std::to_string(result_hand_count);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_hand_count, result_hand_count);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

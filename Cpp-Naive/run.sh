#! /bin/bash


FILENAME=`date '+%B-%d-%Y' | awk '{ printf "flag-o3-%s.txt", $0; }' | tr '[:upper:]' '[:lower:]'`
### FILENAME="out.txt"

echo -n "file = ${FILENAME}, starting at "
date '+%A %B %d, %Y  %T'

date '+%A %B %d, %Y  %T' > ${FILENAME}


./test1 >> ${FILENAME} 2>&1

date '+%A %B %d, %Y  %T' >> ${FILENAME}

{ time ./main1 2>&1 ; } >> ${FILENAME} 2>&1

date '+%A %B %d, %Y  %T' >> ${FILENAME}

cat ${FILENAME}

date '+%A %B %d, %Y  %T'

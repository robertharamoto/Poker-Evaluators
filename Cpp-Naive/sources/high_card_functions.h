#ifndef SOURCES_HIGH_CARD_FUNCTIONS_H_
#define SOURCES_HIGH_CARD_FUNCTIONS_H_

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CHighCardFunctions definition                         ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###                                                        ###
  ###  created March 18, 2024                                ###
  ###    used new single integer bit scheme                  ###
  ###    from http://suffe.cool/poker/evaluator.html         ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <vector>
#include <map>
#include <string>

#include "sources/card.h"
#include "sources/hand.h"

namespace cardlib {
/*
Single card 32-bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

class CHighCardFunctions {
 public:
  CHighCardFunctions();
  ~CHighCardFunctions();

  // do not allow copying nor moving
  CHighCardFunctions(const CHighCardFunctions&) = delete;
  CHighCardFunctions(CHighCardFunctions&&) = delete;
  CHighCardFunctions& operator=(const CHighCardFunctions&) = delete;
  CHighCardFunctions& operator=(CHighCardFunctions&&) = delete;

  // hand evaluation support routines
  void Form5CardHand(
      const std::vector<cardlib::CCard> *input_vector,
      int64_t *output_order,
      int64_t *output7_signature,
      int64_t *output5_signature,
      cardlib::CHand::khand_value *output_handvalue) const;

  void Form7CardHand(
      const std::vector<cardlib::CCard> *input_vector,
      int64_t *output_order,
      int64_t *output7_signature,
      int64_t *output5_signature,
      cardlib::CHand::khand_value *output_handvalue) const;

  void IsHighCard(
      const std::vector<cardlib::CCard> *input_vector,
      int64_t *output_order,
      int64_t *output7_signature,
      int64_t *output5_signature,
      cardlib::CHand::khand_value *output_handvalue) const;

  bool HighCardsGreaterHighCards(
      const cardlib::CHand *chand1,
      const cardlib::CHand *chand2) const;

  bool HighCardsLessThanHighCards(
      const cardlib::CHand *chand1,
      const cardlib::CHand *chand2) const;

  bool HighCardsEqualHighCards(
      const cardlib::CHand *chand1,
      const cardlib::CHand *chand2) const;
};  // class CHighCardFunctions

}  // namespace cardlib

#endif  // SOURCES_HIGH_CARD_FUNCTIONS_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  main program                                          ###
  ###                                                        ###
  ###  last updated September 18, 2024                       ###
  ###    new bitmask definitions                             ###
  ###                                                        ###
  ###  updated July 16, 2022                                 ###
  ###                                                        ###
  ###  updated January 20, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cstdio>
#include <cstdint>
#include <cstdlib>

#include <string>
#include <vector>
#include <map>
#include <chrono>

#include "sources/utils.h"
#include "sources/argopts.h"
#include "sources/configure.h"
#include "sources/fast_calc.h"

int main(int argc, char **argv) {
    void run_program(
        const std::string *title_string,
        mylib::CArgOpts *argopts_obj,
        std::map<std::string, std::string> *config_string_map);
    void add_in_valid_options(
        int argc,
        char **argv,
        std::string *version_string,
        mylib::CArgOpts *argopts_obj);
    void get_config_file_options(
        int argc,
        char **argv,
        std::string *config_filename,
        std::map<std::string, std::string> *config_string_map,
        std::map<std::string, std::string> *config_comments_map);
    mylib::CArgOpts argopts_obj;  // process argument switches, -h, -v,...
    mylib::CUtils utils_obj;
    std::map<std::string, std::string>
        config_string_map, config_comments_map;
    std::string version_string = "2024-09-18";
    std::string title_string;
    std::string config_filename("init0.config");

    title_string =
        static_cast<std::string>(
            "Pocket pair comparison program (version ")
        + version_string
        + static_cast<std::string>(")");

    // load up valid command-line options into argopts_obj
    add_in_valid_options(
        argc, argv, &version_string, &argopts_obj);

    // retreive configuration options from config file
    get_config_file_options(
        argc, argv, &config_filename,
        &config_string_map, &config_comments_map);

    // ###########################################################
    // ###########################################################
    // run main program
    run_program(&title_string,
                &argopts_obj,
                &config_string_map);

    return 0;
}

// #############################################################
// #############################################################
void run_program(
    const std::string *title_string,
    mylib::CArgOpts *argopts_obj,
    std::map<std::string, std::string> *config_string_map) {
    void get_command_line_config_options(
        bool *write_enum_flag,
        std::string *enum_fname,
        std::string *pockets1,
        std::string *pockets2,
        cardlib::CCard *c1,
        cardlib::CCard *c2,
        cardlib::CCard *c3,
        cardlib::CCard *c4,
        mylib::CArgOpts *argopts_obj,
        std::map<std::string, std::string> *config_string_map);
    cardlib::CFastCalc fastcalc_obj;
    mylib::CUtils utils_obj;
    bool write_enum_flag;
    std::string enum_fname;
    std::string pockets1_string, pockets2_string;
    cardlib::CCard c1, c2, c3, c4;
    std::chrono::time_point<std::chrono::steady_clock,
                            std::chrono::duration<double>>
        start_time, end_time;

    start_time = std::chrono::steady_clock::now();

    get_command_line_config_options(
        &write_enum_flag, &enum_fname,
        &pockets1_string, &pockets2_string,
        &c1, &c2, &c3, &c4,
        argopts_obj, config_string_map);

    printf("%s\n", title_string->c_str());
    printf("current date = %s\n",
           utils_obj.CurrentDateTime().c_str());

    printf("(%s, %s) vs (%s, %s)\n",
           c1.CardToString().c_str(),
           c2.CardToString().c_str(),
           c3.CardToString().c_str(),
           c4.CardToString().c_str());
    fflush(stdout);

    if (write_enum_flag == true) {
        printf("output to file %s\n", enum_fname.c_str());
        fflush(stdout);
    }

    fastcalc_obj.ComputeAll(write_enum_flag,
                            &enum_fname,
                            &c1, &c2, &c3, &c4);


    end_time = std::chrono::steady_clock::now();

    printf("total elasped time = %s\n",
           utils_obj.ClockDuration(
               &end_time, &start_time).c_str());
    printf("completed program at %s\n",
           utils_obj.CurrentDateTime().c_str());
    fflush(stdout);

    return;
}

// #############################################################
// #############################################################
inline void add_in_valid_options(
    int argc,
    char **argv,
    std::string *version_string,
    mylib::CArgOpts *argopts_obj) {
    void process_arguments(mylib::CArgOpts *argopts_obj);
    std::string opt1, opt2, opt3, opt4, opt5;

    opt1 = static_cast<std::string>(argv[0]);
    argopts_obj->SetName(&opt1);
    argopts_obj->SetVersion(version_string);

    opt1 = static_cast<std::string>("-c");
    opt2 = static_cast<std::string>("--config");
    opt3 = static_cast<std::string>("configuration file name");
    opt4 = static_cast<std::string>("init0.config");
    opt5 = static_cast<std::string>("");
    argopts_obj->AddValidOptionWithArgument(
        &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

    opt1 = static_cast<std::string>("-w");
    opt2 = static_cast<std::string>("--write-enum");
    opt3 = static_cast<std::string>("write enumeration file (boolean)");
    argopts_obj->AddValidOptionNoArgument(
        &opt1, &opt2, &opt3);

    opt1 = static_cast<std::string>("-e");
    opt2 = static_cast<std::string>("--enum-file");
    opt3 = static_cast<std::string>("enumeration filename");
    opt4 = static_cast<std::string>("enum.txt");
    opt5 = static_cast<std::string>("");
    argopts_obj->AddValidOptionWithArgument(
        &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

    opt1 = static_cast<std::string>("-p");
    opt2 = static_cast<std::string>("--pockets1");
    opt3 = static_cast<std::string>("pocket pair for player 1");
    opt4 = static_cast<std::string>("as ac");
    opt5 = static_cast<std::string>("");
    argopts_obj->AddValidOptionWithArgument(
        &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

    opt1 = static_cast<std::string>("-q");
    opt2 = static_cast<std::string>("--pockets2");
    opt3 = static_cast<std::string>("pocket pair for player 2");
    opt4 = static_cast<std::string>("4d 5d");
    opt5 = static_cast<std::string>("");
    argopts_obj->AddValidOptionWithArgument(
        &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

    // #############################################################
    // #############################################################
    // process argument switches, -h, -v,...
    argopts_obj->ProcessOptions(argc, (const char**)argv);

    process_arguments(argopts_obj);  // handle -h and -v

    return;
}

// #############################################################
// #############################################################
inline void get_config_file_options(
    int argc,
    char **argv,
    std::string *config_filename,
    std::map<std::string, std::string> *config_string_map,
    std::map<std::string, std::string> *config_comments_map) {
    mylib::CConfigure config_obj;
    mylib::CUtils utils_obj;

    // find init configuration file
    if (argc > 0) {
        for (int ii = 1; ii < argc; ++ii) {
            if (utils_obj.FileExists(
                    static_cast<const char *>(argv[ii]))
                == true) {
                *config_filename = argv[ii];
                break;
            }
        }
    }

    if (utils_obj.FileExists(
            static_cast<const char *>(
                config_filename->c_str()))) {
        config_obj.ReadConfig(config_filename,
                              config_string_map,
                              config_comments_map);
    }

    return;
}

// #############################################################
// #############################################################
inline int64_t command_config_option_int64(
    const std::string *option_flag_string,
    const std::string *config_flag_string,
    const std::string *default_value,
    mylib::CArgOpts *argopts_obj,
    std::map<std::string, std::string> *config_string_map) {
    mylib::CUtils utils_obj;
    std::string num_string;
    int64_t ll_result;
    bool bresult;

    bresult = argopts_obj->OptionEntered(option_flag_string);
    if (bresult == true) {
        num_string =
            argopts_obj->GetOptionArgument(option_flag_string);
    }

    if (num_string.size() <= 0) {
        num_string =
            (*config_string_map)[*config_flag_string];
        if (num_string.size() <= 0) {
            num_string = *default_value;
        }
    }

    ll_result =
        utils_obj.StringToInteger(&num_string);

    return ll_result;
}

// #############################################################
// #############################################################
inline std::string command_config_option_string(
    const std::string *option_flag_string,
    const std::string *config_flag_string,
    const std::string *default_value,
    mylib::CArgOpts *argopts_obj,
    std::map<std::string, std::string> *config_string_map) {
    mylib::CUtils utils_obj;
    std::string this_string;
    bool bresult;

    bresult = argopts_obj->OptionEntered(option_flag_string);
    if (bresult == true) {
        this_string =
            argopts_obj->GetOptionArgument(option_flag_string);
    }

    if (this_string.size() <= 0) {
        this_string =
            (*config_string_map)[*config_flag_string];
        if (this_string.size() <= 0) {
            this_string = *default_value;
        }
    }

    return this_string;
}

// #############################################################
// #############################################################
inline bool command_config_option_bool(
    const std::string *option_flag_string,
    const std::string *config_flag_string,
    const std::string *default_value,
    mylib::CArgOpts *argopts_obj,
    std::map<std::string, std::string> *config_string_map) {
    mylib::CUtils utils_obj;
    std::string bool_string, tmp2_string;
    bool bresult, init_flag;

    bresult = argopts_obj->OptionEntered(option_flag_string);
    if (bresult == true) {
        bool_string =
            argopts_obj->GetOptionArgument(option_flag_string);
    }

    if (bool_string.size() <= 0) {
        bool_string =
            (*config_string_map)[*config_flag_string];
        if (bool_string.size() <= 0) {
            bool_string = *default_value;
        }
    }
    if (bool_string.size() > 0) {
        tmp2_string =
            utils_obj.LowerCase(&bool_string);
        if (tmp2_string.compare("true") == 0) {
            init_flag = true;
        } else {
            init_flag = false;
        }
    } else {
        init_flag = true;
    }

    return init_flag;
}

// #############################################################
// #############################################################
inline void check_for_valid_input_cards(
    const std::string *player_string,
    const std::string *player_card1_string,
    const std::string *player_card2_string,
    cardlib::CCard *c1,
    cardlib::CCard *c2) {
    std::string stmp;
    size_t found1, found2;

    if (player_card1_string->size() > 0) {
        c1->Set(player_card1_string);
    } else {
        printf("%s input card 2 = %s : error.  exiting program...\n",
               player_string->c_str(),
               player_card1_string->c_str());
        exit(0);
    }

    if (player_card2_string->size() > 0) {
        c2->Set(player_card2_string);
    } else {
        printf("%s input card 2 = %s : error.  exiting program...\n",
               player_string->c_str(),
               player_card1_string->c_str());
        exit(0);
    }

    stmp = c1->CardToString();
    found1 = stmp.find("error");

    if (found1 != std::string::npos) {
        printf("%s input card 1 = %s : error.  exiting program...\n",
               player_string->c_str(),
               player_card1_string->c_str());
        exit(0);
    }

    stmp = c2->CardToString();
    found2 = stmp.find("error");
    if (found2 != std::string::npos) {
        printf("%s input card 2 = %s : error.  exiting program...\n",
               player_string->c_str(),
               player_card2_string->c_str());
        exit(0);
    }

    return;
}

// #############################################################
// #############################################################
inline void get_command_line_config_options(
    bool *write_enum_flag,
    std::string *enum_fname,
    std::string *pockets1_string,
    std::string *pockets2_string,
    cardlib::CCard *c1,
    cardlib::CCard *c2,
    cardlib::CCard *c3,
    cardlib::CCard *c4,
    mylib::CArgOpts *argopts_obj,
    std::map<std::string, std::string> *config_string_map) {
    void check_for_valid_input_cards(
        const std::string *player_string,
        const std::string *player_card1_string,
        const std::string *player_card2_string,
        cardlib::CCard *c1,
        cardlib::CCard *c2);
    mylib::CUtils utils_obj;
    std::vector<std::string> ptmp;
    std::string stmp;
    std::string p1a, p1b, p2a, p2b;
    std::string opt1, opt2, opt3, opt4, opt5;

    opt1 = static_cast<std::string>("--write-enum");
    opt2 = static_cast<std::string>("write-enum");
    opt3 = static_cast<std::string>("false");
    *write_enum_flag = command_config_option_bool(
        &opt1, &opt2, &opt3,
        argopts_obj, config_string_map);

    opt1 = static_cast<std::string>("--enum-file");
    opt2 = static_cast<std::string>("enum-file");
    opt3 = static_cast<std::string>("def-enum.txt");
    *enum_fname = command_config_option_string(
        &opt1, &opt2, &opt3,
        argopts_obj, config_string_map);

    // find pocket pairs for player 1
    opt1 = static_cast<std::string>("--pockets1");
    opt2 = static_cast<std::string>("pockets1");
    opt3 = static_cast<std::string>("as ac");
    *pockets1_string = command_config_option_string(
        &opt1, &opt2, &opt3,
        argopts_obj, config_string_map);

    ptmp.clear();
    utils_obj.StringTokenize(pockets1_string, ' ', &ptmp);
    p1a = ptmp[0];
    p1b = ptmp[1];
    stmp = static_cast<std::string>("player 1");
    check_for_valid_input_cards(&stmp, &p1a, &p1b, c1, c2);

    // find pocket pairs for player 2
    opt1 = static_cast<std::string>("--pockets2");
    opt2 = static_cast<std::string>("pockets2");
    opt3 = static_cast<std::string>("4d 5d");
    *pockets2_string = command_config_option_string(
        &opt1, &opt2, &opt3,
        argopts_obj, config_string_map);

    ptmp.clear();
    utils_obj.StringTokenize(pockets2_string, ' ', &ptmp);
    p2a = ptmp[0];
    p2b = ptmp[1];
    stmp = static_cast<std::string>("player 2");
    check_for_valid_input_cards(&stmp, &p2a, &p2b, c3, c4);

    return;
}

// #############################################################
// #############################################################
void process_arguments(mylib::CArgOpts *argopts_obj) {
    std::string opt1;
    bool bresult;

    opt1 = static_cast<std::string>("--help");
    bresult = argopts_obj->OptionEntered(&opt1);
    if (bresult == true) {
        argopts_obj->DisplayUsage();
        argopts_obj->Clear();
        ::exit(0);
    }

    opt1 = static_cast<std::string>("--version");
    bresult = argopts_obj->OptionEntered(&opt1);
    if (bresult == true) {
        argopts_obj->DisplayVersion();
        argopts_obj->Clear();
        ::exit(0);
    }

    return;
}
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

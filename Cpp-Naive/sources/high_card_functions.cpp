/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CHighCardFunctions implementation                     ###
  ###  namespace cardlib                                     ###
  ###                                                        ###
  ###  last updated April 18, 2024                           ###
  ###                                                        ###
  ###  created March 18, 2024                                ###
  ###    used new single integer bit scheme                  ###
  ###    from http://suffe.cool/poker/evaluator.html         ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/high_card_functions.h"

#include <cstdint>

#include <map>
#include <algorithm>
#include <vector>
#include <string>
#include <typeinfo>

#include "sources/card.h"
#include "sources/hand.h"


namespace cardlib {
/*
Single card 32-bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

// #############################################################
// #############################################################
CHighCardFunctions::CHighCardFunctions() {
}

// #############################################################
// #############################################################
CHighCardFunctions::~CHighCardFunctions() {
}

// #############################################################
// #############################################################
// assumes we only have 5-cards
void CHighCardFunctions::Form5CardHand(
    const std::vector<cardlib::CCard> *input_vector,
    int64_t *output_order,
    int64_t *output7_signature,
    int64_t *output5_signature,
    cardlib::CHand::khand_value *output_handvalue) const {
  std::vector<cardlib::CCard>::const_iterator vc_cit;
  int64_t this_order, hand_order;
  int64_t this_prime, prime7_signature, prime5_signature;

  // smallest value shouldbe large so it's easy to find the smallest
  hand_order = 0L;
  prime7_signature = prime5_signature = 1L;
  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    this_prime = vc_cit->GetCardPrimeValue();
    prime7_signature *= this_prime;
    prime5_signature *= this_prime;

    this_order = vc_cit->GetCardOrder();
    hand_order = hand_order | this_order;
  }

  *output_order = hand_order;
  *output7_signature = prime7_signature;
  *output5_signature = prime5_signature;
  *output_handvalue =
      cardlib::CHand::khand_value::kHighCard;

  return;
}

// #############################################################
// #############################################################
void CHighCardFunctions::Form7CardHand(
    const std::vector<cardlib::CCard> *input_vector,
    int64_t *output_order,
    int64_t *output7_signature,
    int64_t *output5_signature,
    cardlib::CHand::khand_value *output_handvalue) const {
  std::vector<cardlib::CCard>::const_iterator vc_cit;
  int64_t ncounter, number_cards_constant(5L);
  cardlib::CCard::kcard_value
      this_value, smallest_value, second_smallest_value;
  int64_t this_order, hand_order;
  int64_t this_prime, prime7_signature, prime5_signature;

  // smallest value shouldbe large so it's easy to find the smallest
  smallest_value = second_smallest_value =
      static_cast<cardlib::CCard::kcard_value>(
          static_cast<int64_t>(cardlib::CCard::kcard_value::kAce)
          * 10L);
  prime7_signature = 1L;
  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    this_value = vc_cit->GetCardValue();
    this_prime = vc_cit->GetCardPrimeValue();
    prime7_signature *= this_prime;

    if (this_value < smallest_value) {
      second_smallest_value = smallest_value;
      smallest_value = this_value;
    } else if (this_value < second_smallest_value) {
      second_smallest_value = this_value;
    }
  }

  ncounter = 0L;
  hand_order = 0L;
  prime5_signature = 1L;
  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    this_value = vc_cit->GetCardValue();
    this_prime = vc_cit->GetCardPrimeValue();

    if ((this_value != smallest_value)
        && (this_value != second_smallest_value)) {
      if (ncounter < number_cards_constant) {
        ++ncounter;
        prime5_signature *= this_prime;

        this_order = vc_cit->GetCardOrder();
        hand_order = hand_order | this_order;
      }
    }
  }

  *output_order = hand_order;
  *output7_signature = prime7_signature;
  *output5_signature = prime5_signature;
  *output_handvalue =
      cardlib::CHand::khand_value::kHighCard;

  return;
}

// #############################################################
// #############################################################
void CHighCardFunctions::IsHighCard(
    const std::vector<cardlib::CCard> *input_vector,
    int64_t *output_order,
    int64_t *output7_signature,
    int64_t *output5_signature,
    cardlib::CHand::khand_value *output_handvalue) const {
  int64_t nsize;

  nsize = static_cast<int64_t>(input_vector->size());

  if (nsize >= 7L) {
    // 7-card hands
    Form7CardHand(
        input_vector, output_order,
        output7_signature, output5_signature,
        output_handvalue);
  } else {
    // 5-card hands
    Form5CardHand(
        input_vector, output_order,
        output7_signature, output5_signature,
        output_handvalue);
  }

  return;
}

// #############################################################
// #############################################################
bool CHighCardFunctions::HighCardsGreaterHighCards(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  int64_t hand_order_1(0L), hand_order_2(0L);

  hand_order_1 = chand1->GetHandOrder();
  hand_order_2 = chand2->GetHandOrder();

  return (hand_order_1 > hand_order_2);
}

// #############################################################
// #############################################################
bool CHighCardFunctions::HighCardsLessThanHighCards(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  int64_t hand_order_1(0L), hand_order_2(0L);

  hand_order_1 = chand1->GetHandOrder();
  hand_order_2 = chand2->GetHandOrder();

  return (hand_order_1 < hand_order_2);
}

// #############################################################
// #############################################################
bool CHighCardFunctions::HighCardsEqualHighCards(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  int64_t hand_order_1(0L), hand_order_2(0L);

  hand_order_1 = chand1->GetHandOrder();
  hand_order_2 = chand2->GetHandOrder();

  return (hand_order_1 == hand_order_2);
}

}  // namespace cardlib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

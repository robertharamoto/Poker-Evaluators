/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CFlushFunctions  implementation                       ###
  ###  namespace cardlib                                     ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###                                                        ###
  ###  created March 18, 2024                                ###
  ###    used new single integer bit scheme                  ###
  ###    from http://suffe.cool/poker/evaluator.html         ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/flush_functions.h"

#include <cstdio>
#include <cstdint>
#include <map>
#include <algorithm>
#include <vector>
#include <string>
#include <typeinfo>

#include "sources/card.h"
#include "sources/hand.h"

namespace cardlib {
/*
Single card 32-bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

// #############################################################
// #############################################################
CFlushFunctions::CFlushFunctions() {
}

// #############################################################
// #############################################################
CFlushFunctions::~CFlushFunctions() {
}

// #############################################################
// #############################################################
void CFlushFunctions::MakeSuitCountMap(
    const std::vector<cardlib::CCard> *input_vector,
    std::map<cardlib::CCard::kcard_suit, int64_t> *output_map) const {
  cardlib::CCard::kcard_suit this_suit;
  std::vector<cardlib::CCard>::const_iterator vc_cit;

  output_map->clear();

  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    this_suit = vc_cit->GetCardSuit();
    (*output_map)[this_suit]++;
  }

  return;
}

// #############################################################
// #############################################################
cardlib::CCard::kcard_suit CFlushFunctions::CanFormAFlush(
    const std::map<cardlib::CCard::kcard_suit, int64_t>
    *suit_count_map) const {
  std::map<cardlib::CCard::kcard_suit, int64_t>::const_iterator
      mcs_cit;
  cardlib::CCard::kcard_suit cs;
  int64_t count(0L), flush_count_constant(5L);

  for (mcs_cit = suit_count_map->cbegin();
       mcs_cit != suit_count_map->cend(); ++mcs_cit) {
    cs = mcs_cit->first;
    count = mcs_cit->second;

    if (count >= flush_count_constant) {
      return cs;
    }
  }

  return CCard::kcard_suit::kNullSuit;
}

// #############################################################
// #############################################################
cardlib::CCard::kcard_suit CFlushFunctions::GetFlushSuit(
    const std::vector<cardlib::CCard> *input_vector) const {
  std::vector<cardlib::CCard>::const_iterator vc_cit;
  int64_t nspades(0L), nhearts(0L);
  int64_t ndiamonds(0L), nclubs(0L);
  int64_t count_const(5L);
  cardlib::CCard::kcard_suit tmp_cs;

  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    tmp_cs = vc_cit->GetCardSuit();

    if (tmp_cs == cardlib::CCard::kcard_suit::kSpades) {
      nspades++;
    } else if (tmp_cs == cardlib::CCard::kcard_suit::kHearts) {
      nhearts++;
    } else if (tmp_cs == cardlib::CCard::kcard_suit::kDiamonds) {
      ndiamonds++;
    } else if (tmp_cs == cardlib::CCard::kcard_suit::kClubs) {
      nclubs++;
    }
  }

  if (nspades >= count_const) {
    return cardlib::CCard::kcard_suit::kSpades;
  } else if (nhearts >= count_const) {
    return cardlib::CCard::kcard_suit::kHearts;
  } else if (ndiamonds >= count_const) {
    return cardlib::CCard::kcard_suit::kDiamonds;
  } else if (nclubs >= count_const) {
    return cardlib::CCard::kcard_suit::kClubs;
  }

  return cardlib::CCard::kcard_suit::kNullSuit;
}

// #############################################################
// #############################################################
void CFlushFunctions::IsFlush(
    cardlib::CCard::kcard_suit flush_suit,
    const std::vector<cardlib::CCard> *input_vector,
    int64_t *output_hand_order,
    int64_t *output7_signature,
    int64_t *output5_signature,
    cardlib::CHand::khand_value *output_handvalue) const {
  std::vector<cardlib::CCard>::const_iterator vc_cit;
  std::vector<cardlib::CCard> tmp_vector;
  cardlib::CCard this_card;
  cardlib::CCard::kcard_suit this_suit;
  int64_t this_order, ccount;
  cardlib::CCard::kcard_value this_value;
  cardlib::CCard::kcard_value
      smallest_value, second_smallest_value;
  int64_t max_count_const(5L);
  int64_t prime7_signature, prime5_signature;
  int64_t this_prime, order;
  // card values from 0 to 12, so card_value = 1000L is an initial value

  smallest_value = second_smallest_value =
      static_cast<cardlib::CCard::kcard_value>(1000L);
  ccount = 0L;
  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    this_value = vc_cit->GetCardValue();
    this_suit = vc_cit->GetCardSuit();

    if (this_suit == flush_suit) {
      if (this_value < smallest_value) {
        second_smallest_value = smallest_value;
        smallest_value = this_value;
      } else if (this_value < second_smallest_value) {
        second_smallest_value = this_value;
      }
      ++ccount;
    }
  }

  if (ccount < max_count_const) {
    *output_hand_order = -1L;
    *output7_signature = -1L;
    *output5_signature = -1L;
    *output_handvalue =
        cardlib::CHand::khand_value::kHandValueNull;

    return;
  }

  // take care of the case where we don't have to remove
  // the smallest and second smallest cards from the flush hand
  if (ccount == max_count_const + 1L) {
    second_smallest_value =
        cardlib::CCard::kcard_value::kValueNull;
  } else if (ccount == max_count_const) {
    smallest_value =
        cardlib::CCard::kcard_value::kValueNull;
    second_smallest_value =
        cardlib::CCard::kcard_value::kValueNull;
  }

  prime7_signature = 1L;
  prime5_signature = 1L;
  order = 0L;
  ccount = 0L;
  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    this_value = vc_cit->GetCardValue();
    this_suit = vc_cit->GetCardSuit();
    this_prime = vc_cit->GetCardPrimeValue();
    this_order = vc_cit->GetCardOrder();
    prime7_signature *= this_prime;
    if (this_suit == flush_suit) {
      // take just the first 5 cards, except for the 2 smallest
      if (ccount < max_count_const) {
        if ((this_value != smallest_value)
            && (this_value != second_smallest_value)) {
          prime5_signature *= this_prime;
          order = order | this_order;
          ++ccount;
        }
      }
    }
  }

  *output_hand_order = order;
  *output7_signature = prime7_signature;
  *output5_signature = prime5_signature;
  *output_handvalue = cardlib::CHand::khand_value::kFlush;

  return;
}

// #############################################################
// #############################################################
bool CFlushFunctions::FlushGreaterFlush(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  int64_t hand_order_1(0L), hand_order_2(0L);

  hand_order_1 = chand1->GetHandOrder();
  hand_order_2 = chand2->GetHandOrder();

  return (hand_order_1 > hand_order_2);
}

// #############################################################
// #############################################################
bool CFlushFunctions::FlushLessThanFlush(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  int64_t hand_order_1(0L), hand_order_2(0L);

  hand_order_1 = chand1->GetHandOrder();
  hand_order_2 = chand2->GetHandOrder();

  return (hand_order_1 < hand_order_2);
}

// #############################################################
// #############################################################
bool CFlushFunctions::FlushEqualFlush(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  int64_t hand_order_1(0L), hand_order_2(0L);

  hand_order_1 = chand1->GetHandOrder();
  hand_order_2 = chand2->GetHandOrder();

  return (hand_order_1 == hand_order_2);
}

}  // namespace cardlib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

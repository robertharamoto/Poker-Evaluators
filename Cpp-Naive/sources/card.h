#ifndef SOURCES_CARD_H_
#define SOURCES_CARD_H_

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CCard definition                                      ###
  ###                                                        ###
  ###  last updated April 29, 2024                           ###
  ###    new bitmask definition for cards                    ###
  ###                                                        ###
  ###  created March 18, 2024                                ###
  ###    used new single integer bit scheme                  ###
  ###    from http://suffe.cool/poker/evaluator.html         ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cstdint>
#include <string>
#include <vector>

/*
Single card bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

namespace cardlib {

class CCard  {
 public:
  enum kcard_value : int64_t {
    kValueNull = -1,
    kTwo = 2, kThree, kFour, kFive, kSix,
    kSeven, kEight, kNine, kTen, kJack,
    kQueen, kKing, kAce };
  enum kprime_value : int64_t {
    kpTwo = 2, kpThree = 3, kpFour = 5,
    kpFive = 7, kpSix = 11, kpSeven = 13,
    kpEight = 17, kpNine = 19, kpTen = 23,
    kpJack = 29, kpQueen = 31, kpKing = 37,
    kpAce = 41 };
  enum kcard_suit : int64_t {
    kNullSuit = -1,
    kSpades = 1, kHearts = 2, kDiamonds = 4, kClubs = 8 };

 public:
  CCard();
  explicit CCard(const std::string stmp);
  CCard(const CCard &ctmp);

  ~CCard();

  void Clear();

  void Set(const std::string astring);
  void Set(const std::string *astring);
  void Set(const std::string *value_suit_string, int64_t *cmask);
  void Set(const std::string *value_string,
           const std::string *suit_string,
           int64_t *cmask);
  void Set(const std::string value_string,
           const std::string suit_string);

  int64_t GetCardMask(void) const;
  int64_t GetCardOrder(void) const;
  int64_t GetCardSuitMask(void) const;
  int64_t GetCardPrimeValue(void) const;

  CCard::kcard_value GetCardValue(void) const;
  CCard::kcard_value GetCardNullValue() {
    return CCard::kcard_value::kValueNull; }
  CCard::kcard_suit GetCardSuit(void) const;

  std::string GetValueString(void) const;
  std::string GetSuitString(void) const;
  std::string GetValueName(void) const;
  std::string GetSuitName(void) const;

  std::string CardToString(void) const;
  void StringToCard(const std::string *stmp, int64_t *cmask);

  std::string CardVectorToString(
      const std::vector<cardlib::CCard> *input_vector);

  void ToLower(std::string *stmp);

  bool IsIdentical(const CCard *ctmp) const;
  bool ValueEqual(const CCard *ctmp) const;
  bool Greater(const CCard &ctmp) const;

  void Copy(const CCard& ctmp);

  bool operator==(const CCard& c1) const;
  bool operator!=(const CCard& c1) const;
  bool operator>=(const CCard& c1) const;
  bool operator>(const CCard& c1) const;
  bool operator<(const CCard& c1) const;
  bool operator<=(const CCard& c1) const;

  CCard& operator=(const CCard& c1);

 private:
  int64_t card_mask_;

  void SetValue(const std::string *value_string, int64_t *cmask);
  void SetSuit(const std::string *suit_string, int64_t *cmask);
};  // class CCard

}  // namespace cardlib

#endif  // SOURCES_CARD_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

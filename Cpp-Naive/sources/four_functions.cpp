/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CFourFunctions implementation                         ###
  ###  namespace cardlib                                     ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###                                                        ###
  ###  created March 18, 2024                                ###
  ###    used new single integer bit scheme                  ###
  ###    from http://suffe.cool/poker/evaluator.html         ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/four_functions.h"

#include <cstdint>

#include <map>
#include <vector>
#include <string>
#include <algorithm>
#include <typeinfo>

#include "sources/card.h"
#include "sources/hand.h"

namespace cardlib {
/*
Single card 32-bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

// #############################################################
// #############################################################
CFourFunctions::CFourFunctions() {
}

// #############################################################
// #############################################################
CFourFunctions::~CFourFunctions() {
}

// #############################################################
// #############################################################
void CFourFunctions::IsFourOfAKind(
    const std::vector<cardlib::CCard> *input_vector,
    const std::map<cardlib::CCard::kcard_value, int64_t>
    *value_count_map,
    cardlib::CCard::kcard_value *output_quad_value,
    cardlib::CCard::kcard_value *output_kicker1,
    int64_t *output7_signature,
    int64_t *output5_signature,
    cardlib::CHand::khand_value *output_handvalue) const {
  std::vector<cardlib::CCard>::const_iterator vc_cit;
  std::map<cardlib::CCard::kcard_value, int64_t>::const_iterator
      mvi_cit;
  int64_t this_prime, prime5_signature, prime7_signature;
  int64_t count(0L), nn4count_constant(4L);
  cardlib::CCard::kcard_value value;
  cardlib::CCard::kcard_value max_value, nn4value;
  bool init_flag(false);

  max_value = nn4value =
      cardlib::CCard::kcard_value::kValueNull;

  for (mvi_cit = value_count_map->cbegin();
       mvi_cit != value_count_map->cend(); ++mvi_cit) {
    value = mvi_cit->first;
    count = mvi_cit->second;

    if (count >= nn4count_constant) {
      if (nn4value < value) {
        nn4value = value;
      }
    } else if (max_value < value) {
      max_value = value;
    }
  }

  prime7_signature = 1L;
  prime5_signature = 1L;
  init_flag = false;
  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    value = vc_cit->GetCardValue();
    this_prime = vc_cit->GetCardPrimeValue();
    prime7_signature = prime7_signature * this_prime;

    if (value == nn4value) {
      prime5_signature = prime5_signature * this_prime;
    } else {
      // look for max non-4-of-a-kind first
      if (value == max_value) {
        if (init_flag == false) {
          prime5_signature = prime5_signature * this_prime;
          init_flag = true;
        }
      }
    }
  }

  if ((nn4value < 0L) || (max_value < 0L)) {
    *output_quad_value = cardlib::CCard::kcard_value::kValueNull;
    *output_kicker1 = cardlib::CCard::kcard_value::kValueNull;
    *output5_signature = -1L;
    *output7_signature = -1L;
    *output_handvalue =
        cardlib::CHand::khand_value::kHandValueNull;

    return;
  }

  *output_quad_value = nn4value;
  *output_kicker1 = max_value;
  *output5_signature = prime5_signature;
  *output7_signature = prime7_signature;
  *output_handvalue = cardlib::CHand::khand_value::kQuads;

  return;
}

// #############################################################
// #############################################################
bool CFourFunctions::QuadsGreaterQuads(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CCard::kcard_value quadvalue1, quadvalue2;
  cardlib::CCard::kcard_value kicker1, kicker2;

  quadvalue1 = chand1->GetQuadValue();
  quadvalue2 = chand2->GetQuadValue();

  if (quadvalue1 > quadvalue2) {
    return true;
  } else if (quadvalue1 < quadvalue2) {
    return false;
  }

  kicker1 = chand1->GetKicker1();
  kicker2 = chand2->GetKicker1();

  return (kicker1 > kicker2);
}

// #############################################################
// #############################################################
bool CFourFunctions::QuadsLessThanQuads(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CCard::kcard_value quadvalue1, quadvalue2;
  cardlib::CCard::kcard_value kicker1, kicker2;

  quadvalue1 = chand1->GetQuadValue();
  quadvalue2 = chand2->GetQuadValue();

  if (quadvalue1 < quadvalue2) {
    return true;
  } else if (quadvalue1 > quadvalue2) {
    return false;
  }

  kicker1 = chand1->GetKicker1();
  kicker2 = chand2->GetKicker1();

  return (kicker1 < kicker2);
}

// #############################################################
// #############################################################
bool CFourFunctions::QuadsEqualQuads(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CCard::kcard_value quadvalue1, quadvalue2;
  cardlib::CCard::kcard_value kicker1, kicker2;

  quadvalue1 = chand1->GetQuadValue();
  quadvalue2 = chand2->GetQuadValue();

  kicker1 = chand1->GetKicker1();
  kicker2 = chand2->GetKicker1();

  if ((quadvalue1 == quadvalue2)
      && (kicker1 == kicker2)) {
    return true;
  }

  return false;
}

}  // namespace cardlib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#ifndef SOURCES_UTILS_H_
#define SOURCES_UTILS_H_
/*

// Copyright 2024 <robert haramoto>

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  utilities class definition                            ###
  ###                                                        ###
  ###  last updated May 4, 2024                              ###
  ###                                                        ###
  ###  updated July 31, 2022                                 ###
  ###                                                        ###
  ###  updated February 5, 2020                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <gmp.h>
#include <gmpxx.h>

#include <stdint.h>
#include <cmath>
#include <ctime>

#include <string>
#include <vector>
#include <set>
#include <map>
#include <chrono>

namespace mylib {

class CUtils {
 public:
  CUtils() {}

  // do not allow copying nor moving
  CUtils(const CUtils&) = delete;
  CUtils& operator=(const CUtils&) = delete;
  CUtils(CUtils&&) = delete;
  CUtils& operator=(CUtils&&) = delete;

  // numeric functions
  std::string CommafyNum(const int64_t lvalue) const;
  std::string CommafyMpzNum(
      const mpz_class *bn_value) const;
  std::string MpzNumToSciString(
      const mpz_class *bn_value) const;
  std::string MpzNumToNiceSciString(
      const mpz_class *bn_value) const;

  double RoundFloat(double dvalue, double ndecimals) const;
  std::string DoubleToString(double dvalue,
                             const double ndecimals) const;

  // string functions
  std::string LowerCase(const std::string *stmp) const;
  std::string StringTrim(const std::string *stmp) const;
  std::string StringRemoveComment(
      const std::string *input_string,
      const char comment_char) const;
  void StringTokenize(
      const std::string *input_string,
      const char delim_char,
      std::vector<std::string> *tokens) const;
  std::string StringRemoveCommas(
      const std::string *input_string) const;
  int64_t StringToInteger(const std::string *input_string) const;
  mpz_class StringToMpzClass(const std::string *input_string) const;

  // debug functions
  std::string IntVectorToString(
      const std::vector<int> *input_vector) const;
  std::string Vector64ToString(
      const std::vector<int64_t> *input_vector,
      const std::string *open_bracket,
      const std::string *close_bracket,
      const std::string *separator_string) const;
  std::string Int64VectorToString(
      const std::vector<int64_t> *input_vector) const;
  std::string Int64SequenceVectorToString(
      const std::vector<int64_t> *input_vector) const;
  std::string VectorMpzToString(
      const std::vector<mpz_class> *input_vector,
      const std::string *open_bracket,
      const std::string *close_bracket,
      const std::string *separator_string) const;
  std::string MpzVectorToString(
      const std::vector<mpz_class> *input_vector) const;
  std::string MpzSequenceVectorToString(
      const std::vector<mpz_class> *input_vector) const;
  std::string Int64SetToString(
      const std::set<int64_t> *input_set) const;
  std::string MpzSetToString(
      const std::set<mpz_class> *input_set) const;
  std::string MapInt64Int64ToString(
      const std::map<int64_t, int64_t> *input_map) const;
  std::string MapInt64BoolToString(
      const std::map<int64_t, bool> *input_map) const;
  std::string MapMpzBoolToString(
      const std::map<mpz_class, bool> *input_map) const;

  // comparison functions
  bool Vector1EqualsVector2(
      const std::vector<int64_t> *vector_1,
      const std::vector<int64_t> *vector_2) const;

  // date/time functions
  std::string CurrentTime(void) const;
  std::string CurrentDate(void) const;
  std::string CurrentDateTime(void) const;
  // recommended time interval calculations
  std::string ClockDuration(
      const std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> *end_time,
      const std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> *start_time) const;
  double ClockDurationInSeconds(
      const std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> *end_time,
      const std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> *start_time) const;
  double ClockDurationInMinutes(
      const std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> *end_time,
      const std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> *start_time) const;

  // file functions
  bool FileExists(const char *filename) const;

 private:
  std::string AddCommasToNumberString(
      const std::string *stmp) const;
};  // end of class CUtils

}  // namespace mylib

#endif  // SOURCES_UTILS_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

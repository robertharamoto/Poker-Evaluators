#ifndef SOURCES_HAND_H_
#define SOURCES_HAND_H_

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CHand definition                                      ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###    new bitmask definition for cards                    ###
  ###                                                        ###
  ###  created March 4, 2024                                 ###
  ###    used new single integer bit scheme                  ###
  ###    from http://suffe.cool/poker/evaluator.html         ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cstdint>
#include <vector>
#include <map>
#include <string>

#include "sources/card.h"

namespace cardlib {

/*
Single card bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

class CHand {
 public:
  enum khand_value : int64_t {
    kHandValueNull = -1,
    kHighCard, kOnePair, kTwoPair,
    kTriples, kStraight, kFlush,
    kFullHouse, kQuads, kStraightFlush,
    kRoyalFlush,
  };

  enum kstraight_mask : int64_t {
    kStraightValueNull = -1, kAceHighStraight = 0x1F00,
    kKingHighStraight = 0x0F80, kQueenHighStraight = 0x07C0,
    kJackHighStraight = 0x03E0, kTenHighStraight = 0x01F0,
    kNineHighStraight = 0x00F8, kEightHighStraight = 0x007C,
    kSevenHighStraight = 0x003E, kSixHighStraight = 0x001F,
    kFiveHighStraight = 0x100F,
  };

  enum kstraight_signature : int64_t {
    kStraightSignatureNull = -1, kAceHighSignature = 31367009L,
    kKingHighSignature = 14535931L, kQueenHighSignature = 6678671L,
    kJackHighSignature = 2800733L, kTenHighSignature = 1062347L,
    kNineHighSignature = 323323L, kEightHighSignature = 85085L,
    kSevenHighSignature = 15015L, kSixHighSignature = 2310L,
    kFiveHighSignature = 8610L,
  };

 public:
  CHand();
  CHand(const CHand&);
  ~CHand();

  void Clear();
  void InitHand();

  bool IsCalculated() const;

  std::string GetCardsString() const;

  std::string HandName() const;
  std::string HandLongName() const;
  std::string HandName(const int64_t hval) const;
  std::string HandToString();

  void SetHandName(
      const cardlib::CHand::khand_value ahand_value);

  void SetHandLongName(
      const cardlib::CHand::khand_value ahand_value);

  cardlib::CHand::khand_value GetHandValue() const;

  void ComputeHandPrimeSignatures(
      const std::vector<cardlib::CCard> *input_vector,
      int64_t *all_primes) const;

  cardlib::CHand::kstraight_mask GetHandStraightMask() const;

  cardlib::CCard::kcard_suit GetFlushSuit() const;
  int64_t GetHandOrder() const;

  int64_t GetPrime7Signature() const;
  int64_t GetPrime5Signature() const;

  cardlib::CCard::kcard_value GetQuadValue() const;
  cardlib::CCard::kcard_value GetTripValue() const;
  cardlib::CCard::kcard_value GetTopPair() const;
  cardlib::CCard::kcard_value GetSecondPair() const;

  cardlib::CCard::kcard_value GetKicker1() const;
  cardlib::CCard::kcard_value GetKicker2() const;
  cardlib::CCard::kcard_value GetKicker3() const;

  void PopulateNthOfAKindMap(
      const std::vector<cardlib::CCard> *input_vector,
      std::map<cardlib::CCard::kcard_value, int64_t>
      *output_map) const;

  void CalculateHelper(
      cardlib::CHand::khand_value input_hand_value,
      cardlib::CCard::kcard_suit input_flush_value,
      cardlib::CHand::kstraight_mask input_straight_mask,
      int64_t input_order,
      int64_t input_p7signature, int64_t input_p5signature,
      cardlib::CCard::kcard_value input_quad_value,
      cardlib::CCard::kcard_value input_trip_value,
      cardlib::CCard::kcard_value input_top_pair,
      cardlib::CCard::kcard_value input_second_pair,
      cardlib::CCard::kcard_value input_kicker1,
      cardlib::CCard::kcard_value input_kicker2,
      cardlib::CCard::kcard_value input_kicker3);

  void Calculate(const std::vector<cardlib::CCard> *input_vector);

  bool Hand1GreaterHand2(const cardlib::CHand *h2) const;
  bool Hand1LessThanHand2(const cardlib::CHand *h2) const;
  bool Hand1EqualHand2(const cardlib::CHand *h2) const;

  // functions to help with operators
  bool StraightsLessThan(const cardlib::CHand *csh) const;
  bool QuadsLessThan(const cardlib::CHand *csh) const;
  bool FullsLessThan(const cardlib::CHand *csh) const;
  bool FlushesLessThan(const cardlib::CHand *csh) const;
  bool TriplesLessThan(const cardlib::CHand *csh) const;
  bool TwoPairsLessThan(const cardlib::CHand *csh) const;
  bool OnePairsLessThan(const cardlib::CHand *csh) const;

  bool StraightsGreaterThan(const cardlib::CHand *csh) const;
  bool QuadsGreaterThan(const cardlib::CHand *csh) const;
  bool FullsGreaterThan(const cardlib::CHand *csh) const;
  bool FlushesGreaterThan(const cardlib::CHand *csh) const;
  bool TriplesGreaterThan(const cardlib::CHand *csh) const;
  bool TwoPairsGreaterThan(const cardlib::CHand *csh) const;
  bool OnePairsGreaterThan(const cardlib::CHand *csh) const;

  bool StraightsEqual(const cardlib::CHand *csh) const;
  bool QuadsEqual(const cardlib::CHand *csh) const;
  bool FullsEqual(const cardlib::CHand *csh) const;
  bool FlushesEqual(const cardlib::CHand *csh) const;
  bool TriplesEqual(const cardlib::CHand *csh) const;
  bool TwoPairsEqual(const cardlib::CHand *csh) const;
  bool OnePairsEqual(const cardlib::CHand *csh) const;

  // operators
  CHand& operator=(const cardlib::CHand &csh);
  bool operator<(const cardlib::CHand &sh1) const;
  bool operator>(const cardlib::CHand &sh1) const;
  bool operator==(const cardlib::CHand &sh1) const;

  // ###########################################################
  // ###########################################################
 private:
  void Initialize();
  void CopyHand(const cardlib::CHand *);

  void trim2(std::string *str);

  // member data
  bool is_calculated_;

  std::string hand_name_;
  std::string hand_long_name_;
  std::string card_string_;

  // every hand has a hand_value_
  cardlib::CHand::khand_value hand_value_;

  // straights and straight-flushes have hand_straight_mask_
  cardlib::CHand::kstraight_mask hand_straight_mask_;

  // only flushes and straight-flushes have a flush_suit_
  cardlib::CCard::kcard_suit flush_suit_;

  // hand_order_ is used for flushes and high-card hands
  int64_t hand_order_;

  // prime7_signature_ is used for determining straights
  int64_t prime7_signature_;
  int64_t prime5_signature_;

  cardlib::CCard::kcard_value quad_value_, trip_value_;
  cardlib::CCard::kcard_value top_pair_, second_pair_;
  cardlib::CCard::kcard_value kicker1_, kicker2_, kicker3_;
};  // class CHand

}  // namespace cardlib

#endif  // SOURCES_HAND_H_

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CTripleFunctions implementation                       ###
  ###    namespace cardlib                                   ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###                                                        ###
  ###  created March 18, 2024                                ###
  ###    used new single integer bit scheme                  ###
  ###    from http://suffe.cool/poker/evaluator.html         ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/triple_functions.h"

#include <cstdint>

#include <map>
#include <algorithm>
#include <vector>
#include <string>
#include <typeinfo>

#include "sources/card.h"
#include "sources/hand.h"

namespace cardlib {
/*
Single card 32-bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

// #############################################################
// #############################################################
CTripleFunctions::CTripleFunctions() {
}

// #############################################################
// #############################################################
CTripleFunctions::~CTripleFunctions() {
}

// #############################################################
// #############################################################
// assumes kicker1 > kicker2
void CTripleFunctions::FormTriplesHand(
    const std::vector<cardlib::CCard> *input_vector,
    std::map<cardlib::CCard::kcard_value, int64_t>
    *value_count_map,
    cardlib::CCard::kcard_value *output_triple,
    cardlib::CCard::kcard_value *output_kicker1,
    cardlib::CCard::kcard_value *output_kicker2,
    int64_t *output7_signature,
    int64_t *output5_signature,
    cardlib::CHand::khand_value *output_handvalue) const {
  std::vector<cardlib::CCard>::const_iterator vc_cit;
  cardlib::CCard::kcard_value card_value, ntriple_value;
  cardlib::CCard::kcard_value first_cvalue, second_cvalue;
  int64_t ntriple_count_constant(3L);
  int64_t card_count;
  int64_t this_prime;
  int64_t prime7_signature, prime5_signature;
  bool b1_init(false), b2_init(false);

  ntriple_value = first_cvalue = second_cvalue =
      static_cast<cardlib::CCard::kcard_value>(-1L);

  prime7_signature = 1L;
  prime5_signature = 1L;
  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    card_value = vc_cit->GetCardValue();
    this_prime = vc_cit->GetCardPrimeValue();

    card_count = (*value_count_map)[card_value];
    prime7_signature *= this_prime;

    if (card_count == ntriple_count_constant) {
      if (card_value > ntriple_value) {
        ntriple_value = card_value;
      }
    } else {
      // not a three-of-a-kind card, must be kicker cards
      if (card_value > first_cvalue) {
        second_cvalue = first_cvalue;
        first_cvalue = card_value;
      } else if (card_value > second_cvalue) {
        second_cvalue = card_value;
      }
    }
  }

  if ((ntriple_value < 0L)
      || (first_cvalue < 0L) || (second_cvalue < 0L)) {
    *output_triple =
        cardlib::CCard::kcard_value::kValueNull;
    *output_kicker1 =
        cardlib::CCard::kcard_value::kValueNull;
    *output_kicker2 =
        cardlib::CCard::kcard_value::kValueNull;
    *output7_signature = -1L;
    *output5_signature = -1L;
    *output_handvalue =
        cardlib::CHand::khand_value::kHandValueNull;

    return;
  }

  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    card_value = vc_cit->GetCardValue();
    this_prime = vc_cit->GetCardPrimeValue();

    if (card_value == ntriple_value) {
      prime5_signature *= this_prime;
    } else if ((card_value == first_cvalue)
               && (b1_init == false)) {
      prime5_signature *= this_prime;
      b1_init = true;
    } else if ((card_value == second_cvalue)
               && (b2_init == false)) {
      prime5_signature *= this_prime;
      b2_init = true;
    }
  }

  *output_triple = ntriple_value;
  *output_kicker1 = first_cvalue;
  *output_kicker2 = second_cvalue;
  *output7_signature = prime7_signature;
  *output5_signature = prime5_signature;
  *output_handvalue =
      cardlib::CHand::khand_value::kTriples;

  return;
}

// #############################################################
// #############################################################
void CTripleFunctions::IsThreeOfAKind(
    const std::vector<cardlib::CCard> *input_vector,
    std::map<cardlib::CCard::kcard_value, int64_t>
    *value_count_map,
    cardlib::CCard::kcard_value *output_triple,
    cardlib::CCard::kcard_value *output_kicker1,
    cardlib::CCard::kcard_value *output_kicker2,
    int64_t *output7_signature,
    int64_t *output5_signature,
    cardlib::CHand::khand_value *output_handvalue) const {
  FormTriplesHand(
      input_vector, value_count_map,
      output_triple, output_kicker1, output_kicker2,
      output7_signature, output5_signature,
      output_handvalue);

  return;
}

// #############################################################
// #############################################################
bool CTripleFunctions::TripleGreaterTriple(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CCard::kcard_value triples1, kicker1_1, kicker2_1;
  cardlib::CCard::kcard_value triples2, kicker1_2, kicker2_2;

  triples1 = chand1->GetTripValue();
  triples2 = chand2->GetTripValue();

  if (triples1 > triples2) {
    return true;
  } else if (triples1 < triples2) {
    return false;
  }

  kicker1_1 = chand1->GetKicker1();
  kicker1_2 = chand2->GetKicker1();

  if (kicker1_1 > kicker1_2) {
    return true;
  } else if (kicker1_1 < kicker1_2) {
    return false;
  } else {
    kicker2_1 = chand1->GetKicker2();
    kicker2_2 = chand2->GetKicker2();

    return (kicker2_1 > kicker2_2);
  }
}

// #############################################################
// #############################################################
bool CTripleFunctions::TripleLessThanTriple(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CCard::kcard_value triples1, kicker1_1, kicker2_1;
  cardlib::CCard::kcard_value triples2, kicker1_2, kicker2_2;

  triples1 = chand1->GetTripValue();
  triples2 = chand2->GetTripValue();

  if (triples1 < triples2) {
    return true;
  } else if (triples1 > triples2) {
    return false;
  }

  kicker1_1 = chand1->GetKicker1();
  kicker1_2 = chand2->GetKicker1();

  if (kicker1_1 < kicker1_2) {
    return true;
  } else if (kicker1_1 > kicker1_2) {
    return false;
  } else {
    kicker2_1 = chand1->GetKicker2();
    kicker2_2 = chand2->GetKicker2();

    return (kicker2_1 < kicker2_2);
  }
}

// #############################################################
// #############################################################
bool CTripleFunctions::TripleEqualTriple(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CCard::kcard_value triples1, kicker1_1, kicker2_1;
  cardlib::CCard::kcard_value triples2, kicker1_2, kicker2_2;

  triples1 = chand1->GetTripValue();
  triples2 = chand2->GetTripValue();

  if (triples1 < triples2) {
    return false;
  } else if (triples1 > triples2) {
    return false;
  }

  kicker1_1 = chand1->GetKicker1();
  kicker1_2 = chand2->GetKicker1();

  if (kicker1_1 < kicker1_2) {
    return false;
  } else if (kicker1_1 > kicker1_2) {
    return false;
  } else {
    kicker2_1 = chand1->GetKicker2();
    kicker2_2 = chand2->GetKicker2();

    return (kicker2_1 == kicker2_2);
  }
}

}  // namespace cardlib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

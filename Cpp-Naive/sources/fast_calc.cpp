/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CFastCalc class implementation                        ###
  ###                                                        ###
  ###  last updated September 19, 2024                       ###
  ###    revised with new bitmask definition                 ###
  ###                                                        ###
  ###  created April 18, 2024                                ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/fast_calc.h"

#include <cstdint>
#include <cstdlib>
#include <cstdio>

#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <iterator>

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/utils.h"
#include "sources/flush_functions.h"

namespace cardlib {

/*
Single card bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

// #############################################################
// #############################################################
CFastCalc::CFastCalc() : card_deck_(), card_string_deck_() {
  InitDeck();
}

// #############################################################
// #############################################################
CFastCalc::~CFastCalc() {
  card_deck_.clear();
  card_string_deck_.clear();
}

// #############################################################
// #############################################################
void CFastCalc::ComputeAll(const bool write_enum_flag,
                           const std::string *fname,
                           const cardlib::CCard *card_a1,
                           const cardlib::CCard *card_a2,
                           const cardlib::CCard *card_b1,
                           const cardlib::CCard *card_b2) const {
  std::map<cardlib::CHand, int64_t> five_count_map;
  std::map<cardlib::CHand, int64_t> five_rank_map;
  std::map<cardlib::CHand, int64_t> seven_count_map;
  std::map<cardlib::CHand, int64_t> seven_rank_map;

  CalculateEquivalenceClass7Cards(
      &seven_count_map, &seven_rank_map);

  if (write_enum_flag == true) {
    CalculateEquivalenceClass5Cards(
        &five_count_map, &five_rank_map);

    WriteOutHandEnumerationToFile(
        fname,
        &five_rank_map, &five_count_map,
        &seven_count_map);
  }

  CalculateEquivalenceClassValueRankCountStructs(
      &five_count_map, &five_rank_map);

  PocketComparison_7cards(
      card_a1, card_a2, card_b1, card_b2,
      &seven_rank_map, &card_deck_);

  return;
}

// #############################################################
// #############################################################
void CFastCalc::CalculateEquivalenceClass7Cards(
    std::map<cardlib::CHand, int64_t> *hand_count_map,
    std::map<cardlib::CHand, int64_t> *hand_rank_map) const {
  std::string seven_card_hand_string("7-card");
  mylib::CUtils utils_obj;
  std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> start_time, end_time;

  start_time = std::chrono::steady_clock::now();

  printf("\nloading %s mask maps : %s\n",
         seven_card_hand_string.c_str(),
         utils_obj.CurrentTime().c_str());

  LoadMaskMap7Cards(hand_count_map);

  MakeEquivalentRankings(hand_count_map, hand_rank_map);

  end_time = std::chrono::steady_clock::now();

  // 133,784,560 hands calculated, so summarize equivalence classes
  SummarizeCardHandCounts(
      &seven_card_hand_string, hand_count_map,
      &end_time, &start_time);

  printf("completed CalculateEquivalenceClass7Cards\n");
  printf("elapsed time = %s : %s\n",
         utils_obj.ClockDuration(&end_time, &start_time).c_str(),
         utils_obj.CurrentTime().c_str());

  printf("\n");
  fflush(stdout);

  return;
}

// #############################################################
// #############################################################
void CFastCalc::CalculateEquivalenceClass5Cards(
    std::map<cardlib::CHand, int64_t> *five_count_map,
    std::map<cardlib::CHand, int64_t> *five_rank_map) const {
  std::string lname, sname, sname1;
  std::string five_card_hand_string("5-card");
  mylib::CUtils utils_obj;
  std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> start_time, end_time;

  start_time = std::chrono::steady_clock::now();

  five_count_map->clear();
  five_rank_map->clear();

  printf("\nloading %s mask maps : %s\n",
         five_card_hand_string.c_str(),
         utils_obj.CurrentTime().c_str());

  LoadMaskMap5Cards(five_count_map);

  // 2,598,960 hands calculated now find the equivalence classes
  MakeEquivalentRankings(five_count_map, five_rank_map);

  end_time = std::chrono::steady_clock::now();

  SummarizeCardHandCounts(
      &five_card_hand_string, five_count_map,
      &end_time, &start_time);

  printf("completed CalculateEquivalenceClass5Cards\n");
  printf("elapsed time = %s : %s\n",
         utils_obj.ClockDuration(&end_time, &start_time).c_str(),
         utils_obj.CurrentTime().c_str());

  printf("\n");
  fflush(stdout);

  return;
}

// #############################################################
// #############################################################
void CFastCalc::CalculateEquivalenceClassValueRankCountStructs(
    std::map<cardlib::CHand, int64_t> *five_count_map,
    std::map<cardlib::CHand, int64_t> *five_rank_map) const {
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      flushes_vrc_map, nonflushes_vrc_map;
  std::string lname, sname, sname1;
  std::string five_card_hand_string("5-card");
  mylib::CUtils utils_obj;
  std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> start_time, end_time;

  start_time = std::chrono::steady_clock::now();

  five_count_map->clear();
  five_rank_map->clear();

  printf("\nloading %s mask maps : %s\n",
         five_card_hand_string.c_str(),
         utils_obj.CurrentTime().c_str());

  LoadMaskMap5Cards(five_count_map);

  // 2,598,960 hands calculated now find the equivalence classes
  MakeEquivalentRankings(five_count_map, five_rank_map);

  CombineMaps(five_count_map, five_rank_map,
              &flushes_vrc_map, &nonflushes_vrc_map);

  end_time = std::chrono::steady_clock::now();

  SummarizeValueRankCountstructs(
      &five_card_hand_string, &flushes_vrc_map,
      &nonflushes_vrc_map,
      &end_time, &start_time);

  printf("completed CalculateEquivalenceClassValueRankCountStructs\n");
  printf("elapsed time = %s : %s\n",
         utils_obj.ClockDuration(&end_time, &start_time).c_str(),
         utils_obj.CurrentTime().c_str());

  printf("\n");
  fflush(stdout);

  return;
}

// #############################################################
// #############################################################
void CFastCalc::PocketComparison_7cards(
    const cardlib::CCard *card_a1,
    const cardlib::CCard *card_a2,
    const cardlib::CCard *card_b1,
    const cardlib::CCard *card_b2,
    std::map<cardlib::CHand, int64_t> *seven_rank_map,
    const std::vector<cardlib::CCard> *card_deck) const {
  mylib::CUtils utils_obj;
  cardlib::CFlushFunctions flushf_obj;
  std::vector<cardlib::CCard> local_card_deck;
  std::vector<cardlib::CCard>::iterator cd_it;
  std::vector<cardlib::CCard> hand1_vector, hand2_vector;
  cardlib::CCard card1, card2, card3, card4, card5;
  cardlib::CHand hand1, hand2;
  int64_t k1, k2, k3, k4, k5;
  int64_t ntotals;
  int64_t nwins, nlosses, nties;
  int64_t rank1, rank2;
  int64_t nsize;
  std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> start_time, end_time;
  double dtmp, dsecs;
  double pcnt_wins, pcnt_losses, pcnt_ties, pcnt_total;
  std::string sname1, sname2;
  std::string spocket1, spocket2;

  local_card_deck = *card_deck;

  start_time = std::chrono::steady_clock::now();

  printf("starting 7-card prime-signature pocket comparison loop : %s\n",
         utils_obj.CurrentDateTime().c_str());

  // remove pocket cards from deck
  cd_it = find(local_card_deck.begin(), local_card_deck.end(),
             *card_a1);
  local_card_deck.erase(cd_it);
  cd_it = find(local_card_deck.begin(), local_card_deck.end(),
               *card_a2);
  local_card_deck.erase(cd_it);
  cd_it = find(local_card_deck.begin(), local_card_deck.end(),
               *card_b1);
  local_card_deck.erase(cd_it);
  cd_it = find(local_card_deck.begin(), local_card_deck.end(),
               *card_b2);
  local_card_deck.erase(cd_it);

  hand1_vector.clear();
  hand1_vector.push_back(*card_a1);
  hand1_vector.push_back(*card_a2);

  hand2_vector.clear();
  hand2_vector.push_back(*card_b1);
  hand2_vector.push_back(*card_b2);

  ntotals = 0;
  nwins = nlosses = nties = 0;
  nsize = local_card_deck.size();
  for (k1 = 0L; k1 < (nsize - 4L); ++k1) {
    card1 = local_card_deck[k1];
    hand1_vector.push_back(card1);
    hand2_vector.push_back(card1);
    for (k2 = k1 + 1L; k2 < (nsize - 3L); ++k2) {
      card2 = local_card_deck[k2];
      hand1_vector.push_back(card2);
      hand2_vector.push_back(card2);
      for (k3 = k2 + 1L; k3 < (nsize - 2L); ++k3) {
        card3 = local_card_deck[k3];
        hand1_vector.push_back(card3);
        hand2_vector.push_back(card3);
        for (k4 = k3 + 1L; k4 < (nsize - 1L); ++k4) {
          card4 = local_card_deck[k4];
          hand1_vector.push_back(card4);
          hand2_vector.push_back(card4);
          for (k5 = k4 + 1L; k5 < nsize; ++k5) {
            card5 = local_card_deck[k5];
            hand1_vector.push_back(card5);
            hand2_vector.push_back(card5);

            hand1.Calculate(&hand1_vector);

            hand2.Calculate(&hand2_vector);

            rank1 = (*seven_rank_map)[hand1];
            rank2 = (*seven_rank_map)[hand2];

            ++ntotals;
            if (rank1 > rank2) {
              ++nwins;
            } else if (rank1 < rank2) {
              ++nlosses;
            } else {
              ++nties;
            }

            hand1.Clear();
            hand2.Clear();

            // remove card5
            hand1_vector.pop_back();
            hand2_vector.pop_back();
          }
          // remove card4
          hand1_vector.pop_back();
          hand2_vector.pop_back();
        }
        // remove card3
        hand1_vector.pop_back();
        hand2_vector.pop_back();
      }
      // remove card2
      hand1_vector.pop_back();
      hand2_vector.pop_back();
    }
    // remove card1
    hand1_vector.pop_back();
    hand2_vector.pop_back();
  }

  printf("completed pocket comparison loop : %s\n",
         utils_obj.CurrentDateTime().c_str());

  spocket1 = "[" + card_a1->CardToString() +
      ", " + card_a2->CardToString() + "]";
  spocket2 = "[" + card_b1->CardToString() +
      ", " + card_b2->CardToString() + "]";

  printf("%s versus %s\n", spocket1.c_str(), spocket2.c_str());

  double dtotals = static_cast<double>(ntotals);
  pcnt_wins = utils_obj.RoundFloat((100.0 * nwins) / dtotals, 2.0);
  pcnt_losses = utils_obj.RoundFloat((100.0 * nlosses) / dtotals, 2.0);
  pcnt_ties = utils_obj.RoundFloat((100.0 * nties) / dtotals, 2.0);
  pcnt_total = utils_obj.RoundFloat((100.0 * (nwins + nlosses + nties)
                                     / dtotals), 2.0);

  printf("Number of wins for %s = %s (%4.2f%%)\n",
         spocket1.c_str(), utils_obj.CommafyNum(nwins).c_str(),
         pcnt_wins);
  printf("Number of losses for %s = %s (%4.2f%%)\n",
         spocket1.c_str(), utils_obj.CommafyNum(nlosses).c_str(),
         pcnt_losses);
  printf("Number of ties for %s = %s (%4.2f%%)\n",
         spocket1.c_str(), utils_obj.CommafyNum(nties).c_str(),
         pcnt_ties);
  printf("Total %s = %s (%4.2f%%)\n", spocket1.c_str(),
         utils_obj.CommafyNum(nwins+nlosses+nties).c_str(),
         pcnt_total);

  end_time = std::chrono::steady_clock::now();

  dsecs =
      utils_obj.ClockDurationInSeconds(&end_time, &start_time);
  dtmp = static_cast<double>(ntotals) / dsecs;

  printf("number of hands evaluated = %s\n",
         utils_obj.CommafyNum(ntotals).c_str());
  printf("5-card (best-hand) 2-pocket comparisons per second = %s\n",
         utils_obj.CommafyNum(static_cast<int64_t>(dtmp)).c_str());
  printf("elapsed time = %s : %s\n",
         utils_obj.ClockDuration(&end_time, &start_time).c_str(),
         utils_obj.CurrentDateTime().c_str());
  printf("\n");
  fflush(stdout);

  return;
}

// #############################################################
// #############################################################
void CFastCalc::SubtotalCardHandCounts(
    const std::vector<cardlib::CHand::khand_value> *sorted_key_vector,
    std::map<cardlib::CHand, int64_t> *hand_count_map,
    std::map<cardlib::CHand::khand_value, int64_t>
    *subtotal_hand_count_map,
    std::map<cardlib::CHand::khand_value, int64_t>
    *subtotal_distinct_count_map,
    int64_t *total_hand_count,
    int64_t *total_distinct_count) const {
  cardlib::CHand ahand_obj, this_hand_obj;
  std::vector<cardlib::CHand::khand_value>::const_iterator vhv_cit;
  std::map<cardlib::CHand, int64_t>::const_iterator msi_cit;
  cardlib::CHand::khand_value main_khv, this_khv;
  int64_t nhands;
  int64_t distinct_subtotal;

  *total_hand_count = 0L;
  *total_distinct_count = 0L;

  for (vhv_cit = sorted_key_vector->cbegin();
       vhv_cit != sorted_key_vector->cend(); ++vhv_cit) {
    main_khv = *vhv_cit;

    distinct_subtotal = 0L;
    for (msi_cit = hand_count_map->cbegin();
         msi_cit != hand_count_map->cend(); ++msi_cit) {
      this_hand_obj = msi_cit->first;
      this_khv = this_hand_obj.GetHandValue();

      if (main_khv == this_khv) {
        nhands = (*hand_count_map)[this_hand_obj];
        (*subtotal_hand_count_map)[this_khv] += nhands;
        *total_hand_count += nhands;

        distinct_subtotal++;
      }
    }

    (*subtotal_distinct_count_map)[main_khv] = distinct_subtotal;

    *total_distinct_count += distinct_subtotal;
  }

  return;
}

// #############################################################
// #############################################################
void CFastCalc::SummarizeCardHandCounts(
    const std::string *card_hand_string,
    std::map<cardlib::CHand, int64_t> *hand_count_map,
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *end_time,
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *start_time) const {
  mylib::CUtils utils_obj;
  std::map<cardlib::CHand, int64_t>::const_iterator msi_cit;
  std::vector<cardlib::CHand::khand_value> key_vector;
  std::map<cardlib::CHand::khand_value, int64_t>
      subtotal_hand_count_map, subtotal_distinct_count_map;
  std::vector<cardlib::CHand::khand_value>::iterator vhv_it;
  cardlib::CHand ahand_obj;
  cardlib::CHand::khand_value main_khv;
  int64_t nhands;
  int64_t total_hand_count(0L);
  int64_t subtotal_distinct_count, total_distinct_count;
  double dtmp, dsecs;
  std::string sname;

  for (msi_cit = hand_count_map->cbegin();
       msi_cit != hand_count_map->cend(); ++msi_cit) {
    ahand_obj = msi_cit->first;

    main_khv = ahand_obj.GetHandValue();
    key_vector.push_back(main_khv);
  }

  std::sort(key_vector.begin(), key_vector.end());
  std::reverse(key_vector.begin(), key_vector.end());

  vhv_it = std::unique(key_vector.begin(), key_vector.end());
  key_vector.resize(std::distance(key_vector.begin(), vhv_it));

  SubtotalCardHandCounts(
      &key_vector,
      hand_count_map,
      &subtotal_hand_count_map,
      &subtotal_distinct_count_map,
      &total_hand_count,
      &total_distinct_count);

  printf("\nCFastCalc statistics (%s hands)\n",
         card_hand_string->c_str());
  printf("Hand type : hand count : distinct hands\n");
  for (vhv_it = key_vector.begin();
       vhv_it != key_vector.end(); ++vhv_it) {
    main_khv = *vhv_it;

    nhands = subtotal_hand_count_map[main_khv];

    sname = ahand_obj.HandName(main_khv);

    subtotal_distinct_count = subtotal_distinct_count_map[main_khv];

    printf("  %s  :  %s  :  %s\n",
           sname.c_str(),
           utils_obj.CommafyNum(nhands).c_str(),
           utils_obj.CommafyNum(subtotal_distinct_count).c_str());
    fflush(stdout);
  }

  printf("  totals  :  %s  :  %s\n",
         utils_obj.CommafyNum(total_hand_count).c_str(),
         utils_obj.CommafyNum(total_distinct_count).c_str());

  dsecs =
      utils_obj.ClockDurationInSeconds(end_time, start_time);
  dtmp = static_cast<double>(total_hand_count) / dsecs;

  printf("%s hand evaluations per second = %s\n",
         card_hand_string->c_str(),
         utils_obj.CommafyNum(
             static_cast<int64_t>(dtmp)).c_str());
  printf("%s\n", utils_obj.CurrentDateTime().c_str());

  fflush(stdout);

  return;
}

// #############################################################
// #############################################################
void CFastCalc::PopulateSignatureValueMaps(
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *flushes_vrc_map,
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *nonflushes_vrc_map) const {
  std::map<cardlib::CHand, int64_t>
      hand_count_map, hand_rank_map;

  flushes_vrc_map->clear();
  nonflushes_vrc_map->clear();

  LoadMaskMap5Cards(&hand_count_map);

  // 2,598,960 hands calculated now find the equivalence classes
  MakeEquivalentRankings(&hand_count_map, &hand_rank_map);

  CombineMaps(&hand_count_map, &hand_rank_map,
              flushes_vrc_map, nonflushes_vrc_map);

  return;
}

// #############################################################
// #############################################################
void CFastCalc::SubtotalValueRankCountstructs(
    const std::vector<cardlib::CHand::khand_value>
    *sorted_key_vector,
    std::map<int64_t,
    cardlib::CFastCalc::value_rank_count> *flushes_vrc_map,
    std::map<int64_t,
    cardlib::CFastCalc::value_rank_count> *nonflushes_vrc_map,
    std::map<cardlib::CHand::khand_value, int64_t>
    *subtotal_hand_count_map,
    std::map<cardlib::CHand::khand_value, int64_t>
    *subtotal_distinct_count_map,
    int64_t *total_hand_count,
    int64_t *total_distinct_count) const {
  std::vector<cardlib::CHand::khand_value>::const_iterator vhv_cit;
  std::map<int64_t,
           cardlib::CFastCalc::value_rank_count>::const_iterator
      miv_cit;
  cardlib::CHand::khand_value main_khv, this_khv;
  cardlib::CFastCalc::value_rank_count this_struct;
  int64_t nhands;
  int64_t distinct_subtotal;

  *total_hand_count = 0L;
  *total_distinct_count = 0L;

  for (vhv_cit = sorted_key_vector->cbegin();
       vhv_cit != sorted_key_vector->cend(); ++vhv_cit) {
    main_khv = *vhv_cit;

    // first subtotal flushes
    distinct_subtotal = 0L;
    for (miv_cit = flushes_vrc_map->cbegin();
         miv_cit != flushes_vrc_map->cend(); ++miv_cit) {
      this_struct = miv_cit->second;
      this_khv =
          static_cast<cardlib::CHand::khand_value>(
              this_struct.value);

      if (main_khv == this_khv) {
        nhands = this_struct.count;
        (*subtotal_hand_count_map)[this_khv] += nhands;
        *total_hand_count += nhands;

        distinct_subtotal++;
      }
    }

    // second subtotal non-flushes
    for (miv_cit = nonflushes_vrc_map->cbegin();
         miv_cit != nonflushes_vrc_map->cend(); ++miv_cit) {
      this_struct = miv_cit->second;
      this_khv =
          static_cast<cardlib::CHand::khand_value>(
              this_struct.value);

      if (main_khv == this_khv) {
        nhands = this_struct.count;
        (*subtotal_hand_count_map)[this_khv] += nhands;
        *total_hand_count += nhands;

        distinct_subtotal++;
      }
    }

    (*subtotal_distinct_count_map)[main_khv] = distinct_subtotal;

    *total_distinct_count += distinct_subtotal;
  }

  return;
}

// #############################################################
// #############################################################
void CFastCalc::SummarizeValueRankCountstructs(
    const std::string *card_hand_string,
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *flushes_vrc_map,
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *nonflushes_vrc_map,
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *end_time,
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *start_time) const {
  mylib::CUtils utils_obj;
  std::map<int64_t,
           cardlib::CFastCalc::value_rank_count>::const_iterator
      miv_cit;
  std::vector<cardlib::CHand::khand_value> key_vector;
  std::map<cardlib::CHand::khand_value, int64_t>
      subtotal_hand_count_map, subtotal_distinct_count_map;
  std::vector<cardlib::CHand::khand_value>::iterator vhv_it;
  cardlib::CHand ahand_obj;
  cardlib::CHand::khand_value main_khv;
  cardlib::CFastCalc::value_rank_count vrc_struct;
  int64_t nhands;
  int64_t total_hand_count(0L);
  int64_t subtotal_distinct_count, total_distinct_count;
  double dseconds, dtmp;
  std::string sname;

  key_vector.clear();
  // get the flush hand values
  for (miv_cit = flushes_vrc_map->cbegin();
       miv_cit != flushes_vrc_map->cend(); ++miv_cit) {
    vrc_struct = miv_cit->second;

    main_khv =
        static_cast<cardlib::CHand::khand_value>(
            vrc_struct.value);
    key_vector.push_back(main_khv);
  }

  // get the non-flush hand values
  for (miv_cit = nonflushes_vrc_map->cbegin();
       miv_cit != nonflushes_vrc_map->cend(); ++miv_cit) {
    vrc_struct = miv_cit->second;

    main_khv =
        static_cast<cardlib::CHand::khand_value>(
            vrc_struct.value);
    key_vector.push_back(main_khv);
  }

  std::sort(key_vector.begin(), key_vector.end());
  std::reverse(key_vector.begin(), key_vector.end());

  vhv_it = std::unique(key_vector.begin(), key_vector.end());
  key_vector.resize(std::distance(key_vector.begin(), vhv_it));

  SubtotalValueRankCountstructs(
      &key_vector,
      flushes_vrc_map,
      nonflushes_vrc_map,
      &subtotal_hand_count_map,
      &subtotal_distinct_count_map,
      &total_hand_count,
      &total_distinct_count);

  printf("\nCFastCalc statistics (%s hands)\n",
         card_hand_string->c_str());
  printf("Hand type : hand count : equivalent hands\n");
  for (vhv_it = key_vector.begin();
       vhv_it != key_vector.end(); ++vhv_it) {
    main_khv = *vhv_it;

    nhands = subtotal_hand_count_map[main_khv];

    sname = ahand_obj.HandName(main_khv);

    subtotal_distinct_count = subtotal_distinct_count_map[main_khv];

    printf("  %s  :  %s  :  %s\n",
           sname.c_str(),
           utils_obj.CommafyNum(nhands).c_str(),
           utils_obj.CommafyNum(subtotal_distinct_count).c_str());
    fflush(stdout);
  }

  printf("  totals  :  %s  :  %s\n",
         utils_obj.CommafyNum(total_hand_count).c_str(),
         utils_obj.CommafyNum(total_distinct_count).c_str());

  dseconds =
      utils_obj.ClockDurationInSeconds(end_time, start_time);
  dtmp = static_cast<double>(total_hand_count) / dseconds;

  printf("%s hand evaluations per second = %s\n",
         card_hand_string->c_str(),
         utils_obj.CommafyNum(
             static_cast<int64_t>(dtmp)).c_str());
  printf("%s\n", utils_obj.CurrentDateTime().c_str());

  fflush(stdout);

  return;
}

// #############################################################
// #############################################################
void CFastCalc::CombineMaps(
    std::map<cardlib::CHand, int64_t> *hand_count_map,
    std::map<cardlib::CHand, int64_t> *hand_rank_map,
    std::map<int64_t, struct CFastCalc::value_rank_count>
    *flushes_vrc_map,
    std::map<int64_t, struct CFastCalc::value_rank_count>
    *nonflushes_vrc_map) const {
  std::map<cardlib::CHand, int64_t>::const_iterator mhi_cit;
  cardlib::CHand hand_obj;
  cardlib::CHand::khand_value this_khv;
  int64_t prime5_signature, this_count, this_rank;
  cardlib::CFastCalc::value_rank_count this_vrc;

  for (mhi_cit = hand_count_map->cbegin();
       mhi_cit != hand_count_map->cend(); ++mhi_cit) {
    hand_obj = mhi_cit->first;
    this_count = mhi_cit->second;
    this_rank = (*hand_rank_map)[hand_obj];

    prime5_signature = hand_obj.GetPrime5Signature();
    this_khv = hand_obj.GetHandValue();

    this_vrc.value = this_khv;
    this_vrc.rank = this_rank;
    this_vrc.count = this_count;

    if ((this_khv == cardlib::CHand::khand_value::kRoyalFlush)
        || (this_khv == cardlib::CHand::khand_value::kStraightFlush)
        || (this_khv == cardlib::CHand::khand_value::kFlush)) {
      (*flushes_vrc_map)[prime5_signature] = this_vrc;
    } else {
      (*nonflushes_vrc_map)[prime5_signature] = this_vrc;
    }
  }

  return;
}

// #############################################################
// #############################################################
void CFastCalc::LoadMaskMap5Cards(
    std::map<cardlib::CHand, int64_t> *hand_count_map) const {
  mylib::CUtils utils_obj;
  cardlib::CCard ca1, ca2, ca3, ca4, ca5;
  int64_t ii_1, ii_2, ii_3, ii_4, ii_5;
  cardlib::CHand hand_obj;
  std::vector<cardlib::CCard> card_vector;
  int64_t ncount;

  hand_count_map->clear();

  ncount = 0;
  for (ii_1 = 0L; ii_1 < 48L; ++ii_1) {
    ca1 = card_deck_[ii_1];
    card_vector.push_back(ca1);
    for (ii_2 = (ii_1 + 1L); ii_2 < 49L; ++ii_2) {
      ca2 = card_deck_[ii_2];
      card_vector.push_back(ca2);
      for (ii_3 = (ii_2 + 1L); ii_3 < 50L; ++ii_3) {
        ca3 = card_deck_[ii_3];
        card_vector.push_back(ca3);
        for (ii_4 = (ii_3 + 1L); ii_4 < 51L; ++ii_4) {
          ca4 = card_deck_[ii_4];
          card_vector.push_back(ca4);
          for (ii_5 = (ii_4 + 1L); ii_5 < 52L; ++ii_5) {
            ca5 = card_deck_[ii_5];
            card_vector.push_back(ca5);

            ++ncount;

            hand_obj.Clear();
            hand_obj.Calculate(&card_vector);

            (*hand_count_map)[hand_obj]++;

            // pop back card 5
            card_vector.pop_back();
          }
          // pop back card 4
          card_vector.pop_back();
        }
        // pop back card 3
        card_vector.pop_back();
      }
      // pop back card 2
      card_vector.pop_back();
    }
    // pop back card 1
    card_vector.pop_back();
  }

  return;
}

// #############################################################
// #############################################################
void CFastCalc::MakeEquivalentRankings(
    const std::map<cardlib::CHand, int64_t> *hand_count_map,
    std::map<cardlib::CHand, int64_t> *hand_rank_map) const {
  std::vector<cardlib::CHand> sh_vector, equiv_vector;
  std::map<cardlib::CHand, int64_t>::const_iterator msi_cit;
  std::vector<cardlib::CHand>::iterator vs_it;
  cardlib::CHand this_hand;
  int64_t this_rank;

  hand_rank_map->clear();

  // sort all hands
  sh_vector.clear();
  for (msi_cit = hand_count_map->cbegin();
       msi_cit != hand_count_map->cend(); ++msi_cit) {
    this_hand = msi_cit->first;

    sh_vector.push_back(this_hand);
  }

  std::sort(sh_vector.begin(), sh_vector.end());

  // hands are sorted so, get rid of the duplicates
  vs_it = std::unique(sh_vector.begin(), sh_vector.end());
  sh_vector.resize(std::distance(sh_vector.begin(), vs_it));

  // change hand_count_map from hand counts to equivalent ranking
  this_rank = 0L;
  for (vs_it = sh_vector.begin();
       vs_it != sh_vector.end(); ++vs_it) {
    this_rank++;

    (*hand_rank_map)[*vs_it] = this_rank;
  }

  return;
}

/*
Single card bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

// #############################################################
// #############################################################
void CFastCalc::LoadMaskMap7Cards(
    std::map<cardlib::CHand, int64_t> *hand_count_map) const {
  mylib::CUtils utils_obj;
  int64_t ii_1, ii_2, ii_3, ii_4, ii_5, ii_6, ii_7;
  std::string sa1, sa2, sa3, sa4, sa5, sa6, sa7;
  cardlib::CCard ca1, ca2, ca3, ca4, ca5, ca6, ca7;
  cardlib::CHand ahand_obj;
  std::vector<cardlib::CCard> card_vector;
  int64_t ncount(0L);

  hand_count_map->clear();
  ncount = 0L;
  // there are 133,784,560 7-card hands
  for (ii_1 = 0L; ii_1 < 46L; ++ii_1) {
    sa1 = card_string_deck_[ii_1];
    ca1.Set(&sa1);
    card_vector.push_back(ca1);
    if (((ii_1 % 12L) == 0L) && (ii_1 > 0L)) {
      printf("%s : %s\n", ca1.CardToString().c_str(),
             utils_obj.CurrentTime().c_str());
    } else {
      printf("%s ", ca1.CardToString().c_str());
    }
    fflush(stdout);

    for (ii_2 = (ii_1 + 1L); ii_2 < 47L; ++ii_2) {
      sa2 = card_string_deck_[ii_2];
      ca2.Set(&sa2);
      card_vector.push_back(ca2);
      for (ii_3 = (ii_2 + 1L); ii_3 < 48L; ++ii_3) {
        sa3 = card_string_deck_[ii_3];
        ca3.Set(&sa3);
        card_vector.push_back(ca3);
        for (ii_4 = (ii_3 + 1L); ii_4 < 49L; ++ii_4) {
          sa4 = card_string_deck_[ii_4];
          ca4.Set(&sa4);
          card_vector.push_back(ca4);
          for (ii_5 = (ii_4 + 1L); ii_5 < 50L; ++ii_5) {
            sa5 = card_string_deck_[ii_5];
            ca5.Set(&sa5);
            card_vector.push_back(ca5);
            for (ii_6 = (ii_5 + 1L); ii_6 < 51L; ++ii_6) {
              sa6 = card_string_deck_[ii_6];
              ca6.Set(&sa6);
              card_vector.push_back(ca6);
              for (ii_7 = (ii_6 + 1L); ii_7 < 52L; ++ii_7) {
                sa7 = card_string_deck_[ii_7];
                ca7.Set(&sa7);
                card_vector.push_back(ca7);

                ncount++;

                ahand_obj.Clear();
                ahand_obj.Calculate(&card_vector);

                (*hand_count_map)[ahand_obj]++;

                // pop back card 7
                card_vector.pop_back();
              }
              // pop back card 6
              card_vector.pop_back();
            }
            // pop back card 5
            card_vector.pop_back();
          }
          // pop back card 4
          card_vector.pop_back();
        }
        // pop back card 3
        card_vector.pop_back();
      }
      // pop back card 2
      card_vector.pop_back();
    }
    // pop back card 1
    card_vector.pop_back();
  }

  printf("%s\n", utils_obj.CurrentTime().c_str());

  return;
}

// #############################################################
// #############################################################
// ### output rank, 5-card and 7-card hand count data
// ### can recover distinct number of hands from this file
void CFastCalc::WriteOutHandEnumerationToFile(
    const std::string *rank_fname,
    const std::map<cardlib::CHand, int64_t> *five_rank_map,
    std::map<cardlib::CHand, int64_t> *five_count_map,
    std::map<cardlib::CHand, int64_t> *seven_count_map) const {
  FILE * r_file;
  std::map<cardlib::CHand, int64_t>::const_iterator mhi_cit;
  cardlib::CHand a_hand;
  int64_t this_rank, this_five_count, this_seven_count;
  std::string this_string;

  r_file = fopen(rank_fname->c_str(), "w");

  for (mhi_cit = five_rank_map->cbegin();
       mhi_cit != five_rank_map->cend(); ++mhi_cit) {
    a_hand = mhi_cit->first;
    this_rank = mhi_cit->second;

    this_five_count = (*five_count_map)[a_hand];
    this_seven_count = (*seven_count_map)[a_hand];
    this_string = a_hand.HandToString();

    fprintf(r_file, "%ld      %ld      %ld      %s\n",
            this_rank, this_five_count, this_seven_count,
            this_string.c_str());
    fflush(r_file);
  }

  fclose(r_file);

  return;
}

// #############################################################
// #############################################################
void CFastCalc::InitDeck(void) {
  std::vector<std::string> cdvalues = {
    "2", "3",  "4",  "5",  "6",
    "7", "8",  "9",  "t",  "j",
    "q", "k",  "a" };
  std::vector<std::string> cdsuits = {
    "h", "d", "c", "s" };
  std::vector<std::string>::const_iterator vsv_cit, vss_cit;
  CCard tc;
  std::string stmp, scard, sresult;

  // ### initialize card deck
  card_deck_.clear();
  for (vss_cit = cdsuits.cbegin();
       vss_cit != cdsuits.cend(); ++vss_cit) {
    stmp = *vss_cit;
    for (vsv_cit = cdvalues.cbegin();
         vsv_cit != cdvalues.cend(); ++vsv_cit) {
      scard = *vsv_cit + stmp;
      tc = CCard(scard);

      card_deck_.push_back(tc);
      card_string_deck_.push_back(scard);
    }
  }

  return;
}

}  // namespace cardlib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

{
  if ($3 > 0) {
    db[$4] += $3
    dbcount[$4]++
  }
}
END {
  distinct = 0
  total = 0
  for (iter in db) {
    print iter, dbcount[iter], db[iter]
    distinct += dbcount[iter]
    total += db[iter]
  }
  print "distinct hands = ", distinct, " : total hands = ", total
}

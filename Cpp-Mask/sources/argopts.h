#ifndef SOURCES_ARGOPTS_H_
#define SOURCES_ARGOPTS_H_
/*

// Copyright 2024 <robert haramoto>

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  argument options class implementation                 ###
  ###                                                        ###
  ###  last updated April 29, 2024                           ###
  ###                                                        ###
  ###  updated July 31, 2022                                 ###
  ###                                                        ###
  ###  updated February 5, 2020                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <string>
#include <map>

// command line option processing
namespace mylib {

class CArgOpts {
 public:
  CArgOpts();
  ~CArgOpts();

  // do not allow copying nor moving
  CArgOpts(const CArgOpts&) = delete;
  CArgOpts& operator=(const CArgOpts&) = delete;
  CArgOpts(CArgOpts&&) = delete;
  CArgOpts& operator=(CArgOpts&&) = delete;

  void Clear();

  void LowerCase(std::string *stmp);

  void SetName(const std::string *sname);

  void SetVersion(const std::string *sversion);

  void DisplayUsage(void);

  void DisplayVersion(void);

  void AddValidOptionNoArgument(
      std::string *shortOpt,
      std::string *longOpt,
      std::string *description);

  void AddValidOptionWithArgument(
      std::string *shortOpt,
      std::string *longOpt,
      std::string *description,
      bool has_argument,
      bool bool_is_arg_numeric,
      std::string *default_argument,
      std::string *argument);

  void ParseArgToOptFlag(
      const std::string *option,
      const std::string *possible1,
      const std::string *possible2,
      std::string *opt_flag,
      std::string *opt_arg);

  void ParsePocketsOption(
      const std::string *option,
      const std::string *possible1,
      const std::string *possible2,
      const std::string *possible3,
      std::string *opt_flag,
      std::string *opt_arg);

  void ProcessOptions(int argc, const char **argv);

  bool OptionEntered(const std::string *opt_flag);

  std::string GetOptionArgument(const std::string *opt_flag);

 private:
  void SetDefaultOptions();

  struct OptStruct {
    OptStruct() : short_opt(""), long_opt(""),
                  description(""),
                  bool_has_argument(false),
                  bool_is_arg_numeric(false),
                  default_argument(""),
                  argument(""),
                  count(0) { }

    std::string short_opt;
    std::string long_opt;
    std::string description;

    bool bool_has_argument;
    bool bool_is_arg_numeric;

    std::string default_argument;
    std::string argument;

    int count;
  };

  std::string version_;
  std::string programName_;

  std::map<std::string, struct OptStruct> valid_opts_map_;
};  // class CArgOpts

}  // namespace mylib

#endif  // SOURCES_ARGOPTS_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CStraightFunctions implementation                     ###
  ###    namespace cardlib                                   ###
  ###                                                        ###
  ###  last updated April 18, 2024                           ###
  ###                                                        ###
  ###  created March 18, 2024                                ###
  ###    used new single integer bit scheme                  ###
  ###    from http://suffe.cool/poker/evaluator.html         ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/straight_functions.h"

#include <cstdint>

#include <map>
#include <algorithm>
#include <vector>
#include <string>
#include <typeinfo>

#include "sources/card.h"
#include "sources/hand.h"

namespace cardlib {
/*
Single card 32-bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

// #############################################################
// #############################################################
CStraightFunctions::CStraightFunctions() {
}

// #############################################################
// #############################################################
CStraightFunctions::~CStraightFunctions() {
}

// #############################################################
// #############################################################
void CStraightFunctions::SetStraightMask(
    int64_t input_order,
    cardlib::CHand::kstraight_mask *output_mask,
    int64_t *output_signature,
    cardlib::CHand::khand_value *output_handvalue) const {
  int64_t ace_signature = {
    cardlib::CHand::kstraight_signature::kAceHighSignature };
  int64_t king_signature = {
    cardlib::CHand::kstraight_signature::kKingHighSignature };
  int64_t queen_signature = {
    cardlib::CHand::kstraight_signature::kQueenHighSignature };
  int64_t jack_signature = {
    cardlib::CHand::kstraight_signature::kJackHighSignature };
  int64_t ten_signature = {
    cardlib::CHand::kstraight_signature::kTenHighSignature };
  int64_t nine_signature = {
    cardlib::CHand::kstraight_signature::kNineHighSignature };
  int64_t eight_signature = {
    cardlib::CHand::kstraight_signature::kEightHighSignature };
  int64_t seven_signature = {
    cardlib::CHand::kstraight_signature::kSevenHighSignature };
  int64_t six_signature = {
    cardlib::CHand::kstraight_signature::kSixHighSignature };
  int64_t five_signature = {
    cardlib::CHand::kstraight_signature::kFiveHighSignature };
  cardlib::CHand::kstraight_mask ace_mask = {
    cardlib::CHand::kstraight_mask::kAceHighStraight };
  cardlib::CHand::kstraight_mask king_mask = {
    cardlib::CHand::kstraight_mask::kKingHighStraight };
  cardlib::CHand::kstraight_mask queen_mask = {
    cardlib::CHand::kstraight_mask::kQueenHighStraight };
  cardlib::CHand::kstraight_mask jack_mask = {
    cardlib::CHand::kstraight_mask::kJackHighStraight };
  cardlib::CHand::kstraight_mask ten_mask = {
    cardlib::CHand::kstraight_mask::kTenHighStraight };
  cardlib::CHand::kstraight_mask nine_mask = {
    cardlib::CHand::kstraight_mask::kNineHighStraight };
  cardlib::CHand::kstraight_mask eight_mask = {
    cardlib::CHand::kstraight_mask::kEightHighStraight };
  cardlib::CHand::kstraight_mask seven_mask = {
    cardlib::CHand::kstraight_mask::kSevenHighStraight };
  cardlib::CHand::kstraight_mask six_mask = {
    cardlib::CHand::kstraight_mask::kSixHighStraight };
  cardlib::CHand::kstraight_mask five_mask = {
    cardlib::CHand::kstraight_mask::kFiveHighStraight };
  cardlib::CHand::kstraight_mask this_mask;

  this_mask =
      static_cast<cardlib::CHand::kstraight_mask>(input_order);

  if ((this_mask & ace_mask) == ace_mask) {
    *output_mask = ace_mask;
    *output_signature = ace_signature;
    *output_handvalue =
        cardlib::CHand::khand_value::kStraight;
    return;
  } else if ((this_mask & king_mask) == king_mask) {
    *output_mask = king_mask;
    *output_signature = king_signature;
    *output_handvalue =
        cardlib::CHand::khand_value::kStraight;
    return;
  } else if ((this_mask & queen_mask) == queen_mask) {
    *output_mask = queen_mask;
    *output_signature = queen_signature;
    *output_handvalue =
        cardlib::CHand::khand_value::kStraight;
    return;
  } else if ((this_mask & jack_mask) == jack_mask) {
    *output_mask = jack_mask;
    *output_signature = jack_signature;
    *output_handvalue =
        cardlib::CHand::khand_value::kStraight;
    return;
  } else if ((this_mask & ten_mask) == ten_mask) {
    *output_mask = ten_mask;
    *output_signature = ten_signature;
    *output_handvalue =
        cardlib::CHand::khand_value::kStraight;
    return;
  } else if ((this_mask & nine_mask) == nine_mask) {
    *output_mask = nine_mask;
    *output_signature = nine_signature;
    *output_handvalue =
        cardlib::CHand::khand_value::kStraight;
    return;
  } else if ((this_mask & eight_mask) == eight_mask) {
    *output_mask = eight_mask;
    *output_signature = eight_signature;
    *output_handvalue =
        cardlib::CHand::khand_value::kStraight;
    return;
  } else if ((this_mask & seven_mask) == seven_mask) {
    *output_mask = seven_mask;
    *output_signature = seven_signature;
    *output_handvalue =
        cardlib::CHand::khand_value::kStraight;
    return;
  } else if ((this_mask & six_mask) == six_mask) {
    *output_mask = six_mask;
    *output_signature = six_signature;
    *output_handvalue =
        cardlib::CHand::khand_value::kStraight;
    return;
  } else if ((this_mask & five_mask) == five_mask) {
    *output_mask = five_mask;
    *output_signature = five_signature;
    *output_handvalue =
        cardlib::CHand::khand_value::kStraight;
    return;
  }

  *output_mask =
      cardlib::CHand::kstraight_mask::kStraightValueNull;
  *output_signature = -1L;
  *output_handvalue =
      cardlib::CHand::khand_value::kHandValueNull;
  return;
}

// #############################################################
// #############################################################
void CStraightFunctions::IsStraight(
    const std::vector<cardlib::CCard> *input_vector,
    cardlib::CHand::kstraight_mask *output_mask,
    int64_t *output_order,
    int64_t *output7_signature,
    int64_t *output5_signature,
    cardlib::CHand::khand_value *output_handvalue) const {
  int64_t this_prime(0L);
  int64_t this_order, hand_order;
  int64_t prime7_signature, this_signature;
  cardlib::CHand::kstraight_mask hand_mask;
  CHand::khand_value hresult;
  std::vector<cardlib::CCard>::const_iterator vc_cit;

  hand_order = 0L;
  prime7_signature = 1L;
  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    this_prime = vc_cit->GetCardPrimeValue();
    this_order = vc_cit->GetCardOrder();

    prime7_signature *= this_prime;
    hand_order = hand_order | this_order;
  }

  SetStraightMask(
      hand_order, &hand_mask,
      &this_signature, &hresult);

  if (hand_mask ==
      cardlib::CHand::kstraight_mask::kStraightValueNull) {
    *output_mask = hand_mask;
    *output_order =
        static_cast<int64_t>(
            cardlib::CHand::kstraight_mask::kStraightValueNull);
    *output7_signature =
        cardlib::CHand::kstraight_signature::kStraightSignatureNull;
    *output5_signature =
        cardlib::CHand::kstraight_signature::kStraightSignatureNull;
    *output_handvalue =
        cardlib::CHand::khand_value::kHandValueNull;

    return;
  }

  *output_mask = hand_mask;
  *output_order = static_cast<int64_t>(hand_mask);
  *output7_signature = prime7_signature;
  *output5_signature = this_signature;
  *output_handvalue = hresult;

  return;
}

// #############################################################
// #############################################################
bool CStraightFunctions::StraightGreaterStraight(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CHand::kstraight_mask sm1, sm2;
  cardlib::CHand::kstraight_mask
      fhs(cardlib::CHand::kstraight_mask::kFiveHighStraight);

  sm1 = chand1->GetHandStraightMask();
  sm2 = chand2->GetHandStraightMask();

  if (sm1 == fhs) {
    return false;
  } else if (sm2 == fhs) {
    // then sm1 != five-high straight-flush so
    return true;
  }

  return (sm1 > sm2);
}

// #############################################################
// #############################################################
bool CStraightFunctions::StraightLessThanStraight(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CHand::kstraight_mask sm1, sm2;
  cardlib::CHand::kstraight_mask
      fhs(cardlib::CHand::kstraight_mask::kFiveHighStraight);

  sm1 = chand1->GetHandStraightMask();
  sm2 = chand2->GetHandStraightMask();

  if (sm2 == fhs) {
    return false;
  } else if (sm1 == fhs) {
    // then sm2 != five-high straight-flush so
    return true;
  }

  return (sm1 < sm2);
}

// #############################################################
// #############################################################
bool CStraightFunctions::StraightEqualStraight(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CHand::kstraight_mask sm1, sm2;

  sm1 = chand1->GetHandStraightMask();
  sm2 = chand2->GetHandStraightMask();

  return (sm1 == sm2);
}

}  // namespace cardlib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

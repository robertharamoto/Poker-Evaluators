/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CEval class implementation                            ###
  ###                                                        ###
  ###  last updated May 1, 2024                              ###
  ###                                                        ###
  ###  created April 20, 2024                                ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/eval.h"

#include <cstdlib>
#include <cstdio>
#include <cstdint>

#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include <chrono>

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/utils.h"
#include "sources/flush_functions.h"
#include "sources/fast_calc.h"
#include "sources/straight_flush_functions.h"

namespace cardlib {

// #############################################################
// #############################################################
CEval::CEval() {
}

// #############################################################
// #############################################################
CEval::~CEval() {
}

// #############################################################
// #############################################################
void CEval::ComputeAll(
    const cardlib::CCard *card_a1, const cardlib::CCard *card_a2,
    const cardlib::CCard *card_b1, const cardlib::CCard *card_b2) const {
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      flushes_vrc_map, nonflushes_vrc_map;
  std::vector<cardlib::CCard> card_deck;

  InitDeck(&card_deck);

  LoadHandMaps(&flushes_vrc_map, &nonflushes_vrc_map);

  HandEval_5cards(&flushes_vrc_map, &nonflushes_vrc_map,
                  &card_deck);

  HandEval_7cards(&flushes_vrc_map, &nonflushes_vrc_map,
                  &card_deck);

  PocketComparison_7cards(
      card_a1, card_a2, card_b1, card_b2,
      &flushes_vrc_map, &nonflushes_vrc_map,
      &card_deck);

  return;
}

// #############################################################
// #############################################################
void CEval::PrintStats(
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *flushes_stats_map,
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *nonflushes_stats_map) const {
  mylib::CUtils utils_obj;
  cardlib::CHand hand_obj;
  std::map<int64_t,
           cardlib::CFastCalc::value_rank_count>::const_iterator
      miv_cit;
  cardlib::CHand::khand_value this_khv, main_khv;
  cardlib::CFastCalc::value_rank_count this_vrc;
  std::vector<cardlib::CHand::khand_value> khv_vector;
  std::vector<cardlib::CHand::khand_value>::const_iterator vhv_cit;
  std::vector<cardlib::CHand::khand_value>::iterator vhv_it;
  std::map<cardlib::CHand::khand_value, int64_t>
      distinct_count_map, count_map;
  int64_t total(0L), sub_total(0L);
  int64_t distinct_subtotal(0L), distinct_total(0L);
  std::string sname;

  // many prime signatures per hand value
  for (miv_cit = flushes_stats_map->cbegin();
       miv_cit != flushes_stats_map->cend(); ++miv_cit) {
    this_vrc = miv_cit->second;
    this_khv =
        static_cast<cardlib::CHand::khand_value>(this_vrc.value);

    khv_vector.push_back(this_khv);
  }

  for (miv_cit = nonflushes_stats_map->cbegin();
       miv_cit != nonflushes_stats_map->cend(); ++miv_cit) {
    this_vrc = miv_cit->second;
    this_khv =
        static_cast<cardlib::CHand::khand_value>(this_vrc.value);

    khv_vector.push_back(this_khv);
  }

  std::sort(khv_vector.begin(), khv_vector.end());
  std::reverse(khv_vector.begin(), khv_vector.end());

  vhv_it = std::unique(khv_vector.begin(), khv_vector.end());
  khv_vector.resize(std::distance(khv_vector.begin(), vhv_it));

  // construct the subtotals
  distinct_count_map.clear();
  count_map.clear();
  for (vhv_cit = khv_vector.cbegin();
       vhv_cit != khv_vector.cend(); ++vhv_cit) {
    main_khv = *vhv_cit;

    // if this hand value a flush hand
    for (miv_cit = flushes_stats_map->cbegin();
         miv_cit != flushes_stats_map->cend(); ++miv_cit) {
      this_vrc = miv_cit->second;
      this_khv =
          static_cast<cardlib::CHand::khand_value>(this_vrc.value);

      if (this_khv == main_khv) {
        distinct_count_map[main_khv]++;
        count_map[main_khv] += this_vrc.count;
      }
    }

    // if this hand value a non-flush hand
    for (miv_cit = nonflushes_stats_map->cbegin();
         miv_cit != nonflushes_stats_map->cend(); ++miv_cit) {
      this_vrc = miv_cit->second;
      this_khv =
          static_cast<cardlib::CHand::khand_value>(this_vrc.value);

      if (this_khv == main_khv) {
        distinct_count_map[main_khv]++;
        count_map[main_khv] += this_vrc.count;
      }
    }
  }

  // print the subtotals
  printf("  Hand totals  :  hand counts  :  distinct hands\n");
  total = 0L;
  distinct_total = 0L;
  for (vhv_cit = khv_vector.cbegin();
       vhv_cit != khv_vector.cend(); ++vhv_cit) {
    main_khv = *vhv_cit;
    sname = hand_obj.HandName(main_khv);

    sub_total = count_map[main_khv];
    distinct_subtotal = distinct_count_map[main_khv];
    printf("  %s  :  %s  :  %s\n",
           sname.c_str(),
           utils_obj.CommafyNum(sub_total).c_str(),
           utils_obj.CommafyNum(distinct_subtotal).c_str());
    total += sub_total;
    distinct_total += distinct_subtotal;
  }

  printf("  totals  :  %s  :  %s\n",
         utils_obj.CommafyNum(total).c_str(),
         utils_obj.CommafyNum(distinct_total).c_str());
  fflush(stdout);

  return;
}

// #############################################################
// #############################################################
void CEval::HandEval_5cards(
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *flushes_vrc_map,
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *nonflushes_vrc_map,
    std::vector<cardlib::CCard> *card_deck) const {
  mylib::CUtils utils_obj;
  cardlib::CCard card1, card2, card3, card4, card5;
  cardlib::CCard::kcard_suit
      suit1, suit2, suit3, suit4, suit5;
  int64_t k1, k2, k3, k4, k5;
  int64_t prime1, prime2, prime3, prime4, prime5;
  int64_t icount, prime_signature;
  int64_t nsize;
  std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> start_time, end_time;
  double dtmp, dsecs;
  cardlib::CFastCalc::value_rank_count this_vrc, stats_vrc;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      flushes_stats_map, nonflushes_stats_map;

  start_time = std::chrono::steady_clock::now();

  printf("starting 5-card hand eval speed test : %s\n",
         utils_obj.CurrentDateTime().c_str());
  fflush(stdout);

  nsize = static_cast<int64_t>(card_deck->size());
  prime_signature = 1L;
  icount = 0L;
  for (k1 = 0L; k1 < (nsize - 4L); ++k1) {
    card1 = card_deck->at(k1);
    suit1 = card1.GetCardSuit();
    prime1 = card1.GetCardPrimeValue();
    prime_signature = prime_signature * prime1;
    for (k2 = k1 + 1L; k2 < (nsize - 3L); ++k2) {
      card2 = card_deck->at(k2);
      suit2 = card2.GetCardSuit();
      prime2 = card2.GetCardPrimeValue();
      prime_signature = prime_signature * prime2;
      for (k3 = k2 + 1L; k3 < (nsize - 2L); ++k3) {
        card3 = card_deck->at(k3);
        suit3 = card3.GetCardSuit();
        prime3 = card3.GetCardPrimeValue();
        prime_signature = prime_signature * prime3;
        for (k4 = k3 + 1L; k4 < (nsize - 1L); ++k4) {
          card4 = card_deck->at(k4);
          suit4 = card4.GetCardSuit();
          prime4 = card4.GetCardPrimeValue();
          prime_signature = prime_signature * prime4;
          for (k5 = k4 + 1L; k5 < nsize; ++k5) {
            card5 = card_deck->at(k5);
            suit5 = card5.GetCardSuit();
            prime5 = card5.GetCardPrimeValue();
            prime_signature = prime_signature * prime5;
            ++icount;

            if ((suit1 == suit2)
                && (suit1 == suit3)
                && (suit1 == suit4)
                && (suit1 == suit5)) {
              this_vrc = (*flushes_vrc_map)[prime_signature];

              stats_vrc = flushes_stats_map[prime_signature];
              stats_vrc.value = this_vrc.value;
              stats_vrc.rank = this_vrc.rank;
              stats_vrc.count += 1L;
              flushes_stats_map[prime_signature] = stats_vrc;
            } else {
              this_vrc = (*nonflushes_vrc_map)[prime_signature];

              stats_vrc = nonflushes_stats_map[prime_signature];
              stats_vrc.value = this_vrc.value;
              stats_vrc.rank = this_vrc.rank;
              stats_vrc.count += 1L;
              nonflushes_stats_map[prime_signature] = this_vrc;
            }

            prime_signature = prime_signature / prime5;
          }
          prime_signature = prime_signature / prime4;
        }
        prime_signature = prime_signature / prime3;
      }
      prime_signature = prime_signature / prime2;
    }
    prime_signature = prime_signature / prime1;
  }

  PrintStats(&flushes_stats_map, &nonflushes_stats_map);

  end_time = std::chrono::steady_clock::now();

  dsecs =
      utils_obj.ClockDurationInSeconds(&end_time, &start_time);
  dtmp = static_cast<double>(icount) / dsecs;

  printf("number of hands evaluated = %s\n",
         utils_obj.CommafyNum(icount).c_str());
  printf("5-card hand evaluations per second = %s\n",
         utils_obj.CommafyNum(static_cast<int64_t>(dtmp)).c_str());

  printf("Completed 5-card hand eval speed test\n");
  printf("elapsed time = %s : %s\n",
         utils_obj.ClockDuration(&end_time, &start_time).c_str(),
         utils_obj.CurrentDateTime().c_str());

  printf("\n");
  fflush(stdout);

  return;
}

// #############################################################
// #############################################################
void CEval::HandEval_7cards(
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *flushes_vrc_map,
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *nonflushes_vrc_map,
    std::vector<cardlib::CCard> *card_deck) const {
  mylib::CUtils utils_obj;
  cardlib::CHand hand_obj;
  cardlib::CFlushFunctions flushf_obj;
  cardlib::CCard
      card1, card2, card3, card4, card5, card6, card7;
  cardlib::CCard::kcard_suit fsuit;
  std::vector<cardlib::CCard> card_hand_vector;
  int64_t k1, k2, k3, k4, k5, k6, k7;
  int64_t icount, nsize;
  int64_t prime_signature;
  std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> start_time, end_time;
  double dtmp, dsecs;
  cardlib::CFastCalc::value_rank_count this_vrc, stats_vrc;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      flushes_stats_map, nonflushes_stats_map;

  start_time = std::chrono::steady_clock::now();

  printf("starting 7-card hand eval speed test : %s\n",
         utils_obj.CurrentDateTime().c_str());
  fflush(stdout);

  nsize = static_cast<int64_t>(card_deck->size());
  icount = 0L;
  for (k1 = 0L; k1 < (nsize - 6L); ++k1) {
    card1 = card_deck->at(k1);
    card_hand_vector.push_back(card1);
    for (k2 = (k1 + 1L); k2 < (nsize - 5L); ++k2) {
      card2 = card_deck->at(k2);
      card_hand_vector.push_back(card2);
      for (k3 = (k2 + 1L); k3 < (nsize - 4L); ++k3) {
        card3 = card_deck->at(k3);
        card_hand_vector.push_back(card3);
        for (k4 = (k3 + 1L); k4 < (nsize - 3L); ++k4) {
          card4 = card_deck->at(k4);
          card_hand_vector.push_back(card4);
          for (k5 = (k4 + 1L); k5 < (nsize - 2L); ++k5) {
            card5 = card_deck->at(k5);
            card_hand_vector.push_back(card5);
            for (k6 = (k5 + 1L); k6 < (nsize - 1L); ++k6) {
              card6 = card_deck->at(k6);
              card_hand_vector.push_back(card6);
              for (k7 = (k6 + 1L); k7 < nsize; ++k7) {
                card7 = card_deck->at(k7);
                card_hand_vector.push_back(card7);

                ++icount;
                fsuit = flushf_obj.GetFlushSuit(&card_hand_vector);

                BestFiveCardsOfSeven(
                    &card_hand_vector, fsuit,
                    flushes_vrc_map, nonflushes_vrc_map,
                    &prime_signature, &this_vrc);

                if (fsuit != cardlib::CCard::kcard_suit::kNullSuit) {
                  stats_vrc = flushes_stats_map[prime_signature];
                  stats_vrc.value = this_vrc.value;
                  stats_vrc.rank = this_vrc.rank;
                  stats_vrc.count += 1L;
                  flushes_stats_map[prime_signature] = stats_vrc;
                } else {
                  stats_vrc = nonflushes_stats_map[prime_signature];
                  stats_vrc.value = this_vrc.value;
                  stats_vrc.rank = this_vrc.rank;
                  stats_vrc.count += 1L;
                  nonflushes_stats_map[prime_signature] = stats_vrc;
                }

                // remove card 7
                card_hand_vector.pop_back();
              }
              // remove card 6
              card_hand_vector.pop_back();
            }
            // remove card 5
            card_hand_vector.pop_back();
          }
          // remove card 4
          card_hand_vector.pop_back();
        }
        // remove card 3
        card_hand_vector.pop_back();
      }
      // remove card 2
      card_hand_vector.pop_back();
    }
    // remove card 1
    card_hand_vector.pop_back();
  }

  PrintStats(&flushes_stats_map, &nonflushes_stats_map);

  end_time = std::chrono::steady_clock::now();

  dsecs =
      utils_obj.ClockDurationInSeconds(&end_time, &start_time);
  dtmp = static_cast<double>(icount) / dsecs;

  printf("number of hands evaluated = %s\n",
         utils_obj.CommafyNum(icount).c_str());
  printf("7-card hand evaluations per second = %s\n",
         utils_obj.CommafyNum(static_cast<int64_t>(dtmp)).c_str());

  printf("Completed 7-card hand eval speed test\n");
  printf("elapsed time = %s : %s\n",
         utils_obj.ClockDuration(&end_time, &start_time).c_str(),
         utils_obj.CurrentDateTime().c_str());

  printf("\n");
  fflush(stdout);

  return;
}

// #############################################################
// #############################################################
void CEval::PocketComparison_7cards(
    const cardlib::CCard *card_a1,
    const cardlib::CCard *card_a2,
    const cardlib::CCard *card_b1,
    const cardlib::CCard *card_b2,
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *flushes_vrc_map,
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *nonflushes_vrc_map,
    const std::vector<cardlib::CCard> *card_deck) const {
  mylib::CUtils utils_obj;
  cardlib::CFlushFunctions flushf_obj;
  std::vector<cardlib::CCard> local_card_deck;
  std::vector<cardlib::CCard>::iterator cd_it;
  std::vector<cardlib::CCard> hand1_vector, hand2_vector;
  cardlib::CCard card1, card2, card3, card4, card5;
  cardlib::CCard::kcard_suit suit1, suit2;
  cardlib::CFastCalc::value_rank_count this1_vrc, this2_vrc;
  int64_t k1, k2, k3, k4, k5;
  int64_t ntotals;
  int64_t nwins, nlosses, nties;
  int64_t prime_signature;
  int64_t nsize;
  std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> start_time, end_time;
  double dtmp, dsecs;
  double pcnt_wins, pcnt_losses, pcnt_ties, pcnt_total;
  std::string sname1, sname2;
  std::string spocket1, spocket2;

  local_card_deck = *card_deck;

  start_time = std::chrono::steady_clock::now();

  printf("starting 7-card prime-signature pocket comparison loop : %s\n",
         utils_obj.CurrentDateTime().c_str());

  // remove pocket cards from deck
  cd_it = find(local_card_deck.begin(), local_card_deck.end(),
             *card_a1);
  local_card_deck.erase(cd_it);
  cd_it = find(local_card_deck.begin(), local_card_deck.end(),
               *card_a2);
  local_card_deck.erase(cd_it);
  cd_it = find(local_card_deck.begin(), local_card_deck.end(),
               *card_b1);
  local_card_deck.erase(cd_it);
  cd_it = find(local_card_deck.begin(), local_card_deck.end(),
               *card_b2);
  local_card_deck.erase(cd_it);


  nsize = local_card_deck.size();
  hand1_vector.clear();
  hand1_vector.push_back(*card_a1);
  hand1_vector.push_back(*card_a2);
  hand2_vector.clear();
  hand2_vector.push_back(*card_b1);
  hand2_vector.push_back(*card_b2);
  ntotals = 0;
  nwins = nlosses = nties = 0;
  for (k1 = 0L; k1 < (nsize - 4L); ++k1) {
    card1 = local_card_deck[k1];
    hand1_vector.push_back(card1);
    hand2_vector.push_back(card1);
    for (k2 = k1 + 1L; k2 < (nsize - 3L); ++k2) {
      card2 = local_card_deck[k2];
      hand1_vector.push_back(card2);
      hand2_vector.push_back(card2);
      for (k3 = k2 + 1L; k3 < (nsize - 2L); ++k3) {
        card3 = local_card_deck[k3];
        hand1_vector.push_back(card3);
        hand2_vector.push_back(card3);
        for (k4 = k3 + 1L; k4 < (nsize - 1L); ++k4) {
          card4 = local_card_deck[k4];
          hand1_vector.push_back(card4);
          hand2_vector.push_back(card4);
          for (k5 = k4 + 1L; k5 < nsize; ++k5) {
            card5 = local_card_deck[k5];
            hand1_vector.push_back(card5);
            hand2_vector.push_back(card5);

            suit1 = flushf_obj.GetFlushSuit(&hand1_vector);
            suit2 = flushf_obj.GetFlushSuit(&hand2_vector);

            BestFiveCardsOfSeven(
                &hand1_vector, suit1,
                flushes_vrc_map, nonflushes_vrc_map,
                &prime_signature, &this1_vrc);

            BestFiveCardsOfSeven(
                &hand2_vector, suit2,
                flushes_vrc_map, nonflushes_vrc_map,
                &prime_signature, &this2_vrc);

            ++ntotals;
            if (this1_vrc.rank > this2_vrc.rank) {
              ++nwins;
            } else if (this1_vrc.rank < this2_vrc.rank) {
              ++nlosses;
            } else {
              ++nties;
            }

            // remove card5
            hand1_vector.pop_back();
            hand2_vector.pop_back();
          }
          // remove card4
          hand1_vector.pop_back();
          hand2_vector.pop_back();
        }
        // remove card3
        hand1_vector.pop_back();
        hand2_vector.pop_back();
      }
      // remove card2
      hand1_vector.pop_back();
      hand2_vector.pop_back();
    }
    // remove card1
    hand1_vector.pop_back();
    hand2_vector.pop_back();
  }

  printf("completed pocket comparison loop : %s\n",
         utils_obj.CurrentDateTime().c_str());

  spocket1 = "[" + card_a1->CardToString() +
      ", " + card_a2->CardToString() + "]";
  spocket2 = "[" + card_b1->CardToString() +
      ", " + card_b2->CardToString() + "]";

  printf("%s versus %s\n", spocket1.c_str(), spocket2.c_str());

  double dtotals = static_cast<double>(ntotals);
  pcnt_wins = utils_obj.RoundFloat((100.0 * nwins) / dtotals, 2.0);
  pcnt_losses = utils_obj.RoundFloat((100.0 * nlosses) / dtotals, 2.0);
  pcnt_ties = utils_obj.RoundFloat((100.0 * nties) / dtotals, 2.0);
  pcnt_total = utils_obj.RoundFloat((100.0 * (nwins + nlosses + nties)
                                     / dtotals), 2.0);

  printf("Number of wins for %s = %s (%4.2f%%)\n",
         spocket1.c_str(), utils_obj.CommafyNum(nwins).c_str(),
         pcnt_wins);
  printf("Number of losses for %s = %s (%4.2f%%)\n",
         spocket1.c_str(), utils_obj.CommafyNum(nlosses).c_str(),
         pcnt_losses);
  printf("Number of ties for %s = %s (%4.2f%%)\n",
         spocket1.c_str(), utils_obj.CommafyNum(nties).c_str(),
         pcnt_ties);
  printf("Total %s = %s (%4.2f%%)\n", spocket1.c_str(),
         utils_obj.CommafyNum(nwins+nlosses+nties).c_str(),
         pcnt_total);

  end_time = std::chrono::steady_clock::now();

  dsecs =
      utils_obj.ClockDurationInSeconds(&end_time, &start_time);
  dtmp = static_cast<double>(ntotals) / dsecs;

  printf("number of hands evaluated = %s\n",
         utils_obj.CommafyNum(ntotals).c_str());
  printf("5-card (best-hand) 2-pocket comparisons per second = %s\n",
         utils_obj.CommafyNum(static_cast<int64_t>(dtmp)).c_str());
  printf("elapsed time = %s : %s\n",
         utils_obj.ClockDuration(&end_time, &start_time).c_str(),
         utils_obj.CurrentDateTime().c_str());

  printf("\n");
  fflush(stdout);

  return;
}

// #############################################################
// #############################################################
void CEval::BestFiveCardsOfSeven(
    const std::vector<cardlib::CCard> *cc_vector,
    const cardlib::CCard::kcard_suit flush_suit,
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *flushes_vrc_map,
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *nonflushes_vrc_map,
    int64_t *best_prime_signature,
    cardlib::CFastCalc::value_rank_count *result_vrc) const {

  if (flush_suit != cardlib::CCard::kcard_suit::kNullSuit) {
    BestFlushCardsOfSeven(
        cc_vector, flush_suit, flushes_vrc_map,
        best_prime_signature, result_vrc);
  } else {
    BestOtherCardsOfSeven(
        cc_vector, nonflushes_vrc_map,
        best_prime_signature, result_vrc);
  }

  return;
}

// #############################################################
// #############################################################
void CEval::BestFlushCardsOfSeven(
    const std::vector<cardlib::CCard> *cc_vector,
    const cardlib::CCard::kcard_suit flush_suit,
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *flushes_vrc_map,
    int64_t *best_prime_signature,
    cardlib::CFastCalc::value_rank_count *result_vrc) const {
  cardlib::CStraightFlushFunctions sff_obj;
  cardlib::CCard::kcard_suit this_suit;
  int64_t this_prime, prime_signature;
  int64_t card_count;
  const int64_t max_card_count = 5L;
  cardlib::CHand::kstraight_mask output_mask;
  int64_t output_order, output7_signature, output5_signature;
  cardlib::CHand::khand_value output_handvalue;
  std::vector<cardlib::CCard> local_vector;
  cardlib::CFastCalc::value_rank_count this_vrc;
  std::vector<cardlib::CCard>::const_iterator vc_cit;

  // first check if it's a straight flush
  sff_obj.IsStraightFlush(
      flush_suit, cc_vector,
      &output_mask, &output_order,
      &output7_signature, &output5_signature,
      &output_handvalue);
  if (output_handvalue != CHand::khand_value::kHandValueNull) {
    this_vrc = (*flushes_vrc_map)[output5_signature];

    *best_prime_signature = output5_signature;
    result_vrc->value = output_handvalue;
    result_vrc->rank = this_vrc.rank;
    result_vrc->count = 1L;

    return;
  }

  // next check for ordinary flush
  local_vector.clear();
  prime_signature = 1L;
  for (vc_cit = cc_vector->cbegin();
       vc_cit != cc_vector->cend(); ++vc_cit) {
    this_suit = vc_cit->GetCardSuit();
    if (this_suit == flush_suit) {
      local_vector.push_back(*vc_cit);
    }
  }

  std::sort(local_vector.begin(), local_vector.end());
  std::reverse(local_vector.begin(), local_vector.end());

  card_count = 0L;
  for (vc_cit = local_vector.cbegin();
       vc_cit != local_vector.cend(); ++vc_cit) {
    if (card_count >= max_card_count) {
      break;
    }

    this_prime = vc_cit->GetCardPrimeValue();
    prime_signature = prime_signature * this_prime;
    ++card_count;
  }

  this_vrc = (*flushes_vrc_map)[prime_signature];

  *best_prime_signature = prime_signature;

  result_vrc->value = this_vrc.value;
  result_vrc->rank = this_vrc.rank;
  result_vrc->count = this_vrc.count;

  return;
}

// #############################################################
// #############################################################
// ###  choose best 5 cards from 7
// ###  there are 7(Choose)5 = (7 * 6) / (2 * 1) = 21 ways
// ###  to choose 5 cards from 7.
void CEval::BestOtherCardsOfSeven(
    const std::vector<cardlib::CCard> *cc_vector,
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *nonflushes_vrc_map,
    int64_t *best_prime_signature,
    cardlib::CFastCalc::value_rank_count *result_vrc) const {
  cardlib::CFlushFunctions flushf_obj;
  int64_t max_handvalue;
  int64_t max_rank;
  int64_t cc_size;
  int64_t max_ii_1, max_ii_2, max_ii_3, max_ii_4, max_ii_5;
  int64_t ii_1, ii_2, ii_3, ii_4, ii_5;
  int64_t prime_signature;
  int64_t this_prime1, this_prime2, this_prime3;
  int64_t this_prime4, this_prime5;
  cardlib::CCard card1, card2, card3, card4, card5;
  cardlib::CFastCalc::value_rank_count this_vrc { 0, 0, 0 };

  cc_size = static_cast<int64_t>(cc_vector->size());

  max_ii_1 = cc_size - 4L;
  max_ii_2 = max_ii_1 + 1L;
  max_ii_3 = max_ii_2 + 1L;
  max_ii_4 = max_ii_3 + 1L;
  max_ii_5 = max_ii_4 + 1L;

  max_handvalue = -1L;
  max_rank = -1L;
  prime_signature = 1L;
  // find the best 5-card hand out of 7-cards
  for (ii_1 = 0L; ii_1 < max_ii_1; ++ii_1) {
    card1 = cc_vector->at(ii_1);
    this_prime1 = card1.GetCardPrimeValue();
    prime_signature = prime_signature * this_prime1;
    for (ii_2 = (ii_1 + 1L); ii_2 < max_ii_2; ++ii_2) {
      card2 = cc_vector->at(ii_2);
      this_prime2 = card2.GetCardPrimeValue();
      prime_signature = prime_signature * this_prime2;
      for (ii_3 = (ii_2 + 1L); ii_3 < max_ii_3; ++ii_3) {
        card3 = cc_vector->at(ii_3);
        this_prime3 = card3.GetCardPrimeValue();
        prime_signature = prime_signature * this_prime3;
        for (ii_4 = (ii_3 + 1L); ii_4 < max_ii_4; ++ii_4) {
          card4 = cc_vector->at(ii_4);
          this_prime4 = card4.GetCardPrimeValue();
          prime_signature = prime_signature * this_prime4;
          for (ii_5 = (ii_4 + 1L); ii_5 < max_ii_5; ++ii_5) {
            card5 = cc_vector->at(ii_5);
            this_prime5 = card5.GetCardPrimeValue();
            prime_signature = prime_signature * this_prime5;

            this_vrc = (*nonflushes_vrc_map)[prime_signature];

            if (this_vrc.rank > max_rank) {
              max_rank = this_vrc.rank;
              *best_prime_signature = prime_signature;

              if (this_vrc.value > max_handvalue) {
                max_handvalue = this_vrc.value;
              }
            }

            prime_signature = prime_signature / this_prime5;
          }
          prime_signature = prime_signature / this_prime4;
        }
        prime_signature = prime_signature / this_prime3;
      }
      prime_signature = prime_signature / this_prime2;
    }
    prime_signature = prime_signature / this_prime1;
  }

  result_vrc->value = max_handvalue;
  result_vrc->rank = max_rank;
  result_vrc->count = 0L;

  return;
}

// #############################################################
// #############################################################
void CEval::LoadHandMaps(
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *flushes_signature_vrc_map,
    std::map<int64_t, cardlib::CFastCalc::value_rank_count>
    *nonflushes_signature_vrc_map) const {
  cardlib::CFastCalc fastcalc_obj;

  fastcalc_obj.PopulateSignatureValueMaps(
      flushes_signature_vrc_map, nonflushes_signature_vrc_map);

  return;
}

// #############################################################
// #############################################################
void CEval::InitDeck(
    std::vector<cardlib::CCard> *card_deck) const {
  std::vector<std::string> cdvalues = {
    "2", "3",  "4",  "5",  "6",
    "7", "8",  "9",  "t",  "j",
    "q", "k",  "a"};
  std::vector<std::string> cdsuits = {
    "h", "d", "c", "s"};
  std::vector<std::string>::const_iterator vsv_cit, vss_cit;
  cardlib::CCard tc;
  std::string stmp, scard, sresult;

  // ### initialize card deck
  card_deck->clear();
  for (vss_cit = cdsuits.cbegin();
       vss_cit != cdsuits.cend(); ++vss_cit) {
    stmp = *vss_cit;
    for (vsv_cit = cdvalues.cbegin();
         vsv_cit != cdvalues.cend(); ++vsv_cit) {
      scard = *vsv_cit + stmp;
      tc = CCard(scard);

      card_deck->push_back(tc);
    }
  }

  return;
}

}  // namespace cardlib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

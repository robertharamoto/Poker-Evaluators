/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  COnePairFunctions implementation                      ###
  ###  namespace cardlib                                     ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###                                                        ###
  ###  created March 18, 2024                                ###
  ###    used new single integer bit scheme                  ###
  ###    from http://suffe.cool/poker/evaluator.html         ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/one_pair_functions.h"

#include <cstdint>

#include <map>
#include <algorithm>
#include <vector>
#include <string>
#include <typeinfo>

#include "sources/card.h"
#include "sources/hand.h"


namespace cardlib {
/*
Single card 32-bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

// #############################################################
// #############################################################
COnePairFunctions::COnePairFunctions() {
}

// #############################################################
// #############################################################
COnePairFunctions::~COnePairFunctions() {
}

// #############################################################
// #############################################################
void COnePairFunctions::FormOnePair(
    const std::vector<cardlib::CCard> *input_vector,
    const std::map<cardlib::CCard::kcard_value, int64_t>
    *value_count_map,
    cardlib::CCard::kcard_value *output_toppair,
    cardlib::CCard::kcard_value *output_kicker1,
    cardlib::CCard::kcard_value *output_kicker2,
    cardlib::CCard::kcard_value *output_kicker3,
    int64_t *output7_signature,
    int64_t *output5_signature,
    cardlib::CHand::khand_value *output_handvalue) const {
  std::vector<cardlib::CCard>::const_iterator vc_cit;
  std::map<cardlib::CCard::kcard_value, int64_t>::const_iterator
      mvi_cit;
  int64_t nn2count_constant(2L), nn1count_constant(1L);
  int64_t jtmp, this_prime;
  cardlib::CCard::kcard_value this_value, first_pair;
  cardlib::CCard::kcard_value high_card1, high_card2, high_card3;
  int64_t prime7_signature, prime5_signature;

  first_pair = static_cast<cardlib::CCard::kcard_value>(-1L);
  high_card1 = static_cast<cardlib::CCard::kcard_value>(-1L);
  high_card2 = static_cast<cardlib::CCard::kcard_value>(-1L);
  high_card3 = static_cast<cardlib::CCard::kcard_value>(-1L);

  // find top pair value, with 3-kickers
  for (mvi_cit = value_count_map->cbegin();
       mvi_cit != value_count_map->cend(); ++mvi_cit) {
    this_value = mvi_cit->first;
    jtmp = mvi_cit->second;
    if (jtmp == nn2count_constant) {
      if (this_value > first_pair) {
        first_pair = this_value;
      }
    } else if (jtmp == nn1count_constant) {
      // find high cards
      if (this_value > high_card1) {
        high_card3 = high_card2;
        high_card2 = high_card1;
        high_card1 = this_value;
      } else if (this_value > high_card2) {
        high_card3 = high_card2;
        high_card2 = this_value;
      } else if (this_value > high_card3) {
        high_card3 = this_value;
      }
    }
  }

  if ((first_pair < 0L)
      || (high_card1 < 0L)
      || (high_card2 < 0L)
      || (high_card3 < 0L)) {
    *output_toppair =
        cardlib::CCard::kcard_value::kValueNull;
    *output_kicker1 =
        cardlib::CCard::kcard_value::kValueNull;
    *output_kicker2 =
        cardlib::CCard::kcard_value::kValueNull;
    *output_kicker3 =
        cardlib::CCard::kcard_value::kValueNull;
    *output7_signature = -1L;
    *output5_signature = -1L;
    *output_handvalue =
        cardlib::CHand::khand_value::kHandValueNull;

    return;
  }

  // calculate signature of the best of 5 hand
  prime7_signature = prime5_signature = 1L;
  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    this_value = vc_cit->GetCardValue();
    this_prime = vc_cit->GetCardPrimeValue();

    prime7_signature *= this_prime;
    if ((this_value == first_pair)
        || (this_value == high_card1)
        || (this_value == high_card2)
        || (this_value == high_card3)) {
      prime5_signature *= this_prime;
    }
  }

  *output_toppair = first_pair;
  *output_kicker1 = high_card1;
  *output_kicker2 = high_card2;
  *output_kicker3 = high_card3;
  *output7_signature = prime7_signature;
  *output5_signature = prime5_signature;
  *output_handvalue =
      cardlib::CHand::khand_value::kOnePair;

  return;
}

// #############################################################
// #############################################################
void COnePairFunctions::IsOnePair(
    const std::vector<cardlib::CCard> *input_vector,
    const std::map<cardlib::CCard::kcard_value, int64_t>
    *value_count_map,
    cardlib::CCard::kcard_value *output_toppair,
    cardlib::CCard::kcard_value *output_kicker1,
    cardlib::CCard::kcard_value *output_kicker2,
    cardlib::CCard::kcard_value *output_kicker3,
    int64_t *output7_signature,
    int64_t *output5_signature,
    cardlib::CHand::khand_value *output_handvalue) const {
  FormOnePair(
      input_vector, value_count_map,
      output_toppair, output_kicker1,
      output_kicker2, output_kicker3,
      output7_signature, output5_signature,
      output_handvalue);

  return;
}

// #############################################################
// #############################################################
bool COnePairFunctions::OnePairGreaterOnePair(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  int64_t toppair_1, kicker1_1, kicker2_1, kicker3_1;
  int64_t toppair_2, kicker1_2, kicker2_2, kicker3_2;

  toppair_1 = chand1->GetTopPair();
  toppair_2 = chand2->GetTopPair();
  if (toppair_1 > toppair_2) {
    return true;
  } else if (toppair_1 < toppair_2) {
    return false;
  }

  // if we reach here, then the one pairs are identical
  kicker1_1 = chand1->GetKicker1();
  kicker1_2 = chand2->GetKicker1();
  if (kicker1_1 > kicker1_2) {
    return true;
  } else if (kicker1_1 < kicker1_2) {
    return false;
  }

  // if we reach here, then the second high max cards are identical
  kicker2_1 = chand1->GetKicker2();
  kicker2_2 = chand2->GetKicker2();
  if (kicker2_1 > kicker2_2) {
    return true;
  } else if (kicker2_1 < kicker2_2) {
    return false;
  }

  // if we reach here, then the second high max cards are identical
  kicker3_1 = chand1->GetKicker3();
  kicker3_2 = chand2->GetKicker3();

  return (kicker3_1 > kicker3_2);
}

// #############################################################
// #############################################################
bool COnePairFunctions::OnePairLessThanOnePair(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  int64_t toppair_1, kicker1_1, kicker2_1, kicker3_1;
  int64_t toppair_2, kicker1_2, kicker2_2, kicker3_2;

  toppair_1 = chand1->GetTopPair();
  toppair_2 = chand2->GetTopPair();
  if (toppair_1 < toppair_2) {
    return true;
  } else if (toppair_1 > toppair_2) {
    return false;
  }

  // if we reach here, then the one pairs are identical
  kicker1_1 = chand1->GetKicker1();
  kicker1_2 = chand2->GetKicker1();
  if (kicker1_1 < kicker1_2) {
    return true;
  } else if (kicker1_1 > kicker1_2) {
    return false;
  }

  // if we reach here, then the second high max cards are identical
  kicker2_1 = chand1->GetKicker2();
  kicker2_2 = chand2->GetKicker2();
  if (kicker2_1 < kicker2_2) {
    return true;
  } else if (kicker2_1 > kicker2_2) {
    return false;
  }

  // if we reach here, then the second high max cards are identical
  kicker3_1 = chand1->GetKicker3();
  kicker3_2 = chand2->GetKicker3();

  return (kicker3_1 < kicker3_2);
}

// #############################################################
// #############################################################
bool COnePairFunctions::OnePairEqualOnePair(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  int64_t toppair_1, kicker1_1, kicker2_1, kicker3_1;
  int64_t toppair_2, kicker1_2, kicker2_2, kicker3_2;

  toppair_1 = chand1->GetTopPair();
  toppair_2 = chand2->GetTopPair();
  if (toppair_1 < toppair_2) {
    return false;
  } else if (toppair_1 > toppair_2) {
    return false;
  }

  // if we reach here, then the one pairs are identical
  kicker1_1 = chand1->GetKicker1();
  kicker1_2 = chand2->GetKicker1();
  if (kicker1_1 < kicker1_2) {
    return false;
  } else if (kicker1_1 > kicker1_2) {
    return false;
  }

  // if we reach here, then the second high max cards are identical
  kicker2_1 = chand1->GetKicker2();
  kicker2_2 = chand2->GetKicker2();
  if (kicker2_1 < kicker2_2) {
    return false;
  } else if (kicker2_1 > kicker2_2) {
    return false;
  }

  // if we reach here, then the second high max cards are identical
  kicker3_1 = chand1->GetKicker3();
  kicker3_2 = chand2->GetKicker3();

  return (kicker3_1 == kicker3_2);
}

}  // namespace cardlib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

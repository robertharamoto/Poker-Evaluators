/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CTwoPairFunctions implementation                      ###
  ###  namespace cardlib                                     ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###                                                        ###
  ###  created March 18, 2024                                ###
  ###    used new single integer bit scheme                  ###
  ###    from http://suffe.cool/poker/evaluator.html         ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/two_pair_functions.h"

#include <cstdint>

#include <map>
#include <algorithm>
#include <vector>
#include <string>
#include <typeinfo>

#include "sources/card.h"

namespace cardlib {
/*
Single card 32-bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

// #############################################################
// #############################################################
CTwoPairFunctions::CTwoPairFunctions() {
}

// #############################################################
// #############################################################
CTwoPairFunctions::~CTwoPairFunctions() {
}

// #############################################################
// #############################################################
void CTwoPairFunctions::FormTwoPair(
    const std::vector<cardlib::CCard> *input_vector,
    const std::map<cardlib::CCard::kcard_value, int64_t>
    *value_count_map,
    cardlib::CCard::kcard_value *output_toppair,
    cardlib::CCard::kcard_value *output_secondpair,
    cardlib::CCard::kcard_value *output_kicker1,
    int64_t *output7_signature,
    int64_t *output5_signature,
    cardlib::CHand::khand_value *output_handvalue) const {
  std::vector<cardlib::CCard>::const_iterator vc_cit;
  std::map<cardlib::CCard::kcard_value, int64_t>::const_iterator
      mvi_cit;
  int64_t nn2count_constant(2L), nn1count_constant(1L);
  int64_t card_count;
  cardlib::CCard::kcard_value card_value;
  cardlib::CCard::kcard_value first_pair, second_pair;
  cardlib::CCard::kcard_value third_pair, kicker;
  int64_t prime7_signature, prime5_signature;
  int64_t this_prime;
  bool binit;

  first_pair = second_pair = third_pair = kicker =
      static_cast<cardlib::CCard::kcard_value>(-1L);

  // find first and second pair values, possible to have 3-pairs
  for (mvi_cit = value_count_map->cbegin();
       mvi_cit != value_count_map->cend(); ++mvi_cit) {
    card_value = mvi_cit->first;
    card_count = mvi_cit->second;
    if (card_count == nn2count_constant) {
      if (card_value > first_pair) {
        third_pair = second_pair;
        second_pair = first_pair;
        first_pair = card_value;
      } else if (card_value > second_pair) {
        third_pair = second_pair;
        second_pair = card_value;
      } else if (card_value > third_pair) {
        third_pair = card_value;
      }
    } else if (card_count == nn1count_constant) {
      // find high card
      if (card_value > kicker) {
        kicker = card_value;
      }
    }
  }

  if (third_pair > kicker) {
    kicker = third_pair;
  }

  if ((first_pair < 0L)
      || (second_pair < 0L)
      || (kicker < 0L)) {
    *output_toppair =
        cardlib::CCard::kcard_value::kValueNull;
    *output_secondpair =
        cardlib::CCard::kcard_value::kValueNull;
    *output_kicker1 =
        cardlib::CCard::kcard_value::kValueNull;
    *output7_signature = -1L;
    *output5_signature = -1L;
    *output_handvalue =
        CHand::khand_value::kHandValueNull;

    return;
  }

  // form best of 5 hand
  prime7_signature = 1L;
  prime5_signature = 1L;
  binit = false;
  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    card_value = vc_cit->GetCardValue();
    this_prime = vc_cit->GetCardPrimeValue();

    prime7_signature *= this_prime;

    if ((card_value == first_pair)
        || (card_value == second_pair)) {
      prime5_signature *= this_prime;
    } else if (card_value == kicker) {
      // use only the first kicker card
      if (binit == false) {
        prime5_signature *= this_prime;
        binit = true;
      }
    }
  }

  *output_toppair = first_pair;
  *output_secondpair = second_pair;
  *output_kicker1 = kicker;
  *output7_signature = prime7_signature;
  *output5_signature = prime5_signature;
  *output_handvalue = CHand::khand_value::kTwoPair;

  return;
}

// #############################################################
// #############################################################
void CTwoPairFunctions::IsTwoPair(
    const std::vector<cardlib::CCard> *input_vector,
    const std::map<cardlib::CCard::kcard_value, int64_t>
    *value_count_map,
    cardlib::CCard::kcard_value *output_toppair,
    cardlib::CCard::kcard_value *output_secondpair,
    cardlib::CCard::kcard_value *output_kicker1,
    int64_t *output7_signature,
    int64_t *output5_signature,
    cardlib::CHand::khand_value *output_handvalue) const {
  FormTwoPair(
      input_vector, value_count_map,
      output_toppair, output_secondpair, output_kicker1,
      output7_signature, output5_signature,
      output_handvalue);

  return;
}

// #############################################################
// #############################################################
bool CTwoPairFunctions::TwoPairGreaterTwoPair(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CCard::kcard_value
      twopairs1_1, twopairs2_1, kicker_1;
  cardlib::CCard::kcard_value
      twopairs1_2, twopairs2_2, kicker_2;

  twopairs1_1 = chand1->GetTopPair();
  twopairs1_2 = chand2->GetTopPair();
  if (twopairs1_1 > twopairs1_2) {
    return true;
  } else if (twopairs1_1 < twopairs1_2) {
    return false;
  }

  // if we reach here, then the largest two-pairs are the same
  twopairs2_1 = chand1->GetSecondPair();
  twopairs2_2 = chand2->GetSecondPair();
  if (twopairs2_1 > twopairs2_2) {
    return true;
  } else if (twopairs2_1 < twopairs2_2) {
    return false;
  }

  // if we reach here, then the both two pairs are identical
  kicker_1 = chand1->GetKicker1();
  kicker_2 = chand2->GetKicker1();
  return (kicker_1 > kicker_2);
}

// #############################################################
// #############################################################
bool CTwoPairFunctions::TwoPairLessThanTwoPair(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CCard::kcard_value
      twopairs1_1, twopairs2_1, kicker_1;
  cardlib::CCard::kcard_value
      twopairs1_2, twopairs2_2, kicker_2;

  twopairs1_1 = chand1->GetTopPair();
  twopairs1_2 = chand2->GetTopPair();
  if (twopairs1_1 < twopairs1_2) {
    return true;
  } else if (twopairs1_1 > twopairs1_2) {
    return false;
  }

  // if we reach here, then the largest two-pairs are the same
  twopairs2_1 = chand1->GetSecondPair();
  twopairs2_2 = chand2->GetSecondPair();
  if (twopairs2_1 < twopairs2_2) {
    return true;
  } else if (twopairs2_1 > twopairs2_2) {
    return false;
  }

  // if we reach here, then the both two pairs are identical
  kicker_1 = chand1->GetKicker1();
  kicker_2 = chand2->GetKicker1();
  return (kicker_1 < kicker_2);
}

// #############################################################
// #############################################################
bool CTwoPairFunctions::TwoPairEqualTwoPair(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CCard::kcard_value
      twopairs1_1, twopairs2_1, kicker_1;
  cardlib::CCard::kcard_value
      twopairs1_2, twopairs2_2, kicker_2;

  twopairs1_1 = chand1->GetTopPair();
  twopairs1_2 = chand2->GetTopPair();
  if (twopairs1_1 < twopairs1_2) {
    return false;
  } else if (twopairs1_1 > twopairs1_2) {
    return false;
  }

  // if we reach here, then the largest two-pairs are the same
  twopairs2_1 = chand1->GetSecondPair();
  twopairs2_2 = chand2->GetSecondPair();
  if (twopairs2_1 < twopairs2_2) {
    return false;
  } else if (twopairs2_1 > twopairs2_2) {
    return false;
  }

  // if we reach here, then the both two pairs are identical
  kicker_1 = chand1->GetKicker1();
  kicker_2 = chand2->GetKicker1();
  return (kicker_1 == kicker_2);
}

}  // namespace cardlib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

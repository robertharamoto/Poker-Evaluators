/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CCard implementation                                  ###
  ###    namespace CardLib                                   ###
  ###                                                        ###
  ###  last updated April 29, 2024                           ###
  ###                                                        ###
  ###  created March 18, 2024                                ###
  ###    card representation using 32-bit integers           ###
  ###    see: http://suffe.cool/poker/evaluator.html         ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/card.h"

#include <cstdio>
#include <cstdlib>
#include <cstdint>

#include <string>
#include <vector>
#include <algorithm>


// #############################################################
// #############################################################

/*
Single card bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/


// #############################################################
// #############################################################
namespace cardlib {

// #############################################################
// #############################################################
CCard::CCard()
    : card_mask_(0L) {
}

// #############################################################
// #############################################################
CCard::CCard(const std::string stmp)
    : card_mask_(0L) {
  StringToCard(&stmp, &card_mask_);
}

// #############################################################
// #############################################################
CCard::CCard(const CCard &ctmp)
    : card_mask_(0L) {
  Copy(ctmp);
}

// #############################################################
// #############################################################
CCard::~CCard() {
}

/*
Single card 32-bits scheme
//    3          2          1
//   10987654 32109876 54321098 76543210
//  +--------+--------+--------+--------+
//  |xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
//  +--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/
// #############################################################
// #############################################################
void CCard::SetValue(const std::string *val_string, int64_t *cmask) {
  int64_t cnum_shift(8L), val_pos_shift(16L);

  // first find card value from a string like "2"
  if (val_string->find("2") != std::string::npos) {
    // bit position for straight
    *cmask = *cmask | (1 << val_pos_shift);
    // bit position for numeric card value
    *cmask = *cmask | (kcard_value::kTwo << cnum_shift);
    // bit position for prime value
    *cmask = *cmask | (kprime_value::kpTwo);
  } else if (val_string->find("3") != std::string::npos) {
    // bit position for straight
    *cmask = *cmask | (1 << (val_pos_shift + 1L));
    // bit position for card value
    *cmask = *cmask | (kcard_value::kThree << cnum_shift);
    // bit position for prime value
    *cmask = *cmask | (kprime_value::kpThree);
  } else if (val_string->find("4") != std::string::npos) {
    // bit position for straight
    *cmask = *cmask | (1 << (val_pos_shift + 2L));
    // bit position for card value
    *cmask = *cmask | (kcard_value::kFour << cnum_shift);
    // bit position for prime value
    *cmask = *cmask | (kprime_value::kpFour);
  } else if (val_string->find("5") != std::string::npos) {
    // bit position for straight
    *cmask = *cmask | (1 << (val_pos_shift + 3L));
    // bit position for card value
    *cmask = *cmask | (kcard_value::kFive << cnum_shift);
    // bit position for prime value
    *cmask = *cmask | (kprime_value::kpFive);
  } else if (val_string->find("6") != std::string::npos) {
    // bit position for straight
    *cmask = *cmask | (1 << (val_pos_shift + 4L));
    // bit position for card value
    *cmask = *cmask | (kcard_value::kSix << cnum_shift);
    // bit position for prime value
    *cmask = *cmask | (kprime_value::kpSix);
  } else if (val_string->find("7") != std::string::npos) {
    // bit position for straight
    *cmask = *cmask | (1 << (val_pos_shift + 5L));
    // bit position for card value
    *cmask = *cmask | (kcard_value::kSeven << cnum_shift);
    // bit position for prime value
    *cmask = *cmask | (kprime_value::kpSeven);
  } else if (val_string->find("8") != std::string::npos) {
    // bit position for straight
    *cmask = *cmask | (1 << (val_pos_shift + 6L));
    // bit position for card value
    *cmask = *cmask | (kcard_value::kEight << cnum_shift);
    // bit position for prime value
    *cmask = *cmask | (kprime_value::kpEight);
  } else if (val_string->find("9") != std::string::npos) {
    // bit position for straight
    *cmask = *cmask | (1 << (val_pos_shift + 7L));
    // bit position for card value
    *cmask = *cmask | (kcard_value::kNine << cnum_shift);
    // bit position for prime value
    *cmask = *cmask | (kprime_value::kpNine);
  } else if ((val_string->find("10") != std::string::npos)
             || (val_string->find("T") != std::string::npos)
             || (val_string->find("t") != std::string::npos)) {
    // bit position for straight
    *cmask = *cmask | (1 << (val_pos_shift + 8L));
    // bit position for card value
    *cmask = *cmask | (kcard_value::kTen << cnum_shift);
    // bit position for prime value
    *cmask = *cmask | (kprime_value::kpTen);
  } else if ((val_string->find("j") != std::string::npos)
             || (val_string->find("J") != std::string::npos)) {
    // bit position for straight
    *cmask = *cmask | (1 << (val_pos_shift + 9L));
    // bit position for card value
    *cmask = *cmask | (kcard_value::kJack << cnum_shift);
    // bit position for prime value
    *cmask = *cmask | (kprime_value::kpJack);
  } else if ((val_string->find("q") != std::string::npos)
             || (val_string->find("Q") != std::string::npos)) {
    // bit position for straight
    *cmask = *cmask | (1 << (val_pos_shift + 10L));
    // bit position for card value
    *cmask = *cmask | (kcard_value::kQueen << cnum_shift);
    // bit position for prime value
    *cmask = *cmask | (kprime_value::kpQueen);
  } else if ((val_string->find("k") != std::string::npos)
             || (val_string->find("K") != std::string::npos)) {
    // bit position for straight
    *cmask = *cmask | (1 << (val_pos_shift + 11L));
    // bit position for card value
    *cmask = *cmask | (kcard_value::kKing << cnum_shift);
    // bit position for prime value
    *cmask = *cmask | (kprime_value::kpKing);
  } else if ((val_string->find("a") != std::string::npos)
             || (val_string->find("A") != std::string::npos)) {
    // bit position for straight
    *cmask = *cmask | (1 << (val_pos_shift + 12L));
    // bit position for card value
    *cmask = *cmask | (kcard_value::kAce << cnum_shift);
    // bit position for prime value
    *cmask = *cmask | (kprime_value::kpAce);
  } else {
    *cmask = 0;
  }

  return;
}

// #############################################################
// #############################################################
void CCard::SetSuit(const std::string *suit_string,
                    int64_t *cmask) {
  int64_t snum_shift(12L);

  // find card suit from a std::string like "2h"
  if ((suit_string->find("s") != std::string::npos)
      || (suit_string->find("S") != std::string::npos)) {
    *cmask = *cmask | (1 << snum_shift);
  } else if ((suit_string->find("h") != std::string::npos)
             || (suit_string->find("H") != std::string::npos)) {
    *cmask = *cmask | (1 << (snum_shift + 1L));
  } else if ((suit_string->find("d") != std::string::npos)
             || (suit_string->find("D") != std::string::npos)) {
    *cmask = *cmask | (1 << (snum_shift + 2L));
  } else if ((suit_string->find("c") != std::string::npos)
             || (suit_string->find("C") != std::string::npos)) {
    *cmask = *cmask | (1 << (snum_shift + 3L));
  } else {
    *cmask = 0;
  }

  return;
}

// #############################################################
// #############################################################
void CCard::Clear() {
  card_mask_ = 0;
  return;
}

// #############################################################
// #############################################################
void CCard::Set(const std::string val_and_suit) {
  card_mask_ = 0L;
  SetValue(&val_and_suit, &card_mask_);
  SetSuit(&val_and_suit, &card_mask_);

  return;
}

// #############################################################
// #############################################################
void CCard::Set(const std::string *val_and_suit) {
  card_mask_ = 0L;
  SetValue(val_and_suit, &card_mask_);
  SetSuit(val_and_suit, &card_mask_);
  return;
}

// #############################################################
// #############################################################
void CCard::Set(const std::string *val_and_suit, int64_t *cmask) {
  *cmask = 0L;
  SetValue(val_and_suit, cmask);
  SetSuit(val_and_suit, cmask);
  return;
}

// #############################################################
// #############################################################
void CCard::Set(const std::string *tvalue,
                const std::string *tsuit,
                int64_t *cmask) {
  *cmask = 0L;
  SetValue(tvalue, cmask);
  SetSuit(tsuit, cmask);
  return;
}

// #############################################################
// #############################################################
void CCard::Set(const std::string tvalue,
                const std::string tsuit) {
  card_mask_ = 0L;
  SetValue(&tvalue, &card_mask_);
  SetSuit(&tsuit, &card_mask_);
  return;
}

// #############################################################
// #############################################################
void CCard::StringToCard(const std::string *stmp, int64_t *cmask) {
  *cmask = 0L;

  // first find card value from a string like "2h"
  SetValue(stmp, cmask);

  // next find card suit from a string like "2h"
  SetSuit(stmp, cmask);
  return;
}

// #############################################################
// #############################################################
int64_t CCard::GetCardMask(void) const {
  return card_mask_;
}

/*
Single card 32-bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

// #############################################################
// #############################################################
cardlib::CCard::kcard_value CCard::GetCardValue(void) const {
  int64_t itmp;

  itmp = card_mask_ & 0x0F00;
  itmp = itmp >> 8;

  return static_cast<cardlib::CCard::kcard_value>(itmp);
}

// #############################################################
// #############################################################
int64_t CCard::GetCardPrimeValue(void) const {
  int64_t itmp;

  itmp = card_mask_ & 0x003F;
  return itmp;
}

// #############################################################
// #############################################################
int64_t CCard::GetCardSuitMask(void) const {
  int64_t itmp;

  itmp = (card_mask_ & 0x00F000);
  itmp = itmp >> 12;

  return itmp;
}

// #############################################################
// #############################################################
int64_t CCard::GetCardOrder(void) const {
  int64_t itmp;
  int64_t order_shift_constant(16L);
  int64_t bitmask(0x001FFF);

  itmp = card_mask_ >> order_shift_constant;
  itmp = (itmp & bitmask);

  return itmp;
}

// #############################################################
// #############################################################
std::string CCard::GetValueString(void) const {
  // first find card value from a string like "2h"
  CCard::kcard_value ctmp;
  std::string tvalue;

  ctmp = GetCardValue();

  switch (ctmp) {
    case kcard_value::kTwo:
      tvalue = static_cast<std::string>("2");
      break;
    case kcard_value::kThree:
      tvalue = static_cast<std::string>("3");
      break;
    case kcard_value::kFour:
      tvalue = static_cast<std::string>("4");
      break;
    case kcard_value::kFive:
      tvalue = static_cast<std::string>("5");
      break;
    case kcard_value::kSix:
      tvalue = static_cast<std::string>("6");
      break;
    case kcard_value::kSeven:
      tvalue = static_cast<std::string>("7");
      break;
    case kcard_value::kEight:
      tvalue = static_cast<std::string>("8");
      break;
    case kcard_value::kNine:
      tvalue = static_cast<std::string>("9");
      break;
    case kcard_value::kTen:
      tvalue = static_cast<std::string>("t");
      break;
    case kcard_value::kJack:
      tvalue = static_cast<std::string>("j");
      break;
    case kcard_value::kQueen:
      tvalue = static_cast<std::string>("q");
      break;
    case kcard_value::kKing:
      tvalue = static_cast<std::string>("k");
      break;
    case kcard_value::kAce:
      tvalue = static_cast<std::string>("a");
      break;
    default:
      tvalue = static_cast<std::string>("value error ");
      break;
  }

  return tvalue;
}

// #############################################################
// #############################################################
std::string CCard::GetValueName(void) const {
  // first find card value from a string like "2h"
  CCard::kcard_value ctmp;
  std::string tvalue;

  ctmp = GetCardValue();

  switch (ctmp) {
    case kcard_value::kTwo:
      tvalue = static_cast<std::string>("two");
      break;
    case kcard_value::kThree:
      tvalue = static_cast<std::string>("three");
      break;
    case kcard_value::kFour:
      tvalue = static_cast<std::string>("four");
      break;
    case kcard_value::kFive:
      tvalue = static_cast<std::string>("five");
      break;
    case kcard_value::kSix:
      tvalue = static_cast<std::string>("six");
      break;
    case kcard_value::kSeven:
      tvalue = static_cast<std::string>("seven");
      break;
    case kcard_value::kEight:
      tvalue = static_cast<std::string>("eight");
      break;
    case kcard_value::kNine:
      tvalue = static_cast<std::string>("nine");
      break;
    case kcard_value::kTen:
      tvalue = static_cast<std::string>("ten");
      break;
    case kcard_value::kJack:
      tvalue = static_cast<std::string>("jack");
      break;
    case kcard_value::kQueen:
      tvalue = static_cast<std::string>("queen");
      break;
    case kcard_value::kKing:
      tvalue = static_cast<std::string>("king");
      break;
    case kcard_value::kAce:
      tvalue = static_cast<std::string>("ace");
      break;
    default:
      tvalue = static_cast<std::string>("value error ");
      break;
  }

  return tvalue;
}

// #############################################################
// #############################################################
CCard::kcard_suit CCard::GetCardSuit(void) const {
  int64_t itmp;
  CCard::kcard_suit sresult;

  itmp = GetCardSuitMask();
  sresult = static_cast<CCard::kcard_suit>(itmp);

  return sresult;
}

// #############################################################
// #############################################################
std::string CCard::GetSuitString(void) const {
  int64_t itmp;
  CCard::kcard_suit stmp;
  std::string tsuit;

  itmp = GetCardSuitMask();
  stmp = static_cast<CCard::kcard_suit>(itmp);

  switch (stmp) {
    case CCard::kcard_suit::kSpades:
      tsuit = static_cast<std::string>("s");
      break;
    case CCard::kcard_suit::kHearts:
      tsuit = static_cast<std::string>("h");
      break;
    case CCard::kcard_suit::kDiamonds:
      tsuit = static_cast<std::string>("d");
      break;
    case CCard::kcard_suit::kClubs:
      tsuit = static_cast<std::string>("c");
      break;
    default:
      tsuit = static_cast<std::string>("suit error ");
      break;
  }

  return tsuit;
}

// #############################################################
// #############################################################
std::string CCard::GetSuitName(void) const {
  int64_t itmp;
  CCard::kcard_suit stmp;
  std::string tsuit;

  itmp = GetCardSuitMask();
  stmp = static_cast<CCard::kcard_suit>(itmp);

  switch (stmp) {
    case CCard::kcard_suit::kSpades:
      tsuit = static_cast<std::string>("spades");
      break;
    case CCard::kcard_suit::kHearts:
      tsuit = static_cast<std::string>("hearts");
      break;
    case CCard::kcard_suit::kDiamonds:
      tsuit = static_cast<std::string>("diamonds");
      break;
    case CCard::kcard_suit::kClubs:
      tsuit = static_cast<std::string>("clubs");
      break;
    default:
      tsuit = static_cast<std::string>("suit error ");
      break;
  }

  return tsuit;
}

// #############################################################
// #############################################################
std::string CCard::CardToString(void) const {
  // first find card value from a string like "2h"
  std::string tvalue, tsuit, tresult;

  tvalue = GetValueString();
  tsuit = GetSuitString();

  tresult = tvalue + tsuit;

  return tresult;
}

// #############################################################
// #############################################################
std::string CCard::CardVectorToString(
    const std::vector<cardlib::CCard> *input_vector) {
  std::vector<cardlib::CCard>::const_iterator vc_cit;
  int64_t ii_length;
  cardlib::CCard card;
  std::string open_brace("[ "), close_brace(" ]");
  std::string separator_string(", ");
  std::string result_string = static_cast<std::string>("");

  ii_length = static_cast<int64_t>(input_vector->size());

  if (ii_length > 1L) {
    result_string = open_brace;

    for (vc_cit = input_vector->cbegin();
         vc_cit != input_vector->cend(); ++vc_cit) {
      card = *vc_cit;

      if (vc_cit == input_vector->begin()) {
        result_string += card.CardToString();
      } else {
        result_string += separator_string + card.CardToString();
      }
    }

    result_string += close_brace;
  } else if (ii_length == 1L) {
    result_string =
        open_brace
        + (input_vector->cbegin())->CardToString()
        + close_brace;
  } else {
    result_string = static_cast<std::string>("");
  }

  return result_string;
}

// #############################################################
// #############################################################
void CCard::ToLower(std::string *myString) {
  // explicit cast needed to resolve ambiguity
  std::transform(
      myString->begin(), myString->end(),
      myString->begin(), ::tolower);

  return;
}

// #############################################################
// #############################################################
void CCard::Copy(const CCard& ctmp) {
  card_mask_ = ctmp.card_mask_;

  return;
}

// #############################################################
// #############################################################
bool CCard::IsIdentical(const CCard *ctmp) const {
  return (card_mask_ == ctmp->card_mask_);
}

// #############################################################
// #############################################################
bool CCard::ValueEqual(const CCard *ctmp) const {
  CCard::kcard_value itmp = GetCardValue();
  CCard::kcard_value jtmp = ctmp->GetCardValue();

  return (itmp == jtmp);
}

// #############################################################
// #############################################################
bool CCard::Greater(const CCard &ctmp) const {
  CCard::kcard_value itmp = GetCardValue();
  CCard::kcard_value jtmp = ctmp.GetCardValue();

  return (itmp > jtmp);
}

// #############################################################
// #############################################################
bool CCard::operator==(const CCard& c1) const {
  return IsIdentical(&c1);
}

// #############################################################
// #############################################################
bool CCard::operator!=(const CCard& c1) const {
  return (!(IsIdentical(&c1)));
}

// #############################################################
// #############################################################
bool CCard::operator>(const CCard& c1) const {
  return Greater(c1);
}

// #############################################################
// #############################################################
bool CCard::operator>=(const CCard& c1) const {
  CCard::kcard_value itmp = GetCardValue();
  CCard::kcard_value jtmp = c1.GetCardValue();

  return (itmp >= jtmp);
}

// #############################################################
// #############################################################
bool CCard::operator<(const CCard& c1) const {
  CCard::kcard_value itmp = GetCardValue();
  CCard::kcard_value jtmp = c1.GetCardValue();

  return (itmp < jtmp);
}

// #############################################################
// #############################################################
bool CCard::operator<=(const CCard& c1) const {
  CCard::kcard_value itmp = GetCardValue();
  CCard::kcard_value jtmp = c1.GetCardValue();

  return (itmp <= jtmp);
}

// #############################################################
// #############################################################
CCard& CCard::operator=(const CCard& c1) {
  card_mask_ = c1.card_mask_;

  return *this;
}

}  // namespace cardlib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

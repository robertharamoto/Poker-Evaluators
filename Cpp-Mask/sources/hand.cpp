/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CHand implementation                                  ###
  ###  namespace cardlib                                     ###
  ###                                                        ###
  ###  last updated April 18, 2024                           ###
  ###    new bitmask definition for cards                    ###
  ###                                                        ###
  ###  created March 4, 2024                                 ###
  ###    used new single integer bit scheme                  ###
  ###    from http://suffe.cool/poker/evaluator.html         ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/hand.h"

#include <cstdio>
#include <cstdlib>

#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include "sources/card.h"
#include "sources/straight_flush_functions.h"
#include "sources/four_functions.h"
#include "sources/full_house_functions.h"
#include "sources/flush_functions.h"
#include "sources/straight_functions.h"
#include "sources/triple_functions.h"
#include "sources/two_pair_functions.h"
#include "sources/one_pair_functions.h"
#include "sources/high_card_functions.h"

namespace cardlib {

/*
Single card bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/


// #############################################################
// #############################################################
CHand::CHand()
    : is_calculated_(false),
      hand_name_(""),
      hand_long_name_(""),
      card_string_(""),
      hand_value_(
          cardlib::CHand::khand_value::kHandValueNull),
      hand_straight_mask_(
          cardlib::CHand::kstraight_mask::kStraightValueNull),
      flush_suit_(cardlib::CCard::kcard_suit::kNullSuit),
      hand_order_(0L),
      prime7_signature_(-1L),
      prime5_signature_(-1L),
      quad_value_(cardlib::CCard::kcard_value::kValueNull),
      trip_value_(cardlib::CCard::kcard_value::kValueNull),
      top_pair_(cardlib::CCard::kcard_value::kValueNull),
      second_pair_(cardlib::CCard::kcard_value::kValueNull),
      kicker1_(cardlib::CCard::kcard_value::kValueNull),
      kicker2_(cardlib::CCard::kcard_value::kValueNull),
      kicker3_(cardlib::CCard::kcard_value::kValueNull) {
  Initialize();
}

// #############################################################
// #############################################################
CHand::CHand(const CHand& htmp)
    : is_calculated_(false),
      hand_name_(""),
      hand_long_name_(""),
      card_string_(""),
      hand_value_(cardlib::CHand::khand_value::kHandValueNull),
      hand_straight_mask_(
          cardlib::CHand::kstraight_mask::kStraightValueNull),
      flush_suit_(cardlib::CCard::kcard_suit::kNullSuit),
      hand_order_(0L),
      prime7_signature_(-1L),
      prime5_signature_(-1L),
      quad_value_(cardlib::CCard::kcard_value::kValueNull),
      trip_value_(cardlib::CCard::kcard_value::kValueNull),
      top_pair_(cardlib::CCard::kcard_value::kValueNull),
      second_pair_(cardlib::CCard::kcard_value::kValueNull),
      kicker1_(cardlib::CCard::kcard_value::kValueNull),
      kicker2_(cardlib::CCard::kcard_value::kValueNull),
      kicker3_(cardlib::CCard::kcard_value::kValueNull) {
  CopyHand(&htmp);
}

// #############################################################
// #############################################################
CHand::~CHand() {
}

// #############################################################
// #############################################################
void CHand::Clear() {
  Initialize();
}

// #############################################################
// #############################################################
bool CHand::IsCalculated() const {
  return is_calculated_;
}

// #############################################################
// #############################################################
std::string CHand::GetCardsString() const {
  return card_string_;
}

// #############################################################
// #############################################################
std::string CHand::HandName() const {
  return hand_name_;
}

// #############################################################
// #############################################################
std::string CHand::HandLongName() const {
  return hand_long_name_;
}

// #############################################################
// #############################################################
std::string CHand::HandName(const int64_t hval) const {
  cardlib::CHand::khand_value hv_tmp;

  hv_tmp = static_cast<cardlib::CHand::khand_value>(hval);
  switch (hv_tmp) {
    case cardlib::CHand::khand_value::kHighCard:
      return static_cast<std::string>("high-card");
      break;
    case cardlib::CHand::khand_value::kOnePair:
      return static_cast<std::string>("one-pair");
      break;
    case cardlib::CHand::khand_value::kTwoPair:
      return static_cast<std::string>("two-pair");
      break;
    case cardlib::CHand::khand_value::kTriples:
      return static_cast<std::string>("three-of-a-kind");
      break;
    case cardlib::CHand::khand_value::kStraight:
      return static_cast<std::string>("straight");
      break;
    case cardlib::CHand::khand_value::kFlush:
      return static_cast<std::string>("flush");
      break;
    case cardlib::CHand::khand_value::kFullHouse:
      return static_cast<std::string>("full-house");
      break;
    case cardlib::CHand::khand_value::kQuads:
      return static_cast<std::string>("four-of-a-kind");
      break;
    case cardlib::CHand::khand_value::kStraightFlush:
      return static_cast<std::string>("straight-flush");
      break;
    case cardlib::CHand::khand_value::kRoyalFlush:
      return static_cast<std::string>("royal-flush");
      break;
    default:
      return static_cast<std::string>("hand-error");
      break;
  }
}

// #############################################################
// #############################################################
std::string CHand::HandToString() {
  std::string stmp;

  stmp = hand_name_
      + static_cast<std::string>(" ")
      + card_string_;

  trim2(&stmp);

  return stmp;
}

// #############################################################
// #############################################################
void CHand::ComputeHandPrimeSignatures(
    const std::vector<cardlib::CCard> *input_vector,
    int64_t *all_primes) const {
  std::vector<cardlib::CCard>::const_iterator vc_cit;
  int64_t prime_prod;

  prime_prod = 1L;
  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    prime_prod = prime_prod * vc_cit->GetCardPrimeValue();
  }

  *all_primes = prime_prod;

  return;
}

// #############################################################
// #############################################################
void CHand::SetHandName(
    const cardlib::CHand::khand_value ahand_value) {
  if (ahand_value ==
      cardlib::CHand::khand_value::kHighCard) {
    hand_name_ = "high-cards";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kOnePair) {
    hand_name_ = "one-pair";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kTwoPair) {
    hand_name_ = "two-pairs";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kTriples) {
    hand_name_ = "three-of-a-kind";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kStraight) {
    hand_name_ = "straight";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kFlush) {
    hand_name_ = "flush";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kFullHouse) {
    hand_name_ = "full-house";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kQuads) {
    hand_name_ = "four-of-a-kind";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kStraightFlush) {
    hand_name_ = "straight-flush";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kRoyalFlush) {
    hand_name_ = "royal-flush";
  } else {
    hand_name_ = "hand error";
  }
  return;
}

// #############################################################
// #############################################################
void CHand::SetHandLongName(
    const cardlib::CHand::khand_value ahand_value) {
  if (ahand_value ==
      cardlib::CHand::khand_value::kHighCard) {
    hand_name_ = "high-cards";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kOnePair) {
    hand_name_ = "one-pair";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kTwoPair) {
    hand_name_ = "two-pairs";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kTriples) {
    hand_name_ = "three-of-a-kind";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kStraight) {
    hand_name_ = "straight";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kFlush) {
    hand_name_ = "flush";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kFullHouse) {
    hand_name_ = "full-house";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kQuads) {
    hand_name_ = "four-of-a-kind";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kStraightFlush) {
    hand_name_ = "straight-flush";
  } else if (ahand_value ==
             cardlib::CHand::khand_value::kRoyalFlush) {
    hand_name_ = "royal-flush";
  } else {
    hand_name_ = "hand error";
  }
  return;
}

// #############################################################
// #############################################################
cardlib::CHand::khand_value CHand::GetHandValue() const {
  return hand_value_;
}

// #############################################################
// #############################################################
CHand::kstraight_mask CHand::GetHandStraightMask() const {
  return hand_straight_mask_;
}

// #############################################################
// #############################################################
cardlib::CCard::kcard_suit CHand::GetFlushSuit() const {
  return flush_suit_;
}

// #############################################################
// #############################################################
int64_t CHand::GetHandOrder() const {
  return hand_order_;
}

// #############################################################
// #############################################################
int64_t CHand::GetPrime7Signature() const {
  return prime7_signature_;
}

// #############################################################
// #############################################################
int64_t CHand::GetPrime5Signature() const {
  return prime5_signature_;
}

// #############################################################
// #############################################################
cardlib::CCard::kcard_value CHand::GetQuadValue() const {
  return quad_value_;
}

// #############################################################
// #############################################################
cardlib::CCard::kcard_value CHand::GetTripValue() const {
  return trip_value_;
}

// #############################################################
// #############################################################
cardlib::CCard::kcard_value CHand::GetTopPair() const {
  return top_pair_;
}

// #############################################################
// #############################################################
cardlib::CCard::kcard_value CHand::GetSecondPair() const {
  return second_pair_;
}

// #############################################################
// #############################################################
cardlib::CCard::kcard_value CHand::GetKicker1() const {
  return kicker1_;
}

// #############################################################
// #############################################################
cardlib::CCard::kcard_value CHand::GetKicker2() const {
  return kicker2_;
}

// #############################################################
// #############################################################
cardlib::CCard::kcard_value CHand::GetKicker3() const {
  return kicker3_;
}

// #############################################################
// #############################################################
void CHand::PopulateNthOfAKindMap(
    const std::vector<cardlib::CCard> *input_vector,
    std::map<cardlib::CCard::kcard_value, int64_t>
    *output_map) const {
  std::vector<cardlib::CCard>::const_iterator vc_cit;
  cardlib::CCard::kcard_value card_value;

  output_map->clear();

  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    card_value = vc_cit->GetCardValue();

    (*output_map)[card_value]++;
  }

  return;
}

// #############################################################
// #############################################################
void CHand::CalculateHelper(
    cardlib::CHand::khand_value input_hand_value,
    cardlib::CCard::kcard_suit input_flush_value,
    cardlib::CHand::kstraight_mask input_straight_mask,
    int64_t input_order,
    int64_t input_p7signature,
    int64_t input_p5signature,
    cardlib::CCard::kcard_value input_quad_value,
    cardlib::CCard::kcard_value input_trip_value,
    cardlib::CCard::kcard_value input_top_pair,
    cardlib::CCard::kcard_value input_second_pair,
    cardlib::CCard::kcard_value input_kicker1,
    cardlib::CCard::kcard_value input_kicker2,
    cardlib::CCard::kcard_value input_kicker3) {
  SetHandName(input_hand_value);

  SetHandLongName(input_hand_value);

  hand_value_ = input_hand_value;
  // hand_straight_mask_ set by straight_flush_function object,
  // and straight_function object
  flush_suit_ = input_flush_value;

  hand_straight_mask_ = input_straight_mask;
  hand_order_ = input_order;

  prime7_signature_ = input_p7signature;
  prime5_signature_ = input_p5signature;

  quad_value_ = input_quad_value;
  trip_value_ = input_trip_value;
  top_pair_ = input_top_pair;
  second_pair_ = input_second_pair;
  kicker1_ = input_kicker1;
  kicker2_ = input_kicker2;
  kicker3_ = input_kicker3;

  is_calculated_ = true;

  return;
}

// #############################################################
// #############################################################
void CHand::Calculate(
    const std::vector<cardlib::CCard> *input_vector) {
  CStraightFlushFunctions straightflushfunc_obj;
  CFourFunctions fourfunc_obj;
  CFullHouseFunctions fullhousefunc_obj;
  CFlushFunctions flushfunc_obj;
  CStraightFunctions straightfunc_obj;
  CTripleFunctions triplefunc_obj;
  CTwoPairFunctions twopairfunc_obj;
  COnePairFunctions onepairfunc_obj;
  CHighCardFunctions highcardfunc_obj;
  std::map<cardlib::CCard::kcard_suit, int64_t> suit_count_map;
  std::map<cardlib::CCard::kcard_value, int64_t> value_count_map;
  cardlib::CHand::khand_value tmp_hand_value = {
    cardlib::CHand::khand_value::kHandValueNull };
  cardlib::CHand::kstraight_mask tmp_straight_mask = {
    cardlib::CHand::kstraight_mask::kStraightValueNull };
  cardlib::CCard acard;
  cardlib::CCard::kcard_suit no_flush_suit;
  cardlib::CCard::kcard_suit flush_suit;
  int64_t hand_order;
  int64_t p7signature = {
    cardlib::CHand::kstraight_signature::kStraightSignatureNull };
  int64_t p5signature = {
    cardlib::CHand::kstraight_signature::kStraightSignatureNull };
  cardlib::CCard::kcard_value
      quad_value(cardlib::CCard::kcard_value::kValueNull);
  cardlib::CCard::kcard_value
      trip_value(cardlib::CCard::kcard_value::kValueNull);
  cardlib::CCard::kcard_value
      top_pair(cardlib::CCard::kcard_value::kValueNull);
  cardlib::CCard::kcard_value
      second_pair(cardlib::CCard::kcard_value::kValueNull);
  cardlib::CCard::kcard_value
      kicker1(cardlib::CCard::kcard_value::kValueNull);
  cardlib::CCard::kcard_value
      kicker2(cardlib::CCard::kcard_value::kValueNull);
  cardlib::CCard::kcard_value
      kicker3(cardlib::CCard::kcard_value::kValueNull);

  suit_count_map.clear();
  value_count_map.clear();

  hand_order = 0L;
  p7signature = 0L;
  p5signature = 0L;
  tmp_straight_mask =
      cardlib::CHand::kstraight_mask::kStraightValueNull;

  no_flush_suit = cardlib::CCard::kcard_suit::kNullSuit;

  // compute hand names, and set bools
  if (static_cast<int64_t>(input_vector->size()) < 5L) {
    is_calculated_ = false;
    return;
  }

  card_string_ = acard.CardVectorToString(input_vector);

  // do preliminary calculations
  flushfunc_obj.MakeSuitCountMap(
      input_vector, &suit_count_map);

  flush_suit = flushfunc_obj.CanFormAFlush(&suit_count_map);

  // compute hand name, and store best hand in only_hand_vector_
  if (flush_suit != no_flush_suit) {
    // IsStraightFlush() populates hand_straight_mask, order, p7signature,
    // p5signature if it is a straight flush
    straightflushfunc_obj.IsStraightFlush(
        flush_suit, input_vector,
        &tmp_straight_mask, &hand_order,
        &p7signature, &p5signature,
        &tmp_hand_value);

    if (tmp_hand_value ==
        cardlib::CHand::khand_value::kRoyalFlush) {
      CalculateHelper(
          cardlib::CHand::khand_value::kRoyalFlush,
          flush_suit, tmp_straight_mask, hand_order,
          p7signature, p5signature,
          quad_value, trip_value,
          top_pair, second_pair,
          kicker1, kicker2, kicker3);
      return;
    } else if (tmp_hand_value ==
               cardlib::CHand::khand_value::kStraightFlush) {
      CalculateHelper(
          cardlib::CHand::khand_value::kStraightFlush,
          flush_suit, tmp_straight_mask, hand_order,
          p7signature, p5signature,
          quad_value, trip_value,
          top_pair, second_pair,
          kicker1, kicker2, kicker3);
      return;
    }
  }

  // value_count_map used for 4-of-a-kind, full-house,
  // 3-of-a-kind, 2-pair, 1-pair
  PopulateNthOfAKindMap(
      input_vector, &value_count_map);

  // IsFourOfAKind()
  // if it is a four-of-a-kind
  fourfunc_obj.IsFourOfAKind(
      input_vector, &value_count_map,
      &quad_value, &kicker1,
      &p7signature, &p5signature,
      &tmp_hand_value);

  if (tmp_hand_value == cardlib::CHand::khand_value::kQuads) {
    CalculateHelper(
        cardlib::CHand::khand_value::kQuads,
        flush_suit, tmp_straight_mask, hand_order,
        p7signature, p5signature,
        quad_value, trip_value,
        top_pair, second_pair,
        kicker1, kicker2, kicker3);
    return;
  }

  // IsFullHouse()
  // if it is a full-house
  fullhousefunc_obj.IsFullHouse(
      input_vector, &value_count_map,
      &trip_value, &top_pair,
      &p7signature, &p5signature,
      &tmp_hand_value);

  if (tmp_hand_value == cardlib::CHand::khand_value::kFullHouse) {
    CalculateHelper(
        cardlib::CHand::khand_value::kFullHouse,
        no_flush_suit, tmp_straight_mask, hand_order,
        p7signature, p5signature,
        quad_value, trip_value,
        top_pair, second_pair,
        kicker1, kicker2, kicker3);
    return;
  }

  // IsFlush()
  // if it is a flush
  if (flush_suit != no_flush_suit) {
    flushfunc_obj.IsFlush(
        flush_suit, input_vector,
        &hand_order, &p7signature, &p5signature,
        &tmp_hand_value);

    if (tmp_hand_value == cardlib::CHand::khand_value::kFlush) {
      CalculateHelper(
          cardlib::CHand::khand_value::kFlush,
          flush_suit, tmp_straight_mask, hand_order,
          p7signature, p5signature,
          quad_value, trip_value,
          top_pair, second_pair,
          kicker1, kicker2, kicker3);
      return;
    }
  }

  // IsStraight()
  // if it is a straight
  straightfunc_obj.IsStraight(
      input_vector, &tmp_straight_mask,
      &hand_order, &p7signature, &p5signature,
      &tmp_hand_value);

  if (tmp_hand_value == cardlib::CHand::khand_value::kStraight) {
    CalculateHelper(
        cardlib::CHand::khand_value::kStraight,
        flush_suit, tmp_straight_mask, hand_order,
        p7signature, p5signature,
        quad_value, trip_value,
        top_pair, second_pair,
        kicker1, kicker2, kicker3);
    return;
  }

  // IsThreeOfAKind()
  // if it is a three-of-a-kind
  triplefunc_obj.IsThreeOfAKind(
      input_vector, &value_count_map,
      &trip_value, &kicker1, &kicker2,
      &p7signature, &p5signature,
      &tmp_hand_value);

  if (tmp_hand_value == cardlib::CHand::khand_value::kTriples) {
    CalculateHelper(
        cardlib::CHand::khand_value::kTriples,
        flush_suit, tmp_straight_mask, hand_order,
        p7signature, p5signature,
        quad_value, trip_value,
        top_pair, second_pair,
        kicker1, kicker2, kicker3);
    return;
  }

  // IsTwoPair()
  // if it is a two-pair hand
  twopairfunc_obj.IsTwoPair(
      input_vector, &value_count_map,
      &top_pair, &second_pair,
      &kicker1, &p7signature, &p5signature,
      &tmp_hand_value);

  if (tmp_hand_value == cardlib::CHand::khand_value::kTwoPair) {
    CalculateHelper(
        cardlib::CHand::khand_value::kTwoPair,
        flush_suit, tmp_straight_mask, hand_order,
        p7signature, p5signature,
        quad_value, trip_value,
        top_pair, second_pair,
        kicker1, kicker2, kicker3);
    return;
  }

  // IsOnePair()
  // if it is a one-pair hand
  onepairfunc_obj.IsOnePair(
      input_vector, &value_count_map,
      &top_pair, &kicker1, &kicker2,
      &kicker3, &p7signature, &p5signature,
      &tmp_hand_value);

  if (tmp_hand_value == CHand::khand_value::kOnePair) {
    CalculateHelper(
        cardlib::CHand::khand_value::kOnePair,
        flush_suit, tmp_straight_mask, hand_order,
        p7signature, p5signature,
        quad_value, trip_value,
        top_pair, second_pair,
        kicker1, kicker2, kicker3);
    return;
  }

  // IsHighCard()
  // if it is a high-card hand
  highcardfunc_obj.IsHighCard(
      input_vector, &hand_order, &p7signature, &p5signature,
      &tmp_hand_value);

  CalculateHelper(
      cardlib::CHand::khand_value::kHighCard,
      flush_suit, tmp_straight_mask, hand_order,
      p7signature, p5signature,
      quad_value, trip_value,
      top_pair, second_pair,
      kicker1, kicker2, kicker3);

  return;
}

// #############################################################
// #############################################################
bool CHand::Hand1GreaterHand2(const cardlib::CHand *h2) const {
  cardlib::CHand htmp;
  CStraightFlushFunctions straightflushfunc_obj;
  CFourFunctions fourfunc_obj;
  CFullHouseFunctions fullhousefunc_obj;
  CFlushFunctions flushfunc_obj;
  CStraightFunctions straightfunc_obj;
  CTripleFunctions triplefunc_obj;
  CTwoPairFunctions twopairfunc_obj;
  COnePairFunctions onepairfunc_obj;
  CHighCardFunctions highcardfunc_obj;
  CHand::khand_value handvalue1, handvalue2;
  bool bresult;

  htmp = *h2;

  handvalue1 = hand_value_;
  handvalue2 = htmp.hand_value_;

  if (handvalue1 > handvalue2) {
    return true;
  } else if (handvalue1 < handvalue2) {
    return false;
  } else {
    // then handvalue1 == handvalue2
    if (handvalue1 == cardlib::CHand::khand_value::kRoyalFlush) {
      return false;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kStraightFlush) {
      bresult =
          straightflushfunc_obj.StraightFlushGreaterStraightFlush(
              this, &htmp);
      return bresult;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kQuads) {
      bresult =
          fourfunc_obj.QuadsGreaterQuads(this, &htmp);
      return bresult;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kFullHouse) {
      bresult =
          fullhousefunc_obj.FullHouseGreaterFullHouse(this, &htmp);
      return bresult;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kFlush) {
      bresult =
          flushfunc_obj.FlushGreaterFlush(this, &htmp);
      return bresult;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kStraight) {
      bresult =
          straightfunc_obj.StraightGreaterStraight(this, &htmp);
      return bresult;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kTriples) {
      bresult =
          triplefunc_obj.TripleGreaterTriple(this, &htmp);
      return bresult;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kTwoPair) {
      bresult =
          twopairfunc_obj.TwoPairGreaterTwoPair(this, &htmp);
      return bresult;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kOnePair) {
      bresult =
          onepairfunc_obj.OnePairGreaterOnePair(this, &htmp);
      return bresult;
    } else {
      bresult =
          highcardfunc_obj.HighCardsGreaterHighCards(this, &htmp);
      return bresult;
    }
  }

  return false;
}

// #############################################################
// #############################################################
bool CHand::Hand1LessThanHand2(const cardlib::CHand *h2) const {
  cardlib::CHand htmp;
  CStraightFlushFunctions straightflushfunc_obj;
  CFourFunctions fourfunc_obj;
  CFullHouseFunctions fullhousefunc_obj;
  CFlushFunctions flushfunc_obj;
  CStraightFunctions straightfunc_obj;
  CTripleFunctions triplefunc_obj;
  CTwoPairFunctions twopairfunc_obj;
  COnePairFunctions onepairfunc_obj;
  CHighCardFunctions highcardfunc_obj;
  CHand::khand_value handvalue1, handvalue2;
  bool bresult;

  htmp = *h2;

  handvalue1 = hand_value_;
  handvalue2 = htmp.hand_value_;

  if (handvalue1 < handvalue2) {
    return true;
  } else if (handvalue1 > handvalue2) {
    return false;
  } else {
    // then handvalue1 == handvalue2
    if (handvalue1 ==
        cardlib::CHand::khand_value::kRoyalFlush) {
      return false;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kStraightFlush) {
      bresult =
          straightflushfunc_obj.StraightFlushLessThanStraightFlush(
              this, &htmp);
      return bresult;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kQuads) {
      bresult =
          fourfunc_obj.QuadsLessThanQuads(this, &htmp);
      return bresult;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kFullHouse) {
      bresult =
          fullhousefunc_obj.FullHouseLessThanFullHouse(this, &htmp);
      return bresult;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kFlush) {
      bresult =
          flushfunc_obj.FlushLessThanFlush(this, &htmp);
      return bresult;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kStraight) {
      bresult =
          straightfunc_obj.StraightLessThanStraight(this, &htmp);
      return bresult;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kTriples) {
      bresult =
          triplefunc_obj.TripleLessThanTriple(this, &htmp);
      return bresult;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kTwoPair) {
      bresult =
          twopairfunc_obj.TwoPairLessThanTwoPair(this, &htmp);
      return bresult;
    } else if (handvalue1 ==
               cardlib::CHand::khand_value::kOnePair) {
      bresult =
          onepairfunc_obj.OnePairLessThanOnePair(this, &htmp);
      return bresult;
    } else {
      bresult =
          highcardfunc_obj.HighCardsLessThanHighCards(this, &htmp);
      return bresult;
    }
  }

  return false;
}

// #############################################################
// #############################################################
bool CHand::Hand1EqualHand2(const cardlib::CHand *h2) const {
  cardlib::CHand htmp;
  cardlib::CHand::khand_value handvalue1, handvalue2;

  handvalue1 = hand_value_;

  htmp = *h2;

  handvalue2 = htmp.hand_value_;

  if (handvalue1 != handvalue2) {
    return false;
  } else {
    int64_t prime1, prime2;
    bool bresult;

    // handvalue1 == handvalue2
    prime1 = GetPrime5Signature();
    prime2 = htmp.GetPrime5Signature();
    bresult = (prime1 == prime2);

    return bresult;
  }
}

// #############################################################
// #############################################################
// assume is_calculated_ is true for both hands
bool CHand::operator>(const cardlib::CHand &hand1) const {
  bool bresult;

  bresult = Hand1GreaterHand2(&hand1);

  return bresult;
}

// #############################################################
// #############################################################
// assume is_calculated_ is true for both hands
bool CHand::operator<(const cardlib::CHand &hand1) const {
  bool bresult;

  bresult = Hand1LessThanHand2(&hand1);

  return bresult;
}

// #############################################################
// #############################################################
// assume is_calculated_ is true for both hands
bool CHand::operator==(const cardlib::CHand &hand1) const {
  bool bresult;

  bresult = Hand1EqualHand2(&hand1);

  return bresult;
}

// #############################################################
// #############################################################
// assume is_calculated_ is true for both hands
CHand& CHand::operator=(const cardlib::CHand &hand1) {
  CopyHand(&hand1);

  return *this;
}

// #############################################################
// #############################################################
void CHand::Initialize(void) {
  is_calculated_ = false;

  hand_name_.clear();
  hand_long_name_.clear();
  card_string_.clear();

  hand_value_ = cardlib::CHand::khand_value::kHandValueNull;
  hand_straight_mask_ =
      cardlib::CHand::kstraight_mask::kStraightValueNull;

  flush_suit_ = cardlib::CCard::kcard_suit::kNullSuit;

  hand_order_ = 0L;
  prime7_signature_ = 0L;
  prime5_signature_ = 0L;

  quad_value_ = cardlib::CCard::kcard_value::kValueNull;
  trip_value_ = cardlib::CCard::kcard_value::kValueNull;
  top_pair_ = cardlib::CCard::kcard_value::kValueNull;
  second_pair_ = cardlib::CCard::kcard_value::kValueNull;
  kicker1_ = cardlib::CCard::kcard_value::kValueNull;
  kicker2_ = cardlib::CCard::kcard_value::kValueNull;
  kicker3_ = cardlib::CCard::kcard_value::kValueNull;

  return;
}

// #############################################################
// #############################################################
void CHand::CopyHand(const CHand *htmp) {
  is_calculated_ = htmp->is_calculated_;

  hand_name_ = htmp->hand_name_;
  hand_long_name_ = htmp->hand_long_name_;
  card_string_ = htmp->card_string_;

  hand_value_ = htmp->hand_value_;
  hand_straight_mask_ = htmp->hand_straight_mask_;

  flush_suit_ = htmp->flush_suit_;
  hand_order_ = htmp->hand_order_;
  prime7_signature_ = htmp->prime7_signature_;
  prime5_signature_ = htmp->prime5_signature_;

  quad_value_ = htmp->quad_value_;
  trip_value_ = htmp->trip_value_;
  top_pair_ = htmp->top_pair_;
  second_pair_ = htmp->second_pair_;

  kicker1_ = htmp->kicker1_;
  kicker2_ = htmp->kicker2_;
  kicker3_ = htmp->kicker3_;

  return;
}

// #############################################################
// #############################################################
void CHand::trim2(std::string *str) {
  std::string::size_type pos = str->find_last_not_of(' ');

  if (pos != std::string::npos) {
    str->erase(pos + 1);
    pos = str->find_first_not_of(' ');
    if (pos != std::string::npos) {
      str->erase(0, pos);
    }
  } else {
    str->erase(str->begin(), str->end());
  }
}

}  // namespace cardlib

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#ifndef SOURCES_EVAL_H_
#define SOURCES_EVAL_H_

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CEval class definition                                ###
  ###                                                        ###
  ###  last updated May 1, 2024                              ###
  ###                                                        ###
  ###  created April 20, 2024                                ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cstdint>

#include <vector>
#include <map>
#include <string>

#include "sources/card.h"
#include "sources/fast_calc.h"

namespace cardlib {

class CEval {
  //             1         2         3         4         5         6
  //   0123456789012345678901234567890123456789012345678901234567890123
  //   AKQJT98765432xxxAKQJT98765432xxxAKQJT98765432xxxAKQJT98765432xxx
  //   Spades          Clubs           Diamonds        Hearts

 public:
  CEval();
  ~CEval();

  void ComputeAll(
      const cardlib::CCard *card_a1,
      const cardlib::CCard *card_a2,
      const cardlib::CCard *card_b1,
      const cardlib::CCard *card_b2) const;

  void PrintStats(
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *flushes_stats_map,
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *nonflushes_stats_map) const;

  void HandEval_5cards(
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *flushes_vrc_map,
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *nonflushes_vrc_map,
      std::vector<cardlib::CCard> *card_deck) const;

  void HandEval_7cards(
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *flushes_vrc_map,
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *nonflushes_vrc_map,
      std::vector<cardlib::CCard> *card_deck) const;

  void PocketComparison_7cards(
      const cardlib::CCard *card1,
      const cardlib::CCard *card2,
      const cardlib::CCard *card3,
      const cardlib::CCard *card4,
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *flushes_vrc_map,
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *nonflushes_vrc_map,
      const std::vector<cardlib::CCard> *card_deck) const;

  void BestFiveCardsOfSeven(
      const std::vector<cardlib::CCard> *cc_vector,
      const cardlib::CCard::kcard_suit flush_suit,
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *flushes_vrc_map,
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *nonflushes_vrc_map,
      int64_t *prime_signature,
      cardlib::CFastCalc::value_rank_count *this_vrc) const;

  void BestFlushCardsOfSeven(
      const std::vector<cardlib::CCard> *cc_vector,
      const cardlib::CCard::kcard_suit flush_suit,
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *flushes_vrc_map,
      int64_t *best_prime_signature,
      cardlib::CFastCalc::value_rank_count *result_vrc) const;

  void BestOtherCardsOfSeven(
      const std::vector<cardlib::CCard> *cc_vector,
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *nonflushes_vrc_map,
      int64_t *best_prime_signature,
      cardlib::CFastCalc::value_rank_count *result_vrc) const;

  void LoadHandMaps(
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *flushes_signature_vrc_map,
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *nonflushes_signature_vrc_map) const;

  void InitDeck(std::vector<cardlib::CCard> *card_deck) const;
};  // class CEval

}  // namespace cardlib

#endif  // SOURCES_EVAL_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

/*

// Copyright 2024 <robert haramoto>

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  utilities class implementation                        ###
  ###                                                        ###
  ###  last updated May 4, 2024                              ###
  ###    fixed updated cpplint.py errors                     ###
  ###                                                        ###
  ###  updated July 19, 2022                                 ###
  ###                                                        ###
  ###  updated February 5, 2020                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/utils.h"

#include <gmp.h>
#include <gmpxx.h>

#include <sys/stat.h>

#include <cstdio>
#include <cstdlib>  // strtoll
#include <locale>
#include <cctype>  // tolower
#include <cmath>
#include <ctime>

#include <string>
#include <vector>
#include <set>
#include <map>
#include <algorithm>  // transform
#include <chrono>

// #############################################################
// #############################################################

namespace mylib {

// #############################################################
// #############################################################
std::string CUtils::AddCommasToNumberString(
    const std::string *stmp) const {
  int64_t ii_pos, ii_end, three(3L);
  int64_t last_pos, npos, found, this_size, llen;
  std::string comma_string(","), dot_string(".");
  std::string substring_1_string, substring_2_string;
  std::string result_string(*stmp);

  this_size =
      static_cast<int64_t>(result_string.size());

  found =
      static_cast<int64_t>(result_string.find(dot_string));

  last_pos = static_cast<int64_t>(std::string::npos);

  if ((found != last_pos) && (found <= this_size)
      && (this_size > three)) {
    ii_pos = found - three;
  } else {
    if (found == last_pos) {
      if (this_size > three) {
        ii_pos = this_size - three;
      } else {
        ii_pos = 0L;
      }
    } else {
      ii_pos = 0L;
    }
  }

  // check for negative numbers
  ii_end = static_cast<int64_t>(
      (result_string[0] == '-') ? 1L : 0L);

  while (ii_pos > ii_end) {
    this_size = static_cast<int64_t>(result_string.size());
    npos = static_cast<int64_t>(ii_pos);
    substring_1_string =
        result_string.substr(0, npos);
    llen = this_size - npos;
    substring_2_string =
        result_string.substr(npos, llen);
    result_string =
        substring_1_string + comma_string
        + substring_2_string;
    ii_pos -= three;
  }

  return result_string;
}

// #############################################################
// #############################################################
// commafy integers and int64_ts
std::string CUtils::CommafyNum(const int64_t lvalue) const {
  std::string tmp_string, result_string;

  tmp_string = std::to_string(lvalue);

  result_string = AddCommasToNumberString(&tmp_string);

  return result_string;
}

// #############################################################
// #############################################################
// commafy mpz_class
std::string CUtils::CommafyMpzNum(
    const mpz_class *bn_value) const {
  std::string tmp_string, result_string;

  tmp_string = bn_value->get_str();

  result_string = AddCommasToNumberString(&tmp_string);

  return result_string;
}

// #############################################################
// #############################################################
// commafy integers and int64_ts
std::string CUtils::MpzNumToSciString(
    const mpz_class *bn_value) const {
  const int MSIZE = 256;
  char buffer[MSIZE + 1];
  double dtmp;
  std::string tmp_string, result_string;

  dtmp = bn_value->get_d();

  snprintf(buffer, MSIZE, "%12.4e", dtmp);
  tmp_string.assign(buffer);
  result_string = StringTrim(&tmp_string);

  return result_string;
}

// #############################################################
// #############################################################
// commafy integers and int64_ts
std::string CUtils::MpzNumToNiceSciString(
    const mpz_class *bn_value) const {
  int64_t nzeros(0L), this_digit(0L);
  mpz_class bn_tmp, bn_tmp2, bn_prev;
  std::string tmp_string, result_string;

  bn_prev = *bn_value;

  while (this_digit == 0L) {
    bn_tmp = bn_prev / 10L;
    bn_tmp2 = bn_prev - (10 * bn_tmp);
    this_digit =
        static_cast<int64_t>(bn_tmp2.get_d());

    if (this_digit == 0L) {
      nzeros++;
      bn_prev = bn_tmp;
    }
  }

  if (nzeros > 1L) {
    result_string = bn_prev.get_str();
    tmp_string = AddCommasToNumberString(&result_string);

    result_string =
        tmp_string
        + static_cast<std::string>(" x 10^")
        + std::to_string(nzeros);
  } else {
    tmp_string = bn_value->get_str();
    result_string = AddCommasToNumberString(&tmp_string);
  }

  return result_string;
}

// #############################################################
// #############################################################
// round floats
double CUtils::RoundFloat(double dnum, double decimals) const {
  double dtens, dresult;
  int64_t iexp;

  if (decimals >= 0.0 && decimals <= 4.0) {
    iexp = decimals;
    dtens = pow(static_cast<double>(10.0), iexp);
    if (dnum > 0.0) {
      dresult = static_cast<double>(
          (static_cast<int64_t>(
              (dnum * dtens + 0.50))) / dtens);
    } else {
      dresult = static_cast<double>(
          (static_cast<int64_t>(
              (dnum * dtens - 0.50))) / dtens);
    }

    return dresult;
  }

  return dnum;
}

// #############################################################
// #############################################################
// round floats
std::string CUtils::DoubleToString(
    double dnum, const double ndecimals) const {
  int64_t ii_num;
  double dtens;
  std::string init_string, result_string;
  std::string tmp_string;
  std::string integral_string, decimal_string;
  size_t end_pos, decimal_pos;
  size_t len;

  if ((dnum > 1e10)
      || (dnum < -1e10)) {
    char buffer[100];
    memset(buffer, 0, sizeof(buffer));
    snprintf(buffer, sizeof(buffer), "%g", dnum);
    result_string = static_cast<std::string>(buffer);
    return result_string;
  } else if ((dnum < 1e-6) && (dnum > -1e-6)) {
    char buffer[100];
    memset(buffer, 0, sizeof(buffer));
    snprintf(buffer, sizeof(buffer), "%g", dnum);
    result_string = static_cast<std::string>(buffer);
    return result_string;
  }

  if (ndecimals == 0.0) {
    dtens = 1.0;
  } else {
    dtens = std::pow(
        static_cast<double>(10.0),
        (-1.0 * ndecimals));
  }

  if (dnum >= 0.0) {
    ii_num =
        static_cast<int64_t>(dnum + (dtens * 0.50));
  } else {
    ii_num =
        static_cast<int64_t>(dnum - (dtens * 0.50));
  }

  tmp_string = std::to_string(ii_num);
  integral_string = AddCommasToNumberString(&tmp_string);

  if (ndecimals == 0.0) {
    result_string = integral_string;
  } else {
    if (dnum >= 0.0) {
      init_string = std::to_string(dnum + (dtens * 0.50));
    } else {
      init_string = std::to_string(dnum - (dtens * 0.50));
    }

    decimal_pos = init_string.find(static_cast<std::string>("."));
    end_pos = init_string.size();
    len = std::min(
        std::max(end_pos - decimal_pos - 1,
                 static_cast<size_t>(0)),
        static_cast<size_t>(ndecimals + 1));

    if (len > 0) {
      decimal_string =
          init_string.substr(decimal_pos, len);
      result_string =
          integral_string + decimal_string;
    } else {
      result_string = integral_string;
    }
  }

  return result_string;
}

// #############################################################
// #############################################################
std::string CUtils::LowerCase(const std::string *stmp) const {
  std::string sresult = { *stmp };

  std::transform(
      sresult.cbegin(), sresult.cend(), sresult.begin(),
      ::tolower);

  return sresult;
}

// #############################################################
// #############################################################
// trim string of spaces/tabs at the start and end of a line of text
std::string CUtils::StringTrim(
    const std::string *input_string) const {
  std::string result_string;
  std::string::size_type startpos =
      input_string->find_first_not_of(" \t\n");
  std::string::size_type endpos =
      input_string->find_last_not_of(" \t\n");

  // if all spaces or empty return an empty string
  if ((startpos == std::string::npos)
      || (endpos == std::string::npos)) {
    result_string.clear();
  } else {
    result_string =
        input_string->substr(startpos, endpos - startpos + 1);
  }

  return result_string;
}

// #############################################################
// #############################################################
// remove comments from string
std::string CUtils::StringRemoveComment(
    const std::string *instring,
    const char comment_delim) const {
  std::string result_string;
  std::string::size_type comment_pos = {
    instring->find(comment_delim) };

  if (comment_pos == std::string::npos) {
    result_string = *instring;
    return result_string;
  }

  result_string = *instring;
  result_string.erase(comment_pos);

  return result_string;
}

// #############################################################
// #############################################################
// tokenize a string returns a vector of strings, from
// http://oopweb.com/CPP/Documents/CPPHOWTO/Volume/ ...
// C++Programming-HOWTO-7.html
void CUtils::StringTokenize(
    const std::string *input_string,
    const char token_delim,
    std::vector<std::string> *tokens) const {
  std::string result_string;

  tokens->clear();

  // skip delimiters at the beginning
  std::string::size_type last_pos =
      input_string->find_first_not_of(token_delim, 0);

  // find first "non-delimiter"
  std::string::size_type pos =
      input_string->find_first_of(token_delim, last_pos);

  while ((std::string::npos != pos)
         || (std::string::npos != last_pos)) {
    // found a token, so add it to the vector
    tokens->push_back(
        input_string->substr(last_pos, pos - last_pos));

    last_pos =
        input_string->find_first_not_of(token_delim, pos);

    pos =
        input_string->find_first_of(token_delim, last_pos);
  }

  return;
}

// #############################################################
// #############################################################
// remove commas from string - used for dealing with
// commafied numbers
std::string CUtils::StringRemoveCommas(
    const std::string *input_string) const {
  std::string result_string;
  std::string::size_type comma_pos, next_pos;
  const char comma_delim(',');

  result_string = *input_string;
  comma_pos = result_string.find(comma_delim);
  while (comma_pos != std::string::npos) {
    // found a comma, so replace it with a null string
    result_string.replace(comma_pos, 1, "");
    next_pos = (comma_pos + 1);
    comma_pos = result_string.find(comma_delim, next_pos);
  }

  return result_string;
}

// #############################################################
// #############################################################
// convert string to integer
int64_t CUtils::StringToInteger(
    const std::string *input_string) const {
  std::string stmp;
  char **ppend(NULL);
  int64_t lresult;

  stmp = StringRemoveCommas(input_string);

  lresult = strtoll(stmp.c_str(), ppend, 10);

  return lresult;
}

// #############################################################
// #############################################################
// convert string to mpz_class big num
mpz_class CUtils::StringToMpzClass(
    const std::string *input_string) const {
  std::string stmp;
  mpz_class bn_result;

  stmp = StringRemoveCommas(input_string);

  bn_result = stmp;

  return bn_result;
}

// #############################################################
// #############################################################
std::string CUtils::IntVectorToString(
    const std::vector<int> *input_vector) const {
  int64_t ii_length;
  int64_t ii_num;
  std::vector<int>::const_iterator vi_cit;
  std::string result_string = "";

  ii_length = static_cast<int64_t>(input_vector->size());

  if (ii_length > 1L) {
    result_string = static_cast<std::string>("[");
    for (vi_cit = input_vector->cbegin();
         vi_cit != input_vector->cend(); ++vi_cit) {
      ii_num = *vi_cit;
      if (vi_cit == input_vector->cbegin()) {
        result_string += CommafyNum(ii_num);
      } else {
        result_string +=
            static_cast<std::string>(" ; ")
            + CommafyNum(ii_num);
      }
    }
    result_string += static_cast<std::string>("]");
  } else if (ii_length == 1L) {
    ii_num = *(input_vector->cbegin());
    result_string =
        static_cast<std::string>("[")
        + CommafyNum(ii_num)
        + static_cast<std::string>("]");
  } else {
    result_string = "";
  }

  return result_string;
}

// #############################################################
// #############################################################
std::string CUtils::Vector64ToString(
    const std::vector<int64_t> *input_vector,
    const std::string *open_bracket,
    const std::string *close_bracket,
    const std::string *separator_string) const {
  int64_t ii_length;
  int64_t ii_num;
  std::vector<int64_t>::const_iterator vi_cit;
  std::string result_string = "";

  ii_length = static_cast<int64_t>(input_vector->size());

  if (ii_length > 1L) {
    result_string = *open_bracket;
    for (vi_cit = input_vector->cbegin();
         vi_cit != input_vector->cend(); ++vi_cit) {
      ii_num = *vi_cit;
      if (vi_cit == input_vector->cbegin()) {
        result_string += CommafyNum(ii_num);
      } else {
        result_string +=
            *separator_string
            + CommafyNum(ii_num);
      }
    }
    result_string += *close_bracket;
  } else if (ii_length == 1L) {
    ii_num = *(input_vector->cbegin());
    result_string =
        *open_bracket
        + CommafyNum(ii_num)
        + *close_bracket;
  } else {
    result_string = "";
  }

  return result_string;
}

// #############################################################
// #############################################################
std::string CUtils::Int64VectorToString(
    const std::vector<int64_t> *input_vector) const {
  std::string result_string;
  const std::string open_bracket =
      static_cast<std::string>("[");
  const std::string close_bracket =
      static_cast<std::string>("]");
  const std::string separator_string =
      static_cast<std::string>(" ; ");

  result_string = Vector64ToString(
      input_vector,
      &open_bracket,
      &close_bracket,
      &separator_string);

  return result_string;
}

// #############################################################
// #############################################################
std::string CUtils::Int64SequenceVectorToString(
    const std::vector<int64_t> *input_vector) const {
  std::string result_string;
  const std::string open_bracket("[");
  const std::string close_bracket("]");
  const std::string separator_string(" -> ");

  result_string = Vector64ToString(
      input_vector,
      &open_bracket,
      &close_bracket,
      &separator_string);

  return result_string;
}

// #############################################################
// #############################################################
std::string CUtils::VectorMpzToString(
    const std::vector<mpz_class> *input_vector,
    const std::string *open_bracket,
    const std::string *close_bracket,
    const std::string *separator_string) const {
  int64_t ii_length;
  mpz_class bn_num;
  std::vector<mpz_class>::const_iterator vm_cit;
  std::string result_string;

  ii_length = static_cast<int64_t>(input_vector->size());

  if (ii_length > 1L) {
    result_string = *open_bracket;
    for (vm_cit = input_vector->cbegin();
         vm_cit != input_vector->cend(); ++vm_cit) {
      bn_num = *vm_cit;
      if (vm_cit == input_vector->cbegin()) {
        result_string += CommafyMpzNum(&bn_num);
      } else {
        result_string +=
            *separator_string
            + CommafyMpzNum(&bn_num);
      }
    }
    result_string += *close_bracket;
  } else if (ii_length == 1) {
    bn_num = *(input_vector->cbegin());
    result_string =
        *open_bracket
        + CommafyMpzNum(&bn_num)
        + *close_bracket;
  } else {
    result_string = "";
  }

  return result_string;
}

// #############################################################
// #############################################################
std::string CUtils::MpzVectorToString(
    const std::vector<mpz_class> *input_vector) const {
  std::string result_string;
  const std::string open_bracket =
      static_cast<std::string>("[");
  const std::string close_bracket =
      static_cast<std::string>("]");
  const std::string separator_string =
      static_cast<std::string>(" ; ");

  result_string = VectorMpzToString(
      input_vector,
      &open_bracket,
      &close_bracket,
      &separator_string);

  return result_string;
}

// #############################################################
// #############################################################
std::string CUtils::MpzSequenceVectorToString(
    const std::vector<mpz_class> *input_vector) const {
  std::string result_string;
  const std::string open_bracket =
      static_cast<std::string>("[");
  const std::string close_bracket =
      static_cast<std::string>("]");
  const std::string separator_string =
      static_cast<std::string>(" -> ");

  result_string = VectorMpzToString(
      input_vector,
      &open_bracket,
      &close_bracket,
      &separator_string);

  return result_string;
}

// #############################################################
// #############################################################
std::string CUtils::Int64SetToString(
    const std::set<int64_t> *input_set) const {
  std::set<int64_t>::const_iterator si_cit;
  int64_t ii_length;
  int64_t ll_num;
  std::string result_string = "";

  ii_length = static_cast<int64_t>(input_set->size());

  if (ii_length > 1L) {
    result_string = static_cast<std::string>("{");
    for (si_cit = input_set->cbegin();
         si_cit != input_set->cend(); ++si_cit) {
      ll_num = *si_cit;
      if (si_cit == input_set->cbegin()) {
        result_string += CommafyNum(ll_num);
      } else {
        result_string +=
            static_cast<std::string>(" ; ")
            + CommafyNum(ll_num);
      }
    }

    result_string += static_cast<std::string>("}");
  } else if (ii_length == 1L) {
    result_string =
        static_cast<std::string>("{")
        + CommafyNum(*(input_set->cbegin()))
        + static_cast<std::string>("}");
  } else {
    result_string = "";
  }

  return result_string;
}

// #############################################################
// #############################################################
std::string CUtils::MpzSetToString(
    const std::set<mpz_class> *input_set) const {
  std::set<mpz_class>::const_iterator sm_cit;
  int64_t ii_length;
  mpz_class bn_num;
  std::string result_string;

  ii_length = static_cast<int64_t>(input_set->size());

  if (ii_length > 1L) {
    result_string = static_cast<std::string>("{");
    for (sm_cit = input_set->cbegin();
         sm_cit != input_set->cend(); ++sm_cit) {
      bn_num = *sm_cit;
      if (sm_cit == input_set->cbegin()) {
        result_string += CommafyMpzNum(&bn_num);
      } else {
        result_string +=
            static_cast<std::string>(" ; ")
            + CommafyMpzNum(&bn_num);
      }
    }

    result_string += static_cast<std::string>("}");
  } else if (ii_length == 1L) {
    bn_num = *(input_set->cbegin());
    result_string +=
        static_cast<std::string>("{")
        + CommafyMpzNum(&bn_num)
        + static_cast<std::string>("}");
  } else {
    result_string = "";
  }

  return result_string;
}

// #############################################################
// #############################################################
std::string CUtils::MapInt64Int64ToString(
    const std::map<int64_t, int64_t> *input_map) const {
  std::map<int64_t, int64_t>::const_iterator m_cit;
  int64_t ii_length;
  int64_t ll_key, ll_value;
  std::string result_string = "";
  std::string keyvalue_string;

  ii_length = static_cast<int64_t>(input_map->size());

  if (ii_length > 1L) {
    result_string = static_cast<std::string>("{ ");
    for (m_cit = input_map->cbegin();
         m_cit != input_map->cend(); ++m_cit) {
      ll_key = m_cit->first;
      ll_value = m_cit->second;

      keyvalue_string =
          static_cast<std::string>("(")
          + CommafyNum(ll_key)
          + static_cast<std::string>(" ; ")
          + CommafyNum(ll_value)
          + static_cast<std::string>(")");

      if (m_cit != input_map->cbegin()) {
        result_string +=
            static_cast<std::string>(", ");
      }
      result_string += keyvalue_string;
    }
    result_string += static_cast<std::string>(" }");
  } else if (ii_length == 1L) {
    m_cit = input_map->cbegin();
    ll_key = m_cit->first;
    ll_value = m_cit->second;

    result_string =
        static_cast<std::string>("{ (")
        + CommafyNum(ll_key)
        + static_cast<std::string>(" ; ")
        + CommafyNum(ll_value)
        + static_cast<std::string>(") }");
  } else {
    result_string = "";
  }

  return result_string;
}

// #############################################################
// #############################################################
std::string CUtils::MapInt64BoolToString(
    const std::map<int64_t, bool> *input_map) const {
  std::map<int64_t, bool>::const_iterator m_cit;
  int64_t ii_length;
  int64_t ii_key;
  bool bool_value;
  std::string result_string = "";
  std::string keyvalue_string;

  ii_length = static_cast<int64_t>(input_map->size());

  if (ii_length > 1L) {
    result_string = static_cast<std::string>("{ ");
    for (m_cit = input_map->cbegin();
         m_cit != input_map->cend(); ++m_cit) {
      ii_key = m_cit->first;
      bool_value = m_cit->second;

      keyvalue_string =
          static_cast<std::string>("(")
          + CommafyNum(ii_key)
          + static_cast<std::string>(" ; ")
          + static_cast<std::string>(
              (bool_value ? "true" : "false"))
          + static_cast<std::string>(")");

      if (m_cit != input_map->cbegin()) {
        result_string +=
            static_cast<std::string>(", ");
      }
      result_string += keyvalue_string;
    }
    result_string += static_cast<std::string>(" }");
  } else if (ii_length == 1L) {
    m_cit = input_map->cbegin();
    ii_key = m_cit->first;
    bool_value = m_cit->second;

    result_string =
        static_cast<std::string>("{ (")
        + CommafyNum(ii_key)
        + static_cast<std::string>(" ; ")
        + static_cast<std::string>(
            (bool_value ? "true" : "false"))
        + static_cast<std::string>(") }");
  } else {
    result_string = "";
  }

  return result_string;
}

// #############################################################
// #############################################################
std::string CUtils::MapMpzBoolToString(
    const std::map<mpz_class, bool> *input_map) const {
  std::map<mpz_class, bool>::const_iterator m_cit;
  int64_t ii_length;
  mpz_class mpz_key;
  bool bool_value;
  std::string result_string = "";
  std::string keyvalue_string;

  ii_length = static_cast<int64_t>(input_map->size());

  if (ii_length > 1L) {
    result_string = static_cast<std::string>("{ ");
    for (m_cit = input_map->cbegin();
         m_cit != input_map->cend(); ++m_cit) {
      mpz_key = m_cit->first;
      bool_value = m_cit->second;

      if (m_cit != input_map->cbegin()) {
        result_string +=
            static_cast<std::string>(", ");
      }

      keyvalue_string =
          static_cast<std::string>("(")
          + CommafyMpzNum(&mpz_key)
          + static_cast<std::string>(" ; ")
          + static_cast<std::string>(
              (bool_value ? "true" : "false"))
          + static_cast<std::string>(")");

      result_string += keyvalue_string;
    }
    result_string += static_cast<std::string>(" }");
  } else if (ii_length == 1L) {
    m_cit = input_map->cbegin();
    mpz_key = m_cit->first;
    bool_value = m_cit->second;

    result_string =
        static_cast<std::string>("{ (")
        + CommafyMpzNum(&mpz_key)
        + static_cast<std::string>(" ; ")
        + static_cast<std::string>(
            (bool_value ? "true" : "false"))
        + static_cast<std::string>(") }");
  } else {
    result_string = "";
  }

  return result_string;
}

// #############################################################
// #############################################################
// Vector1EqualsVector2
bool CUtils::Vector1EqualsVector2(
    const std::vector<int64_t> *vector_1,
    const std::vector<int64_t> *vector_2) const {
  int64_t ii_size_1, ii_size_2;
  std::vector<int64_t>::const_iterator vl_1_cit, vl_2_cit;
  int64_t ll_tmp;

  ii_size_1 = static_cast<int64_t>(vector_1->size());
  ii_size_2 = static_cast<int64_t>(vector_2->size());
  if (ii_size_1 != ii_size_2) {
    return false;
  }

  for (vl_1_cit = vector_1->cbegin();
       vl_1_cit != vector_1->cend(); ++vl_1_cit) {
    ll_tmp = *vl_1_cit;

    vl_2_cit =
        find(vector_2->cbegin(), vector_2->cend(), ll_tmp);

    if (vl_2_cit == vector_2->cend()) {
      return false;
    }
  }

  return true;
}

// #############################################################
// #############################################################
// get current time
std::string CUtils::CurrentTime(void) const {
  time_t rawtime;
  struct tm *timeinfo, tm_output;
  std::string pm_string, time_string;
  int64_t this_sec, this_min, this_hour;

  time(&rawtime);
  timeinfo = localtime_r(&rawtime, &tm_output);

  this_sec = timeinfo->tm_sec;
  this_min = timeinfo->tm_min;
  this_hour = timeinfo->tm_hour;
  pm_string.assign("am");
  if (this_hour > 12L) {
    this_hour -= 12L;
    pm_string.assign("pm");
  } else if (this_hour == 12L) {
    pm_string.assign("pm");
  }

  if (this_hour < 10L) {
    time_string =
        static_cast<std::string>("0")
        + std::to_string(this_hour);
  } else {
    time_string =
        std::to_string(this_hour);
  }

  if (this_min < 10L) {
    time_string +=
        static_cast<std::string>(":0")
        + std::to_string(this_min);
  } else {
    time_string +=
        static_cast<std::string>(":")
        + std::to_string(this_min);
  }

  if (this_sec < 10L) {
    time_string +=
        static_cast<std::string>(":0")
        + std::to_string(this_sec);
  } else {
    time_string +=
        static_cast<std::string>(":")
        + std::to_string(this_sec);
  }

  time_string +=
      static_cast<std::string>(" ") + pm_string;

  return time_string;
}

// #############################################################
// #############################################################
// get current date
std::string CUtils::CurrentDate(void) const {
  time_t rawtime;
  struct tm *timeinfo, tm_output;
  const std::vector<std::string> month_vector = {
    "January", "February", "March",
    "April", "May", "June",
    "July", "August", "September",
    "October", "November", "December" };
  const std::vector<std::string> week_vector = {
    "Sunday", "Monday", "Tuesday",
    "Wednesday", "Thursday", "Friday",
    "Saturday" };
  int64_t this_day, this_month, this_year, this_wday;
  std::string this_month_string, this_week_string;
  std::string result_string;

  time(&rawtime);
  timeinfo = localtime_r(&rawtime, &tm_output);

  this_day = timeinfo->tm_mday;
  this_month = timeinfo->tm_mon;
  this_year = 1900L + timeinfo->tm_year;
  this_wday = timeinfo->tm_wday;

  if ((this_month >= 0L) && (this_month <= 11L)) {
    this_month_string = month_vector[this_month];
  } else {
    this_month_string.assign("month error");
  }

  if ((this_wday >= 0L) && (this_wday <= 6L)) {
    this_week_string = week_vector[this_wday];
  } else {
    this_week_string.assign("week day error");
  }

  result_string =
      this_week_string
      + static_cast<std::string>(", ")
      + this_month_string
      + static_cast<std::string>(" ")
      + std::to_string(this_day)
      + static_cast<std::string>(", ")
      + std::to_string(this_year);

  return result_string;
}

// #############################################################
// #############################################################
// get current time
std::string CUtils::CurrentDateTime(void) const {
  std::string sdate, stime, sresult;

  sdate = CurrentDate();
  stime = CurrentTime();

  sresult =
      sdate
      + static_cast<std::string>("  ")
      + stime;

  return sresult;
}

// #############################################################
// #############################################################
std::string CUtils::ClockDuration(
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *end_time,
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *start_time) const {
  std::chrono::duration<double> diff = {
    std::chrono::duration_cast<std::chrono::duration<double>>(
        *end_time - *start_time) };
  double dd_diff_seconds;
  double dtmp1, dtmp2, dtmp3;
  double dd_final_seconds;
  const double dd_seconds_per_day = 86400.0;
  const double dd_seconds_per_hour = 3600.0;
  const double dd_seconds_per_minute = 60.0;
  const double dd_seconds_digits = 2.0;
  int64_t ii_days, ii_hours, ii_minutes, ii_seconds;
  std::string result_string;

  dd_diff_seconds = diff.count();

  // number of days
  dtmp1 = dd_diff_seconds / dd_seconds_per_day;
  ii_days = static_cast<int64_t>(dtmp1);
  if (ii_days < 0L) {
    ii_days = 0L;
  }

  // number of hours
  dtmp2 = dd_diff_seconds
      - static_cast<double>(ii_days) * dd_seconds_per_day;
  ii_hours = static_cast<int64_t>(dtmp2 / dd_seconds_per_hour);
  if (ii_hours < 0L) {
    ii_hours = 0L;
  }

  // number of minutes
  dtmp3 = dtmp2
      - static_cast<double>(ii_hours) * dd_seconds_per_hour;
  ii_minutes = static_cast<int64_t>(dtmp3 / dd_seconds_per_minute);
  if (ii_minutes < 0L) {
    ii_minutes = 0L;
  }

  // number of seconds
  dd_final_seconds = dtmp3
      - static_cast<double>(ii_minutes) * dd_seconds_per_minute;
  ii_seconds = static_cast<int64_t>(dd_final_seconds);

  if (ii_days > 1L) {
    result_string =
        std::to_string(ii_days)
        + static_cast<std::string>(" days");
    if ((ii_hours > 0L)
        || (ii_minutes > 0L)
        || (dd_final_seconds > 0.0)) {
      result_string +=
          static_cast<std::string>(", ");
    }
  } else if (ii_days == 1L) {
    result_string +=
        std::to_string(ii_days)
        + static_cast<std::string>(" day");
    if ((ii_hours > 0L)
        || (ii_minutes > 0L)
        || (dd_final_seconds > 0.0)) {
      result_string +=
          static_cast<std::string>(", ");
    }
  }

  if (ii_hours > 1L) {
    result_string +=
        std::to_string(ii_hours)
        + static_cast<std::string>(" hours");
    if ((ii_minutes > 0L)
        || (dd_final_seconds > 0.0)) {
      result_string +=
          static_cast<std::string>(", ");
    }
  } else if (ii_hours == 1L) {
    result_string +=
        std::to_string(ii_hours)
        + static_cast<std::string>(" hour");
    if ((ii_minutes > 0L)
        || (dd_final_seconds > 0.0)) {
      result_string +=
          static_cast<std::string>(", ");
    }
  }

  if (ii_minutes > 1L) {
    result_string +=
        std::to_string(ii_minutes)
        + static_cast<std::string>(" minutes");
    if (dd_final_seconds > 0.0) {
      result_string +=
          static_cast<std::string>(", ");
    }
  } else if (ii_minutes == 1L) {
    result_string +=
        std::to_string(ii_minutes)
        + static_cast<std::string>(" minute");
    if (dd_final_seconds > 0.0) {
      result_string +=
          static_cast<std::string>(", ");
    }
  }

  if (ii_seconds != 1L) {
    result_string +=
        DoubleToString(dd_final_seconds, dd_seconds_digits)
        + static_cast<std::string>(" seconds");
  } else {
    result_string +=
        DoubleToString(dd_final_seconds, dd_seconds_digits)
        + static_cast<std::string>(" second");
  }

  return result_string;
}

// #############################################################
// #############################################################
double CUtils::ClockDurationInSeconds(
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *end_time,
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *start_time) const {
  double dseconds;
  std::chrono::duration<double> diff {
    std::chrono::duration_cast<std::chrono::duration<double>>(
        *end_time - *start_time) };

  dseconds = diff.count();

  return dseconds;
}

// #############################################################
// #############################################################
double CUtils::ClockDurationInMinutes(
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *end_time,
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *start_time) const {
  double dseconds, dminutes;
  const double seconds_per_minute = { 60.0 };
  std::chrono::duration<double> diff {
    std::chrono::duration_cast<std::chrono::duration<double>>(
        *end_time - *start_time) };

  dseconds = diff.count();

  dminutes = dseconds / seconds_per_minute;

  return dminutes;
}

// #############################################################
// #############################################################
bool CUtils::FileExists(const char *filename) const {
  // The variable that holds the file information, the type stat
  // and function stat have exactly the same names, so to refer
  // the type, we put struct before it to indicate it is an structure.
  struct stat fileAtt;

  // Use the stat function to get the information
  // stat will be 0 when it succeeds
  if (stat(filename, &fileAtt) != 0) {
    return false;
  }

  // S_ISREG is a macro to check if the filepath refers to a file.
  // If you don't know what a macro is, it's ok, you can use S_ISREG
  // as any other function, it 'returns' a bool.
  return S_ISREG(fileAtt.st_mode);
}

}  // namespace mylib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

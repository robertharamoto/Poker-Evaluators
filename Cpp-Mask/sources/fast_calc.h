#ifndef SOURCES_FAST_CALC_H_
#define SOURCES_FAST_CALC_H_

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
 */

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CFastCalc class definition                            ###
  ###                                                        ###
  ###  last updated May 1, 2024                              ###
  ###    revised with new bitmask definition                 ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 20, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cstdint>
#include <vector>
#include <map>
#include <string>
#include <chrono>

#include "sources/card.h"
#include "sources/hand.h"

namespace cardlib {
/*
Single card bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

class CFastCalc {
 public:
  CFastCalc();
  ~CFastCalc();

  // do not allow copying nor moving
  CFastCalc(const CFastCalc&) = delete;
  CFastCalc(CFastCalc&&) = delete;
  CFastCalc& operator=(const CFastCalc&) = delete;
  CFastCalc& operator=(CFastCalc&&) = delete;

  struct value_rank_count {
    int64_t value, rank;
    int64_t count;
  };

  struct hand_stats {
    int64_t nwins, nlosses, nties;
    int64_t ntotals;
  };

  void ComputeAll(const bool write_enum_flag,
                  const std::string *fname) const;

  void CalculateEquivalenceClass5Cards(
      std::map<cardlib::CHand, int64_t> *hand_count_map,
      std::map<cardlib::CHand, int64_t> *hand_rank_map) const;

  void CalculateEquivalenceClass7Cards(
    std::map<cardlib::CHand, int64_t> *hand_count_map,
    std::map<cardlib::CHand, int64_t> *hand_rank_map) const;

  void CalculateEquivalenceClassValueRankCountStructs(
    std::map<cardlib::CHand, int64_t> *five_count_map,
    std::map<cardlib::CHand, int64_t> *five_rank_map) const;

  void PopulateSignatureValueMaps(
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *flushes_vrc_map,
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *nonflushes_vrc_map) const;

  void Create5CardMaps(
      std::map<int64_t, struct CFastCalc::value_rank_count>
      *flush_hand_map,
      std::map<int64_t, struct CFastCalc::value_rank_count>
      *nonflush_hand_map) const;

  void CombineMaps(
      std::map<cardlib::CHand, int64_t> *hand_count_map,
      std::map<cardlib::CHand, int64_t> *hand_rank_map,
      std::map<int64_t, struct CFastCalc::value_rank_count>
      *flush_hand_map,
      std::map<int64_t, struct CFastCalc::value_rank_count>
      *nonflush_hand_map) const;

  void MakeEquivalentRankings(
      const std::map<cardlib::CHand, int64_t> *hand_count_map,
      std::map<cardlib::CHand, int64_t> *hand_rank_map) const;

  void LoadMaskMap5Cards(
      std::map<cardlib::CHand, int64_t> *hand_count_map) const;

  void LoadMaskMap7Cards(
      std::map<cardlib::CHand, int64_t> *hand_count_map) const;

  void SubtotalCardHandCounts(
      const std::vector<cardlib::CHand::khand_value> *sorted_key_vector,
      std::map<cardlib::CHand, int64_t> *hand_count_map,
      std::map<cardlib::CHand::khand_value, int64_t>
      *subtotal_hand_count_map,
      std::map<cardlib::CHand::khand_value, int64_t>
      *subtotal_distinct_count_map,
      int64_t *total_hand_count,
      int64_t *total_distinct_count) const;

  void SummarizeCardHandCounts(
      const std::string *card_hand_string,
      std::map<cardlib::CHand, int64_t> *hand_count_map,
      const std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> *end_time,
      const std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> *start_time) const;

  void SubtotalValueRankCountstructs(
      const std::vector<cardlib::CHand::khand_value>
      *sorted_key_vector,
      std::map<int64_t,
      cardlib::CFastCalc::value_rank_count> *flushes_vrc_map,
      std::map<int64_t,
      cardlib::CFastCalc::value_rank_count> *nonflushes_vrc_map,
      std::map<cardlib::CHand::khand_value, int64_t>
      *subtotal_hand_count_map,
      std::map<cardlib::CHand::khand_value, int64_t>
      *subtotal_distinct_count_map,
      int64_t *total_hand_count,
      int64_t *total_distinct_count) const;

  void SummarizeValueRankCountstructs(
      const std::string *card_hand_string,
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *flush_vrc_map,
      std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      *nonflush_vrc_map,
      const std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> *end_time,
      const std::chrono::time_point<std::chrono::steady_clock,
      std::chrono::duration<double>> *start_time) const;

  void WriteOutHandEnumerationToFile(
      const std::string *fname,
      const std::map<cardlib::CHand, int64_t> *five_rank_map,
      std::map<cardlib::CHand, int64_t> *five_count_map,
      std::map<cardlib::CHand, int64_t> *seven_count_map) const;

 private:
  std::vector<cardlib::CCard> card_deck_;
  std::vector<std::string> card_string_deck_;

  void InitDeck();
};  // class CFastCalc

}  // namespace cardlib

#endif  // SOURCES_FAST_CALC_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

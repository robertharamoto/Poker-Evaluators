/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CFullHouseFunctions implementation                    ###
  ###    namespace cardlib                                   ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###                                                        ###
  ###  created March 18, 2024                                ###
  ###    used new single integer bit scheme                  ###
  ###    from http://suffe.cool/poker/evaluator.html         ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/full_house_functions.h"

#include <cstdint>

#include <map>
#include <algorithm>
#include <vector>
#include <string>
#include <typeinfo>

#include "sources/card.h"
#include "sources/hand.h"

namespace cardlib {
/*
Single card 32-bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

// #############################################################
// #############################################################
CFullHouseFunctions::CFullHouseFunctions() {
}

// #############################################################
// #############################################################
CFullHouseFunctions::~CFullHouseFunctions() {
}

// #############################################################
// #############################################################
void CFullHouseFunctions::Find3rdAndMax(
    const std::map<cardlib::CCard::kcard_value, int64_t>
    *value_count_map,
    cardlib::CCard::kcard_value *triplevalue,
    cardlib::CCard::kcard_value *twopairvalue) const {
  std::map<cardlib::CCard::kcard_value, int64_t>::const_iterator
      mvi_cit;
  int64_t icount;
  int64_t three_count_constant(3L), two_pair_constant(2L);
  cardlib::CCard::kcard_value
      cvalue, two_pair, first_triple, second_triple;

  *triplevalue = cardlib::CCard::kcard_value::kValueNull;
  *twopairvalue = cardlib::CCard::kcard_value::kValueNull;

  two_pair = cardlib::CCard::kcard_value::kValueNull;
  first_triple = cardlib::CCard::kcard_value::kValueNull;
  second_triple = cardlib::CCard::kcard_value::kValueNull;

  // copy input to output
  for (mvi_cit = value_count_map->cbegin();
       mvi_cit != value_count_map->cend(); ++mvi_cit) {
    cvalue = mvi_cit->first;
    icount = mvi_cit->second;

    if (icount == three_count_constant) {
      // then we have icount = 3 of a kind
      if (cvalue > first_triple) {
        second_triple = first_triple;
        first_triple = cvalue;
      } else if (cvalue > second_triple) {
        second_triple = cvalue;
      }
    } else if (icount == two_pair_constant) {
      // then we have icount = 2 of a kind
      if (cvalue > two_pair) {
        two_pair = cvalue;
      }
    }
  }

  if (first_triple == cardlib::CCard::kcard_value::kValueNull) {
    *triplevalue = cardlib::CCard::kcard_value::kValueNull;
    *twopairvalue = cardlib::CCard::kcard_value::kValueNull;
    return;
  }

  if (second_triple == cardlib::CCard::kcard_value::kValueNull) {
    if (two_pair == cardlib::CCard::kcard_value::kValueNull) {
      *triplevalue = cardlib::CCard::kcard_value::kValueNull;
      *twopairvalue = cardlib::CCard::kcard_value::kValueNull;
      return;
    } else {
      *triplevalue = first_triple;
      *twopairvalue = two_pair;
      return;
    }
  } else {
    // we have a second_triple, which implies we don't have a two_pair
    *triplevalue = first_triple;
    *twopairvalue = second_triple;
    return;
  }
}

// #############################################################
// #############################################################
void CFullHouseFunctions::IsFullHouse(
    const std::vector<cardlib::CCard> *input_vector,
    const std::map<cardlib::CCard::kcard_value, int64_t>
    *value_count_map,
    cardlib::CCard::kcard_value *trip_value,
    cardlib::CCard::kcard_value *top_pair,
    int64_t *prime7_signature,
    int64_t *prime5_signature,
    cardlib::CHand::khand_value *output_handvalue) const {
  std::vector<cardlib::CCard>::const_iterator vc_cit;
  std::map<int64_t, int64_t>::const_iterator mii_cit;
  cardlib::CCard::kcard_value cvalue;
  cardlib::CCard::kcard_value triplevalue, twopairvalue;
  int64_t npair(0L), pair_constant(2L);
  int64_t this_prime;
  int64_t prime7_product, prime5_product;

  Find3rdAndMax(value_count_map, &triplevalue, &twopairvalue);

  if ((triplevalue == cardlib::CCard::kcard_value::kValueNull)
      || (twopairvalue == cardlib::CCard::kcard_value::kValueNull)) {
    // three-of-a-kind or 2-pair not found
    *trip_value = cardlib::CCard::kcard_value::kValueNull;
    *top_pair = cardlib::CCard::kcard_value::kValueNull;
    *prime7_signature = -1L;
    *prime5_signature = -1L;
    *output_handvalue =
        cardlib::CHand::khand_value::kHandValueNull;

    return;
  }

  npair = 0L;
  prime7_product = 1L;
  prime5_product = 1L;
  for (vc_cit = input_vector->cbegin();
       vc_cit != input_vector->cend(); ++vc_cit) {
    cvalue = vc_cit->GetCardValue();
    this_prime = vc_cit->GetCardPrimeValue();
    prime7_product *= this_prime;

    if (cvalue == triplevalue) {
      // store 3-of-a-kind
      prime5_product *= this_prime;
    } else if (cvalue == twopairvalue) {
      // store the remaining pair
      if (npair < pair_constant) {
        npair++;
        prime5_product *= this_prime;
      }
    }
  }

  *trip_value = triplevalue;
  *top_pair = twopairvalue;
  *prime7_signature = prime7_product;
  *prime5_signature = prime5_product;
  *output_handvalue =
      cardlib::CHand::khand_value::kFullHouse;

  return;
}

// #############################################################
// #############################################################
bool CFullHouseFunctions::FullHouseGreaterFullHouse(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CCard::kcard_value triples1, pairs1;
  cardlib::CCard::kcard_value triples2, pairs2;

  triples1 = chand1->GetTripValue();
  triples2 = chand2->GetTripValue();

  if (triples1 > triples2) {
    return true;
  } else if (triples1 < triples2) {
    return false;
  } else {
    pairs1 = chand1->GetTopPair();
    pairs2 = chand2->GetTopPair();
    return (pairs1 > pairs2);
  }
}

// #############################################################
// #############################################################
bool CFullHouseFunctions::FullHouseLessThanFullHouse(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CCard::kcard_value triples1, pairs1;
  cardlib::CCard::kcard_value triples2, pairs2;

  triples1 = chand1->GetTripValue();
  triples2 = chand2->GetTripValue();

  if (triples1 < triples2) {
    return true;
  } else if (triples1 > triples2) {
    return false;
  } else {
    pairs1 = chand1->GetTopPair();
    pairs2 = chand2->GetTopPair();
    return (pairs1 < pairs2);
  }
}

// #############################################################
// #############################################################
bool CFullHouseFunctions::FullHouseEqualFullHouse(
    const cardlib::CHand *chand1,
    const cardlib::CHand *chand2) const {
  cardlib::CCard::kcard_value triples1, pairs1;
  cardlib::CCard::kcard_value triples2, pairs2;

  triples1 = chand1->GetTripValue();
  triples2 = chand2->GetTripValue();

  if (triples1 < triples2) {
    return false;
  } else if (triples1 > triples2) {
    return false;
  } else {
    // three-of-a-kinds are equal so check pairs
    pairs1 = chand1->GetTopPair();
    pairs2 = chand2->GetTopPair();

    return (pairs1 == pairs2);
  }
}

}  // namespace cardlib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

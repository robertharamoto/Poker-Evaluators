/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  THandTest implementation                              ###
  ###    namespace handtest                                  ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###    split into multiple files                           ###
  ###    new bitmask definition for cards                    ###
  ###    fixed updated cpplint.py errors                     ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/hand_test.h"

#include <cstdint>

#include <string>
#include <vector>
#include <algorithm>
#include <cctype>       // std::tolower

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/utils.h"
#include "tests/assert_functions.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::THandTest);

namespace handtest {

// #############################################################
// #############################################################
void THandTest::TestHand1GreaterHand2FL1() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand1_obj, hand2_obj;
  cardlib::CCard card_obj;
  handtest::CAssertFunctions af_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    bool shouldbe_1v2, shouldbe_2v1;
  };
  std::vector<atest_struct> test_vector = {
    // flush versus king-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // flush versus queen-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // flush versus six-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("5h"),
        cardlib::CCard("4h"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false, true,
    },
    // flush versus five-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("7h"),
        cardlib::CCard("qh"), cardlib::CCard("5h"),
        cardlib::CCard("4h"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false, true,
    },
    // flush versus quad twos
    { { cardlib::CCard("2h"), cardlib::CCard("2c"),
        cardlib::CCard("qh"), cardlib::CCard("2d"),
        cardlib::CCard("th"), cardlib::CCard("2s"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // flush versus full house
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("qs"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // flush versus flush
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // flush versus straight
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("qh"), cardlib::CCard("kc"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // flush versus triple aces
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("as"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // flush versus two pairs
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("3d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // flush versus one pair
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("3d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // flush versus high card
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("4d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> flush_vector = {
    cardlib::CCard("jh"), cardlib::CCard("ah"),
    cardlib::CCard("qh"), cardlib::CCard("kd"),
    cardlib::CCard("2h"), cardlib::CCard("js"),
    cardlib::CCard("3h"), };
  std::vector<cardlib::CCard> card_vector;
  bool shouldbe_bool1, shouldbe_bool2;
  bool result_bool1, result_bool2;
  std::string sub_name = {
    static_cast<std::string>("THandTest(3)::")
    + static_cast<std::string>("TestHand1GreaterHand2FL1:") };
  std::string error_message;
  std::string flush_string;

  hand1_obj.Clear();
  hand1_obj.Calculate(&flush_vector);

  flush_string =
      card_obj.CardVectorToString(&flush_vector);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;

    shouldbe_bool1 = va_cit->shouldbe_1v2;
    shouldbe_bool2 = va_cit->shouldbe_2v1;

    hand2_obj.Clear();
    hand2_obj.Calculate(&card_vector);

    result_bool1 = hand1_obj.Hand1GreaterHand2(&hand2_obj);

    result_bool2 = hand2_obj.Hand1GreaterHand2(&hand1_obj);

    error_message = sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(") : card vector 1 = ")
        + flush_string
        + static_cast<std::string>(" : card vector 2 = ")
        + card_obj.CardVectorToString(&card_vector)
        + static_cast<std::string>(" : ");

    af_obj.Assert1Greater2(
        &error_message,
        &hand1_obj, &hand2_obj,
        shouldbe_bool1, shouldbe_bool2,
        result_bool1, result_bool2);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THandTest::TestHand1GreaterHand2ST1() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand1_obj, hand2_obj;
  cardlib::CCard card_obj;
  handtest::CAssertFunctions af_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    bool shouldbe_1v2, shouldbe_2v1;
  };
  std::vector<atest_struct> test_vector = {
    // king-high straight versus king-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // king-high straight versus queen-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // king-high straight versus six-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("5h"),
        cardlib::CCard("4h"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false, true,
    },
    // king-high straight versus five-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("8h"),
        cardlib::CCard("qh"), cardlib::CCard("5h"),
        cardlib::CCard("4h"), cardlib::CCard("2h"),
        cardlib::CCard("3h"),
      },
      false, true,
    },
    // king-high straight versus quad sevens
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("7s"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // king-high straight versus sevens full of queens
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("qs"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // king-high straight versus queen-high flush
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // king-high straight versus ace-high straight
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("qh"), cardlib::CCard("kc"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8d"),
      },
      false, true,
    },
    // king-high straight versus three-of-a-kind
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("as"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8d"),
      },
      true, false,
    },
    // king-high straight versus two-pairs
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("2d"),
        cardlib::CCard("th"), cardlib::CCard("2s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // king-high straight versus one-pair
    { { cardlib::CCard("kd"), cardlib::CCard("3c"),
        cardlib::CCard("ac"), cardlib::CCard("2d"),
        cardlib::CCard("th"), cardlib::CCard("2s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // king-high straight versus high-card
    { { cardlib::CCard("kd"), cardlib::CCard("3c"),
        cardlib::CCard("ac"), cardlib::CCard("4d"),
        cardlib::CCard("th"), cardlib::CCard("2s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> straight_vector = {
    cardlib::CCard("jh"), cardlib::CCard("tc"),
    cardlib::CCard("qh"), cardlib::CCard("kd"),
    cardlib::CCard("2h"), cardlib::CCard("js"),
    cardlib::CCard("9s")
  };
  std::vector<cardlib::CCard> card_vector;
  bool shouldbe_bool1, shouldbe_bool2;
  bool result_bool1, result_bool2;
  std::string sub_name = {
    static_cast<std::string>("THandTest(3)::")
    + static_cast<std::string>("TestHand1GreaterHand2ST1:") };
  std::string error_message;
  std::string straight_string;

  hand1_obj.Clear();
  hand1_obj.Calculate(&straight_vector);

  straight_string = card_obj.CardVectorToString(&straight_vector);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;

    shouldbe_bool1 = va_cit->shouldbe_1v2;
    shouldbe_bool2 = va_cit->shouldbe_2v1;

    hand2_obj.Clear();
    hand2_obj.Calculate(&card_vector);

    result_bool1 = hand1_obj.Hand1GreaterHand2(&hand2_obj);

    result_bool2 = hand2_obj.Hand1GreaterHand2(&hand1_obj);

    error_message = sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(") : card vector 1 = ")
        + straight_string
        + static_cast<std::string>(" : card vector 2 = ")
        + card_obj.CardVectorToString(&card_vector)
        + static_cast<std::string>(" : ");

    af_obj.Assert1Greater2(
        &error_message,
        &hand1_obj, &hand2_obj,
        shouldbe_bool1, shouldbe_bool2,
        result_bool1, result_bool2);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THandTest::TestHand1GreaterHand2Triples1() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand1_obj, hand2_obj;
  cardlib::CCard card_obj;
  handtest::CAssertFunctions af_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    bool shouldbe_1v2, shouldbe_2v1;
  };
  std::vector<atest_struct> test_vector = {
    // triple tens versus king-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // triple tens versus queen-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // triple tens versus six-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("5h"),
        cardlib::CCard("4h"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false, true,
    },
    // triple tens versus five-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("8h"),
        cardlib::CCard("qh"), cardlib::CCard("5h"),
        cardlib::CCard("4h"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false, true,
    },
    // triple tens versus quad sevens
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("7s"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // triple tens versus full house
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("qs"),
        cardlib::CCard("td"),
      },
      false, true,
    },
    // triple tens versus queen-high flush
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // triple tens versus straight
    { { cardlib::CCard("ad"), cardlib::CCard("jc"),
        cardlib::CCard("qh"), cardlib::CCard("kh"),
        cardlib::CCard("2h"), cardlib::CCard("th"),
        cardlib::CCard("8c"),
      },
      false, true,
    },
    // triple tens versus triple aces
    { { cardlib::CCard("ad"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("2h"),
        cardlib::CCard("as"), cardlib::CCard("th"),
        cardlib::CCard("8c"),
      },
      false, true,
    },
    // triple tens versus two pairs
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("3d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8d"),
      },
      true, false,
    },
    // triple tens versus one pair
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("3d"),
        cardlib::CCard("th"), cardlib::CCard("2s"),
        cardlib::CCard("8d"),
      },
      true, false,
    },
    // triple tens versus high card
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("4d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8d"),
      },
      true, false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> triple_vector = {
    cardlib::CCard("jh"), cardlib::CCard("tc"),
    cardlib::CCard("th"), cardlib::CCard("kd"),
    cardlib::CCard("2h"), cardlib::CCard("ts"),
    cardlib::CCard("4s")
  };
  std::vector<cardlib::CCard> card_vector;
  bool shouldbe_bool1, shouldbe_bool2;
  bool result_bool1, result_bool2;
  std::string sub_name = {
    static_cast<std::string>("THandTest(3)::")
    + static_cast<std::string>("TestHand1GreaterHand2Triples1:") };
  std::string error_message;
  std::string triple_string;

  hand1_obj.Clear();
  hand1_obj.Calculate(&triple_vector);

  triple_string = card_obj.CardVectorToString(&triple_vector);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;

    shouldbe_bool1 = va_cit->shouldbe_1v2;
    shouldbe_bool2 = va_cit->shouldbe_2v1;

    hand2_obj.Clear();
    hand2_obj.Calculate(&card_vector);

    result_bool1 = hand1_obj.Hand1GreaterHand2(&hand2_obj);

    result_bool2 = hand2_obj.Hand1GreaterHand2(&hand1_obj);

    error_message = sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(") : card vector 1 = ")
        + triple_string
        + static_cast<std::string>(" : card vector 2 = ")
        + card_obj.CardVectorToString(&card_vector)
        + static_cast<std::string>(" : ");

    af_obj.Assert1Greater2(
        &error_message,
        &hand1_obj, &hand2_obj,
        shouldbe_bool1, shouldbe_bool2,
        result_bool1, result_bool2);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THandTest::TestHand1GreaterHand2TwoPair1() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand1_obj, hand2_obj;
  cardlib::CCard card_obj;
  handtest::CAssertFunctions af_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    bool shouldbe_1v2, shouldbe_2v1;
  };
  std::vector<atest_struct> test_vector = {
    // two pairs versus king-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("8h"),
        cardlib::CCard("9h"),
      },
      false, true,
    },
    // two pairs versus jack-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("6h"),
        cardlib::CCard("5h"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("8h"),
        cardlib::CCard("9h"),
      },
      false, true,
    },
    // two pairs versus six-high straight flush
    { { cardlib::CCard("th"), cardlib::CCard("ah"),
        cardlib::CCard("5h"), cardlib::CCard("2h"),
        cardlib::CCard("6h"), cardlib::CCard("3h"),
        cardlib::CCard("4h"),
      },
      false, true,
    },
    // two pairs versus five-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("8h"),
        cardlib::CCard("5h"), cardlib::CCard("2h"),
        cardlib::CCard("qh"), cardlib::CCard("3h"),
        cardlib::CCard("4h"),
      },
      false, true,
    },
    // two pairs versus quad sevens
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("7s"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // two pairs versus full house
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("qd"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // two pairs versus flush
    { { cardlib::CCard("7d"), cardlib::CCard("7c"),
        cardlib::CCard("qd"), cardlib::CCard("jd"),
        cardlib::CCard("2d"), cardlib::CCard("3d"),
        cardlib::CCard("8d"),
      },
      false, true,
    },
    // two pairs versus straight
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("qh"), cardlib::CCard("kc"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      false, true,
    },
    // two pairs versus triple aces
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("as"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      false, true,
    },
    // two pairs versus two pairs
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("2d"),
        cardlib::CCard("th"), cardlib::CCard("2s"),
        cardlib::CCard("8c"),
      },
      false, true,
    },
    // two pairs versus one pair
    { { cardlib::CCard("kd"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("2d"),
        cardlib::CCard("th"), cardlib::CCard("2s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // two pairs versus high card
    { { cardlib::CCard("kd"), cardlib::CCard("3c"),
        cardlib::CCard("ac"), cardlib::CCard("2d"),
        cardlib::CCard("th"), cardlib::CCard("7s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> twopairs_vector = {
    cardlib::CCard("jh"), cardlib::CCard("2c"),
    cardlib::CCard("th"), cardlib::CCard("kd"),
    cardlib::CCard("2h"), cardlib::CCard("ts"),
    cardlib::CCard("4s")
  };
  std::vector<cardlib::CCard> card_vector;
  bool shouldbe_bool1, shouldbe_bool2;
  bool result_bool1, result_bool2;
  std::string sub_name = {
    static_cast<std::string>("THandTest(3)::")
    + static_cast<std::string>("TestHand1GreaterHand2TwoPair1:") };
  std::string error_message;
  std::string twopairs_string;

  hand1_obj.Clear();
  hand1_obj.Calculate(&twopairs_vector);

  twopairs_string =
      card_obj.CardVectorToString(&twopairs_vector);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;

    shouldbe_bool1 = va_cit->shouldbe_1v2;
    shouldbe_bool2 = va_cit->shouldbe_2v1;

    hand2_obj.Clear();
    hand2_obj.Calculate(&card_vector);

    result_bool1 = hand1_obj.Hand1GreaterHand2(&hand2_obj);

    result_bool2 = hand2_obj.Hand1GreaterHand2(&hand1_obj);

    error_message = sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(") : card vector 1 = ")
        + twopairs_string
        + static_cast<std::string>(" : card vector 2 = ")
        + card_obj.CardVectorToString(&card_vector)
        + static_cast<std::string>(" : ");

    af_obj.Assert1Greater2(
        &error_message,
        &hand1_obj, &hand2_obj,
        shouldbe_bool1, shouldbe_bool2,
        result_bool1, result_bool2);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THandTest::TestHand1GreaterHand2OnePair1() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand1_obj, hand2_obj;
  cardlib::CCard card_obj;
  handtest::CAssertFunctions af_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    bool shouldbe_1v2, shouldbe_2v1;
  };
  std::vector<atest_struct> test_vector = {
    // one pair of tens versus king-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("8h"),
        cardlib::CCard("9h"),
      },
      false, true,
    },
    // one pair of tens versus queen-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("8h"),
        cardlib::CCard("9h"),
      },
      false, true,
    },
    // one pair of tens versus six-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("6d"),
        cardlib::CCard("qh"), cardlib::CCard("5d"),
        cardlib::CCard("4d"), cardlib::CCard("2d"),
        cardlib::CCard("3d"),
      },
      false, true,
    },
    // one pair of tens versus five-high straight flush
    { { cardlib::CCard("ad"), cardlib::CCard("6s"),
        cardlib::CCard("qh"), cardlib::CCard("5d"),
        cardlib::CCard("4d"), cardlib::CCard("2d"),
        cardlib::CCard("3d"),
      },
      false, true,
    },
    // one pair of tens versus quad twos
    { { cardlib::CCard("2d"), cardlib::CCard("2s"),
        cardlib::CCard("qh"), cardlib::CCard("2c"),
        cardlib::CCard("4d"), cardlib::CCard("2h"),
        cardlib::CCard("3d"),
      },
      false, true,
    },
    // one pair of tens versus full house
    { { cardlib::CCard("2d"), cardlib::CCard("2s"),
        cardlib::CCard("qh"), cardlib::CCard("2c"),
        cardlib::CCard("4d"), cardlib::CCard("3h"),
        cardlib::CCard("3d"),
      },
      false, true,
    },
    // one pair of tens versus flush
    { { cardlib::CCard("2h"), cardlib::CCard("2s"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("3h"), cardlib::CCard("7h"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // one pair of tens versus straight
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("qh"), cardlib::CCard("kc"),
        cardlib::CCard("td"), cardlib::CCard("7s"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // one pair of tens versus triple aces
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("kc"),
        cardlib::CCard("th"), cardlib::CCard("as"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // one pair of tens versus two pair
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("3c"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // one pair of tens versus one pair of threes
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("3c"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // one pair of tens versus high card
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("4d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> onepair_vector = {
    cardlib::CCard("jh"), cardlib::CCard("tc"),
    cardlib::CCard("th"), cardlib::CCard("kd"),
    cardlib::CCard("2h"), cardlib::CCard("5s"),
    cardlib::CCard("4s")
  };
  std::vector<cardlib::CCard> card_vector;
  bool shouldbe_bool1, shouldbe_bool2;
  bool result_bool1, result_bool2;
  std::string sub_name = {
    static_cast<std::string>("THandTest(3)::")
    + static_cast<std::string>("TestHand1GreaterHand2OnePair1:") };
  std::string error_message;
  std::string onepair_string;

  hand1_obj.Clear();
  hand1_obj.Calculate(&onepair_vector);

  onepair_string =
      card_obj.CardVectorToString(&onepair_vector);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;

    shouldbe_bool1 = va_cit->shouldbe_1v2;
    shouldbe_bool2 = va_cit->shouldbe_2v1;

    hand2_obj.Clear();
    hand2_obj.Calculate(&card_vector);

    result_bool1 = hand1_obj.Hand1GreaterHand2(&hand2_obj);

    result_bool2 = hand2_obj.Hand1GreaterHand2(&hand1_obj);

    error_message = sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(") : card vector 1 = ")
        + onepair_string
        + static_cast<std::string>(" : card vector 2 = ")
        + card_obj.CardVectorToString(&card_vector)
        + static_cast<std::string>(" : ");

    af_obj.Assert1Greater2(
        &error_message,
        &hand1_obj, &hand2_obj,
        shouldbe_bool1, shouldbe_bool2,
        result_bool1, result_bool2);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THandTest::TestHand1GreaterHand2HighCard1() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand1_obj, hand2_obj;
  cardlib::CCard card_obj;
  handtest::CAssertFunctions af_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    bool shouldbe_1v2, shouldbe_2v1;
  };
  std::vector<atest_struct> test_vector = {
    // high-card versus king-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("8h"),
        cardlib::CCard("9h"),
      },
      false, true,
    },
    // high-card versus queen-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("8h"),
        cardlib::CCard("9h"),
      },
      false, true,
    },
    // high-card versus six-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("6h"),
        cardlib::CCard("5h"), cardlib::CCard("jh"),
        cardlib::CCard("4h"), cardlib::CCard("2h"),
        cardlib::CCard("3h"),
      },
      false, true,
    },
    // high-card versus five-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("8h"),
        cardlib::CCard("5h"), cardlib::CCard("jh"),
        cardlib::CCard("4h"), cardlib::CCard("2h"),
        cardlib::CCard("3h"),
      },
      false, true,
    },
    // high-card versus quad sevens
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("7s"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // high-card versus full house
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("qs"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // high-card versus flush
    { { cardlib::CCard("7c"), cardlib::CCard("9c"),
        cardlib::CCard("qh"), cardlib::CCard("kc"),
        cardlib::CCard("tc"), cardlib::CCard("2c"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // high-card versus straight
    { { cardlib::CCard("jd"), cardlib::CCard("9c"),
        cardlib::CCard("qh"), cardlib::CCard("kd"),
        cardlib::CCard("tc"), cardlib::CCard("2s"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // high-card versus triple aces
    { { cardlib::CCard("jd"), cardlib::CCard("ac"),
        cardlib::CCard("qh"), cardlib::CCard("ad"),
        cardlib::CCard("tc"), cardlib::CCard("as"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // high-card versus two pairs
    { { cardlib::CCard("jd"), cardlib::CCard("th"),
        cardlib::CCard("qh"), cardlib::CCard("ad"),
        cardlib::CCard("tc"), cardlib::CCard("as"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // high-card versus one pair
    { { cardlib::CCard("jd"), cardlib::CCard("th"),
        cardlib::CCard("qh"), cardlib::CCard("2d"),
        cardlib::CCard("tc"), cardlib::CCard("as"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // high-card versus high card
    { { cardlib::CCard("jd"), cardlib::CCard("th"),
        cardlib::CCard("qh"), cardlib::CCard("2d"),
        cardlib::CCard("3c"), cardlib::CCard("5s"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> highcard_vector = {
    cardlib::CCard("jh"), cardlib::CCard("tc"),
    cardlib::CCard("7d"), cardlib::CCard("kd"),
    cardlib::CCard("2h"), cardlib::CCard("5s"),
    cardlib::CCard("4s")
  };
  std::vector<cardlib::CCard> card_vector;
  bool shouldbe_bool1, shouldbe_bool2;
  bool result_bool1, result_bool2;
  std::string sub_name = {
    static_cast<std::string>("THandTest(3)::")
    + static_cast<std::string>("TestHand1GreaterHand2HighCard1:") };
  std::string error_message;
  std::string highcard_string;

  hand1_obj.Clear();
  hand1_obj.Calculate(&highcard_vector);

  highcard_string =
      card_obj.CardVectorToString(&highcard_vector);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;

    shouldbe_bool1 = va_cit->shouldbe_1v2;
    shouldbe_bool2 = va_cit->shouldbe_2v1;

    hand2_obj.Clear();
    hand2_obj.Calculate(&card_vector);

    result_bool1 = hand1_obj.Hand1GreaterHand2(&hand2_obj);

    result_bool2 = hand2_obj.Hand1GreaterHand2(&hand1_obj);

    error_message = sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(") : card vector 1 = ")
        + highcard_string
        + static_cast<std::string>(" : card vector 2 = ")
        + card_obj.CardVectorToString(&card_vector)
        + static_cast<std::string>(" : ");

    af_obj.Assert1Greater2(
        &error_message,
        &hand1_obj, &hand2_obj,
        shouldbe_bool1, shouldbe_bool2,
        result_bool1, result_bool2);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

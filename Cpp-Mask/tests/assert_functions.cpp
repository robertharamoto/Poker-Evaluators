/*

// Copyright 2024 robert haramoto

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CAssertFunctions implementation                       ###
  ###  namespace HandTest                                    ###
  ###                                                        ###
  ###  last updated April 30, 2024                           ###
  ###    new card mask definition                            ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 19, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/assert_functions.h"

#include <cstdio>
#include <cstdint>

#include <string>
#include <vector>
#include <algorithm>

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/utils.h"

namespace handtest {

// #############################################################
// #############################################################
CAssertFunctions::CAssertFunctions() {
}

// #############################################################
// #############################################################
CAssertFunctions::~CAssertFunctions() {
}


// #############################################################
// #############################################################
void CAssertFunctions::AssertPrimesHelper(
    const std::string *err_start,
    const int64_t shouldbe_prime_product,
    const int64_t result_prime_product,
    const int64_t shouldbe_straight_mask,
    const int64_t result_straight_mask) const {
  std::string error_message;

  error_message =
      *err_start
      + static_cast<std::string>("prime signature shouldbe = ")
      + std::to_string(shouldbe_prime_product)
      + static_cast<std::string>(", result = ")
      + std::to_string(result_prime_product);

  CPPUNIT_ASSERT_EQUAL_MESSAGE(
      error_message, shouldbe_prime_product, result_prime_product);

  error_message =
      *err_start
      + static_cast<std::string>("straight mask shouldbe = ")
      + std::to_string(shouldbe_straight_mask)
      + static_cast<std::string>(", result = ")
      + std::to_string(result_straight_mask);

  CPPUNIT_ASSERT_EQUAL_MESSAGE(
      error_message, shouldbe_straight_mask, result_straight_mask);

  return;
}

// #############################################################
// #############################################################
void CAssertFunctions::AssertCardVectorsEqual(
    const std::string *err_1,
    const std::vector<cardlib::CCard> *shouldbe_vec,
    const std::vector<cardlib::CCard> *result_vec) const {
  cardlib::CCard card_obj;
  std::vector<cardlib::CCard>::const_iterator
      vc_cit, find_cit;
  int64_t ii, s_size, r_size;
  std::string err_2, shouldbe_vec_string, result_vec_string;

  shouldbe_vec_string =
      card_obj.CardVectorToString(shouldbe_vec);
  result_vec_string =
      card_obj.CardVectorToString(result_vec);

  // first check if sizes are equal
  s_size = static_cast<int64_t>(shouldbe_vec->size());
  r_size = static_cast<int64_t>(result_vec->size());
  err_2 = *err_1
      + static_cast<std::string>("shouldbe vector = ")
      + shouldbe_vec_string
      + static_cast<std::string>(", shouldbe size = ")
      + std::to_string(s_size)
      + static_cast<std::string>(" : result vector = ")
      + result_vec_string
      + static_cast<std::string>(", result size = ")
      + std::to_string(r_size);

  CPPUNIT_ASSERT_EQUAL_MESSAGE(err_2, s_size, r_size);

  ii = 0L;
  for (vc_cit = shouldbe_vec->cbegin();
       vc_cit != shouldbe_vec->cend(); ++vc_cit) {
    find_cit =
        find(result_vec->cbegin(),
             result_vec->cend(), *vc_cit);

    err_2 = *err_1
        + static_cast<std::string>("shouldbe vector = ")
        + shouldbe_vec_string
        + static_cast<std::string>(" : element check(")
        + std::to_string(ii)
        + static_cast<std::string>(") missing element = ")
        + vc_cit->CardToString()
        + static_cast<std::string>(" from result vector = ")
        + result_vec_string;

    CPPUNIT_ASSERT_MESSAGE(
        err_2, (find_cit != result_vec->cend()));

    ++ii;
  }

  ii = 0L;
  for (vc_cit = result_vec->cbegin();
       vc_cit != result_vec->cend(); ++vc_cit) {
    find_cit =
        find(shouldbe_vec->cbegin(),
             shouldbe_vec->cend(), *vc_cit);

    err_2 = *err_1
        + static_cast<std::string>("result vector = ")
        + result_vec_string
        + static_cast<std::string>(" : element check(")
        + std::to_string(ii)
        + static_cast<std::string>(") missing element = ")
        + vc_cit->CardToString()
        + static_cast<std::string>(" from shouldbe vector = ")
        + shouldbe_vec_string;

    CPPUNIT_ASSERT_MESSAGE(
        err_2, (find_cit != shouldbe_vec->cend()));

    ++ii;
  }

  return;
}

// #############################################################
// #############################################################
void CAssertFunctions::AssertOrderedCardVectorsEqual(
    const std::string *err_1,
    const std::vector<cardlib::CCard> *shouldbe_vec,
    const std::vector<cardlib::CCard> *result_vec) const {
  cardlib::CCard card_obj;
  int64_t jj, jj_length, s_size, r_size;
  cardlib::CCard scard, rcard;
  std::string err_2, shouldbe_vec_string, result_vec_string;
  std::string s_string, r_string;

  shouldbe_vec_string =
      card_obj.CardVectorToString(shouldbe_vec);
  result_vec_string =
      card_obj.CardVectorToString(result_vec);

  // first check if sizes are equal
  s_size = static_cast<int64_t>(shouldbe_vec->size());
  r_size = static_cast<int64_t>(result_vec->size());
  err_2 = *err_1
      + static_cast<std::string>("shouldbe vector = ")
      + shouldbe_vec_string
      + static_cast<std::string>(", shouldbe size = ")
      + std::to_string(s_size)
      + static_cast<std::string>(" : result vector = ")
      + result_vec_string
      + static_cast<std::string>(", result size = ")
      + std::to_string(r_size);

  CPPUNIT_ASSERT_EQUAL_MESSAGE(err_2, s_size, r_size);

  jj_length = s_size;

  for (jj = 0; jj < jj_length; ++jj) {
    scard = (*shouldbe_vec)[jj];
    rcard = (*result_vec)[jj];

    s_string = scard.CardToString();
    r_string = rcard.CardToString();

    err_2 = *err_1
        + static_cast<std::string>("shouldbe vector = ")
        + shouldbe_vec_string
        + static_cast<std::string>(" : element check(")
        + std::to_string(jj)
        + static_cast<std::string>(") discrepancy at = ")
        + s_string
        + static_cast<std::string>(", instead result = ")
        + r_string
        + static_cast<std::string>(", where result vector = ")
        + result_vec_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(err_2, s_string, r_string);
  }

  return;
}

// #############################################################
// #############################################################
void CAssertFunctions::AssertOrderedInt64VectorsEqual(
    const std::string *err_1,
    const std::vector<int64_t> *shouldbe_vec,
    const std::vector<int64_t> *result_vec) const {
  cardlib::CCard card_obj;
  mylib::CUtils utils_obj;
  int64_t jj, jj_length, s_size, r_size;
  int64_t shouldbe_int, result_int;
  std::string err_2, shouldbe_vec_string, result_vec_string;
  std::string s_string, r_string;

  shouldbe_vec_string =
      utils_obj.Int64VectorToString(shouldbe_vec);

  result_vec_string =
      utils_obj.Int64VectorToString(result_vec);

  // first check if sizes are equal
  s_size = static_cast<int64_t>(shouldbe_vec->size());
  r_size = static_cast<int64_t>(result_vec->size());

  err_2 = *err_1
      + static_cast<std::string>("shouldbe vector = ")
      + shouldbe_vec_string
      + static_cast<std::string>(", shouldbe size = ")
      + std::to_string(s_size)
      + static_cast<std::string>(" : result vector = ")
      + result_vec_string
      + static_cast<std::string>(", result size = ")
      + std::to_string(r_size);

  CPPUNIT_ASSERT_EQUAL_MESSAGE(err_2, s_size, r_size);

  jj_length = s_size;

  for (jj = 0; jj < jj_length; ++jj) {
    shouldbe_int = (*shouldbe_vec)[jj];
    result_int = (*result_vec)[jj];

    s_string = std::to_string(shouldbe_int);
    r_string = std::to_string(result_int);

    err_2 = *err_1
        + static_cast<std::string>("shouldbe vector = ")
        + shouldbe_vec_string
        + static_cast<std::string>(" : element check(")
        + std::to_string(jj)
        + static_cast<std::string>(") discrepancy at = ")
        + s_string
        + static_cast<std::string>(", instead result = ")
        + r_string
        + static_cast<std::string>(", where result vector = ")
        + result_vec_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(err_2, s_string, r_string);
  }

  return;
}

// #############################################################
// #############################################################
void CAssertFunctions::Assert1Greater2(
    const std::string *err_1,
    const cardlib::CHand *hand1_obj,
    const cardlib::CHand *hand2_obj,
    const bool shouldbe_bool1,
    const bool shouldbe_bool2,
    const bool result_bool1,
    const bool result_bool2) const {
  mylib::CUtils utils_obj;
  std::string err_start, error_message;

  err_start = *err_1
      + static_cast<std::string>("for ")
      + hand1_obj->HandName()
      + static_cast<std::string>(" versus ")
      + hand2_obj->HandName()
      + static_cast<std::string>(" : ");

  error_message =
      err_start
      + static_cast<std::string>("shouldbe = ")
      + static_cast<std::string>(
          (shouldbe_bool1 ? "true" : "false"))
      + static_cast<std::string>(", result = ")
      + static_cast<std::string>(
          (result_bool1 ? "true" : "false"));

  CPPUNIT_ASSERT_EQUAL_MESSAGE(
      error_message, shouldbe_bool1, result_bool1);

  err_start = *err_1
      + static_cast<std::string>("for ")
      + hand2_obj->HandName()
      + static_cast<std::string>(" versus ")
      + hand1_obj->HandName()
      + static_cast<std::string>(" : ");

  error_message =
      err_start
      + static_cast<std::string>("shouldbe = ")
      + static_cast<std::string>(
          (shouldbe_bool2 ? "true" : "false"))
      + static_cast<std::string>(", result = ")
      + static_cast<std::string>(
          (result_bool2 ? "true" : "false"));

  CPPUNIT_ASSERT_EQUAL_MESSAGE(
      error_message, shouldbe_bool2, result_bool2);

  return;
}

}  // namespace handtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

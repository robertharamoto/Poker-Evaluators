/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TEvalTest class implementation                        ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ###  last updated April 22, 2024                           ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 23, 2015                              ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/eval_test.h"

#include <cstdint>
#include <algorithm>
#include <cctype>       // std::tolower
#include <string>
#include <vector>
#include <map>

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/eval.h"
#include "sources/flush_functions.h"
#include "sources/utils.h"

CPPUNIT_TEST_SUITE_REGISTRATION(cardtest::TEvalTest);

namespace cardtest {

// #############################################################
// #############################################################
TEvalTest::TEvalTest() {
}

// #############################################################
// #############################################################
TEvalTest::~TEvalTest() {
}

// #############################################################
// #############################################################
void TEvalTest::setUp() {
}

// #############################################################
// #############################################################
void TEvalTest::tearDown() {
}

/*
Single card 32-bit scheme
+--------+--------+--------+--------+
|xxxbbbbb|bbbbbbbb|cdhsrrrr|xxpppppp|
+--------+--------+--------+--------+

p = prime number of rank (deuce=2,trey=3,four=5,...,ace=41)
r = rank of card (deuce=0,trey=1,four=2,five=3,...,ace=12)
cdhs = suit of card (bit turned on based on suit of card)
b = bit turned on depending on rank of card


Using such a scheme, here are some bit pattern examples:

//  3          2          1
// 10987654 32109876 54321098 76543210
// xxxAKQJT 98765432 CDHSrrrr xxPPPPPP
// 00001000 00000000 01001011 00100101    King of Diamonds
// 00000000 00001000 00010011 00000111    Five of Spades
// 00000010 00000000 10001001 00011101    Jack of Clubs

*/

// #############################################################
// #############################################################
// helper function
void TEvalTest::AssertBestIntString(
    const std::string &err_start,
    const int64_t ishouldbe,
    const int64_t iresult,
    const std::string &shouldbe_string,
    const std::string &result_string) {
  mylib::CUtils utils_obj;
  std::string error_message;

  error_message =
      err_start
      + static_cast<std::string>(", shouldbe = ")
      + std::to_string(ishouldbe)
      + static_cast<std::string>(", result = ")
      + std::to_string(iresult);

  CPPUNIT_ASSERT_EQUAL_MESSAGE(
      error_message, ishouldbe, iresult);

  error_message =
      err_start
      + static_cast<std::string>(", shouldbe = ")
      + shouldbe_string
      + static_cast<std::string>(", result = ")
      + result_string;

  CPPUNIT_ASSERT_EQUAL_MESSAGE(
      error_message, shouldbe_string, result_string);

  return;
}

// #############################################################
// #############################################################
// helper function
void TEvalTest::AssertBestString(
    const std::string &err_start,
    const std::string &shouldbe_string,
    const std::string &result_string) {
  mylib::CUtils utils_obj;
  std::string error_message;

  error_message =
      err_start
      + static_cast<std::string>(", shouldbe = ")
      + shouldbe_string
      + static_cast<std::string>(", result = ")
      + result_string;

  CPPUNIT_ASSERT_EQUAL_MESSAGE(
      error_message, shouldbe_string, result_string);

  return;
}

// #############################################################
// #############################################################
// irank = 1, royal flush
void TEvalTest::TestBestFiveCardsOfSevenStraightFlushes1() {
  cardlib::CEval eval_obj;
  mylib::CUtils utils_obj;
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    int64_t rank;
    std::string hand_name;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("ah"), cardlib::CCard("kh"),
        cardlib::CCard("9h"), cardlib::CCard("jh"),
        cardlib::CCard("qh"), cardlib::CCard("8h"),
        cardlib::CCard("th"),
      },
      7462, "royal-flush",
    },
    { { cardlib::CCard("td"), cardlib::CCard("kd"),
        cardlib::CCard("9d"), cardlib::CCard("jd"),
        cardlib::CCard("qd"), cardlib::CCard("8d"),
        cardlib::CCard("ad"),
      },
      7462, "royal-flush",
    },
    { { cardlib::CCard("tc"), cardlib::CCard("8h"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("9d"), cardlib::CCard("kc"),
        cardlib::CCard("ac"),
      },
      7462, "royal-flush",
    },
    { { cardlib::CCard("as"), cardlib::CCard("ts"),
        cardlib::CCard("8s"), cardlib::CCard("ks"),
        cardlib::CCard("qs"), cardlib::CCard("9h"),
        cardlib::CCard("js"),
      },
      7462, "royal-flush",
    },
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("9h"), cardlib::CCard("jh"),
        cardlib::CCard("qh"), cardlib::CCard("8h"),
        cardlib::CCard("th"),
      },
      7461, "straight-flush",
    },
    { { cardlib::CCard("6d"), cardlib::CCard("5d"),
        cardlib::CCard("9d"), cardlib::CCard("jd"),
        cardlib::CCard("qd"), cardlib::CCard("8d"),
        cardlib::CCard("td"),
      },
      7460, "straight-flush",
    },
    { { cardlib::CCard("ac"), cardlib::CCard("7c"),
        cardlib::CCard("9c"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("8c"),
        cardlib::CCard("tc"),
      },
      7459, "straight-flush",
    },
    { { cardlib::CCard("ac"), cardlib::CCard("7c"),
        cardlib::CCard("9c"), cardlib::CCard("6c"),
        cardlib::CCard("5c"), cardlib::CCard("8c"),
        cardlib::CCard("tc"),
      },
      7458, "straight-flush",
    },
    { { cardlib::CCard("ac"), cardlib::CCard("7c"),
        cardlib::CCard("9c"), cardlib::CCard("6c"),
        cardlib::CCard("5c"), cardlib::CCard("8c"),
        cardlib::CCard("th"),
      },
      7457, "straight-flush",
    },
    { { cardlib::CCard("4s"), cardlib::CCard("7s"),
        cardlib::CCard("2h"), cardlib::CCard("6s"),
        cardlib::CCard("5s"), cardlib::CCard("8s"),
        cardlib::CCard("9d"),
      },
      7456, "straight-flush",
    },
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("6h"), cardlib::CCard("5h"),
        cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("8d"),
      },
      7455, "straight-flush",
    },
    { { cardlib::CCard("6d"), cardlib::CCard("5d"),
        cardlib::CCard("9c"), cardlib::CCard("2d"),
        cardlib::CCard("qh"), cardlib::CCard("3d"),
        cardlib::CCard("4d"),
      },
      7454, "straight-flush",
    },
    { { cardlib::CCard("ac"), cardlib::CCard("7c"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("3c"), cardlib::CCard("6d"),
        cardlib::CCard("4c"),
      },
      7453, "straight-flush",
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector, card_deck;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      flushes_signature_vrc_map;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      nonflushes_signature_vrc_map;
  cardlib::CCard::kcard_suit flush_suit;
  cardlib::CFastCalc::value_rank_count this_vrc = { 0L, 0L, 0L };
  int64_t result_rank, shouldbe_rank;
  int64_t best_prime_signature;
  std::string result_string, shouldbe_string;
  std::string sub_name = {
    static_cast<std::string>("TEvalTest::")
    + static_cast<std::string>(
        "TestBestFiveCardsOfSevenStraightFlushes1:") };
  std::string err_start, error_message;

  eval_obj.InitDeck(&card_deck);

  eval_obj.LoadHandMaps(&flushes_signature_vrc_map,
                        &nonflushes_signature_vrc_map);

  test_label_index = 0;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;
    shouldbe_rank = va_cit->rank;
    shouldbe_string = va_cit->hand_name;

    flush_suit = flushf_obj.GetFlushSuit(&card_vector);

    eval_obj.BestFiveCardsOfSeven(
        &card_vector, flush_suit,
        &flushes_signature_vrc_map,
        &nonflushes_signature_vrc_map,
        &best_prime_signature,
        &this_vrc);

    result_rank = this_vrc.rank;
    result_string = hand_obj.HandName(this_vrc.value);

    err_start = sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), card vector = ")
        + card_obj.CardVectorToString(&card_vector);

    AssertBestIntString(
        err_start,
        shouldbe_rank, result_rank,
        shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// 4-of-a-kind
void TEvalTest::TestBestFiveCardsOfSevenQuads1() {
  cardlib::CEval eval_obj;
  mylib::CUtils utils_obj;
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    std::string hand_name;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("9h"), cardlib::CCard("as"),
        cardlib::CCard("ac"), cardlib::CCard("ad"),
        cardlib::CCard("ah"),
      },
      "four-of-a-kind",
    },
    { { cardlib::CCard("6d"), cardlib::CCard("5d"),
        cardlib::CCard("ks"), cardlib::CCard("kc"),
        cardlib::CCard("kh"), cardlib::CCard("kd"),
        cardlib::CCard("td"),
      },
      "four-of-a-kind",
    },
    { { cardlib::CCard("qc"), cardlib::CCard("qd"),
        cardlib::CCard("qs"), cardlib::CCard("qh"),
        cardlib::CCard("5c"), cardlib::CCard("8c"),
        cardlib::CCard("tc"),
      },
      "four-of-a-kind",
    },
    { { cardlib::CCard("ac"), cardlib::CCard("jc"),
        cardlib::CCard("js"), cardlib::CCard("jh"),
        cardlib::CCard("jd"), cardlib::CCard("8c"),
        cardlib::CCard("tc"),
      },
      "four-of-a-kind",
    },
    { { cardlib::CCard("tc"), cardlib::CCard("7c"),
        cardlib::CCard("td"), cardlib::CCard("6c"),
        cardlib::CCard("th"), cardlib::CCard("8c"),
        cardlib::CCard("ts"),
      },
      "four-of-a-kind",
    },
    { { cardlib::CCard("4s"), cardlib::CCard("9s"),
        cardlib::CCard("9h"), cardlib::CCard("6s"),
        cardlib::CCard("9d"), cardlib::CCard("8s"),
        cardlib::CCard("9c"),
      },
      "four-of-a-kind",
    },
    { { cardlib::CCard("8h"), cardlib::CCard("8d"),
        cardlib::CCard("6h"), cardlib::CCard("8c"),
        cardlib::CCard("3h"), cardlib::CCard("4h"),
        cardlib::CCard("8s"),
      },
      "four-of-a-kind",
    },
    { { cardlib::CCard("6d"), cardlib::CCard("7s"),
        cardlib::CCard("7c"), cardlib::CCard("2d"),
        cardlib::CCard("7h"), cardlib::CCard("7d"),
        cardlib::CCard("4d"),
      },
      "four-of-a-kind",
    },
    { { cardlib::CCard("6c"), cardlib::CCard("7c"),
        cardlib::CCard("6d"), cardlib::CCard("6h"),
        cardlib::CCard("3c"), cardlib::CCard("5d"),
        cardlib::CCard("6s"),
      },
      "four-of-a-kind",
    },
    { { cardlib::CCard("5c"), cardlib::CCard("5s"),
        cardlib::CCard("5d"), cardlib::CCard("6h"),
        cardlib::CCard("3c"), cardlib::CCard("5h"),
        cardlib::CCard("6s"),
      },
      "four-of-a-kind",
    },
    { { cardlib::CCard("6c"), cardlib::CCard("7c"),
        cardlib::CCard("6d"), cardlib::CCard("4h"),
        cardlib::CCard("4c"), cardlib::CCard("4d"),
        cardlib::CCard("4s"),
      },
      "four-of-a-kind", },
    { { cardlib::CCard("3c"), cardlib::CCard("3d"),
        cardlib::CCard("6d"), cardlib::CCard("3h"),
        cardlib::CCard("3s"), cardlib::CCard("5d"),
        cardlib::CCard("6s"),
      },
      "four-of-a-kind",
    },
    { { cardlib::CCard("6c"), cardlib::CCard("2c"),
        cardlib::CCard("2d"), cardlib::CCard("2h"),
        cardlib::CCard("2s"), cardlib::CCard("5d"),
        cardlib::CCard("6s"),
      },
      "four-of-a-kind",
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector, card_deck;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      flushes_signature_vrc_map;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      nonflushes_signature_vrc_map;
  cardlib::CCard::kcard_suit flush_suit;
  cardlib::CFastCalc::value_rank_count this_vrc;
  int64_t best_prime_signature;
  std::string result_string, shouldbe_string;
  std::string sub_name = {
    static_cast<std::string>("TEvalTest::")
    + static_cast<std::string>(
        "TestBestFiveCardsOfSevenQuads1:") };
  std::string err_start;

  eval_obj.InitDeck(&card_deck);

  eval_obj.LoadHandMaps(&flushes_signature_vrc_map,
                        &nonflushes_signature_vrc_map);

  test_label_index = 0;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;
    shouldbe_string = va_cit->hand_name;

    flush_suit = flushf_obj.GetFlushSuit(&card_vector);

    eval_obj.BestFiveCardsOfSeven(
        &card_vector, flush_suit,
        &flushes_signature_vrc_map,
        &nonflushes_signature_vrc_map,
        &best_prime_signature,
        &this_vrc);

    result_string = hand_obj.HandName(this_vrc.value);

    err_start = sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), card vector = ")
        + card_obj.CardVectorToString(&card_vector);

    AssertBestString(
        err_start,
        shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// full house
void TEvalTest::TestBestFiveCardsOfSevenFullHouse1() {
  cardlib::CEval eval_obj;
  mylib::CUtils utils_obj;
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    std::string hand_name;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("kd"), cardlib::CCard("kh"),
        cardlib::CCard("9h"), cardlib::CCard("as"),
        cardlib::CCard("ac"), cardlib::CCard("ad"),
        cardlib::CCard("2h"),
      },
      "full-house",
    },
    { { cardlib::CCard("th"), cardlib::CCard("5d"),
        cardlib::CCard("ks"), cardlib::CCard("kc"),
        cardlib::CCard("qh"), cardlib::CCard("kd"),
        cardlib::CCard("td"),
      },
      "full-house",
    },
    { { cardlib::CCard("qc"), cardlib::CCard("qd"),
        cardlib::CCard("qs"), cardlib::CCard("5h"),
        cardlib::CCard("5c"), cardlib::CCard("8c"),
        cardlib::CCard("tc"),
      },
      "full-house",
    },
    { { cardlib::CCard("ac"), cardlib::CCard("jc"),
        cardlib::CCard("js"), cardlib::CCard("ah"),
        cardlib::CCard("jd"), cardlib::CCard("8c"),
        cardlib::CCard("tc"),
      },
      "full-house",
    },
    { { cardlib::CCard("tc"), cardlib::CCard("7c"),
        cardlib::CCard("td"), cardlib::CCard("6c"),
        cardlib::CCard("7h"), cardlib::CCard("8c"),
        cardlib::CCard("ts"),
      },
      "full-house",
    },
    { { cardlib::CCard("4s"), cardlib::CCard("as"),
        cardlib::CCard("9h"), cardlib::CCard("6s"),
        cardlib::CCard("9d"), cardlib::CCard("ad"),
        cardlib::CCard("9c"),
      },
      "full-house",
    },
    { { cardlib::CCard("8h"), cardlib::CCard("kd"),
        cardlib::CCard("6h"), cardlib::CCard("8c"),
        cardlib::CCard("kh"), cardlib::CCard("4h"),
        cardlib::CCard("8s"),
      },
      "full-house",
    },
    { { cardlib::CCard("6d"), cardlib::CCard("7s"),
        cardlib::CCard("7c"), cardlib::CCard("4h"),
        cardlib::CCard("6h"), cardlib::CCard("7d"),
        cardlib::CCard("4d"),
      },
      "full-house",
    },
    { { cardlib::CCard("6c"), cardlib::CCard("7c"),
        cardlib::CCard("6d"), cardlib::CCard("6h"),
        cardlib::CCard("5c"), cardlib::CCard("5d"),
        cardlib::CCard("7s"),
      },
      "full-house",
    },
    { { cardlib::CCard("5c"), cardlib::CCard("5s"),
        cardlib::CCard("5d"), cardlib::CCard("6h"),
        cardlib::CCard("3c"), cardlib::CCard("6d"),
        cardlib::CCard("6s"),
      },
      "full-house",
    },
    { { cardlib::CCard("6c"), cardlib::CCard("7c"),
        cardlib::CCard("6d"), cardlib::CCard("4h"),
        cardlib::CCard("4c"), cardlib::CCard("4d"),
        cardlib::CCard("7s"),
      },
      "full-house",
    },
    { { cardlib::CCard("3c"), cardlib::CCard("3d"),
        cardlib::CCard("2d"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("2h"),
        cardlib::CCard("2s"),
      },
      "full-house", },
    { { cardlib::CCard("6c"), cardlib::CCard("2c"),
        cardlib::CCard("2d"), cardlib::CCard("2h"),
        cardlib::CCard("5s"), cardlib::CCard("5d"),
        cardlib::CCard("6s"),
      },
      "full-house",
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector, card_deck;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      flushes_signature_vrc_map;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      nonflushes_signature_vrc_map;
  cardlib::CCard::kcard_suit flush_suit;
  cardlib::CFastCalc::value_rank_count this_vrc;
  int64_t best_prime_signature;
  std::string result_string, shouldbe_string;
  std::string sub_name = {
    static_cast<std::string>("TEvalTest::")
    + static_cast<std::string>(
        "TestBestFiveCardsOfSevenFullHouse1:") };
  std::string err_start;

  eval_obj.InitDeck(&card_deck);

  eval_obj.LoadHandMaps(&flushes_signature_vrc_map,
                        &nonflushes_signature_vrc_map);

  test_label_index = 0;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;
    shouldbe_string = va_cit->hand_name;

    flush_suit = flushf_obj.GetFlushSuit(&card_vector);

    eval_obj.BestFiveCardsOfSeven(
        &card_vector, flush_suit,
        &flushes_signature_vrc_map,
        &nonflushes_signature_vrc_map,
        &best_prime_signature,
        &this_vrc);

    result_string = hand_obj.HandName(this_vrc.value);

    err_start = sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), card vector = ")
        + card_obj.CardVectorToString(&card_vector);

    AssertBestString(
        err_start,
        shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// flush
void TEvalTest::TestBestFiveCardsOfSevenFlush1() {
  cardlib::CEval eval_obj;
  mylib::CUtils utils_obj;
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    std::string hand_name;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("kd"), cardlib::CCard("jd"),
        cardlib::CCard("9d"), cardlib::CCard("as"),
        cardlib::CCard("ac"), cardlib::CCard("ad"),
        cardlib::CCard("7d"),
      },
      "flush",
    },
    { { cardlib::CCard("ts"), cardlib::CCard("5d"),
        cardlib::CCard("ks"), cardlib::CCard("kc"),
        cardlib::CCard("qs"), cardlib::CCard("2s"),
        cardlib::CCard("4s"),
      },
      "flush",
    },
    { { cardlib::CCard("qc"), cardlib::CCard("qd"),
        cardlib::CCard("jc"), cardlib::CCard("5h"),
        cardlib::CCard("3c"), cardlib::CCard("8c"),
        cardlib::CCard("tc"),
      },
      "flush",
    },
    { { cardlib::CCard("ac"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("5h"),
        cardlib::CCard("3h"), cardlib::CCard("2h"),
        cardlib::CCard("7h"),
      },
      "flush",
    },
    { { cardlib::CCard("td"), cardlib::CCard("7d"),
        cardlib::CCard("8d"), cardlib::CCard("5d"),
        cardlib::CCard("3d"), cardlib::CCard("2d"),
        cardlib::CCard("4d"),
      },
      "flush",
    },
    { { cardlib::CCard("4s"), cardlib::CCard("9s"),
        cardlib::CCard("9h"), cardlib::CCard("8s"),
        cardlib::CCard("jd"), cardlib::CCard("5s"),
        cardlib::CCard("2s"),
      },
      "flush",
    },
    { { cardlib::CCard("8c"), cardlib::CCard("kd"),
        cardlib::CCard("7c"), cardlib::CCard("6c"),
        cardlib::CCard("4c"), cardlib::CCard("4h"),
        cardlib::CCard("2c"),
      },
      "flush",
    },
    { { cardlib::CCard("7d"), cardlib::CCard("7s"),
        cardlib::CCard("6d"), cardlib::CCard("4h"),
        cardlib::CCard("5d"), cardlib::CCard("3d"),
        cardlib::CCard("2d"),
      },
      "flush",
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector, card_deck;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      flushes_signature_vrc_map;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      nonflushes_signature_vrc_map;
  cardlib::CCard::kcard_suit flush_suit;
  cardlib::CFastCalc::value_rank_count this_vrc;
  int64_t best_prime_signature;
  std::string result_string, shouldbe_string;
  std::string sub_name = {
    static_cast<std::string>("TEvalTest::")
    + static_cast<std::string>(
        "TestBestFiveCardsOfSevenFlush1:") };
  std::string err_start;

  eval_obj.InitDeck(&card_deck);

  eval_obj.LoadHandMaps(&flushes_signature_vrc_map,
                        &nonflushes_signature_vrc_map);

  test_label_index = 0;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;
    shouldbe_string = va_cit->hand_name;

    flush_suit = flushf_obj.GetFlushSuit(&card_vector);

    eval_obj.BestFiveCardsOfSeven(
        &card_vector, flush_suit,
        &flushes_signature_vrc_map,
        &nonflushes_signature_vrc_map,
        &best_prime_signature,
        &this_vrc);

    result_string = hand_obj.HandName(this_vrc.value);

    err_start = sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), card vector = ")
        + card_obj.CardVectorToString(&card_vector);

    AssertBestString(
        err_start,
        shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// straight
void TEvalTest::TestBestFiveCardsOfSevenStraight1() {
  cardlib::CEval eval_obj;
  mylib::CUtils utils_obj;
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    std::string hand_name;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("kd"), cardlib::CCard("jh"),
        cardlib::CCard("9d"), cardlib::CCard("as"),
        cardlib::CCard("tc"), cardlib::CCard("ad"),
        cardlib::CCard("qd"),
      },
      "straight",
    },
    { { cardlib::CCard("9h"), cardlib::CCard("5d"),
        cardlib::CCard("ks"), cardlib::CCard("kc"),
        cardlib::CCard("qs"), cardlib::CCard("jd"),
        cardlib::CCard("tc"),
      },
      "straight",
    },
    { { cardlib::CCard("qc"), cardlib::CCard("7d"),
        cardlib::CCard("jc"), cardlib::CCard("th"),
        cardlib::CCard("3c"), cardlib::CCard("8c"),
        cardlib::CCard("9d"),
      },
      "straight",
    },
    { { cardlib::CCard("7c"), cardlib::CCard("jh"),
        cardlib::CCard("tc"), cardlib::CCard("5h"),
        cardlib::CCard("9h"), cardlib::CCard("8s"),
        cardlib::CCard("5d"),
      },
      "straight",
    },
    { { cardlib::CCard("td"), cardlib::CCard("7s"),
        cardlib::CCard("8d"), cardlib::CCard("5d"),
        cardlib::CCard("3d"), cardlib::CCard("6h"),
        cardlib::CCard("9c"),
      },
      "straight",
    },
    { { cardlib::CCard("5s"), cardlib::CCard("9s"),
        cardlib::CCard("jh"), cardlib::CCard("8s"),
        cardlib::CCard("7d"), cardlib::CCard("6s"),
        cardlib::CCard("2c"),
      },
      "straight" },
    { { cardlib::CCard("8d"), cardlib::CCard("kd"),
        cardlib::CCard("7c"), cardlib::CCard("6c"),
        cardlib::CCard("4h"), cardlib::CCard("5h"),
        cardlib::CCard("2c"),
      },
      "straight",
    },
    { { cardlib::CCard("td"), cardlib::CCard("7s"),
        cardlib::CCard("6d"), cardlib::CCard("4h"),
        cardlib::CCard("5d"), cardlib::CCard("3h"),
        cardlib::CCard("2c"),
      },
      "straight",
    },
    { { cardlib::CCard("td"), cardlib::CCard("as"),
        cardlib::CCard("6d"), cardlib::CCard("4h"),
        cardlib::CCard("5d"), cardlib::CCard("3h"),
        cardlib::CCard("2c"),
      },
      "straight",
    },
    { { cardlib::CCard("ad"), cardlib::CCard("7s"),
        cardlib::CCard("2d"), cardlib::CCard("4h"),
        cardlib::CCard("5d"), cardlib::CCard("3d"),
        cardlib::CCard("2c"),
      },
      "straight",
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector, card_deck;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      flushes_signature_vrc_map;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      nonflushes_signature_vrc_map;
  cardlib::CCard::kcard_suit flush_suit;
  cardlib::CFastCalc::value_rank_count this_vrc;
  int64_t best_prime_signature;
  std::string result_string, shouldbe_string;
  std::string sub_name = {
    static_cast<std::string>("TEvalTest::")
    + static_cast<std::string>(
        "TestBestFiveCardsOfSevenStraight1:") };
  std::string err_start;

  eval_obj.InitDeck(&card_deck);

  eval_obj.LoadHandMaps(&flushes_signature_vrc_map,
                        &nonflushes_signature_vrc_map);

  test_label_index = 0;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;
    shouldbe_string = va_cit->hand_name;

    flush_suit = flushf_obj.GetFlushSuit(&card_vector);

    eval_obj.BestFiveCardsOfSeven(
        &card_vector, flush_suit,
        &flushes_signature_vrc_map,
        &nonflushes_signature_vrc_map,
        &best_prime_signature,
        &this_vrc);

    result_string = hand_obj.HandName(this_vrc.value);

    err_start = sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), card vector = ")
        + card_obj.CardVectorToString(&card_vector);

    AssertBestString(
        err_start,
        shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// 3-of-a-kind
void TEvalTest::TestBestFiveCardsOfSevenTriples1() {
  cardlib::CEval eval_obj;
  mylib::CUtils utils_obj;
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    std::string hand_name;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("kd"), cardlib::CCard("ac"),
        cardlib::CCard("9d"), cardlib::CCard("as"),
        cardlib::CCard("tc"), cardlib::CCard("ad"),
        cardlib::CCard("2d"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("9h"), cardlib::CCard("5d"),
        cardlib::CCard("ks"), cardlib::CCard("kc"),
        cardlib::CCard("qs"), cardlib::CCard("2d"),
        cardlib::CCard("kh"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("qc"), cardlib::CCard("7d"),
        cardlib::CCard("qd"), cardlib::CCard("th"),
        cardlib::CCard("qs"), cardlib::CCard("8c"),
        cardlib::CCard("5d"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("jc"), cardlib::CCard("jh"),
        cardlib::CCard("tc"), cardlib::CCard("4h"),
        cardlib::CCard("3d"), cardlib::CCard("8s"),
        cardlib::CCard("jd"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("7s"),
        cardlib::CCard("8d"), cardlib::CCard("5d"),
        cardlib::CCard("tc"), cardlib::CCard("th"),
        cardlib::CCard("9c"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("5s"), cardlib::CCard("9s"),
        cardlib::CCard("jh"), cardlib::CCard("8s"),
        cardlib::CCard("9d"), cardlib::CCard("9h"),
        cardlib::CCard("2c"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("8d"), cardlib::CCard("8h"),
        cardlib::CCard("8c"), cardlib::CCard("6c"),
        cardlib::CCard("4h"), cardlib::CCard("th"),
        cardlib::CCard("2c"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("td"), cardlib::CCard("js"),
        cardlib::CCard("6d"), cardlib::CCard("4h"),
        cardlib::CCard("7d"), cardlib::CCard("7h"),
        cardlib::CCard("7c"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("6h"), cardlib::CCard("as"),
        cardlib::CCard("6d"), cardlib::CCard("4h"),
        cardlib::CCard("5d"), cardlib::CCard("6c"),
        cardlib::CCard("td"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("ad"), cardlib::CCard("5s"),
        cardlib::CCard("5c"), cardlib::CCard("4h"),
        cardlib::CCard("5d"), cardlib::CCard("3h"),
        cardlib::CCard("kc"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("4d"), cardlib::CCard("5s"),
        cardlib::CCard("4c"), cardlib::CCard("4h"),
        cardlib::CCard("9d"), cardlib::CCard("3h"),
        cardlib::CCard("qc"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("ad"), cardlib::CCard("5s"),
        cardlib::CCard("3c"), cardlib::CCard("4h"),
        cardlib::CCard("3d"), cardlib::CCard("3h"),
        cardlib::CCard("jc"),
      },
      "three-of-a-kind",
    },
    { { cardlib::CCard("2d"), cardlib::CCard("2s"),
        cardlib::CCard("5c"), cardlib::CCard("jh"),
        cardlib::CCard("qd"), cardlib::CCard("3h"),
        cardlib::CCard("2c"),
      },
      "three-of-a-kind",
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector, card_deck;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      flushes_signature_vrc_map;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      nonflushes_signature_vrc_map;
  cardlib::CCard::kcard_suit flush_suit;
  cardlib::CFastCalc::value_rank_count this_vrc;
  int64_t best_prime_signature;
  std::string result_string, shouldbe_string;
  std::string sub_name = {
    static_cast<std::string>("TEvalTest::")
    + static_cast<std::string>(
        "TestBestFiveCardsOfSevenTriples1:") };
  std::string err_start;

  eval_obj.InitDeck(&card_deck);

  eval_obj.LoadHandMaps(&flushes_signature_vrc_map,
                        &nonflushes_signature_vrc_map);

  test_label_index = 0;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;
    shouldbe_string = va_cit->hand_name;

    flush_suit = flushf_obj.GetFlushSuit(&card_vector);

    eval_obj.BestFiveCardsOfSeven(
        &card_vector, flush_suit,
        &flushes_signature_vrc_map,
        &nonflushes_signature_vrc_map,
        &best_prime_signature,
        &this_vrc);

    result_string = hand_obj.HandName(this_vrc.value);

    err_start = sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), card vector = ")
        + card_obj.CardVectorToString(&card_vector);

    AssertBestString(
        err_start,
        shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// two pair
void TEvalTest::TestBestFiveCardsOfSevenTwoPair1() {
  cardlib::CEval eval_obj;
  mylib::CUtils utils_obj;
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    std::string hand_name;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("kd"), cardlib::CCard("ac"),
        cardlib::CCard("9d"), cardlib::CCard("as"),
        cardlib::CCard("tc"), cardlib::CCard("kc"),
        cardlib::CCard("2d")
      },
      "two-pair",
    },
    { { cardlib::CCard("9h"), cardlib::CCard("5d"),
        cardlib::CCard("ks"), cardlib::CCard("kc"),
        cardlib::CCard("qs"), cardlib::CCard("qd"),
        cardlib::CCard("2h"),
      },
      "two-pair",
    },
    { { cardlib::CCard("qc"), cardlib::CCard("7d"),
        cardlib::CCard("qd"), cardlib::CCard("th"),
        cardlib::CCard("7s"), cardlib::CCard("8c"),
        cardlib::CCard("5d"),
      },
      "two-pair",
    },
    { { cardlib::CCard("jc"), cardlib::CCard("jh"),
        cardlib::CCard("tc"), cardlib::CCard("4h"),
        cardlib::CCard("3d"), cardlib::CCard("8s"),
        cardlib::CCard("3h"),
      },
      "two-pair",
    },
    { { cardlib::CCard("9d"), cardlib::CCard("7s"),
        cardlib::CCard("ad"), cardlib::CCard("5d"),
        cardlib::CCard("tc"), cardlib::CCard("th"),
        cardlib::CCard("9c"),
      },
      "two-pair",
    },
    { { cardlib::CCard("5s"), cardlib::CCard("9s"),
        cardlib::CCard("jh"), cardlib::CCard("8s"),
        cardlib::CCard("2d"), cardlib::CCard("9h"),
        cardlib::CCard("2c"),
      },
      "two-pair",
    },
    { { cardlib::CCard("8d"), cardlib::CCard("8h"),
        cardlib::CCard("6h"), cardlib::CCard("6c"),
        cardlib::CCard("4h"), cardlib::CCard("ts"),
        cardlib::CCard("2c"),
      },
      "two-pair",
    },
    { { cardlib::CCard("td"), cardlib::CCard("js"),
        cardlib::CCard("6d"), cardlib::CCard("4h"),
        cardlib::CCard("7d"), cardlib::CCard("7h"),
        cardlib::CCard("6c"),
      },
      "two-pair",
    },
    { { cardlib::CCard("6h"), cardlib::CCard("as"),
        cardlib::CCard("6d"), cardlib::CCard("5h"),
        cardlib::CCard("5d"), cardlib::CCard("kc"),
        cardlib::CCard("td"),
      },
      "two-pair",
    },
    { { cardlib::CCard("kd"), cardlib::CCard("5s"),
        cardlib::CCard("5c"), cardlib::CCard("4h"),
        cardlib::CCard("2d"), cardlib::CCard("3h"),
        cardlib::CCard("2c"),
      },
      "two-pair",
    },
    { { cardlib::CCard("4d"), cardlib::CCard("5s"),
        cardlib::CCard("3c"), cardlib::CCard("4h"),
        cardlib::CCard("9d"), cardlib::CCard("3h"),
        cardlib::CCard("qc"),
      },
      "two-pair",
    },
    { { cardlib::CCard("ad"), cardlib::CCard("3s"),
        cardlib::CCard("3c"), cardlib::CCard("4h"),
        cardlib::CCard("2d"), cardlib::CCard("2h"),
        cardlib::CCard("jc"),
      },
      "two-pair",
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector, card_deck;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      flushes_signature_vrc_map;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      nonflushes_signature_vrc_map;
  cardlib::CCard::kcard_suit flush_suit;
  cardlib::CFastCalc::value_rank_count this_vrc;
  int64_t best_prime_signature;
  std::string result_string, shouldbe_string;
  std::string sub_name = {
    static_cast<std::string>("TEvalTest::")
    + static_cast<std::string>(
        "TestBestFiveCardsOfSevenTwoPair1:") };
  std::string err_start;

  eval_obj.InitDeck(&card_deck);

  eval_obj.LoadHandMaps(&flushes_signature_vrc_map,
                        &nonflushes_signature_vrc_map);

  test_label_index = 0;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;
    shouldbe_string = va_cit->hand_name;

    flush_suit = flushf_obj.GetFlushSuit(&card_vector);

    eval_obj.BestFiveCardsOfSeven(
        &card_vector, flush_suit,
        &flushes_signature_vrc_map,
        &nonflushes_signature_vrc_map,
        &best_prime_signature,
        &this_vrc);

    result_string = hand_obj.HandName(this_vrc.value);

    err_start = sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), card vector = ")
        + card_obj.CardVectorToString(&card_vector);

    AssertBestString(
        err_start,
        shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// one pair
void TEvalTest::TestBestFiveCardsOfSevenOnePair1() {
  cardlib::CEval eval_obj;
  mylib::CUtils utils_obj;
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    std::string hand_name;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("3c"), cardlib::CCard("ac"),
        cardlib::CCard("9d"), cardlib::CCard("as"),
        cardlib::CCard("tc"), cardlib::CCard("kd"),
        cardlib::CCard("2d"),
      },
      "one-pair",
    },
    { { cardlib::CCard("9h"), cardlib::CCard("5d"),
        cardlib::CCard("ks"), cardlib::CCard("kc"),
        cardlib::CCard("3s"), cardlib::CCard("qd"),
        cardlib::CCard("2h"),
      },
      "one-pair",
    },
    { { cardlib::CCard("qc"), cardlib::CCard("7d"),
        cardlib::CCard("qd"), cardlib::CCard("th"),
        cardlib::CCard("2s"), cardlib::CCard("8c"),
        cardlib::CCard("5d"),
      },
      "one-pair",
    },
    { { cardlib::CCard("jc"), cardlib::CCard("jh"),
        cardlib::CCard("tc"), cardlib::CCard("4h"),
        cardlib::CCard("7d"), cardlib::CCard("8s"),
        cardlib::CCard("3h"),
      },
      "one-pair",
    },
    { { cardlib::CCard("9d"), cardlib::CCard("7s"),
        cardlib::CCard("ad"), cardlib::CCard("5d"),
        cardlib::CCard("tc"), cardlib::CCard("th"),
        cardlib::CCard("2c"),
      },
      "one-pair",
    },
    { { cardlib::CCard("5s"), cardlib::CCard("9s"),
        cardlib::CCard("jh"), cardlib::CCard("8s"),
        cardlib::CCard("ad"), cardlib::CCard("9h"),
        cardlib::CCard("2c"),
      },
      "one-pair",
    },
    { { cardlib::CCard("8d"), cardlib::CCard("8h"),
        cardlib::CCard("6h"), cardlib::CCard("qc"),
        cardlib::CCard("4h"), cardlib::CCard("ts"),
        cardlib::CCard("2c"),
      },
      "one-pair",
    },
    { { cardlib::CCard("td"), cardlib::CCard("js"),
        cardlib::CCard("kd"), cardlib::CCard("4h"),
        cardlib::CCard("7d"), cardlib::CCard("7h"),
        cardlib::CCard("6c"),
      },
      "one-pair",
    },
    { { cardlib::CCard("6h"), cardlib::CCard("as"),
        cardlib::CCard("6d"), cardlib::CCard("3h"),
        cardlib::CCard("5d"), cardlib::CCard("kc"),
        cardlib::CCard("td"),
      },
      "one-pair",
    },
    { { cardlib::CCard("ad"), cardlib::CCard("5s"),
        cardlib::CCard("5c"), cardlib::CCard("4h"),
        cardlib::CCard("kd"), cardlib::CCard("3h"),
        cardlib::CCard("tc"),
      },
      "one-pair",
    },
    { { cardlib::CCard("4d"), cardlib::CCard("5s"),
        cardlib::CCard("3c"), cardlib::CCard("4h"),
        cardlib::CCard("9d"), cardlib::CCard("jh"),
        cardlib::CCard("qc"),
      },
      "one-pair",
    },
    { { cardlib::CCard("ad"), cardlib::CCard("3s"),
        cardlib::CCard("3c"), cardlib::CCard("4h"),
        cardlib::CCard("qd"), cardlib::CCard("2h"),
        cardlib::CCard("jc"),
      },
      "one-pair",
    },
    { { cardlib::CCard("ad"), cardlib::CCard("2s"),
        cardlib::CCard("3c"), cardlib::CCard("8h"),
        cardlib::CCard("qd"), cardlib::CCard("2h"),
        cardlib::CCard("jc"),
      },
      "one-pair",
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector, card_deck;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      flushes_signature_vrc_map;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      nonflushes_signature_vrc_map;
  cardlib::CCard::kcard_suit flush_suit;
  cardlib::CFastCalc::value_rank_count this_vrc;
  int64_t best_prime_signature;
  std::string result_string, shouldbe_string;
  std::string sub_name = {
    static_cast<std::string>("TEvalTest::")
    + static_cast<std::string>(
        "TestBestFiveCardsOfSevenOnePair1:") };
  std::string err_start;

  eval_obj.InitDeck(&card_deck);

  eval_obj.LoadHandMaps(&flushes_signature_vrc_map,
                        &nonflushes_signature_vrc_map);

  test_label_index = 0;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;
    shouldbe_string = va_cit->hand_name;

    flush_suit = flushf_obj.GetFlushSuit(&card_vector);

    eval_obj.BestFiveCardsOfSeven(
        &card_vector, flush_suit,
        &flushes_signature_vrc_map,
        &nonflushes_signature_vrc_map,
        &best_prime_signature,
        &this_vrc);

    result_string = hand_obj.HandName(this_vrc.value);

    err_start = sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), card vector = ")
        + card_obj.CardVectorToString(&card_vector);

    AssertBestString(
        err_start,
        shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// one pair
void TEvalTest::TestBestFiveCardsOfSevenHighCard1() {
  cardlib::CEval eval_obj;
  mylib::CUtils utils_obj;
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    std::string hand_name;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("3c"), cardlib::CCard("ac"),
        cardlib::CCard("9d"), cardlib::CCard("2s"),
        cardlib::CCard("tc"), cardlib::CCard("kd"),
        cardlib::CCard("7d"),
      },
      "high-card",
    },
    { { cardlib::CCard("9h"), cardlib::CCard("5d"),
        cardlib::CCard("ks"), cardlib::CCard("tc"),
        cardlib::CCard("3s"), cardlib::CCard("qd"),
        cardlib::CCard("2h"),
      },
      "high-card",
    },
    { { cardlib::CCard("6h"), cardlib::CCard("5d"),
        cardlib::CCard("7s"), cardlib::CCard("tc"),
        cardlib::CCard("3s"), cardlib::CCard("9d"),
        cardlib::CCard("2h"),
      },
      "high-card",
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector, card_deck;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      flushes_signature_vrc_map;
  std::map<int64_t, cardlib::CFastCalc::value_rank_count>
      nonflushes_signature_vrc_map;
  cardlib::CCard::kcard_suit flush_suit;
  cardlib::CFastCalc::value_rank_count this_vrc;
  int64_t best_prime_signature;
  std::string result_string, shouldbe_string;
  std::string sub_name = {
    static_cast<std::string>("TEvalTest::")
    + static_cast<std::string>(
        "TestBestFiveCardsOfSevenHighCard1:") };
  std::string err_start;

  eval_obj.InitDeck(&card_deck);

  eval_obj.LoadHandMaps(&flushes_signature_vrc_map,
                        &nonflushes_signature_vrc_map);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;
    shouldbe_string = va_cit->hand_name;

    flush_suit = flushf_obj.GetFlushSuit(&card_vector);

    eval_obj.BestFiveCardsOfSeven(
        &card_vector, flush_suit,
        &flushes_signature_vrc_map,
        &nonflushes_signature_vrc_map,
        &best_prime_signature,
        &this_vrc);

    result_string = hand_obj.HandName(this_vrc.value);

    err_start = sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), card vector = ")
        + card_obj.CardVectorToString(&card_vector);

    AssertBestString(
        err_start,
        shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

}  // namespace cardtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#ifndef TESTS_EVAL_TEST_H_
#define TESTS_EVAL_TEST_H_

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TEvalTest definition                                  ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ###  last updated April 22, 2024                           ###
  ###    fixed updated cpplint.py errors                     ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 21, 2015                              ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cppunit/extensions/HelperMacros.h>

#include <cstdint>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include "sources/card.h"
#include "sources/eval.h"

namespace cardtest {

class TEvalTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(TEvalTest);

  CPPUNIT_TEST(TestBestFiveCardsOfSevenStraightFlushes1);
  CPPUNIT_TEST(TestBestFiveCardsOfSevenQuads1);
  CPPUNIT_TEST(TestBestFiveCardsOfSevenFullHouse1);
  CPPUNIT_TEST(TestBestFiveCardsOfSevenFlush1);
  CPPUNIT_TEST(TestBestFiveCardsOfSevenStraight1);
  CPPUNIT_TEST(TestBestFiveCardsOfSevenTriples1);
  CPPUNIT_TEST(TestBestFiveCardsOfSevenTwoPair1);
  CPPUNIT_TEST(TestBestFiveCardsOfSevenOnePair1);
  CPPUNIT_TEST(TestBestFiveCardsOfSevenHighCard1);

  CPPUNIT_TEST_SUITE_END();

 public:
  TEvalTest();
  ~TEvalTest();

  void setUp();
  void tearDown();

  // helper function
  void AssertBestIntString(
      const std::string &err_start,
      const int64_t ishouldbe,
      const int64_t iresult,
      const std::string &shouldbe_string,
      const std::string &result_string);

  void AssertBestString(
      const std::string &err_start,
      const std::string &shouldbe_string,
      const std::string &result_string);

  void TestBestFiveCardsOfSevenStraightFlushes1();
  void TestBestFiveCardsOfSevenQuads1();
  void TestBestFiveCardsOfSevenFullHouse1();
  void TestBestFiveCardsOfSevenFlush1();
  void TestBestFiveCardsOfSevenStraight1();
  void TestBestFiveCardsOfSevenTriples1();
  void TestBestFiveCardsOfSevenTwoPair1();
  void TestBestFiveCardsOfSevenOnePair1();
  void TestBestFiveCardsOfSevenHighCard1();

 private:
};  // class TEvalTest

}  // namespace cardtest

#endif  // TESTS_EVAL_TEST_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

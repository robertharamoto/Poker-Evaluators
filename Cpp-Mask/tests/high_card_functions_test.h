#ifndef TESTS_HIGH_CARD_FUNCTIONS_TEST_H_
#define TESTS_HIGH_CARD_FUNCTIONS_TEST_H_

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  THighCardFunctionsTest1 definition                    ###
  ###  namespace HandTest                                    ###
  ###                                                        ###
  ###  last updated April 15, 2024                           ###
  ###    new card mask definition                            ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 18, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cppunit/extensions/HelperMacros.h>

#include <algorithm>
#include <string>
#include <vector>

#include "sources/card.h"

namespace handtest {

class THighCardFunctionsTest1 : public CppUnit::TestFixture  {
  CPPUNIT_TEST_SUITE(THighCardFunctionsTest1);

  CPPUNIT_TEST(TestForm5CardHand1);
  CPPUNIT_TEST(TestForm7CardHand1);
  CPPUNIT_TEST(TestIsHighCard1);

  CPPUNIT_TEST(TestHighCardsGreaterHighCards1);
  CPPUNIT_TEST(TestHighCardsLessThanHighCards1);
  CPPUNIT_TEST(TestHighCardsEqualHighCards1);

  CPPUNIT_TEST_SUITE_END();

 public:
  THighCardFunctionsTest1();
  ~THighCardFunctionsTest1();

  void setUp();
  void tearDown();

  void TestForm5CardHand1();
  void TestForm7CardHand1();

  void TestIsHighCard1();

  void TestHighCardsGreaterHighCards1();
  void TestHighCardsLessThanHighCards1();
  void TestHighCardsEqualHighCards1();
};  // class THighCardFunctionsTest1

}  // namespace handtest

#endif  // TESTS_HIGH_CARD_FUNCTIONS_TEST_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

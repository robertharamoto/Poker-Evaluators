/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  THandTest implementation                              ###
  ###    namespace handtest                                  ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###    new bitmask definition for cards                    ###
  ###    fixed updated cpplint.py errors                     ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 18, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/hand_test.h"

#include <cstdint>

#include <string>
#include <vector>
#include <algorithm>
#include <cctype>       // std::tolower

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/utils.h"
#include "tests/assert_functions.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::THandTest);

namespace handtest {

// #############################################################
// #############################################################
// tests GetPrime5Signature and GetHandStraightMask
// for straight flushes
void THandTest::TestGetPrime5SignatureSF1() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand_obj;
  cardlib::CCard card_obj;
  handtest::CAssertFunctions af_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    int64_t prime_product;
    cardlib::CHand::kstraight_mask straight_mask;
  };
  std::vector<atest_struct> test_vector = {
    // royal flush
    { { cardlib::CCard("qh"), cardlib::CCard("kh"),
        cardlib::CCard("ah"), cardlib::CCard("jh"),
        cardlib::CCard("8h"), cardlib::CCard("th"),
        cardlib::CCard("js"),
      },
      cardlib::CHand::kstraight_signature::kAceHighSignature,
      cardlib::CHand::kstraight_mask::kAceHighStraight,
    },
    // king-high straight flush
    { { cardlib::CCard("qd"), cardlib::CCard("kd"),
        cardlib::CCard("9d"), cardlib::CCard("jd"),
        cardlib::CCard("8d"), cardlib::CCard("td"),
        cardlib::CCard("js"),
      },
      cardlib::CHand::kstraight_signature::kKingHighSignature,
      cardlib::CHand::kstraight_mask::kKingHighStraight,
    },
    // queen-high straight flush
    { { cardlib::CCard("qc"), cardlib::CCard("7c"),
        cardlib::CCard("9c"), cardlib::CCard("jc"),
        cardlib::CCard("8c"), cardlib::CCard("tc"),
        cardlib::CCard("js"),
      },
      cardlib::CHand::kstraight_signature::kQueenHighSignature,
      cardlib::CHand::kstraight_mask::kQueenHighStraight,
    },
    // jack-high straight flush
    { { cardlib::CCard("7s"), cardlib::CCard("6s"),
        cardlib::CCard("9s"), cardlib::CCard("js"),
        cardlib::CCard("8s"), cardlib::CCard("ts"),
        cardlib::CCard("jh"),
      },
      cardlib::CHand::kstraight_signature::kJackHighSignature,
      cardlib::CHand::kstraight_mask::kJackHighStraight},
    // ten-high straight flush
    { { cardlib::CCard("6h"), cardlib::CCard("7h"),
        cardlib::CCard("8h"), cardlib::CCard("9h"),
        cardlib::CCard("th"), cardlib::CCard("ah"),
        cardlib::CCard("js"),
      },
      cardlib::CHand::kstraight_signature::kTenHighSignature,
      cardlib::CHand::kstraight_mask::kTenHighStraight,
    },
    // nine-high straight flush
    { { cardlib::CCard("6d"), cardlib::CCard("7d"),
        cardlib::CCard("8d"), cardlib::CCard("9d"),
        cardlib::CCard("jd"), cardlib::CCard("5d"),
        cardlib::CCard("js"),
      },
      cardlib::CHand::kstraight_signature::kNineHighSignature,
      cardlib::CHand::kstraight_mask::kNineHighStraight,
    },
    // eight-high straight flush
    { { cardlib::CCard("4c"), cardlib::CCard("5c"),
        cardlib::CCard("6c"), cardlib::CCard("7c"),
        cardlib::CCard("8c"), cardlib::CCard("tc"),
        cardlib::CCard("js"),
      },
      cardlib::CHand::kstraight_signature::kEightHighSignature,
      cardlib::CHand::kstraight_mask::kEightHighStraight,
    },
    // seven-high straight flush
    { { cardlib::CCard("5s"), cardlib::CCard("6s"),
        cardlib::CCard("3s"), cardlib::CCard("4s"),
        cardlib::CCard("7s"), cardlib::CCard("ts"),
        cardlib::CCard("jd"),
      },
      cardlib::CHand::kstraight_signature::kSevenHighSignature,
      cardlib::CHand::kstraight_mask::kSevenHighStraight,
    },
    // six-high straight flush
    { { cardlib::CCard("4h"), cardlib::CCard("3h"),
        cardlib::CCard("9h"), cardlib::CCard("5h"),
        cardlib::CCard("2h"), cardlib::CCard("6h"),
        cardlib::CCard("js"),
      },
      cardlib::CHand::kstraight_signature::kSixHighSignature,
      cardlib::CHand::kstraight_mask::kSixHighStraight,
    },
    // five-high straight flush
    { { cardlib::CCard("ad"), cardlib::CCard("kd"),
        cardlib::CCard("3d"), cardlib::CCard("4d"),
        cardlib::CCard("2d"), cardlib::CCard("5d"),
        cardlib::CCard("js"), },
      cardlib::CHand::kstraight_signature::kFiveHighSignature,
      cardlib::CHand::kstraight_mask::kFiveHighStraight,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector;
  int64_t shouldbe_prime_product, result_prime_product;
  int64_t shouldbe_straight_mask, result_straight_mask;
  std::string sub_name = {
    static_cast<std::string>("THandTest(5)::")
    + static_cast<std::string>("TestGetPrime5SignatureSF1:") };
  std::string err_start;
  std::string card_vector_string, hand_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;
    shouldbe_prime_product = va_cit->prime_product;
    shouldbe_straight_mask = va_cit->straight_mask;

    hand_obj.Clear();
    hand_obj.Calculate(&card_vector);
    hand_string = hand_obj.GetCardsString();

    result_prime_product = hand_obj.GetPrime5Signature();
    result_straight_mask = hand_obj.GetHandStraightMask();

    card_vector_string = card_obj.CardVectorToString(&card_vector);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + card_vector_string
        + static_cast<std::string>(", ")
        + hand_string
        + static_cast<std::string>(" : ");

    af_obj.AssertPrimesHelper(&err_start,
                              shouldbe_prime_product,
                              result_prime_product,
                              shouldbe_straight_mask,
                              result_straight_mask);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// tests GetPrime5Signature and GetHandStraightMask
// for straight flushes
void THandTest::TestGetPrime5SignatureStraights1() {
    mylib::CUtils utils_obj;
  cardlib::CHand hand_obj;
  cardlib::CCard card_obj;
  handtest::CAssertFunctions af_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    int64_t prime_product;
    cardlib::CHand::kstraight_mask straight_mask;
  };
  std::vector<atest_struct> test_vector = {
    // ace-high straight
    { { cardlib::CCard("qh"), cardlib::CCard("kc"),
        cardlib::CCard("ad"), cardlib::CCard("js"),
        cardlib::CCard("8h"), cardlib::CCard("tc"),
        cardlib::CCard("jd"),
      },
      cardlib::CHand::kstraight_signature::kAceHighSignature,
      cardlib::CHand::kstraight_mask::kAceHighStraight,
    },
    // king-high straight
    { { cardlib::CCard("qd"), cardlib::CCard("kc"),
        cardlib::CCard("9s"), cardlib::CCard("jh"),
        cardlib::CCard("8d"), cardlib::CCard("tc"),
        cardlib::CCard("js"), },
      cardlib::CHand::kstraight_signature::kKingHighSignature,
      cardlib::CHand::kstraight_mask::kKingHighStraight,
    },
    // queen-high straight
    { { cardlib::CCard("qc"), cardlib::CCard("7s"),
        cardlib::CCard("9h"), cardlib::CCard("jd"),
        cardlib::CCard("8c"), cardlib::CCard("ts"),
        cardlib::CCard("js"),
      },
      cardlib::CHand::kstraight_signature::kQueenHighSignature,
      cardlib::CHand::kstraight_mask::kQueenHighStraight,
    },
    // jack-high straight
    { { cardlib::CCard("7s"), cardlib::CCard("6h"),
        cardlib::CCard("9d"), cardlib::CCard("jc"),
        cardlib::CCard("8s"), cardlib::CCard("th"),
        cardlib::CCard("jh"),
      },
      cardlib::CHand::kstraight_signature::kJackHighSignature,
      cardlib::CHand::kstraight_mask::kJackHighStraight},
    // ten-high straight
    { { cardlib::CCard("6h"), cardlib::CCard("7d"),
        cardlib::CCard("8c"), cardlib::CCard("9s"),
        cardlib::CCard("th"), cardlib::CCard("ad"),
        cardlib::CCard("qs"),
      },
      cardlib::CHand::kstraight_signature::kTenHighSignature,
      cardlib::CHand::kstraight_mask::kTenHighStraight},
    // nine-high straight
    { { cardlib::CCard("6d"), cardlib::CCard("7c"),
        cardlib::CCard("8s"), cardlib::CCard("9h"),
        cardlib::CCard("jd"), cardlib::CCard("5c"),
        cardlib::CCard("ks"),
      },
      cardlib::CHand::kstraight_signature::kNineHighSignature,
      cardlib::CHand::kstraight_mask::kNineHighStraight,
    },
    // eight-high straight
    { { cardlib::CCard("4c"), cardlib::CCard("5s"),
        cardlib::CCard("6h"), cardlib::CCard("7d"),
        cardlib::CCard("8c"), cardlib::CCard("ts"),
        cardlib::CCard("qs"),
      },
      cardlib::CHand::kstraight_signature::kEightHighSignature,
      cardlib::CHand::kstraight_mask::kEightHighStraight},
    // seven-high straight
    { { cardlib::CCard("5s"), cardlib::CCard("6h"),
        cardlib::CCard("3d"), cardlib::CCard("4c"),
        cardlib::CCard("as"), cardlib::CCard("th"),
        cardlib::CCard("7d"),
      },
      cardlib::CHand::kstraight_signature::kSevenHighSignature,
      cardlib::CHand::kstraight_mask::kSevenHighStraight},
    // six-high straight
    { { cardlib::CCard("4h"), cardlib::CCard("td"),
        cardlib::CCard("9c"), cardlib::CCard("5s"),
        cardlib::CCard("2h"), cardlib::CCard("6d"),
        cardlib::CCard("3c"),
      },
      cardlib::CHand::kstraight_signature::kSixHighSignature,
      cardlib::CHand::kstraight_mask::kSixHighStraight,
    },
    // five-high straight
    { { cardlib::CCard("ad"), cardlib::CCard("kc"),
        cardlib::CCard("3s"), cardlib::CCard("4h"),
        cardlib::CCard("2d"), cardlib::CCard("5c"),
        cardlib::CCard("js"),
      },
      cardlib::CHand::kstraight_signature::kFiveHighSignature,
      cardlib::CHand::kstraight_mask::kFiveHighStraight,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector;
  int64_t shouldbe_prime_product, result_prime_product;
  int64_t shouldbe_straight_mask, result_straight_mask;
  std::string sub_name = {
    static_cast<std::string>("THandTest(5)::")
    + static_cast<std::string>("TestGetPrime5SignatureStraights1:") };
  std::string err_start;
  std::string card_vector_string, hand_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;
    shouldbe_prime_product = va_cit->prime_product;
    shouldbe_straight_mask = va_cit->straight_mask;

    hand_obj.Clear();
    hand_obj.Calculate(&card_vector);
    hand_string = hand_obj.GetCardsString();

    result_prime_product = hand_obj.GetPrime5Signature();
    result_straight_mask = hand_obj.GetHandStraightMask();

    card_vector_string =
        card_obj.CardVectorToString(&card_vector);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + card_vector_string
        + static_cast<std::string>(", ")
        + hand_string
        + static_cast<std::string>(" : ");

    af_obj.AssertPrimesHelper(&err_start,
                              shouldbe_prime_product,
                              result_prime_product,
                              shouldbe_straight_mask,
                              result_straight_mask);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// tests GetPrime5Signature and GetHandStraightMask
// for straight flushes
void THandTest::TestGetPrime5SignatureVarious1() {
    mylib::CUtils utils_obj;
  cardlib::CHand hand_obj;
  cardlib::CCard card_obj;
  handtest::CAssertFunctions af_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    int64_t prime_product;
    cardlib::CHand::kstraight_mask straight_mask;
  };
  std::vector<atest_struct> test_vector = {
    // four-of-a-kind
    { { cardlib::CCard("qh"), cardlib::CCard("kc"),
        cardlib::CCard("jh"), cardlib::CCard("js"),
        cardlib::CCard("8h"), cardlib::CCard("jc"),
        cardlib::CCard("jd"),
      },
      { cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
      },
      cardlib::CHand::kstraight_mask::kStraightValueNull,
    },
    // full house
    { { cardlib::CCard("qd"), cardlib::CCard("qc"),
        cardlib::CCard("9s"), cardlib::CCard("8h"),
        cardlib::CCard("8d"), cardlib::CCard("8c"),
        cardlib::CCard("js"), },
      { cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpJack
      },
      cardlib::CHand::kstraight_mask::kStraightValueNull,
    },
    // flush
    { { cardlib::CCard("qc"), cardlib::CCard("7c"),
        cardlib::CCard("9h"), cardlib::CCard("jd"),
        cardlib::CCard("8c"), cardlib::CCard("3c"),
        cardlib::CCard("2c"), },
      { cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
      },
      cardlib::CHand::kstraight_mask::kStraightValueNull,
    },
    // three-of-a-kind
    { { cardlib::CCard("5s"), cardlib::CCard("6h"),
        cardlib::CCard("9d"), cardlib::CCard("5c"),
        cardlib::CCard("5d"), cardlib::CCard("ts"),
        cardlib::CCard("jh"), },
      { cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpJack
      },
      cardlib::CHand::kstraight_mask::kStraightValueNull,
    },
    // two-pairs
    { { cardlib::CCard("6h"), cardlib::CCard("7d"),
        cardlib::CCard("8c"), cardlib::CCard("6s"),
        cardlib::CCard("qh"), cardlib::CCard("ad"),
        cardlib::CCard("qs"), },
      { cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpQueen
      },
      cardlib::CHand::kstraight_mask::kStraightValueNull,
    },
    // one-pair
    { { cardlib::CCard("6d"), cardlib::CCard("7c"),
        cardlib::CCard("8s"), cardlib::CCard("8h"),
        cardlib::CCard("3d"), cardlib::CCard("tc"),
        cardlib::CCard("2s"),
      },
      { cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTwo
      },
      cardlib::CHand::kstraight_mask::kStraightValueNull,
    },
    // high card
    { { cardlib::CCard("4c"), cardlib::CCard("5s"),
        cardlib::CCard("2h"), cardlib::CCard("7d"),
        cardlib::CCard("8c"), cardlib::CCard("ts"),
        cardlib::CCard("qs"), },
      { cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpQueen
      },
      cardlib::CHand::kstraight_mask::kStraightValueNull,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector;
  int64_t shouldbe_prime_product, result_prime_product;
  int64_t shouldbe_straight_mask, result_straight_mask;
  std::string sub_name = {
    static_cast<std::string>("THandTest(5)::")
    + static_cast<std::string>(
        "TestGetPrime5SignatureVarious1:") };
  std::string err_start;
  std::string card_vector_string, hand_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;
    shouldbe_prime_product = va_cit->prime_product;
    shouldbe_straight_mask = va_cit->straight_mask;

    hand_obj.Clear();
    hand_obj.Calculate(&card_vector);
    hand_string = hand_obj.GetCardsString();

    result_prime_product =
        hand_obj.GetPrime7Signature();
    result_straight_mask =
        hand_obj.GetHandStraightMask();

    card_vector_string =
        card_obj.CardVectorToString(&card_vector);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + card_vector_string
        + static_cast<std::string>(", ")
        + hand_string
        + static_cast<std::string>(" : ");

    af_obj.AssertPrimesHelper(&err_start,
                              shouldbe_prime_product,
                              result_prime_product,
                              shouldbe_straight_mask,
                              result_straight_mask);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// tests GetHandValue()
void THandTest::TestGetHandValueVarious1() {
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    cardlib::CHand::khand_value hand_value;
  };
  std::vector<atest_struct> test_vector = {
    // royal flush
    { { cardlib::CCard("qh"), cardlib::CCard("kh"),
        cardlib::CCard("ah"), cardlib::CCard("jh"),
        cardlib::CCard("8h"), cardlib::CCard("th"),
        cardlib::CCard("js"),
      },
      cardlib::CHand::khand_value::kRoyalFlush,
    },
    // king-high straight flush
    { { cardlib::CCard("qd"), cardlib::CCard("kd"),
        cardlib::CCard("9d"), cardlib::CCard("jd"),
        cardlib::CCard("8d"), cardlib::CCard("td"),
        cardlib::CCard("js"),
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    // five-high straight flush
    { { cardlib::CCard("ad"), cardlib::CCard("kd"),
        cardlib::CCard("3d"), cardlib::CCard("4d"),
        cardlib::CCard("2d"), cardlib::CCard("5d"),
        cardlib::CCard("js"),
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    // four-of-a-kind
    { { cardlib::CCard("qh"), cardlib::CCard("kc"),
        cardlib::CCard("jh"), cardlib::CCard("js"),
        cardlib::CCard("8h"), cardlib::CCard("jc"),
        cardlib::CCard("jd"),
      },
      cardlib::CHand::khand_value::kQuads,
    },
    // full house
    { { cardlib::CCard("qd"), cardlib::CCard("qc"),
        cardlib::CCard("9s"), cardlib::CCard("8h"),
        cardlib::CCard("8d"), cardlib::CCard("8c"),
        cardlib::CCard("js"),
      },
      cardlib::CHand::khand_value::kFullHouse,
    },
    // flush
    { { cardlib::CCard("qc"), cardlib::CCard("7c"),
        cardlib::CCard("9h"), cardlib::CCard("jd"),
        cardlib::CCard("8c"), cardlib::CCard("3c"),
        cardlib::CCard("2c"),
      },
      cardlib::CHand::khand_value::kFlush,
    },
    // jack-high straight
    { { cardlib::CCard("7s"), cardlib::CCard("6h"),
        cardlib::CCard("9d"), cardlib::CCard("jc"),
        cardlib::CCard("8s"), cardlib::CCard("th"),
        cardlib::CCard("jh"),
      },
      cardlib::CHand::khand_value::kStraight,
    },
    // three-of-a-kind
    { { cardlib::CCard("5s"), cardlib::CCard("6h"),
        cardlib::CCard("9d"), cardlib::CCard("5c"),
        cardlib::CCard("5d"), cardlib::CCard("ts"),
        cardlib::CCard("jh"),
      },
      cardlib::CHand::khand_value::kTriples,
    },
    // two-pairs
    { { cardlib::CCard("6h"), cardlib::CCard("7d"),
        cardlib::CCard("8c"), cardlib::CCard("6s"),
        cardlib::CCard("qh"), cardlib::CCard("ad"),
        cardlib::CCard("qs"),
      },
      cardlib::CHand::khand_value::kTwoPair,
    },
    // one-pair
    { { cardlib::CCard("6d"), cardlib::CCard("7c"),
        cardlib::CCard("8s"), cardlib::CCard("8h"),
        cardlib::CCard("3d"), cardlib::CCard("tc"),
        cardlib::CCard("2s"),
      },
      cardlib::CHand::khand_value::kOnePair,
    },
    // high card
    { { cardlib::CCard("4c"), cardlib::CCard("5s"),
        cardlib::CCard("2h"), cardlib::CCard("7d"),
        cardlib::CCard("8c"), cardlib::CCard("ts"),
        cardlib::CCard("qs"),
      },
      cardlib::CHand::khand_value::kHighCard,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> card_vector;
  int64_t shouldbe_hand_value, result_hand_value;
  std::string sub_name = {
    static_cast<std::string>("THandTest(5)::")
    + static_cast<std::string>("TestGetHandValueVarious1:") };
  std::string error_message;
  std::string card_vector_string, hand_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;
    shouldbe_hand_value =
        static_cast<int64_t>(va_cit->hand_value);

    hand_obj.Clear();
    hand_obj.Calculate(&card_vector);

    hand_string = hand_obj.GetCardsString();

    result_hand_value = hand_obj.GetHandValue();

    card_vector_string =
        card_obj.CardVectorToString(&card_vector);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + card_vector_string
        + static_cast<std::string>(", ")
        + hand_string
        + static_cast<std::string>(", hand value shouldbe = ")
        + std::to_string(shouldbe_hand_value)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_hand_value);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_hand_value, result_hand_value);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

/*

// Copyright 2024 robert haramoto

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TFourFunctionsTest1 implementation                    ###
  ###    namespace HandTest                                  ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###    new card mask definition                            ###
  ###                                                        ###
  ###  created April 1, 2024                                 ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/four_functions_test.h"

#include <cppunit/SourceLine.h>

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cctype>       // std::tolower

#include <algorithm>
#include <string>
#include <vector>
#include <map>

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/four_functions.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::TFourFunctionsTest1);

namespace handtest {

// #############################################################
// #############################################################
TFourFunctionsTest1::TFourFunctionsTest1() {
}

// #############################################################
// #############################################################
TFourFunctionsTest1::~TFourFunctionsTest1() {
}

// #############################################################
// #############################################################
void TFourFunctionsTest1::setUp() {
}

// #############################################################
// #############################################################
void TFourFunctionsTest1::tearDown() {
}

// #############################################################
// #############################################################
void TFourFunctionsTest1::TestIsFour1() {
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CFourFunctions fourf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    cardlib::CCard::kcard_value quad_value;
    cardlib::CCard::kcard_value kicker1;
    int64_t prime7_signature;
    int64_t prime5_signature;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("2d"),
      },
      cardlib::CCard::kcard_value::kTwo,
      cardlib::CCard::kcard_value::kJack,
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTwo
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTwo
      },
      cardlib::CHand::khand_value::kQuads,
    },
    { { cardlib::CCard("5h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("5d"),
        cardlib::CCard("5c"), cardlib::CCard("5s"),
        cardlib::CCard("2d"),
      },
      cardlib::CCard::kcard_value::kFive,
      cardlib::CCard::kcard_value::kThree,
      { cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTwo
      },
      { cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
      },
      cardlib::CHand::khand_value::kQuads,
    },
    { { cardlib::CCard("qh"), cardlib::CCard("3c"),
        cardlib::CCard("qs"), cardlib::CCard("qd"),
        cardlib::CCard("5c"), cardlib::CCard("as"),
        cardlib::CCard("qc"),
      },
      cardlib::CCard::kcard_value::kQueen,
      cardlib::CCard::kcard_value::kAce,
      { cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpQueen
      },
      { cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpAce
      },
      cardlib::CHand::khand_value::kQuads,
    },
    { { cardlib::CCard("kh"), cardlib::CCard("3c"),
        cardlib::CCard("qs"), cardlib::CCard("qd"),
        cardlib::CCard("5c"), cardlib::CCard("as"),
        cardlib::CCard("qc"),
      },
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      -1L, -1L,
      cardlib::CHand::khand_value::kHandValueNull,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  std::map<cardlib::CCard::kcard_value, int64_t> value_count_map;
  cardlib::CHand::khand_value
      shouldbe_handvalue, result_handvalue;
  cardlib::CCard::kcard_value
      shouldbe_quad_value, shouldbe_kicker;
  cardlib::CCard::kcard_value
      result_quad_value, result_kicker;
  int64_t shouldbe7_signature, shouldbe5_signature;
  int64_t result7_signature, result5_signature;
  std::string sub_name = {
    static_cast<std::string>("TFourFunctionsTest1::")
    + static_cast<std::string>("TestIsFour1:") };
  std::string error_message, err_start, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    shouldbe_quad_value = va_cit->quad_value;
    shouldbe_kicker = va_cit->kicker1;
    shouldbe7_signature = va_cit->prime7_signature;
    shouldbe5_signature = va_cit->prime5_signature;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    value_count_map.clear();
    hand_obj.PopulateNthOfAKindMap(&input_card_vector,
                                   &value_count_map);

    fourf_obj.IsFourOfAKind(
        &input_card_vector, &value_count_map,
        &result_quad_value, &result_kicker,
        &result7_signature, &result5_signature,
        &result_handvalue);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("4-of-a-kind value shouldbe = ")
        + std::to_string(shouldbe_quad_value)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_quad_value);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_quad_value, result_quad_value);

    error_message =
        err_start
        + static_cast<std::string>("kicker value shouldbe = ")
        + std::to_string(shouldbe_kicker)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_kicker);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_kicker, result_kicker);

    error_message =
        err_start
        + static_cast<std::string>("7-cards signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-cards value shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("hand value shouldbe = ")
        + hand_obj.HandName(shouldbe_handvalue)
        + static_cast<std::string>(", result = ")
        + hand_obj.HandName(result_handvalue);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFourFunctionsTest1::TestQuadsGreaterQuads1() {
  cardlib::CCard card_obj;
  cardlib::CFourFunctions fourf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("td"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("2d"),
      },
      true,
    },
    { { cardlib::CCard("3h"), cardlib::CCard("3c"),
        cardlib::CCard("3s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("3d"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("td"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("2d"),
      },
      true,
    },
    { { cardlib::CCard("ah"), cardlib::CCard("ac"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("ah"), cardlib::CCard("ac"),
        cardlib::CCard("as"), cardlib::CCard("9d"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("ad"),
      },
      true,
    },
    { { cardlib::CCard("ah"), cardlib::CCard("ac"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("jh"), cardlib::CCard("jc"),
        cardlib::CCard("js"), cardlib::CCard("9d"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("jd"),
      },
      true,
    },
    { { cardlib::CCard("th"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("jh"), cardlib::CCard("jc"),
        cardlib::CCard("js"), cardlib::CCard("9d"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("jd"),
      },
      false,
    },
    { { cardlib::CCard("th"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("th"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("kd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("td"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TFourFunctionsTest1::")
    + static_cast<std::string>("TestQuadsGreaterQuads1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        fourf_obj.QuadsGreaterQuads(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFourFunctionsTest1::TestQuadsLessThanQuads1() {
  cardlib::CCard card_obj;
  cardlib::CFourFunctions fourf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("ad"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("2d"),
      },
      true,
    },
    { { cardlib::CCard("3h"), cardlib::CCard("3c"),
        cardlib::CCard("3s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("3d"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("td"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("2d"),
      },
      false,
    },
    { { cardlib::CCard("ah"), cardlib::CCard("ac"),
        cardlib::CCard("as"), cardlib::CCard("9d"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("ah"), cardlib::CCard("ac"),
        cardlib::CCard("as"), cardlib::CCard("td"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("ad"),
      },
      true,
    },
    { { cardlib::CCard("ah"), cardlib::CCard("ac"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("jh"), cardlib::CCard("jc"),
        cardlib::CCard("js"), cardlib::CCard("9d"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("jd"),
      },
      false,
    },
    { { cardlib::CCard("th"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("jh"), cardlib::CCard("jc"),
        cardlib::CCard("js"), cardlib::CCard("9d"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("jd"),
      },
      true,
    },
    { { cardlib::CCard("th"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("th"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("kd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("td"),
      },
      true,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TFourFunctionsTest1::")
    + static_cast<std::string>("TestQuadsLessThanQuads1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        fourf_obj.QuadsLessThanQuads(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFourFunctionsTest1::TestQuadsEqualQuads1() {
  cardlib::CCard card_obj;
  cardlib::CFourFunctions fourf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("7c"), cardlib::CCard("2c"),
        cardlib::CCard("2d"),
      },
      true,
    },
    { { cardlib::CCard("3h"), cardlib::CCard("3c"),
        cardlib::CCard("3s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("3d"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("td"),
        cardlib::CCard("3c"), cardlib::CCard("2c"),
        cardlib::CCard("2d"),
      },
      false,
    },
    { { cardlib::CCard("ah"), cardlib::CCard("ac"),
        cardlib::CCard("as"), cardlib::CCard("4d"),
        cardlib::CCard("kc"), cardlib::CCard("2c"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("ah"), cardlib::CCard("ac"),
        cardlib::CCard("as"), cardlib::CCard("kd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("ad"),
      },
      true,
    },
    { { cardlib::CCard("ah"), cardlib::CCard("ac"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("jh"), cardlib::CCard("jc"),
        cardlib::CCard("js"), cardlib::CCard("9d"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("jd"),
      },
      false,
    },
    { { cardlib::CCard("th"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("jh"), cardlib::CCard("jc"),
        cardlib::CCard("js"), cardlib::CCard("9d"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("jd"),
      },
      false,
    },
    { { cardlib::CCard("th"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("kc"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("th"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("kd"),
        cardlib::CCard("5c"), cardlib::CCard("2c"),
        cardlib::CCard("td"),
      },
      true,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TFourFunctionsTest1::")
    + static_cast<std::string>("TestQuadsEqualQuads1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        fourf_obj.QuadsEqualQuads(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#ifndef TESTS_HAND_TEST_H_
#define TESTS_HAND_TEST_H_

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CHandTest definition                                  ###
  ###    namespace HandTest                                  ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###    new bitmask definition for cards                    ###
  ###                                                        ###
  ###  last updated July 17, 2022                            ###
  ###                                                        ###
  ###  updated January 16, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cppunit/extensions/HelperMacros.h>

#include <vector>
#include <string>
#include <algorithm>

#include "sources/card.h"
#include "sources/hand.h"

namespace handtest {

class THandTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(THandTest);

  // hand_test1.cpp
  CPPUNIT_TEST(TestConstructor);
  CPPUNIT_TEST(TestCalculate1);
  CPPUNIT_TEST(TestCalculate2);
  CPPUNIT_TEST(TestCalculate3);

  // hand_test2.cpp
  CPPUNIT_TEST(TestHand1GreaterHand2RF1);
  CPPUNIT_TEST(TestHand1GreaterHand2SF1);
  CPPUNIT_TEST(TestHand1GreaterHand2FHSF1);
  CPPUNIT_TEST(TestHand1GreaterHand2Quads1);
  CPPUNIT_TEST(TestHand1GreaterHand2FH1);

  // hand_test3.cpp
  CPPUNIT_TEST(TestHand1GreaterHand2FL1);
  CPPUNIT_TEST(TestHand1GreaterHand2ST1);
  CPPUNIT_TEST(TestHand1GreaterHand2Triples1);
  CPPUNIT_TEST(TestHand1GreaterHand2TwoPair1);
  CPPUNIT_TEST(TestHand1GreaterHand2OnePair1);
  CPPUNIT_TEST(TestHand1GreaterHand2HighCard1);

  // hand_test4.cpp
  CPPUNIT_TEST(TestHand1EqualHand2Various1);
  CPPUNIT_TEST(TestHand1EqualHand2Various2);
  CPPUNIT_TEST(TestPopulateNthOfAKindMap1);

  // hand_test5.cpp
  CPPUNIT_TEST(TestGetPrime5SignatureSF1);
  CPPUNIT_TEST(TestGetPrime5SignatureStraights1);
  CPPUNIT_TEST(TestGetPrime5SignatureVarious1);
  CPPUNIT_TEST(TestGetHandValueVarious1);

  CPPUNIT_TEST_SUITE_END();

 public:
  THandTest();
  ~THandTest();

  void setUp();
  void tearDown();

  // hand_test1.cpp
  void TestConstructor();
  void TestCalculate1();
  void TestCalculate2();
  void TestCalculate3();

  // hand_test2.cpp
  void TestHand1GreaterHand2RF1();
  void TestHand1GreaterHand2SF1();
  void TestHand1GreaterHand2FHSF1();
  void TestHand1GreaterHand2Quads1();
  void TestHand1GreaterHand2FH1();

  // hand_test3.cpp
  void TestHand1GreaterHand2FL1();
  void TestHand1GreaterHand2ST1();
  void TestHand1GreaterHand2Triples1();
  void TestHand1GreaterHand2TwoPair1();
  void TestHand1GreaterHand2OnePair1();
  void TestHand1GreaterHand2HighCard1();

  // hand_test4.cpp
  void TestHand1EqualHand2Various1();
  void TestHand1EqualHand2Various2();
  void TestPopulateNthOfAKindMap1();

  // hand_test5.cpp
  void TestGetPrime5SignatureSF1();
  void TestGetPrime5SignatureStraights1();
  void TestGetPrime5SignatureVarious1();
  void TestGetHandValueVarious1();
};  // class THandTest

}  // namespace handtest

#endif  // TESTS_HAND_TEST_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

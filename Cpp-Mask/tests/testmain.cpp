/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  testmain program                                      ###
  ###                                                        ###
  ###  last updated May 6, 2024                              ###
  ###    changed definitions of bitmask                      ###
  ###                                                        ###
  ###  updated July 15, 2022                                 ###
  ###                                                        ###
  ###  updated January 18, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <stdint.h>
#include <stdlib.h>

#include <cppunit/TextOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <cstdio>
#include <iostream>
#include <string>

#include "sources/argopts.h"
#include "sources/utils.h"

// #############################################################
// #############################################################
int main(int argc, char** argv) {
  mylib::CUtils utils_obj;
  mylib::CArgOpts argopts_obj;
  bool bresult;
  std::string version_string = "2024-05-06";
  std::string title_string;
  std::string opt1;
  std::chrono::time_point<std::chrono::steady_clock,
                          std::chrono::duration<double>>
      start_time, end_time;

  start_time = std::chrono::steady_clock::now();

  title_string =
      static_cast<std::string>(
          "Testing the bitmask generator (version ")
      + version_string
      + static_cast<std::string>(")");

  opt1 = static_cast<std::string>(argv[0]);
  argopts_obj.SetName(&opt1);
  argopts_obj.SetVersion(&version_string);

  argopts_obj.ProcessOptions(argc, (const char**)argv);

  opt1 = static_cast<std::string>("--help");
  bresult = argopts_obj.OptionEntered(&opt1);
  if (bresult == true) {
    argopts_obj.DisplayUsage();
    ::exit(0);
  }

  opt1 = static_cast<std::string>("--version");
  bresult = argopts_obj.OptionEntered(&opt1);
  if (bresult == true) {
    argopts_obj.DisplayVersion();
    ::exit(0);
  }

  printf("%s\n", title_string.c_str());
  printf("current date %s\n",
         utils_obj.CurrentDateTime().c_str());
  fflush(stdout);

  // Get the top level suit from the registry
  CppUnit::Test *suite =
      CppUnit::TestFactoryRegistry::getRegistry().makeTest();

  // adds the test to the list of tests to run
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(suite);

  CppUnit::TextOutputter *tout;
  tout = new CppUnit::TextOutputter(&runner.result(), std::cerr);

  runner.setOutputter(tout);

  // run the tests
  bool wasSuccessful = runner.run();

  tout->printStatistics();

  end_time = std::chrono::steady_clock::now();

  printf("total elapsed time = %s\n",
         utils_obj.ClockDuration(
             &end_time, &start_time).c_str());
  printf("completed program on %s\n",
         utils_obj.CurrentDateTime().c_str());
  fflush(stdout);

  return wasSuccessful ? 0 : 1;
}
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

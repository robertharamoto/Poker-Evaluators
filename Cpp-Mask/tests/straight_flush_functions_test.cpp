/*

// Copyright 2024 robert haramoto

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TStraightFlushFunctionsTest1 implementation           ###
  ###    namespace HandTest                                  ###
  ###                                                        ###
  ###  last updated April 18, 2024                           ###
  ###    new card mask definition                            ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 19, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/straight_flush_functions_test.h"

#include <cppunit/SourceLine.h>

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cctype>       // std::tolower

#include <algorithm>
#include <string>
#include <vector>
#include <map>

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/straight_flush_functions.h"
#include "sources/flush_functions.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::TStraightFlushFunctionsTest1);

namespace handtest {

// #############################################################
// #############################################################
TStraightFlushFunctionsTest1::TStraightFlushFunctionsTest1() {
}

// #############################################################
// #############################################################
TStraightFlushFunctionsTest1::~TStraightFlushFunctionsTest1() {
}

// #############################################################
// #############################################################
void TStraightFlushFunctionsTest1::setUp() {
}

// #############################################################
// #############################################################
void TStraightFlushFunctionsTest1::tearDown() {
}

// #############################################################
// #############################################################
void TStraightFlushFunctionsTest1::TestSetStraightFlushMask1() {
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CFlushFunctions ff_obj;
  cardlib::CStraightFlushFunctions sff_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    cardlib::CHand::kstraight_mask shouldbe_mask;
    int64_t shouldbe_order;
    int64_t shouldbe7_signature;
    int64_t shouldbe5_signature;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("th"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("5c"), cardlib::CCard("7d"),
        cardlib::CCard("ah"),
      },
      cardlib::CHand::kstraight_mask::kAceHighStraight,
      { cardlib::CCard("ah").GetCardOrder()
        | cardlib::CCard("kh").GetCardOrder()
        | cardlib::CCard("qh").GetCardOrder()
        | cardlib::CCard("jh").GetCardOrder()
        | cardlib::CCard("th").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
      },
      cardlib::CHand::khand_value::kRoyalFlush,
    },
    { { cardlib::CCard("td"), cardlib::CCard("kh"),
        cardlib::CCard("qd"), cardlib::CCard("jd"),
        cardlib::CCard("kd"), cardlib::CCard("9d"),
        cardlib::CCard("as"),
      },
      cardlib::CHand::kstraight_mask::kKingHighStraight,
      { cardlib::CCard("kd").GetCardOrder()
        | cardlib::CCard("qd").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
        | cardlib::CCard("td").GetCardOrder()
        | cardlib::CCard("9d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("tc"), cardlib::CCard("8c"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("7c"), cardlib::CCard("9c"),
        cardlib::CCard("ad"),
      },
      cardlib::CHand::kstraight_mask::kQueenHighStraight,
      { cardlib::CCard("qc").GetCardOrder()
        | cardlib::CCard("jc").GetCardOrder()
        | cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("9c").GetCardOrder()
        | cardlib::CCard("8c").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("ah"), cardlib::CCard("ts"),
        cardlib::CCard("7s"), cardlib::CCard("js"),
        cardlib::CCard("ks"), cardlib::CCard("9s"),
        cardlib::CCard("8s"),
      },
      cardlib::CHand::kstraight_mask::kJackHighStraight,
      { cardlib::CCard("js").GetCardOrder()
        | cardlib::CCard("ts").GetCardOrder()
        | cardlib::CCard("9s").GetCardOrder()
        | cardlib::CCard("8s").GetCardOrder()
        | cardlib::CCard("7s").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
      },
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("th"), cardlib::CCard("kh"),
        cardlib::CCard("9h"), cardlib::CCard("8h"),
        cardlib::CCard("6h"), cardlib::CCard("7h"),
        cardlib::CCard("ad"),
      },
      cardlib::CHand::kstraight_mask::kTenHighStraight,
      { cardlib::CCard("th").GetCardOrder()
        | cardlib::CCard("9h").GetCardOrder()
        | cardlib::CCard("8h").GetCardOrder()
        | cardlib::CCard("7h").GetCardOrder()
        | cardlib::CCard("6h").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("5d"), cardlib::CCard("9d"),
        cardlib::CCard("7d"), cardlib::CCard("6d"),
        cardlib::CCard("8d"), cardlib::CCard("2d"),
        cardlib::CCard("as"),
      },
      cardlib::CHand::kstraight_mask::kNineHighStraight,
      { cardlib::CCard("9d").GetCardOrder()
        | cardlib::CCard("8d").GetCardOrder()
        | cardlib::CCard("7d").GetCardOrder()
        | cardlib::CCard("6d").GetCardOrder()
        | cardlib::CCard("5d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFive
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("5c"), cardlib::CCard("7c"),
        cardlib::CCard("8c"), cardlib::CCard("4c"),
        cardlib::CCard("6c"), cardlib::CCard("tc"),
        cardlib::CCard("2d"),
      },
      cardlib::CHand::kstraight_mask::kEightHighStraight,
      { cardlib::CCard("8c").GetCardOrder()
        | cardlib::CCard("7c").GetCardOrder()
        | cardlib::CCard("6c").GetCardOrder()
        | cardlib::CCard("5c").GetCardOrder()
        | cardlib::CCard("4c").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTwo
      },
      { cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFour
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("6s"), cardlib::CCard("3s"),
        cardlib::CCard("qs"), cardlib::CCard("5s"),
        cardlib::CCard("7s"), cardlib::CCard("4s"),
        cardlib::CCard("2s"),
      },
      cardlib::CHand::kstraight_mask::kSevenHighStraight,
      { cardlib::CCard("7s").GetCardOrder()
        | cardlib::CCard("6s").GetCardOrder()
        | cardlib::CCard("5s").GetCardOrder()
        | cardlib::CCard("4s").GetCardOrder()
        | cardlib::CCard("3s").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpTwo
      },
      { cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpThree
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("3h"), cardlib::CCard("2h"),
        cardlib::CCard("6h"), cardlib::CCard("4h"),
        cardlib::CCard("7d"), cardlib::CCard("ah"),
        cardlib::CCard("5h"),
      },
      cardlib::CHand::kstraight_mask::kSixHighStraight,
      { cardlib::CCard("6h").GetCardOrder()
        | cardlib::CCard("5h").GetCardOrder()
        | cardlib::CCard("4h").GetCardOrder()
        | cardlib::CCard("3h").GetCardOrder()
        | cardlib::CCard("2h").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpFive
      },
      { cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("2d"), cardlib::CCard("kd"),
        cardlib::CCard("3d"), cardlib::CCard("jd"),
        cardlib::CCard("4d"), cardlib::CCard("ad"),
        cardlib::CCard("5d"),
      },
      cardlib::CHand::kstraight_mask::kFiveHighStraight,
      { cardlib::CCard("5d").GetCardOrder()
        | cardlib::CCard("4d").GetCardOrder()
        | cardlib::CCard("3d").GetCardOrder()
        | cardlib::CCard("2d").GetCardOrder()
        | cardlib::CCard("ad").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpFive
      },
      { cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpAce
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("2c"), cardlib::CCard("kc"),
        cardlib::CCard("7c"), cardlib::CCard("jc"),
        cardlib::CCard("4c"), cardlib::CCard("ac"),
        cardlib::CCard("9c"),
      },
      cardlib::CHand::kstraight_mask::kStraightValueNull,
      static_cast<int64_t>(
          cardlib::CHand::kstraight_mask::kStraightValueNull),
      cardlib::CHand::kstraight_signature::kStraightSignatureNull,
      cardlib::CHand::kstraight_signature::kStraightSignatureNull,
      cardlib::CHand::khand_value::kHandValueNull
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  std::vector<cardlib::CCard>::const_iterator vc_cit;
  cardlib::CHand::kstraight_mask shouldbe_mask, result_mask;
  cardlib::CCard::kcard_suit flush_suit, this_suit;
  int64_t this_order, hand_order;
  int64_t shouldbe_order, result_order;
  int64_t this_prime, shouldbe7_signature, result7_signature;
  int64_t shouldbe5_signature, result5_signature;
  cardlib::CHand::khand_value shouldbe_handvalue, result_handvalue;
  std::string sub_name = {
    static_cast<std::string>("TStraightFlushFunctionsTest1::")
    + static_cast<std::string>("TestSetStraightFlushMask1:") };
  std::string err_start, error_message, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    shouldbe_mask = va_cit->shouldbe_mask;
    shouldbe_order = va_cit->shouldbe_order;
    shouldbe7_signature = va_cit->shouldbe7_signature;
    shouldbe5_signature = va_cit->shouldbe5_signature;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    flush_suit = ff_obj.GetFlushSuit(&input_card_vector);

    result7_signature = 1L;
    hand_order = 0L;
    for (vc_cit = input_card_vector.begin();
         vc_cit != input_card_vector.end(); ++vc_cit) {
      this_prime = vc_cit->GetCardPrimeValue();
      this_order = vc_cit->GetCardOrder();
      this_suit = vc_cit->GetCardSuit();

      result7_signature *= this_prime;
      if (this_suit == flush_suit) {
        hand_order = hand_order | this_order;
      }
    }

    sff_obj.SetStraightFlushMask(
        hand_order,
        &result_mask,
        &result_order,
        &result5_signature,
        &result_handvalue);

    if (result_order < 0L) {
      result7_signature = -1L;
    }

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("straight_mask shouldbe = ")
        + std::to_string(shouldbe_mask)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_mask);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_mask, result_mask);

    error_message =
        err_start
        + static_cast<std::string>("straight order shouldbe = ")
        + std::to_string(shouldbe_order)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_order);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_order, result_order);

    error_message =
        err_start
        + static_cast<std::string>("7-card signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-card signature shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("hand value shouldbe = ")
        + hand_obj.HandName(
            static_cast<int64_t>(shouldbe_handvalue))
        + static_cast<std::string>(", result = ")
        + hand_obj.HandName(
            static_cast<int64_t>(result_handvalue));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TStraightFlushFunctionsTest1::TestIsStraightFlush1() {
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CStraightFlushFunctions sff_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    cardlib::CCard::kcard_suit shouldbe_suit;
    cardlib::CHand::kstraight_mask shouldbe_mask;
    int64_t shouldbe_order;
    int64_t shouldbe7_signature;
    int64_t shouldbe5_signature;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("th"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("5c"), cardlib::CCard("7d"),
        cardlib::CCard("ah"),
      },
      cardlib::CCard::kcard_suit::kHearts,
      cardlib::CHand::kstraight_mask::kAceHighStraight,
      { cardlib::CCard("ah").GetCardOrder()
        | cardlib::CCard("kh").GetCardOrder()
        | cardlib::CCard("qh").GetCardOrder()
        | cardlib::CCard("jh").GetCardOrder()
        | cardlib::CCard("th").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
      },
      cardlib::CHand::khand_value::kRoyalFlush,
    },
    { { cardlib::CCard("td"), cardlib::CCard("kc"),
        cardlib::CCard("qd"), cardlib::CCard("jd"),
        cardlib::CCard("kd"), cardlib::CCard("9d"),
        cardlib::CCard("as"),
      },
      cardlib::CCard::kcard_suit::kDiamonds,
      cardlib::CHand::kstraight_mask::kKingHighStraight,
      { cardlib::CCard("kd").GetCardOrder()
        | cardlib::CCard("qd").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
        | cardlib::CCard("td").GetCardOrder()
        | cardlib::CCard("9d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("tc"), cardlib::CCard("8c"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("7c"), cardlib::CCard("9c"),
        cardlib::CCard("ad"),
      },
      cardlib::CCard::kcard_suit::kClubs,
      cardlib::CHand::kstraight_mask::kQueenHighStraight,
      { cardlib::CCard("qc").GetCardOrder()
        | cardlib::CCard("jc").GetCardOrder()
        | cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("9c").GetCardOrder()
        | cardlib::CCard("8c").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("ah"), cardlib::CCard("ts"),
        cardlib::CCard("7s"), cardlib::CCard("js"),
        cardlib::CCard("ks"), cardlib::CCard("9s"),
        cardlib::CCard("8s"),
      },
      cardlib::CCard::kcard_suit::kSpades,
      cardlib::CHand::kstraight_mask::kJackHighStraight,
      { cardlib::CCard("js").GetCardOrder()
        | cardlib::CCard("ts").GetCardOrder()
        | cardlib::CCard("9s").GetCardOrder()
        | cardlib::CCard("8s").GetCardOrder()
        | cardlib::CCard("7s").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
      },
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("th"), cardlib::CCard("kh"),
        cardlib::CCard("9h"), cardlib::CCard("8h"),
        cardlib::CCard("6h"), cardlib::CCard("7h"),
        cardlib::CCard("ad"),
      },
      cardlib::CCard::kcard_suit::kHearts,
      cardlib::CHand::kstraight_mask::kTenHighStraight,
      { cardlib::CCard("th").GetCardOrder()
        | cardlib::CCard("9h").GetCardOrder()
        | cardlib::CCard("8h").GetCardOrder()
        | cardlib::CCard("7h").GetCardOrder()
        | cardlib::CCard("6h").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("5d"), cardlib::CCard("9d"),
        cardlib::CCard("7d"), cardlib::CCard("6d"),
        cardlib::CCard("8d"), cardlib::CCard("2d"),
        cardlib::CCard("as"),
      },
      cardlib::CCard::kcard_suit::kDiamonds,
      cardlib::CHand::kstraight_mask::kNineHighStraight,
      { cardlib::CCard("9d").GetCardOrder()
        | cardlib::CCard("8d").GetCardOrder()
        | cardlib::CCard("7d").GetCardOrder()
        | cardlib::CCard("6d").GetCardOrder()
        | cardlib::CCard("5d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFive
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("5c"), cardlib::CCard("7c"),
        cardlib::CCard("8c"), cardlib::CCard("4c"),
        cardlib::CCard("6c"), cardlib::CCard("tc"),
        cardlib::CCard("2d"),
      },
      cardlib::CCard::kcard_suit::kClubs,
      cardlib::CHand::kstraight_mask::kEightHighStraight,
      { cardlib::CCard("8c").GetCardOrder()
        | cardlib::CCard("7c").GetCardOrder()
        | cardlib::CCard("6c").GetCardOrder()
        | cardlib::CCard("5c").GetCardOrder()
        | cardlib::CCard("4c").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTwo
      },
      { cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFour
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("6s"), cardlib::CCard("3s"),
        cardlib::CCard("qs"), cardlib::CCard("5s"),
        cardlib::CCard("7s"), cardlib::CCard("4s"),
        cardlib::CCard("2s"),
      },
      cardlib::CCard::kcard_suit::kSpades,
      cardlib::CHand::kstraight_mask::kSevenHighStraight,
      { cardlib::CCard("7s").GetCardOrder()
        | cardlib::CCard("6s").GetCardOrder()
        | cardlib::CCard("5s").GetCardOrder()
        | cardlib::CCard("4s").GetCardOrder()
        | cardlib::CCard("3s").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpTwo
      },
      { cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpThree
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("3h"), cardlib::CCard("2h"),
        cardlib::CCard("6h"), cardlib::CCard("4h"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("5h"),
      },
      cardlib::CCard::kcard_suit::kHearts,
      cardlib::CHand::kstraight_mask::kSixHighStraight,
      { cardlib::CCard("6h").GetCardOrder()
        | cardlib::CCard("5h").GetCardOrder()
        | cardlib::CCard("4h").GetCardOrder()
        | cardlib::CCard("3h").GetCardOrder()
        | cardlib::CCard("2h").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpFive
      },
      { cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("2d"), cardlib::CCard("kd"),
        cardlib::CCard("3d"), cardlib::CCard("jd"),
        cardlib::CCard("4d"), cardlib::CCard("ad"),
        cardlib::CCard("5d"),
      },
      cardlib::CCard::kcard_suit::kDiamonds,
      cardlib::CHand::kstraight_mask::kFiveHighStraight,
      { cardlib::CCard("5d").GetCardOrder()
        | cardlib::CCard("4d").GetCardOrder()
        | cardlib::CCard("3d").GetCardOrder()
        | cardlib::CCard("2d").GetCardOrder()
        | cardlib::CCard("ad").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpFive
      },
      { cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpAce
      },
      cardlib::CHand::khand_value::kStraightFlush,
    },
    { { cardlib::CCard("2c"), cardlib::CCard("kc"),
        cardlib::CCard("7c"), cardlib::CCard("jc"),
        cardlib::CCard("4c"), cardlib::CCard("ac"),
        cardlib::CCard("9c"),
      },
      cardlib::CCard::kcard_suit::kClubs,
      cardlib::CHand::kstraight_mask::kStraightValueNull,
      static_cast<int64_t>(
          cardlib::CHand::kstraight_mask::kStraightValueNull),
      cardlib::CHand::kstraight_signature::kStraightSignatureNull,
      cardlib::CHand::kstraight_signature::kStraightSignatureNull,
      cardlib::CHand::khand_value::kHandValueNull,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  cardlib::CCard::kcard_suit flush_suit;
  cardlib::CHand::kstraight_mask shouldbe_mask, result_mask;
  int64_t shouldbe_order, result_order;
  int64_t shouldbe7_signature, result7_signature;
  int64_t shouldbe5_signature, result5_signature;
  cardlib::CHand::khand_value
      shouldbe_handvalue, result_handvalue;
  std::string sub_name = {
    static_cast<std::string>("TStraightFlushFunctionsTest1::")
    + static_cast<std::string>("TestIsStraightFlush1:") };
  std::string err_start, error_message, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    flush_suit = va_cit->shouldbe_suit;
    shouldbe_mask = va_cit->shouldbe_mask;
    shouldbe_order = va_cit->shouldbe_order;
    shouldbe7_signature = va_cit->shouldbe7_signature;
    shouldbe5_signature = va_cit->shouldbe5_signature;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    sff_obj.IsStraightFlush(
        flush_suit, &input_card_vector,
        &result_mask, &result_order,
        &result7_signature, &result5_signature,
        &result_handvalue);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("straight_mask shouldbe = ")
        + std::to_string(shouldbe_mask)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_mask);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_mask, result_mask);

    error_message =
        err_start
        + static_cast<std::string>("straight order shouldbe = ")
        + std::to_string(shouldbe_order)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_order);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_order, result_order);

    error_message =
        err_start
        + static_cast<std::string>("7-card prime signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-card prime signature shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("hand value shouldbe = ")
        + hand_obj.HandName(shouldbe_handvalue)
        + static_cast<std::string>(", result = ")
        + hand_obj.HandName(result_handvalue);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TStraightFlushFunctionsTest1::TestStraightFlushGreater1() {
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CStraightFlushFunctions sff_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("kh"), cardlib::CCard("ah"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("5h"), cardlib::CCard("qh"),
        cardlib::CCard("as"),
      },
      { cardlib::CCard("kh"), cardlib::CCard("9h"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("2h"),
      },
      true,
    },
    { { cardlib::CCard("kh"), cardlib::CCard("9h"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("5h"), cardlib::CCard("qh"),
        cardlib::CCard("as"),
      },
      { cardlib::CCard("8h"), cardlib::CCard("9h"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("2h"),
      },
      true,
    },
    { { cardlib::CCard("8h"), cardlib::CCard("9h"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("5h"), cardlib::CCard("qh"),
        cardlib::CCard("as"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5h"), cardlib::CCard("4h"),
        cardlib::CCard("6h"), cardlib::CCard("7d"),
        cardlib::CCard("2h"),
      },
      true,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("4h"), cardlib::CCard("6h"),
        cardlib::CCard("5h"), cardlib::CCard("7d"),
        cardlib::CCard("ah"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("4h"), cardlib::CCard("jh"),
        cardlib::CCard("5h"), cardlib::CCard("7d"),
        cardlib::CCard("ah"),
      },
      true,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("4h"), cardlib::CCard("6h"),
        cardlib::CCard("5h"), cardlib::CCard("7d"),
        cardlib::CCard("ah"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("4h"), cardlib::CCard("6h"),
        cardlib::CCard("5h"), cardlib::CCard("7d"),
        cardlib::CCard("ah"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TStraightFlushFunctionsTest1::")
    + static_cast<std::string>("TestStraightFlushGreater1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        sff_obj.StraightFlushGreaterStraightFlush(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand-1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand-2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TStraightFlushFunctionsTest1::TestStraightFlushLessThan1() {
  cardlib::CCard card_obj;
  cardlib::CStraightFlushFunctions sff_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("kh"), cardlib::CCard("2s"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("9h"), cardlib::CCard("qh"),
        cardlib::CCard("as"),
      },
      { cardlib::CCard("8h"), cardlib::CCard("ah"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("kh"),
      },
      true,
    },
    { { cardlib::CCard("kh"), cardlib::CCard("9h"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("5h"), cardlib::CCard("qh"),
        cardlib::CCard("as"),
      },
      { cardlib::CCard("8h"), cardlib::CCard("9h"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("2h"),
      },
      false,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5h"), cardlib::CCard("4h"),
        cardlib::CCard("6h"), cardlib::CCard("7d"),
        cardlib::CCard("2h"),
      },
      { cardlib::CCard("8h"), cardlib::CCard("9h"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("5h"), cardlib::CCard("7h"),
        cardlib::CCard("as"),
      },
      true,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("4h"), cardlib::CCard("6h"),
        cardlib::CCard("5h"), cardlib::CCard("7d"),
        cardlib::CCard("ah"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("4h"), cardlib::CCard("7h"),
        cardlib::CCard("5h"), cardlib::CCard("6h"),
        cardlib::CCard("ah"),
      },
      true,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("4h"), cardlib::CCard("6h"),
        cardlib::CCard("5h"), cardlib::CCard("7d"),
        cardlib::CCard("ah"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("4h"), cardlib::CCard("6h"),
        cardlib::CCard("5h"), cardlib::CCard("7d"),
        cardlib::CCard("ah"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TStraightFlushFunctionsTest1::")
    + static_cast<std::string>("TestStraightFlushLessThan1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        sff_obj.StraightFlushLessThanStraightFlush(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand-1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand-2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TStraightFlushFunctionsTest1::TestStraightFlushEqual1() {
  cardlib::CCard card_obj;
  cardlib::CStraightFlushFunctions sff_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("kh"), cardlib::CCard("ah"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("5h"), cardlib::CCard("qh"),
        cardlib::CCard("as"),
      },
      { cardlib::CCard("kh"), cardlib::CCard("9h"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("ah"),
      },
      true,
    },
    { { cardlib::CCard("kh"), cardlib::CCard("9h"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("5h"), cardlib::CCard("qh"),
        cardlib::CCard("ah"),
      },
      { cardlib::CCard("8h"), cardlib::CCard("9h"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("qh"), cardlib::CCard("kh"),
        cardlib::CCard("ah"),
      },
      true,
    },
    { { cardlib::CCard("8h"), cardlib::CCard("9h"),
        cardlib::CCard("th"), cardlib::CCard("jh"),
        cardlib::CCard("5h"), cardlib::CCard("qh"),
        cardlib::CCard("as"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5h"), cardlib::CCard("4h"),
        cardlib::CCard("6h"), cardlib::CCard("7d"),
        cardlib::CCard("2h"),
      },
      false,
    },
    { { cardlib::CCard("2d"), cardlib::CCard("3d"),
        cardlib::CCard("4d"), cardlib::CCard("6h"),
        cardlib::CCard("5d"), cardlib::CCard("7d"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("4h"), cardlib::CCard("jh"),
        cardlib::CCard("5h"), cardlib::CCard("7d"),
        cardlib::CCard("ah"),
      },
      true,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("4h"), cardlib::CCard("6h"),
        cardlib::CCard("5h"), cardlib::CCard("7d"),
        cardlib::CCard("ah"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("4h"), cardlib::CCard("6h"),
        cardlib::CCard("5h"), cardlib::CCard("7d"),
        cardlib::CCard("ah"),
      },
      true,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TStraightFlushFunctionsTest1::")
    + static_cast<std::string>("TestStraightFlushEqual1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        sff_obj.StraightFlushEqualStraightFlush(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand-1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand-2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

/*

// Copyright 2024 robert haramoto

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TFullHouseFunctionsTest1 implementation               ###
  ###    namespace HandTest                                  ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###    new card mask definition                            ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 19, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/full_house_functions_test.h"

#include <cppunit/SourceLine.h>

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cctype>       // std::tolower

#include <algorithm>
#include <string>
#include <vector>
#include <map>

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/full_house_functions.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::TFullHouseFunctionsTest1);

namespace handtest {

// #############################################################
// #############################################################
TFullHouseFunctionsTest1::TFullHouseFunctionsTest1() {
}

// #############################################################
// #############################################################
TFullHouseFunctionsTest1::~TFullHouseFunctionsTest1() {
}

// #############################################################
// #############################################################
void TFullHouseFunctionsTest1::setUp() {
}

// #############################################################
// #############################################################
void TFullHouseFunctionsTest1::tearDown() {
}

// #############################################################
// #############################################################
void TFullHouseFunctionsTest1::TestFind3rdAndMax1() {
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CFullHouseFunctions fullf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    cardlib::CCard::kcard_value shouldbe_triple;
    cardlib::CCard::kcard_value shouldbe_twopair;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2d"),
      },
      cardlib::CCard::kcard_value::kTwo,
      cardlib::CCard::kcard_value::kThree,
    },
    { { cardlib::CCard("7h"), cardlib::CCard("jc"),
        cardlib::CCard("7s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("7d"),
      },
      cardlib::CCard::kcard_value::kSeven,
      cardlib::CCard::kcard_value::kJack,
    },
    { { cardlib::CCard("kh"), cardlib::CCard("5s"),
        cardlib::CCard("js"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("kc"),
        cardlib::CCard("kd"),
      },
      cardlib::CCard::kcard_value::kKing,
      cardlib::CCard::kcard_value::kJack,
    },
    { { cardlib::CCard("5h"), cardlib::CCard("kc"),
        cardlib::CCard("5s"), cardlib::CCard("kd"),
        cardlib::CCard("5c"), cardlib::CCard("kh"),
        cardlib::CCard("2d"),
      },
      cardlib::CCard::kcard_value::kKing,
      cardlib::CCard::kcard_value::kFive, },
    { { cardlib::CCard("jh"), cardlib::CCard("kc"),
        cardlib::CCard("js"), cardlib::CCard("kd"),
        cardlib::CCard("jc"), cardlib::CCard("kh"),
        cardlib::CCard("2d"),
      },
      cardlib::CCard::kcard_value::kKing,
      cardlib::CCard::kcard_value::kJack, },
    { { cardlib::CCard("5h"), cardlib::CCard("3c"),
        cardlib::CCard("as"), cardlib::CCard("kd"),
        cardlib::CCard("5c"), cardlib::CCard("kh"),
        cardlib::CCard("2d"),
      },
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  std::map<cardlib::CCard::kcard_value, int64_t>
      value_count_map;
  cardlib::CCard::kcard_value
      shouldbe_triple, shouldbe_twopair;
  cardlib::CCard::kcard_value
      result_triple, result_twopair;
  std::string sub_name = {
    static_cast<std::string>("TFullHouseFunctionsTest1::")
    + static_cast<std::string>("TestFind3rdAndMax1:") };
  std::string error_message, err_start, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    shouldbe_triple = va_cit->shouldbe_triple;
    shouldbe_twopair = va_cit->shouldbe_twopair;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    hand_obj.PopulateNthOfAKindMap(
        &input_card_vector, &value_count_map);

    fullf_obj.Find3rdAndMax(
        &value_count_map, &result_triple, &result_twopair);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("three-of-a-kind shouldbe = ")
        + std::to_string(shouldbe_triple)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_triple);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_triple, result_triple);

    error_message =
        err_start
        + static_cast<std::string>("two-pair shouldbe = ")
        + std::to_string(shouldbe_twopair)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_twopair);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_twopair, result_twopair);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFullHouseFunctionsTest1::TestIsFullHouse1() {
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CFullHouseFunctions fullf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    cardlib::CCard::kcard_value trip_value;
    cardlib::CCard::kcard_value top_pair;
    int64_t prime7_signature;
    int64_t prime5_signature;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2d"),
      },
      cardlib::CCard::kcard_value::kTwo,
      cardlib::CCard::kcard_value::kThree,
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpThree
      },
      cardlib::CHand::khand_value::kFullHouse,
    },
    { { cardlib::CCard("7h"), cardlib::CCard("jc"),
        cardlib::CCard("7s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("7d"),
      },
      cardlib::CCard::kcard_value::kSeven,
      cardlib::CCard::kcard_value::kJack,
      { cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
      },
      cardlib::CHand::khand_value::kFullHouse,
    },
    { { cardlib::CCard("kh"), cardlib::CCard("5s"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("kc"),
        cardlib::CCard("kd"),
      },
      cardlib::CCard::kcard_value::kKing,
      cardlib::CCard::kcard_value::kFive,
      { cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpKing
      },
      { cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFive
      },
      cardlib::CHand::khand_value::kFullHouse,
    },
    { { cardlib::CCard("5h"), cardlib::CCard("3c"),
        cardlib::CCard("5s"), cardlib::CCard("kd"),
        cardlib::CCard("5c"), cardlib::CCard("kh"),
        cardlib::CCard("2d"),
      },
      cardlib::CCard::kcard_value::kFive,
      cardlib::CCard::kcard_value::kKing,
      { cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpTwo
      },
      { cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpKing
      },
      cardlib::CHand::khand_value::kFullHouse,
    },
    { { cardlib::CCard("5h"), cardlib::CCard("3c"),
        cardlib::CCard("as"), cardlib::CCard("kd"),
        cardlib::CCard("5c"), cardlib::CCard("kh"),
        cardlib::CCard("2d"),
      },
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      -1L, -1L,
      cardlib::CHand::khand_value::kHandValueNull
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  std::map<cardlib::CCard::kcard_value, int64_t>
      value_count_map;
  std::vector<cardlib::CCard> shouldbe_card_vector;
  std::vector<cardlib::CCard> result_only_vector;
  cardlib::CCard::kcard_value
      shouldbe_trips, result_trips;
  cardlib::CCard::kcard_value
      shouldbe_pair, result_pair;
  int64_t shouldbe7_signature, shouldbe5_signature;
  int64_t result7_signature, result5_signature;
  cardlib::CHand::khand_value shouldbe_handvalue;
  cardlib::CHand::khand_value result_handvalue;
  std::string sub_name = {
    static_cast<std::string>("TFullHouseFunctionsTest1::")
    + static_cast<std::string>("TestIsFullHouse1:") };
  std::string error_message, err_start, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    shouldbe_trips = va_cit->trip_value;
    shouldbe_pair = va_cit->top_pair;
    shouldbe7_signature = va_cit->prime7_signature;
    shouldbe5_signature = va_cit->prime5_signature;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    value_count_map.clear();
    hand_obj.PopulateNthOfAKindMap(
        &input_card_vector, &value_count_map);

    fullf_obj.IsFullHouse(
        &input_card_vector, &value_count_map,
        &result_trips, &result_pair,
        &result7_signature, &result5_signature,
        &result_handvalue);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("3-of-a-kind value shouldbe = ")
        + std::to_string(shouldbe_trips)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_trips);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_trips, result_trips);

    error_message =
        err_start
        + static_cast<std::string>("top pair shouldbe = ")
        + std::to_string(shouldbe_pair)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_pair);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_pair, result_pair);

    error_message =
        err_start
        + static_cast<std::string>("7-card signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-card signature shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("hand value shouldbe = ")
        + hand_obj.HandName(shouldbe_handvalue)
        + static_cast<std::string>(", result = ")
        + hand_obj.HandName(result_handvalue);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFullHouseFunctionsTest1::TestFullHouseGreaterFullHouse1() {
  cardlib::CCard card_obj;
  cardlib::CFullHouseFunctions fullf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("3h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("3s"), cardlib::CCard("2d"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2d"),
      },
      true,
    },
    { { cardlib::CCard("9h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("9s"), cardlib::CCard("3d"),
        cardlib::CCard("9d"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("3s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2d"),
      },
      true,
    },
    { { cardlib::CCard("8h"), cardlib::CCard("7c"),
        cardlib::CCard("7s"), cardlib::CCard("jd"),
        cardlib::CCard("8s"), cardlib::CCard("8d"),
        cardlib::CCard("js"),
      },
      { cardlib::CCard("9h"), cardlib::CCard("8c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("9c"), cardlib::CCard("8h"),
        cardlib::CCard("8d"),
      },
      true,
    },
    { { cardlib::CCard("8h"), cardlib::CCard("7c"),
        cardlib::CCard("7s"), cardlib::CCard("jd"),
        cardlib::CCard("8s"), cardlib::CCard("8d"),
        cardlib::CCard("js"),
      },
      { cardlib::CCard("9h"), cardlib::CCard("8c"),
        cardlib::CCard("2s"), cardlib::CCard("9d"),
        cardlib::CCard("9c"), cardlib::CCard("8h"),
        cardlib::CCard("8d"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TFullHouseFunctionsTest1::")
    + static_cast<std::string>("TestFullHouseGreater1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        fullf_obj.FullHouseGreaterFullHouse(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFullHouseFunctionsTest1::TestFullHouseLessThanFullHouse1() {
  cardlib::CCard card_obj;
  cardlib::CFullHouseFunctions fullf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // triples1 < triples2
    { { cardlib::CCard("3h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("3s"), cardlib::CCard("2d"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("3c"),
        cardlib::CCard("7s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("7d"),
      },
      true,
    },
    // triples1 > triples2
    { { cardlib::CCard("9h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("9s"), cardlib::CCard("3d"),
        cardlib::CCard("9d"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("3s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2d"),
      },
      false,
    },
    // triples1 == triples2, top pair1 < top pair2
    { { cardlib::CCard("8h"), cardlib::CCard("7c"),
        cardlib::CCard("7s"), cardlib::CCard("jd"),
        cardlib::CCard("8s"), cardlib::CCard("8d"),
        cardlib::CCard("js"),
      },
      { cardlib::CCard("9h"), cardlib::CCard("8c"),
        cardlib::CCard("qs"), cardlib::CCard("qd"),
        cardlib::CCard("9c"), cardlib::CCard("8h"),
        cardlib::CCard("8d"),
      },
      true,
    },
    // triples1 == triples2, top pair1 > top pair2
    { { cardlib::CCard("8h"), cardlib::CCard("7c"),
        cardlib::CCard("7s"), cardlib::CCard("kd"),
        cardlib::CCard("8s"), cardlib::CCard("8d"),
        cardlib::CCard("ks"),
      },
      { cardlib::CCard("9h"), cardlib::CCard("8c"),
        cardlib::CCard("2s"), cardlib::CCard("2d"),
        cardlib::CCard("9c"), cardlib::CCard("8h"),
        cardlib::CCard("8d"),
      },
      false,
    },
    // triples1 < triples2, top pair1 < top pair2
    { { cardlib::CCard("8h"), cardlib::CCard("7c"),
        cardlib::CCard("7s"), cardlib::CCard("jd"),
        cardlib::CCard("8s"), cardlib::CCard("8d"),
        cardlib::CCard("7h"),
      },
      { cardlib::CCard("9h"), cardlib::CCard("8c"),
        cardlib::CCard("2s"), cardlib::CCard("9d"),
        cardlib::CCard("9c"), cardlib::CCard("8h"),
        cardlib::CCard("8d"),
      },
      true,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name =
      { static_cast<std::string>("TFullHouseFunctionsTest1::")
        + static_cast<std::string>("TestFullHouseLessThan1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        fullf_obj.FullHouseLessThanFullHouse(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFullHouseFunctionsTest1::TestFullHouseEqualFullHouse1() {
  cardlib::CCard card_obj;
  cardlib::CFullHouseFunctions fullf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("3h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("3s"), cardlib::CCard("2d"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("5h"), cardlib::CCard("3c"),
        cardlib::CCard("3s"), cardlib::CCard("jd"),
        cardlib::CCard("2c"), cardlib::CCard("3h"),
        cardlib::CCard("2d"),
      },
      true,
    },
    { { cardlib::CCard("9h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("9s"), cardlib::CCard("3d"),
        cardlib::CCard("9d"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("3s"), cardlib::CCard("jd"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("2d"),
      },
      false,
    },
    { { cardlib::CCard("8h"), cardlib::CCard("7c"),
        cardlib::CCard("7s"), cardlib::CCard("jd"),
        cardlib::CCard("8s"), cardlib::CCard("8d"),
        cardlib::CCard("js"),
      },
      { cardlib::CCard("jh"), cardlib::CCard("8c"),
        cardlib::CCard("2s"), cardlib::CCard("jd"),
        cardlib::CCard("9c"), cardlib::CCard("8h"),
        cardlib::CCard("8d"),
      },
      true,
    },
    { { cardlib::CCard("8h"), cardlib::CCard("7c"),
        cardlib::CCard("7s"), cardlib::CCard("jd"),
        cardlib::CCard("8s"), cardlib::CCard("8d"),
        cardlib::CCard("js"),
      },
      { cardlib::CCard("9h"), cardlib::CCard("8c"),
        cardlib::CCard("2s"), cardlib::CCard("9d"),
        cardlib::CCard("9c"), cardlib::CCard("8h"),
        cardlib::CCard("8d"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name =
      { static_cast<std::string>("TFullHouseFunctionsTest1::")
        + static_cast<std::string>("TestFullHouseEqual1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        fullf_obj.FullHouseEqualFullHouse(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  THandTest implementation                              ###
  ###    namespace handtest                                  ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###    split into multiple files                           ###
  ###    new bitmask definition for cards                    ###
  ###    fixed updated cpplint.py errors                     ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/hand_test.h"

#include <cstdint>

#include <string>
#include <vector>
#include <algorithm>
#include <cctype>       // std::tolower

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/utils.h"
#include "tests/assert_functions.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::THandTest);

namespace handtest {

// #############################################################
// #############################################################
void THandTest::TestHand1GreaterHand2RF1() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand1_obj, hand2_obj;
  cardlib::CCard card_obj;
  handtest::CAssertFunctions af_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    bool shouldbe_1v2, shouldbe_2v1;
  };
  std::vector<atest_struct> test_vector = {
    // royal flush versus king-high straight-flush
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      true, false
    },
    // royal flush versus queen-high straight-flush
    { { cardlib::CCard("7h"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // royal flush versus six-high straight-flush
    { { cardlib::CCard("ah"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("5h"),
        cardlib::CCard("4h"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true, false,
    },
    // royal flush versus five-high straight-flush
    { { cardlib::CCard("ah"), cardlib::CCard("8h"),
        cardlib::CCard("qh"), cardlib::CCard("5h"),
        cardlib::CCard("4h"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true, false,
    },
    // royal flush versus quad sevens
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("7s"),
        cardlib::CCard("8d"),
      },
      true, false,
    },
    // royal flush versus sevens full of queens
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("qs"),
        cardlib::CCard("8d"),
      },
      true, false,
    },
    // royal flush versus queen-high flush
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // royal flush versus ace-high straight
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("qh"), cardlib::CCard("kc"),
        cardlib::CCard("th"), cardlib::CCard("3h"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // royal flush versus triple aces
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("as"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // royal flush versus two pair
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("3d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // royal flush versus one pair
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("3d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // royal flush versus high-card
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("4d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> royal_flush_vector = {
    cardlib::CCard("ah"), cardlib::CCard("kh"),
    cardlib::CCard("qh"), cardlib::CCard("jh"),
    cardlib::CCard("th"), cardlib::CCard("9h"),
    cardlib::CCard("8h"),
  };
  std::vector<cardlib::CCard> card_vector;
  bool shouldbe_bool1, shouldbe_bool2;
  bool result_bool1, result_bool2;
  std::string sub_name = {
    static_cast<std::string>("THandTest(2)::")
    + static_cast<std::string>("TestHand1GreaterHand2RF1:") };
  std::string royal_string;
  std::string error_message;

  hand1_obj.Clear();
  hand1_obj.Calculate(&royal_flush_vector);

  royal_string =
      card_obj.CardVectorToString(&royal_flush_vector);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;

    shouldbe_bool1 = va_cit->shouldbe_1v2;
    shouldbe_bool2 = va_cit->shouldbe_2v1;

    hand2_obj.Clear();
    hand2_obj.Calculate(&card_vector);

    result_bool1 =
        hand1_obj.Hand1GreaterHand2(&hand2_obj);

    result_bool2 =
        hand2_obj.Hand1GreaterHand2(&hand1_obj);

    error_message = sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(") : card vector 1 = ")
        + royal_string
        + static_cast<std::string>(" : card vector 2 = ")
        + card_obj.CardVectorToString(&card_vector)
        + static_cast<std::string>(" : ");

    af_obj.Assert1Greater2(
        &error_message,
        &hand1_obj, &hand2_obj,
        shouldbe_bool1, shouldbe_bool2,
        result_bool1, result_bool2);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THandTest::TestHand1GreaterHand2SF1() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand1_obj, hand2_obj;
  cardlib::CCard card_obj;
  handtest::CAssertFunctions af_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    bool shouldbe_1v2, shouldbe_2v1;
  };
  std::vector<atest_struct> test_vector = {
    // king-high straight flush versus queen-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // king-high straight flush versus jack-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("6h"),
        cardlib::CCard("5h"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // king-high straight flush versus six-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("5h"),
        cardlib::CCard("4h"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true, false,
    },
    // king-high straight flush versus five-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("8h"),
        cardlib::CCard("qh"), cardlib::CCard("5h"),
        cardlib::CCard("4h"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      true, false,
    },
    // king-high straight flush versus quad sevens
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("7s"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // king-high straight flush versus sevens full of queens
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("qs"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // king-high straight flush versus queen-high flush
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // king-high straight flush versus ace-high straight
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("qh"), cardlib::CCard("kc"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // king-high straight flush versus triple aces
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("th"),
        cardlib::CCard("as"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // king-high straight flush versus two pairs
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("3d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // king-high straight flush versus one pair
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("3d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // king-high straight flush versus high card
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("4d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> king_straight_flush_vector = {
    cardlib::CCard("7h"), cardlib::CCard("kh"),
    cardlib::CCard("qh"), cardlib::CCard("jh"),
    cardlib::CCard("th"), cardlib::CCard("9h"),
    cardlib::CCard("8h"), };
  std::vector<cardlib::CCard> card_vector;
  bool shouldbe_bool1, shouldbe_bool2;
  bool result_bool1, result_bool2;
  std::string sub_name = {
    static_cast<std::string>("THandTest(2)::")
    + static_cast<std::string>("TestHand1GreaterHand2SF1:") };
  std::string king_string;
  std::string error_message;

  hand1_obj.Clear();
  hand1_obj.Calculate(&king_straight_flush_vector);
  king_string =
      card_obj.CardVectorToString(&king_straight_flush_vector);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;

    shouldbe_bool1 = va_cit->shouldbe_1v2;
    shouldbe_bool2 = va_cit->shouldbe_2v1;

    hand2_obj.Clear();
    hand2_obj.Calculate(&card_vector);

    result_bool1 = hand1_obj.Hand1GreaterHand2(&hand2_obj);

    result_bool2 = hand2_obj.Hand1GreaterHand2(&hand1_obj);

    error_message = sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(") : card vector 1 = ")
        + king_string
        + static_cast<std::string>(" : card vector 2 = ")
        + card_obj.CardVectorToString(&card_vector)
        + static_cast<std::string>(" : ");

    af_obj.Assert1Greater2(
        &error_message,
        &hand1_obj, &hand2_obj,
        shouldbe_bool1, shouldbe_bool2,
        result_bool1, result_bool2);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// five high straight flush versus ...
void THandTest::TestHand1GreaterHand2FHSF1() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand1_obj, hand2_obj;
  cardlib::CCard card_obj;
  handtest::CAssertFunctions af_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    bool shouldbe_1v2, shouldbe_2v1;
  };
  std::vector<atest_struct> test_vector = {
    // five-high straight flush versus king-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // five-high straight flush versus queen-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // five-high straight flush versus six-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("5h"),
        cardlib::CCard("4h"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false, true,
    },
    // five-high straight flush versus five-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("8h"),
        cardlib::CCard("qh"), cardlib::CCard("5h"),
        cardlib::CCard("4h"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false, false,
    },
    // five-high straight flush versus quad sevens
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("7s"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // five-high straight flush versus sevens full of kings
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("kh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("ks"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // five-high straight flush versus jack-high flush
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("7s"), cardlib::CCard("jh"),
        cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // five-high straight flush versus ace-high straight
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("qd"), cardlib::CCard("kc"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // five-high straight flush versus triple aces
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("as"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // five-high straight flush versus two pairs
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("3d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // five-high straight flush versus one pair
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("3d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // five-high straight flush versus high card
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("4d"),
        cardlib::CCard("th"), cardlib::CCard("qs"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> five_high_sflush_vector = {
    cardlib::CCard("ah"), cardlib::CCard("2h"),
    cardlib::CCard("3h"), cardlib::CCard("4h"),
    cardlib::CCard("5h"), cardlib::CCard("9h"),
    cardlib::CCard("8h"), };
  std::vector<cardlib::CCard> card_vector;
  bool shouldbe_bool1, shouldbe_bool2;
  bool result_bool1, result_bool2;
  std::string sub_name = {
    static_cast<std::string>("THandTest(2)::")
    + static_cast<std::string>("TestHand1GreaterHand2FHSF1:") };
  std::string five_string;
  std::string error_message;

  hand1_obj.Clear();
  hand1_obj.Calculate(&five_high_sflush_vector);

  five_string =
      card_obj.CardVectorToString(&five_high_sflush_vector);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;

    shouldbe_bool1 = va_cit->shouldbe_1v2;
    shouldbe_bool2 = va_cit->shouldbe_2v1;

    hand2_obj.Clear();
    hand2_obj.Calculate(&card_vector);

    result_bool1 = hand1_obj.Hand1GreaterHand2(&hand2_obj);

    result_bool2 = hand2_obj.Hand1GreaterHand2(&hand1_obj);

    error_message = sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(") : card vector 1 = ")
        + five_string
        + static_cast<std::string>(" : card vector 2 = ")
        + card_obj.CardVectorToString(&card_vector)
        + static_cast<std::string>(" : ");

    af_obj.Assert1Greater2(
        &error_message,
        &hand1_obj, &hand2_obj,
        shouldbe_bool1, shouldbe_bool2,
        result_bool1, result_bool2);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THandTest::TestHand1GreaterHand2Quads1() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand1_obj, hand2_obj;
  cardlib::CCard card_obj;
  handtest::CAssertFunctions af_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    bool shouldbe_1v2, shouldbe_2v1;
  };
  std::vector<atest_struct> test_vector = {
    // quad jacks versus king-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // quad jacks versus queen-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // quad jacks versus six-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("5h"),
        cardlib::CCard("4h"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false, true,
    },
    // quad jacks versus five-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("8h"),
        cardlib::CCard("qh"), cardlib::CCard("5h"),
        cardlib::CCard("4h"), cardlib::CCard("3h"),
        cardlib::CCard("2h"),
      },
      false, true,
    },
    // quad jacks versus quad sevens
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("7s"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // quad jacks versus sevens full of queens
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("qs"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // quad jacks versus queen-high flush
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // quad jacks versus ace-high straight
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("qh"), cardlib::CCard("kc"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8s"),
      },
      true, false,
    },
    // quad jacks versus triple aces
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("as"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // quad jacks versus two pairs
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("3d"),
        cardlib::CCard("jh"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // quad jacks versus one pair
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("3d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8s"),
      },
      true, false,
    },
    // quad jacks versus high card
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("4d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> quad_jacks_vector = {
    cardlib::CCard("jc"), cardlib::CCard("ah"),
    cardlib::CCard("3c"), cardlib::CCard("js"),
    cardlib::CCard("th"), cardlib::CCard("jd"),
    cardlib::CCard("jh"), };
  std::vector<cardlib::CCard> card_vector;
  bool shouldbe_bool1, shouldbe_bool2;
  bool result_bool1, result_bool2;
  std::string sub_name = {
    static_cast<std::string>("THandTest(2)::")
    + static_cast<std::string>("TestHand1GreaterHand2Quads1:") };
  std::string jacks_string;
  std::string error_message;

  hand1_obj.Clear();
  hand1_obj.Calculate(&quad_jacks_vector);

  jacks_string =
      card_obj.CardVectorToString(&quad_jacks_vector);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;

    shouldbe_bool1 = va_cit->shouldbe_1v2;
    shouldbe_bool2 = va_cit->shouldbe_2v1;

    hand2_obj.Clear();
    hand2_obj.Calculate(&card_vector);

    result_bool1 = hand1_obj.Hand1GreaterHand2(&hand2_obj);

    result_bool2 = hand2_obj.Hand1GreaterHand2(&hand1_obj);

    error_message = sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(") : card vector 1 = ")
        + jacks_string
        + static_cast<std::string>(" : card vector 2 = ")
        + card_obj.CardVectorToString(&card_vector)
        + static_cast<std::string>(" : ");

    af_obj.Assert1Greater2(
        &error_message,
        &hand1_obj, &hand2_obj,
        shouldbe_bool1, shouldbe_bool2,
        result_bool1, result_bool2);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void THandTest::TestHand1GreaterHand2FH1() {
  mylib::CUtils utils_obj;
  cardlib::CHand hand1_obj, hand2_obj;
  cardlib::CCard card_obj;
  handtest::CAssertFunctions af_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> card_vector;
    bool shouldbe_1v2, shouldbe_2v1;
  };
  std::vector<atest_struct> test_vector = {
    // full-house versus king-high straight flush
    { { cardlib::CCard("7h"), cardlib::CCard("kh"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("th"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // full-house versus queen-high straight flush
    { { cardlib::CCard("7d"), cardlib::CCard("6d"),
        cardlib::CCard("qd"), cardlib::CCard("jd"),
        cardlib::CCard("td"), cardlib::CCard("9d"),
        cardlib::CCard("8d"),
      },
      false, true,
    },
    // full-house versus six-high straight flush
    { { cardlib::CCard("ah"), cardlib::CCard("6s"),
        cardlib::CCard("qs"), cardlib::CCard("5s"),
        cardlib::CCard("4s"), cardlib::CCard("3s"),
        cardlib::CCard("2s"),
      },
      false, true,
    },
    // full-house versus five-high straight flush
    { { cardlib::CCard("ac"), cardlib::CCard("8c"),
        cardlib::CCard("qc"), cardlib::CCard("5c"),
        cardlib::CCard("4c"), cardlib::CCard("3c"),
        cardlib::CCard("2c"),
      },
      false, true,
    },
    // full-house versus quad sevens
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qc"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("7s"),
        cardlib::CCard("8h"),
      },
      false, true,
    },
    // full-house versus full-house
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qc"), cardlib::CCard("7d"),
        cardlib::CCard("th"), cardlib::CCard("qs"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // full-house versus flush
    { { cardlib::CCard("7h"), cardlib::CCard("7c"),
        cardlib::CCard("qh"), cardlib::CCard("8d"),
        cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("8h"),
      },
      true, false,
    },
    // full-house versus ace-high straight
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("qh"), cardlib::CCard("kc"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // full-house versus triple aces
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("as"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // full-house versus two pairs
    { { cardlib::CCard("ah"), cardlib::CCard("jc"),
        cardlib::CCard("ac"), cardlib::CCard("3d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // full-house versus one pair
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("3d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
    // full-house versus high card
    { { cardlib::CCard("kd"), cardlib::CCard("2c"),
        cardlib::CCard("ac"), cardlib::CCard("4d"),
        cardlib::CCard("th"), cardlib::CCard("3s"),
        cardlib::CCard("8c"),
      },
      true, false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> full_house_vector = {
    cardlib::CCard("jh"), cardlib::CCard("ah"),
    cardlib::CCard("qh"), cardlib::CCard("jd"),
    cardlib::CCard("qd"), cardlib::CCard("js"),
    cardlib::CCard("8c"), };
  std::vector<cardlib::CCard> card_vector;
  bool shouldbe_bool1, shouldbe_bool2;
  bool result_bool1, result_bool2;
  std::string sub_name = {
    static_cast<std::string>("THandTest(2)::")
    + static_cast<std::string>("TestHand1GreaterHand2FH1:") };
  std::string error_message;
  std::string full_house_string;

  hand1_obj.Clear();
  hand1_obj.Calculate(&full_house_vector);

  full_house_string =
      card_obj.CardVectorToString(&full_house_vector);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    card_vector = va_cit->card_vector;

    shouldbe_bool1 = va_cit->shouldbe_1v2;
    shouldbe_bool2 = va_cit->shouldbe_2v1;

    hand2_obj.Clear();
    hand2_obj.Calculate(&card_vector);

    result_bool1 = hand1_obj.Hand1GreaterHand2(&hand2_obj);

    result_bool2 = hand2_obj.Hand1GreaterHand2(&hand1_obj);

    error_message = sub_name
        + static_cast<std::string>(" (")
        + utils_obj.CommafyNum(test_label_index)
        + static_cast<std::string>(") : card vector 1 = ")
        + full_house_string
        + static_cast<std::string>(" : card vector 2 = ")
        + card_obj.CardVectorToString(&card_vector)
        + static_cast<std::string>(" : ");

    af_obj.Assert1Greater2(
        &error_message,
        &hand1_obj, &hand2_obj,
        shouldbe_bool1, shouldbe_bool2,
        result_bool1, result_bool2);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

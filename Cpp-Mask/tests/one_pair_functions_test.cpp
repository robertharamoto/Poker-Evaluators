/*

// Copyright 2024 robert haramoto

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TOnePairFunctionsTest1 implementation                 ###
  ###    namespace HandTest                                  ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###    new card mask definition                            ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 19, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/one_pair_functions_test.h"

#include <cppunit/SourceLine.h>

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cctype>       // std::tolower

#include <algorithm>
#include <string>
#include <vector>
#include <map>

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/one_pair_functions.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::TOnePairFunctionsTest1);

namespace handtest {

// #############################################################
// #############################################################
TOnePairFunctionsTest1::TOnePairFunctionsTest1() {
}

// #############################################################
// #############################################################
TOnePairFunctionsTest1::~TOnePairFunctionsTest1() {
}

// #############################################################
// #############################################################
void TOnePairFunctionsTest1::setUp() {
}

// #############################################################
// #############################################################
void TOnePairFunctionsTest1::tearDown() {
}

// #############################################################
// #############################################################
void TOnePairFunctionsTest1::TestFormOnePair1() {
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::COnePairFunctions onef_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    cardlib::CCard::kcard_value toppair;
    cardlib::CCard::kcard_value kicker1;
    cardlib::CCard::kcard_value kicker2;
    cardlib::CCard::kcard_value kicker3;
    int64_t signature7;
    int64_t signature5;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("2d"),
      },
      cardlib::CCard::kcard_value::kTwo,
      cardlib::CCard::kcard_value::kAce,
      cardlib::CCard::kcard_value::kJack,
      cardlib::CCard::kcard_value::kTen,
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTwo
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
      },
      cardlib::CHand::khand_value::kOnePair,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("3d"),
      },
      cardlib::CCard::kcard_value::kThree,
      cardlib::CCard::kcard_value::kAce,
      cardlib::CCard::kcard_value::kJack,
      cardlib::CCard::kcard_value::kTen,
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpThree
      },
      { cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
      },
      cardlib::CHand::khand_value::kOnePair
    },
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("4d"),
      },
      cardlib::CCard::kcard_value::kJack,
      cardlib::CCard::kcard_value::kAce,
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kFive,
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpFour
      },
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpFive
      },
      cardlib::CHand::khand_value::kOnePair },
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("4d"),
      },
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      -1L, -1L,
      cardlib::CHand::khand_value::kHandValueNull,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  std::map<cardlib::CCard::kcard_value, int64_t>
      value_count_map;
  cardlib::CCard::kcard_value
      shouldbe_toppair, result_toppair;
  cardlib::CCard::kcard_value
      shouldbe_kicker1, result_kicker1;
  cardlib::CCard::kcard_value
      shouldbe_kicker2, result_kicker2;
  cardlib::CCard::kcard_value
      shouldbe_kicker3, result_kicker3;
  int64_t shouldbe7_signature, shouldbe5_signature;
  int64_t result7_signature, result5_signature;
  cardlib::CHand::khand_value
      shouldbe_handvalue, result_handvalue;
  std::string sub_name = {
    static_cast<std::string>("TOnePairFunctionsTest1::")
    + static_cast<std::string>("TestAttemptToFormOnePair1:") };
  std::string error_message, err_start, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    shouldbe_toppair = va_cit->toppair;
    shouldbe_kicker1 = va_cit->kicker1;
    shouldbe_kicker2 = va_cit->kicker2;
    shouldbe_kicker3 = va_cit->kicker3;
    shouldbe7_signature = va_cit->signature7;
    shouldbe5_signature = va_cit->signature5;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    hand_obj.PopulateNthOfAKindMap(
        &input_card_vector, &value_count_map);

    onef_obj.FormOnePair(
        &input_card_vector, &value_count_map,
        &result_toppair, &result_kicker1,
        &result_kicker2, &result_kicker3,
        &result7_signature, &result5_signature,
        &result_handvalue);


    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("top pair shouldbe = ")
        + std::to_string(shouldbe_toppair)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_toppair);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_toppair, result_toppair);

    error_message =
        err_start
        + static_cast<std::string>("kicker1 shouldbe = ")
        + std::to_string(shouldbe_kicker1)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_kicker1);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_kicker1, result_kicker1);

    error_message =
        err_start
        + static_cast<std::string>("kicker2 shouldbe = ")
        + std::to_string(shouldbe_kicker2)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_kicker2);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_kicker2, result_kicker2);

    error_message =
        err_start
        + static_cast<std::string>("kicker3 shouldbe = ")
        + std::to_string(shouldbe_kicker3)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_kicker3);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_kicker3, result_kicker3);

    error_message =
        err_start
        + static_cast<std::string>("7-card signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-card signature shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("hand value shouldbe = ")
        + std::to_string(shouldbe_handvalue)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_handvalue);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TOnePairFunctionsTest1::TestIsOnePair1() {
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::COnePairFunctions onef_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    cardlib::CCard::kcard_value toppair;
    cardlib::CCard::kcard_value kicker1;
    cardlib::CCard::kcard_value kicker2;
    cardlib::CCard::kcard_value kicker3;
    int64_t signature7;
    int64_t signature5;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("2d"),
      },
      cardlib::CCard::kcard_value::kTwo,
      cardlib::CCard::kcard_value::kAce,
      cardlib::CCard::kcard_value::kJack,
      cardlib::CCard::kcard_value::kTen,
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTwo
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
      },
      cardlib::CHand::khand_value::kOnePair,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("3d"),
      },
      cardlib::CCard::kcard_value::kThree,
      cardlib::CCard::kcard_value::kAce,
      cardlib::CCard::kcard_value::kJack,
      cardlib::CCard::kcard_value::kTen,
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpThree
      },
      { cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
      },
      cardlib::CHand::khand_value::kOnePair,
    },
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("4d"),
      },
      cardlib::CCard::kcard_value::kJack,
      cardlib::CCard::kcard_value::kAce,
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kFive,
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpFour
      },
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpFive
      },
      cardlib::CHand::khand_value::kOnePair,
    },
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("4d"),
      },
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      -1L, -1L,
      cardlib::CHand::khand_value::kHandValueNull,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  std::map<cardlib::CCard::kcard_value, int64_t>
      value_count_map;
  cardlib::CCard::kcard_value
      shouldbe_toppair, result_toppair;
  cardlib::CCard::kcard_value
      shouldbe_kicker1, result_kicker1;
  cardlib::CCard::kcard_value
      shouldbe_kicker2, result_kicker2;
  cardlib::CCard::kcard_value
      shouldbe_kicker3, result_kicker3;
  int64_t shouldbe7_signature, shouldbe5_signature;
  int64_t result7_signature, result5_signature;
  cardlib::CHand::khand_value
      shouldbe_handvalue, result_handvalue;
  std::string sub_name = {
    static_cast<std::string>("TOnePairFunctionsTest1::")
    + static_cast<std::string>("TestIsOnePair1:") };
  std::string error_message, err_start, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    shouldbe_toppair = va_cit->toppair;
    shouldbe_kicker1 = va_cit->kicker1;
    shouldbe_kicker2 = va_cit->kicker2;
    shouldbe_kicker3 = va_cit->kicker3;
    shouldbe7_signature = va_cit->signature7;
    shouldbe5_signature = va_cit->signature5;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    hand_obj.PopulateNthOfAKindMap(
        &input_card_vector, &value_count_map);

    onef_obj.IsOnePair(
        &input_card_vector, &value_count_map,
        &result_toppair, &result_kicker1,
        &result_kicker2, &result_kicker3,
        &result7_signature, &result5_signature,
        &result_handvalue);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("top pair shouldbe = ")
        + std::to_string(shouldbe_toppair)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_toppair);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_toppair, result_toppair);

    error_message =
        err_start
        + static_cast<std::string>("kicker1 shouldbe = ")
        + std::to_string(shouldbe_kicker1)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_kicker1);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_kicker1, result_kicker1);

    error_message =
        err_start
        + static_cast<std::string>("kicker2 shouldbe = ")
        + std::to_string(shouldbe_kicker2)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_kicker2);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_kicker2, result_kicker2);

    error_message =
        err_start
        + static_cast<std::string>("kicker3 shouldbe = ")
        + std::to_string(shouldbe_kicker3)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_kicker3);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_kicker3, result_kicker3);

    error_message =
        err_start
        + static_cast<std::string>("7-card signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-card signature shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("hand value shouldbe = ")
        + std::to_string(shouldbe_handvalue)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_handvalue);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TOnePairFunctionsTest1::TestOnePairGreaterOnePair1() {
  cardlib::CCard card_obj;
  cardlib::COnePairFunctions onef_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // top pair differs
    { { cardlib::CCard("3d"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("2d"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("kh"), cardlib::CCard("8s"),
        cardlib::CCard("tc"),
      },
      true,
    },
    // top pair differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("3h"), cardlib::CCard("3d"),
        cardlib::CCard("5s"), cardlib::CCard("9h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("tc"),
      },
      true,
    },
    // first kicker differs
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("jd"),
        cardlib::CCard("ks"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("4h"),
        cardlib::CCard("5c"),
      },
      true,
    },
    // second kicker differs
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("jh"),
        cardlib::CCard("5c"),
      },
      true,
    },
    // third kicker differs
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("qh"),
        cardlib::CCard("5c"),
      },
      true,
    },
    // top kicker differs
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("ks"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("qh"),
        cardlib::CCard("5c"),
      },
      false,
    },
    // second kicker differs
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("9d"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("qh"),
        cardlib::CCard("5c"),
      },
      false,
    },
    // third kicker differs
    { { cardlib::CCard("7h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("qh"),
        cardlib::CCard("5c"),
      },
      false,
    },
    // top pair lesser
    { { cardlib::CCard("7h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("kc"),
        cardlib::CCard("ks"), cardlib::CCard("qh"),
        cardlib::CCard("5c"),
      },
      false
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TOnePairFunctionsTest1::")
    + static_cast<std::string>("TestOnePairGreaterOnePair1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        onef_obj.OnePairGreaterOnePair(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TOnePairFunctionsTest1::TestOnePairLessThanOnePair1() {
  cardlib::CCard card_obj;
  cardlib::COnePairFunctions onef_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // top pair differs
    { { cardlib::CCard("3d"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("7d"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("kh"), cardlib::CCard("8s"),
        cardlib::CCard("tc"),
      },
      true,
    },
    // top pair differs
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("3h"), cardlib::CCard("3d"),
        cardlib::CCard("5s"), cardlib::CCard("9h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("jc"),
      },
      true,
    },
    // first kicker differs
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("jd"),
        cardlib::CCard("ks"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("4h"),
        cardlib::CCard("5c"),
      },
      false,
    },
    // second kicker differs
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("kh"),
        cardlib::CCard("5c"),
      },
      true,
    },
    // third kicker differs
    { { cardlib::CCard("9h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("jd"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("qh"),
        cardlib::CCard("5c"),
      },
      true,
    },
    // top kicker differs
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("ks"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("qh"),
        cardlib::CCard("5c"),
      },
      true,
    },
    // second kicker differs
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("9d"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("qh"),
        cardlib::CCard("5c"),
      },
      true,
    },
    // third kicker differs
    { { cardlib::CCard("7h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("qh"),
        cardlib::CCard("5c"),
      },
      true,
    },
    // top pair lesser
    { { cardlib::CCard("7h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("kc"),
        cardlib::CCard("ks"), cardlib::CCard("qh"),
        cardlib::CCard("5c"),
      },
      true,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TOnePairFunctionsTest1::")
    + static_cast<std::string>("TestOnePairLessThanOnePair1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        onef_obj.OnePairLessThanOnePair(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TOnePairFunctionsTest1::TestOnePairEqualOnePair1() {
  cardlib::CCard card_obj;
  cardlib::COnePairFunctions onef_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // top pair differs
    { { cardlib::CCard("3d"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("2d"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("kh"), cardlib::CCard("8s"),
        cardlib::CCard("tc"),
      },
      false,
    },
    // top pair same
    { { cardlib::CCard("2h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("9h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("3h"), cardlib::CCard("td"),
        cardlib::CCard("5s"), cardlib::CCard("9h"),
        cardlib::CCard("as"), cardlib::CCard("jd"),
        cardlib::CCard("tc"),
      },
      true,
    },
    // first kicker differs
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("jd"),
        cardlib::CCard("ks"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("4h"),
        cardlib::CCard("5c"),
      },
      false,
    },
    // second kicker differs
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("jh"),
        cardlib::CCard("5c"),
      },
      false,
    },
    // third kicker differs
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("qh"),
        cardlib::CCard("5c"),
      },
      false,
    },
    // top kicker differs
    { { cardlib::CCard("jh"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("ks"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("qh"),
        cardlib::CCard("5c"),
      },
      false,
    },
    // 6th card differs
    { { cardlib::CCard("7h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("qh"),
        cardlib::CCard("as"), cardlib::CCard("9d"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("6h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("qh"),
        cardlib::CCard("5c"),
      },
      true,
    },
    // 7th card differs
    { { cardlib::CCard("7h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("9h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("tc"),
        cardlib::CCard("ts"), cardlib::CCard("qh"),
        cardlib::CCard("4c"),
      },
      true,
    },
    // top pair differs
    { { cardlib::CCard("7h"), cardlib::CCard("tc"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("as"), cardlib::CCard("qd"),
        cardlib::CCard("td"),
      },
      { cardlib::CCard("7h"), cardlib::CCard("9d"),
        cardlib::CCard("as"), cardlib::CCard("kc"),
        cardlib::CCard("ks"), cardlib::CCard("qh"),
        cardlib::CCard("5c"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TOnePairFunctionsTest1::")
    + static_cast<std::string>("TestOnePairEqualOnePair1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        onef_obj.OnePairEqualOnePair(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

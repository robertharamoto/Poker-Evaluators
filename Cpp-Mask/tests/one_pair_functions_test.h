#ifndef TESTS_ONE_PAIR_FUNCTIONS_TEST_H_
#define TESTS_ONE_PAIR_FUNCTIONS_TEST_H_

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TOnePairFunctionsTest1 definition                     ###
  ###    namespace HandTest                                  ###
  ###                                                        ###
  ###  last updated April 16, 2024                           ###
  ###    new card mask definition                            ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 18, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cppunit/extensions/HelperMacros.h>

#include <algorithm>
#include <string>
#include <vector>

#include "sources/card.h"

namespace handtest {

class TOnePairFunctionsTest1 : public CppUnit::TestFixture  {
  CPPUNIT_TEST_SUITE(TOnePairFunctionsTest1);

  CPPUNIT_TEST(TestFormOnePair1);
  CPPUNIT_TEST(TestIsOnePair1);

  CPPUNIT_TEST(TestOnePairGreaterOnePair1);
  CPPUNIT_TEST(TestOnePairLessThanOnePair1);
  CPPUNIT_TEST(TestOnePairEqualOnePair1);

  CPPUNIT_TEST_SUITE_END();

 public:
  TOnePairFunctionsTest1();
  ~TOnePairFunctionsTest1();

  void setUp();
  void tearDown();

  void TestFormOnePair1();
  void TestIsOnePair1();

  void TestOnePairGreaterOnePair1();
  void TestOnePairLessThanOnePair1();
  void TestOnePairEqualOnePair1();
};  // class TOnePairFunctionsTest1

}  // namespace handtest

#endif  // TESTS_ONE_PAIR_FUNCTIONS_TEST_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

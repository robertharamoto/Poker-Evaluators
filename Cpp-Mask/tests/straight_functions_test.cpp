/*

// Copyright 2024 robert haramoto

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TStraightFunctionsTest1 implementation                ###
  ###    namespace HandTest                                  ###
  ###                                                        ###
  ###  last updated April 18, 2024                           ###
  ###    new card mask definition                            ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 19, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/straight_functions_test.h"

#include <cppunit/SourceLine.h>

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cctype>       // std::tolower

#include <algorithm>
#include <string>
#include <vector>
#include <map>

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/straight_functions.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::TStraightFunctionsTest1);

namespace handtest {

// #############################################################
// #############################################################
TStraightFunctionsTest1::TStraightFunctionsTest1() {
}

// #############################################################
// #############################################################
TStraightFunctionsTest1::~TStraightFunctionsTest1() {
}

// #############################################################
// #############################################################
void TStraightFunctionsTest1::setUp() {
}

// #############################################################
// #############################################################
void TStraightFunctionsTest1::tearDown() {
}

// #############################################################
// #############################################################
void TStraightFunctionsTest1::TestIsStraight1() {
  cardlib::CCard card_obj;
  cardlib::CStraightFunctions straightf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    cardlib::CHand::kstraight_mask shouldbe_mask;
    int64_t shouldbe_order;
    int64_t shouldbe7_signature;
    int64_t shouldbe5_signature;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    // five-high straight
    { { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("ad"),
      },
      cardlib::CHand::kstraight_mask::kFiveHighStraight,
      { cardlib::CCard("5c").GetCardOrder()
        | cardlib::CCard("4d").GetCardOrder()
        | cardlib::CCard("3c").GetCardOrder()
        | cardlib::CCard("2h").GetCardOrder()
        | cardlib::CCard("ad").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpAce
      },
      cardlib::CHand::khand_value::kStraight,
    },
    // six-high straight
    { { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("6s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("ad"),
      },
      cardlib::CHand::kstraight_mask::kSixHighStraight,
      { cardlib::CCard("6s").GetCardOrder()
        | cardlib::CCard("5c").GetCardOrder()
        | cardlib::CCard("4d").GetCardOrder()
        | cardlib::CCard("3c").GetCardOrder()
        | cardlib::CCard("2h").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTwo
      },
      cardlib::CHand::khand_value::kStraight,
    },
    // seven-high straight
    { { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("6s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("3h"),
        cardlib::CCard("7d"),
      },
      cardlib::CHand::kstraight_mask::kSevenHighStraight,
      { cardlib::CCard("7d").GetCardOrder()
        | cardlib::CCard("6s").GetCardOrder()
        | cardlib::CCard("5c").GetCardOrder()
        | cardlib::CCard("4d").GetCardOrder()
        | cardlib::CCard("3c").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpSeven
      },
      { cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpThree
      },
      cardlib::CHand::khand_value::kStraight,
    },
    // eight-high straight
    { { cardlib::CCard("7h"), cardlib::CCard("3c"),
        cardlib::CCard("6s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("8h"),
        cardlib::CCard("ad"),
      },
      cardlib::CHand::kstraight_mask::kEightHighStraight,
      { cardlib::CCard("8h").GetCardOrder()
        | cardlib::CCard("7h").GetCardOrder()
        | cardlib::CCard("6s").GetCardOrder()
        | cardlib::CCard("5c").GetCardOrder()
        | cardlib::CCard("4d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpAce },
      { cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpFour
      },
      cardlib::CHand::khand_value::kStraight,
    },
    // nine-high straight
    { { cardlib::CCard("9d"), cardlib::CCard("8s"),
        cardlib::CCard("6s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("7h"),
        cardlib::CCard("ad"),
      },
      cardlib::CHand::kstraight_mask::kNineHighStraight,
      { cardlib::CCard("9d").GetCardOrder()
        | cardlib::CCard("8s").GetCardOrder()
        | cardlib::CCard("7h").GetCardOrder()
        | cardlib::CCard("6s").GetCardOrder()
        | cardlib::CCard("5c").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFive
      },
      cardlib::CHand::khand_value::kStraight,
    },
    // ten-high straight
    { { cardlib::CCard("9h"), cardlib::CCard("7c"),
        cardlib::CCard("6s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("8h"),
        cardlib::CCard("td"),
      },
      cardlib::CHand::kstraight_mask::kTenHighStraight,
      { cardlib::CCard("td").GetCardOrder()
        | cardlib::CCard("9h").GetCardOrder()
        | cardlib::CCard("8h").GetCardOrder()
        | cardlib::CCard("7c").GetCardOrder()
        | cardlib::CCard("6s").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
        * cardlib::CCard::kprime_value::kpFour
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpTen
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpSix
      },
      cardlib::CHand::khand_value::kStraight,
    },
    // jack-high straight
    { { cardlib::CCard("7h"), cardlib::CCard("3c"),
        cardlib::CCard("8s"), cardlib::CCard("td"),
        cardlib::CCard("9c"), cardlib::CCard("jh"),
        cardlib::CCard("ad"),
      },
      cardlib::CHand::kstraight_mask::kJackHighStraight,
      { cardlib::CCard("jh").GetCardOrder()
        | cardlib::CCard("td").GetCardOrder()
        | cardlib::CCard("9c").GetCardOrder()
        | cardlib::CCard("8s").GetCardOrder()
        | cardlib::CCard("7h").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kStraight,
    },
    // queen-high straight
    { { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("9s"), cardlib::CCard("8d"),
        cardlib::CCard("jc"), cardlib::CCard("th"),
        cardlib::CCard("qs"),
      },
      cardlib::CHand::kstraight_mask::kQueenHighStraight,
      { cardlib::CCard("qs").GetCardOrder()
        | cardlib::CCard("jc").GetCardOrder()
        | cardlib::CCard("th").GetCardOrder()
        | cardlib::CCard("9s").GetCardOrder()
        | cardlib::CCard("8d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpQueen
      },
      { cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
      },
      cardlib::CHand::khand_value::kStraight,
    },
    // king-high straight
    { { cardlib::CCard("9h"), cardlib::CCard("tc"),
        cardlib::CCard("js"), cardlib::CCard("qd"),
        cardlib::CCard("kc"), cardlib::CCard("3h"),
        cardlib::CCard("8d"),
      },
      cardlib::CHand::kstraight_mask::kKingHighStraight,
      { cardlib::CCard("kc").GetCardOrder()
        | cardlib::CCard("qd").GetCardOrder()
        | cardlib::CCard("js").GetCardOrder()
        | cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("9h").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpEight
      },
      { cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
      },
      cardlib::CHand::khand_value::kStraight,
    },
    // ace-high straight
    { { cardlib::CCard("9h"), cardlib::CCard("3c"),
        cardlib::CCard("ts"), cardlib::CCard("jd"),
        cardlib::CCard("kc"), cardlib::CCard("qh"),
        cardlib::CCard("ad"), },
      cardlib::CHand::kstraight_mask::kAceHighStraight,
      { cardlib::CCard("ad").GetCardOrder()
        | cardlib::CCard("kc").GetCardOrder()
        | cardlib::CCard("qh").GetCardOrder()
        | cardlib::CCard("jd").GetCardOrder()
        | cardlib::CCard("ts").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
      },
      cardlib::CHand::khand_value::kStraight,
    },
    // no straight
    { { cardlib::CCard("9h"), cardlib::CCard("3c"),
        cardlib::CCard("8s"), cardlib::CCard("jd"),
        cardlib::CCard("kc"), cardlib::CCard("qh"),
        cardlib::CCard("ad"),
      },
      cardlib::CHand::kstraight_mask::kStraightValueNull,
      -1L,
      cardlib::CHand::kstraight_signature::kStraightSignatureNull,
      cardlib::CHand::kstraight_signature::kStraightSignatureNull,
      cardlib::CHand::khand_value::kHandValueNull,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  cardlib::CHand::kstraight_mask shouldbe_mask, result_mask;
  int64_t shouldbe_order, result_order;
  int64_t shouldbe7_signature, result7_signature;
  int64_t shouldbe5_signature, result5_signature;
  cardlib::CHand::khand_value
      shouldbe_handvalue, result_handvalue;
  std::string sub_name = {
    static_cast<std::string>("TStraightFunctionsTest1::")
    + static_cast<std::string>("TestIsStraight1:") };
  std::string error_message, err_start, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    shouldbe_mask = va_cit->shouldbe_mask;
    shouldbe_order = va_cit->shouldbe_order;
    shouldbe7_signature = va_cit->shouldbe7_signature;
    shouldbe5_signature = va_cit->shouldbe5_signature;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    straightf_obj.IsStraight(
        &input_card_vector, &result_mask,
        &result_order, &result7_signature,
        &result5_signature, &result_handvalue);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("straight mask shouldbe = ")
        + std::to_string(shouldbe_mask)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_mask);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_mask, result_mask);

    error_message =
        err_start
        + static_cast<std::string>("straight order shouldbe = ")
        + std::to_string(shouldbe_order)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_order);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_order, result_order);

    error_message =
        err_start
        + static_cast<std::string>("7-card signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-card signature shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("hand value shouldbe = ")
        + std::to_string(shouldbe_handvalue)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_handvalue);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TStraightFunctionsTest1::TestStraightGreaterStraight1() {
  cardlib::CCard card_obj;
  cardlib::CStraightFunctions straightf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("6h"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("5c"), cardlib::CCard("4d"),
        cardlib::CCard("3c"), cardlib::CCard("2h"),
        cardlib::CCard("kc"), cardlib::CCard("3h"),
        cardlib::CCard("ad"),
      },
      true,
    },
    { { cardlib::CCard("7h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("6h"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("3c"), cardlib::CCard("4h"),
        cardlib::CCard("kc"), cardlib::CCard("6h"),
        cardlib::CCard("2d"),
      },
      true,
    },
    { { cardlib::CCard("7h"), cardlib::CCard("3c"),
        cardlib::CCard("8s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("6h"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("3c"), cardlib::CCard("4h"),
        cardlib::CCard("kc"), cardlib::CCard("6h"),
        cardlib::CCard("2d"),
      },
      true,
    },
    { { cardlib::CCard("qh"), cardlib::CCard("jc"),
        cardlib::CCard("ks"), cardlib::CCard("td"),
        cardlib::CCard("5c"), cardlib::CCard("9h"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7c"), cardlib::CCard("4h"),
        cardlib::CCard("kc"), cardlib::CCard("6h"),
        cardlib::CCard("2d"),
      },
      true,
    },
    { { cardlib::CCard("qh"), cardlib::CCard("jc"),
        cardlib::CCard("ks"), cardlib::CCard("td"),
        cardlib::CCard("5c"), cardlib::CCard("9h"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("qh"), cardlib::CCard("jc"),
        cardlib::CCard("ks"), cardlib::CCard("td"),
        cardlib::CCard("5c"), cardlib::CCard("9h"),
        cardlib::CCard("ad"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TStraightFunctionsTest1::")
    + static_cast<std::string>("TestStraightGreaterStraight1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        straightf_obj.StraightGreaterStraight(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TStraightFunctionsTest1::TestStraightLessThanStraight1() {
  cardlib::CCard card_obj;
  cardlib::CStraightFunctions straightf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("8h"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("5c"), cardlib::CCard("4d"),
        cardlib::CCard("3c"), cardlib::CCard("2h"),
        cardlib::CCard("6s"), cardlib::CCard("3h"),
        cardlib::CCard("ad"),
      },
      true,
    },
    { { cardlib::CCard("jh"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("kh"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("3c"), cardlib::CCard("4h"),
        cardlib::CCard("kc"), cardlib::CCard("6h"),
        cardlib::CCard("2d"),
      },
      true,
    },
    { { cardlib::CCard("7h"), cardlib::CCard("3c"),
        cardlib::CCard("8s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("6h"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("3c"), cardlib::CCard("4h"),
        cardlib::CCard("kc"), cardlib::CCard("6h"),
        cardlib::CCard("2d"),
      },
      false,
    },
    { { cardlib::CCard("qh"), cardlib::CCard("jc"),
        cardlib::CCard("ks"), cardlib::CCard("td"),
        cardlib::CCard("5c"), cardlib::CCard("9h"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7s"), cardlib::CCard("4h"),
        cardlib::CCard("kc"), cardlib::CCard("6h"),
        cardlib::CCard("2d"),
      },
      false,
    },
    { { cardlib::CCard("qh"), cardlib::CCard("jc"),
        cardlib::CCard("ks"), cardlib::CCard("td"),
        cardlib::CCard("5c"), cardlib::CCard("9h"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("qh"), cardlib::CCard("jc"),
        cardlib::CCard("ks"), cardlib::CCard("td"),
        cardlib::CCard("5c"), cardlib::CCard("9h"),
        cardlib::CCard("ad"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TStraightFunctionsTest1::")
    + static_cast<std::string>("TestStraightLessThanStraight1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        straightf_obj.StraightLessThanStraight(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TStraightFunctionsTest1::TestStraightEqualStraight1() {
  cardlib::CCard card_obj;
  cardlib::CStraightFunctions straightf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("6h"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("5c"), cardlib::CCard("4d"),
        cardlib::CCard("3c"), cardlib::CCard("2h"),
        cardlib::CCard("kc"), cardlib::CCard("3h"),
        cardlib::CCard("6d"),
      },
      true,
    },
    { { cardlib::CCard("7h"), cardlib::CCard("3c"),
        cardlib::CCard("2s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("6h"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("3c"), cardlib::CCard("4h"),
        cardlib::CCard("7s"), cardlib::CCard("6h"),
        cardlib::CCard("2d"),
      },
      true,
    },
    { { cardlib::CCard("7h"), cardlib::CCard("3c"),
        cardlib::CCard("8s"), cardlib::CCard("4d"),
        cardlib::CCard("5c"), cardlib::CCard("6h"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("3c"), cardlib::CCard("4h"),
        cardlib::CCard("kc"), cardlib::CCard("6h"),
        cardlib::CCard("2d"),
      },
      false,
    },
    { { cardlib::CCard("qh"), cardlib::CCard("jc"),
        cardlib::CCard("ks"), cardlib::CCard("td"),
        cardlib::CCard("5c"), cardlib::CCard("9h"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7c"), cardlib::CCard("4h"),
        cardlib::CCard("kc"), cardlib::CCard("6h"),
        cardlib::CCard("2d"),
      },
      false,
    },
    { { cardlib::CCard("qh"), cardlib::CCard("jc"),
        cardlib::CCard("ks"), cardlib::CCard("td"),
        cardlib::CCard("5c"), cardlib::CCard("9h"),
        cardlib::CCard("ad"),
      },
      { cardlib::CCard("qh"), cardlib::CCard("jc"),
        cardlib::CCard("ks"), cardlib::CCard("td"),
        cardlib::CCard("5c"), cardlib::CCard("9h"),
        cardlib::CCard("ad"),
      },
      true,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TStraightFunctionsTest1::")
    + static_cast<std::string>("TestStraightEqualStraight1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        straightf_obj.StraightEqualStraight(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

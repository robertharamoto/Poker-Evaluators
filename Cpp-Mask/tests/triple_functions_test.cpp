/*

// Copyright 2024 robert haramoto

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TTripleFunctionsTest1 implementation                  ###
  ###    namespace HandTest                                  ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###    new card mask definition                            ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 19, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/triple_functions_test.h"

#include <cppunit/SourceLine.h>

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cctype>       // std::tolower

#include <algorithm>
#include <string>
#include <vector>
#include <map>

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/triple_functions.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::TTripleFunctionsTest1);

namespace handtest {

// #############################################################
// #############################################################
TTripleFunctionsTest1::TTripleFunctionsTest1() {
}

// #############################################################
// #############################################################
TTripleFunctionsTest1::~TTripleFunctionsTest1() {
}

// #############################################################
// #############################################################
void TTripleFunctionsTest1::setUp() {
}

// #############################################################
// #############################################################
void TTripleFunctionsTest1::tearDown() {
}

// #############################################################
// #############################################################
void TTripleFunctionsTest1::TestFormTriplesHand1() {
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CTripleFunctions tripf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    cardlib::CCard::kcard_value triple_value;
    cardlib::CCard::kcard_value kicker1_value;
    cardlib::CCard::kcard_value kicker2_value;
    int64_t prime7_signature;
    int64_t prime5_signature;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("2s"),
      },
      cardlib::CCard::kcard_value::kTwo,
      cardlib::CCard::kcard_value::kNine,
      cardlib::CCard::kcard_value::kSeven,
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpTwo
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kTriples,
    },
    { { cardlib::CCard("3c"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("3s"),
      },
      cardlib::CCard::kcard_value::kThree,
      cardlib::CCard::kcard_value::kNine,
      cardlib::CCard::kcard_value::kSeven,
      { cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpThree
      },
      { cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kTriples,
    },
    { { cardlib::CCard("tc"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("td"),
        cardlib::CCard("7d"), cardlib::CCard("th"),
        cardlib::CCard("as"),
      },
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kAce,
      cardlib::CCard::kcard_value::kSeven,
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kTriples,
    },
    { { cardlib::CCard("tc"), cardlib::CCard("kh"),
        cardlib::CCard("5c"), cardlib::CCard("td"),
        cardlib::CCard("7d"), cardlib::CCard("th"),
        cardlib::CCard("as"),
      },
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kAce,
      cardlib::CCard::kcard_value::kKing,
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpKing
      },
      cardlib::CHand::khand_value::kTriples,
    },
    { { cardlib::CCard("tc"), cardlib::CCard("kh"),
        cardlib::CCard("5c"), cardlib::CCard("td"),
        cardlib::CCard("7d"), cardlib::CCard("7h"),
        cardlib::CCard("as"),
      },
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      -1L, -1L,
      cardlib::CHand::khand_value::kHandValueNull,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  std::map<cardlib::CCard::kcard_value, int64_t>
      value_count_map;
  cardlib::CCard::kcard_value
      shouldbe_triples, result_triples;
  cardlib::CCard::kcard_value
      shouldbe_kicker1, result_kicker1;
  cardlib::CCard::kcard_value
      shouldbe_kicker2, result_kicker2;
  int64_t shouldbe7_signature, shouldbe5_signature;
  int64_t result7_signature, result5_signature;
  cardlib::CHand::khand_value
      shouldbe_handvalue, result_handvalue;
  std::string sub_name = {
    static_cast<std::string>("TTripleFunctionsTest1::")
    + static_cast<std::string>("TestAddOnlyHandCards1:") };
  std::string error_message, err_start, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    shouldbe_triples = va_cit->triple_value;
    shouldbe_kicker1 = va_cit->kicker1_value;
    shouldbe_kicker2 = va_cit->kicker2_value;
    shouldbe7_signature = va_cit->prime7_signature;
    shouldbe5_signature = va_cit->prime5_signature;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    hand_obj.PopulateNthOfAKindMap(
        &input_card_vector, &value_count_map);

    tripf_obj.FormTriplesHand(
        &input_card_vector, &value_count_map,
        &result_triples, &result_kicker1,
        &result_kicker2,
        &result7_signature, &result5_signature,
        &result_handvalue);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("three-of-a-kind shouldbe = ")
        + std::to_string(shouldbe_triples)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_triples);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_triples, result_triples);

    error_message =
        err_start
        + static_cast<std::string>("top kicker shouldbe = ")
        + std::to_string(shouldbe_kicker1)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_kicker1);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_kicker1, result_kicker1);

    error_message =
        err_start
        + static_cast<std::string>("second kicker shouldbe = ")
        + std::to_string(shouldbe_kicker2)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_kicker2);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_kicker2, result_kicker2);

    error_message =
        err_start
        + static_cast<std::string>("7-card signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-card signature shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("hand value shouldbe = ")
        + std::to_string(shouldbe_handvalue)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_handvalue);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TTripleFunctionsTest1::TestIsThreeOfAKind1() {
  cardlib::CCard card_obj;
  cardlib::CHand hand_obj;
  cardlib::CTripleFunctions tripf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    cardlib::CCard::kcard_value triple_value;
    cardlib::CCard::kcard_value kicker1_value;
    cardlib::CCard::kcard_value kicker2_value;
    int64_t prime7_signature;
    int64_t prime5_signature;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("2s"),
      },
      cardlib::CCard::kcard_value::kTwo,
      cardlib::CCard::kcard_value::kNine,
      cardlib::CCard::kcard_value::kSeven,
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpTwo
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kTriples,
    },
    { { cardlib::CCard("3c"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("3s"),
      },
      cardlib::CCard::kcard_value::kThree,
      cardlib::CCard::kcard_value::kNine,
      cardlib::CCard::kcard_value::kSeven,
      { cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpThree
      },
      { cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kTriples,
    },
    { { cardlib::CCard("tc"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("td"),
        cardlib::CCard("7d"), cardlib::CCard("th"),
        cardlib::CCard("as"),
      },
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kAce,
      cardlib::CCard::kcard_value::kSeven,
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpSeven
      },
      cardlib::CHand::khand_value::kTriples,
    },
    { { cardlib::CCard("tc"), cardlib::CCard("kh"),
        cardlib::CCard("5c"), cardlib::CCard("td"),
        cardlib::CCard("7d"), cardlib::CCard("th"),
        cardlib::CCard("as"),
      },
      cardlib::CCard::kcard_value::kTen,
      cardlib::CCard::kcard_value::kAce,
      cardlib::CCard::kcard_value::kKing,
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpAce
      },
      { cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpKing
      },
      cardlib::CHand::khand_value::kTriples },
    { { cardlib::CCard("tc"), cardlib::CCard("kh"),
        cardlib::CCard("5c"), cardlib::CCard("td"),
        cardlib::CCard("7d"), cardlib::CCard("7h"),
        cardlib::CCard("as"),
      },
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      cardlib::CCard::kcard_value::kValueNull,
      -1L, -1L,
      cardlib::CHand::khand_value::kHandValueNull
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  std::map<cardlib::CCard::kcard_value, int64_t>
      value_count_map;
  cardlib::CCard::kcard_value
      shouldbe_triples, result_triples;
  cardlib::CCard::kcard_value
      shouldbe_kicker1, result_kicker1;
  cardlib::CCard::kcard_value
      shouldbe_kicker2, result_kicker2;
  int64_t shouldbe7_signature, shouldbe5_signature;
  int64_t result7_signature, result5_signature;
  cardlib::CHand::khand_value
      shouldbe_handvalue, result_handvalue;
  std::string sub_name = {
    static_cast<std::string>("TTripleFunctionsTest1::")
    + static_cast<std::string>("TestIsThreeOfAKind1:") };
  std::string error_message, err_start, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    shouldbe_triples = va_cit->triple_value;
    shouldbe_kicker1 = va_cit->kicker1_value;
    shouldbe_kicker2 = va_cit->kicker2_value;
    shouldbe7_signature = va_cit->prime7_signature;
    shouldbe5_signature = va_cit->prime5_signature;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    value_count_map.clear();
    hand_obj.PopulateNthOfAKindMap(
        &input_card_vector, &value_count_map);

    tripf_obj.IsThreeOfAKind(
        &input_card_vector, &value_count_map,
        &result_triples, &result_kicker1,
        &result_kicker2,
        &result7_signature, &result5_signature,
        &result_handvalue);


    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("three-of-a-kind shouldbe = ")
        + std::to_string(shouldbe_triples)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_triples);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_triples, result_triples);

    error_message =
        err_start
        + static_cast<std::string>("top kicker shouldbe = ")
        + std::to_string(shouldbe_kicker1)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_kicker1);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_kicker1, result_kicker1);

    error_message =
        err_start
        + static_cast<std::string>("second kicker shouldbe = ")
        + std::to_string(shouldbe_kicker2)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_kicker2);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_kicker2, result_kicker2);

    error_message =
        err_start
        + static_cast<std::string>("7-card signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-card signature shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("hand value shouldbe = ")
        + std::to_string(shouldbe_handvalue)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_handvalue);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TTripleFunctionsTest1::TestTripleGreaterTriple1() {
  cardlib::CCard card_obj;
  cardlib::CTripleFunctions tripf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("3s"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("2s"),
      },
      true,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("7h"),
        cardlib::CCard("5c"), cardlib::CCard("7d"),
        cardlib::CCard("6d"), cardlib::CCard("jh"),
        cardlib::CCard("7s"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("7h"),
        cardlib::CCard("6d"), cardlib::CCard("7d"),
        cardlib::CCard("7s"), cardlib::CCard("th"),
        cardlib::CCard("9s"),
      },
      true,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("kh"),
        cardlib::CCard("5c"), cardlib::CCard("kd"),
        cardlib::CCard("8d"), cardlib::CCard("th"),
        cardlib::CCard("ks"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("kh"),
        cardlib::CCard("7d"), cardlib::CCard("kd"),
        cardlib::CCard("ks"), cardlib::CCard("th"),
        cardlib::CCard("3s"),
      },
      true,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("5c"), cardlib::CCard("qd"),
        cardlib::CCard("6d"), cardlib::CCard("th"),
        cardlib::CCard("qs"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("kh"),
        cardlib::CCard("7d"), cardlib::CCard("kd"),
        cardlib::CCard("ks"), cardlib::CCard("th"),
        cardlib::CCard("2s"),
      },
      false,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("5c"), cardlib::CCard("qd"),
        cardlib::CCard("6d"), cardlib::CCard("th"),
        cardlib::CCard("qs"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("7d"), cardlib::CCard("qd"),
        cardlib::CCard("qs"), cardlib::CCard("9h"),
        cardlib::CCard("js"),
      },
      false,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("5c"), cardlib::CCard("qd"),
        cardlib::CCard("6d"), cardlib::CCard("th"),
        cardlib::CCard("qs"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("6d"), cardlib::CCard("qd"),
        cardlib::CCard("qs"), cardlib::CCard("th"),
        cardlib::CCard("3s"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TTripleFunctionsTest1::")
    + static_cast<std::string>("TestTripleGreaterTriple1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        tripf_obj.TripleGreaterTriple(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TTripleFunctionsTest1::TestTripleLessThanTriple1() {
  cardlib::CCard card_obj;
  cardlib::CTripleFunctions tripf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("3s"),
      },
      { cardlib::CCard("4h"), cardlib::CCard("4s"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("4s"),
      },
      true,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("7h"),
        cardlib::CCard("5c"), cardlib::CCard("7d"),
        cardlib::CCard("6d"), cardlib::CCard("9h"),
        cardlib::CCard("7s"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("7h"),
        cardlib::CCard("6d"), cardlib::CCard("7d"),
        cardlib::CCard("7s"), cardlib::CCard("th"),
        cardlib::CCard("9s"),
      },
      true,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("kh"),
        cardlib::CCard("5c"), cardlib::CCard("kd"),
        cardlib::CCard("6d"), cardlib::CCard("th"),
        cardlib::CCard("ks"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("kh"),
        cardlib::CCard("7d"), cardlib::CCard("kd"),
        cardlib::CCard("ks"), cardlib::CCard("th"),
        cardlib::CCard("3s"),
      },
      true,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("5c"), cardlib::CCard("qd"),
        cardlib::CCard("6d"), cardlib::CCard("th"),
        cardlib::CCard("qs"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("kh"),
        cardlib::CCard("7d"), cardlib::CCard("kd"),
        cardlib::CCard("ks"), cardlib::CCard("th"),
        cardlib::CCard("2s"),
      },
      true,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("5c"), cardlib::CCard("qd"),
        cardlib::CCard("6d"), cardlib::CCard("th"),
        cardlib::CCard("qs"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("7d"), cardlib::CCard("qd"),
        cardlib::CCard("qs"), cardlib::CCard("9h"),
        cardlib::CCard("js"),
      },
      true,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("5c"), cardlib::CCard("qd"),
        cardlib::CCard("7d"), cardlib::CCard("th"),
        cardlib::CCard("qs"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("7d"), cardlib::CCard("qd"),
        cardlib::CCard("qs"), cardlib::CCard("th"),
        cardlib::CCard("3s"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TTripleFunctionsTest1::")
    + static_cast<std::string>("TestTripleLessThanTriple1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        tripf_obj.TripleLessThanTriple(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TTripleFunctionsTest1::TestTripleEqualTriple1() {
  cardlib::CCard card_obj;
  cardlib::CTripleFunctions tripf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("3d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("3s"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5c"), cardlib::CCard("2d"),
        cardlib::CCard("7d"), cardlib::CCard("9h"),
        cardlib::CCard("2s"),
      },
      false,
    },
    { { cardlib::CCard("th"), cardlib::CCard("7h"),
        cardlib::CCard("5c"), cardlib::CCard("7d"),
        cardlib::CCard("6d"), cardlib::CCard("jh"),
        cardlib::CCard("7s"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("7h"),
        cardlib::CCard("6d"), cardlib::CCard("7d"),
        cardlib::CCard("7s"), cardlib::CCard("th"),
        cardlib::CCard("js"),
      },
      true,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("kh"),
        cardlib::CCard("5c"), cardlib::CCard("kd"),
        cardlib::CCard("6d"), cardlib::CCard("th"),
        cardlib::CCard("ks"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("kh"),
        cardlib::CCard("7d"), cardlib::CCard("kd"),
        cardlib::CCard("ks"), cardlib::CCard("th"),
        cardlib::CCard("2s"),
      },
      false,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("5c"), cardlib::CCard("qd"),
        cardlib::CCard("6d"), cardlib::CCard("th"),
        cardlib::CCard("qs"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("kh"),
        cardlib::CCard("7d"), cardlib::CCard("kd"),
        cardlib::CCard("ks"), cardlib::CCard("th"),
        cardlib::CCard("2s"),
      },
      false,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("5c"), cardlib::CCard("qd"),
        cardlib::CCard("6d"), cardlib::CCard("th"),
        cardlib::CCard("qs"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("7d"), cardlib::CCard("qd"),
        cardlib::CCard("qs"), cardlib::CCard("9h"),
        cardlib::CCard("js"),
      },
      false,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("5c"), cardlib::CCard("qd"),
        cardlib::CCard("7d"), cardlib::CCard("th"),
        cardlib::CCard("qs"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("qh"),
        cardlib::CCard("7d"), cardlib::CCard("qd"),
        cardlib::CCard("qs"), cardlib::CCard("th"),
        cardlib::CCard("3s"),
      },
      true,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TTripleFunctionsTest1::")
    + static_cast<std::string>("TestTripleEqualTriple1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        tripf_obj.TripleEqualTriple(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#ifndef TESTS_CARD_TEST_H_
#define TESTS_CARD_TEST_H_

/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TCardTest definition                                  ###
  ###                                                        ###
  ###  last updated March 31, 2024                           ###
  ###    changed definitions of bitmask                      ###
  ###    fixed updated cpplint.py errors                     ###
  ###                                                        ###
  ###  last updated July 17, 2022                            ###
  ###                                                        ###
  ###  updated January 21, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cppunit/extensions/HelperMacros.h>

#include <algorithm>
#include <string>

#include "sources/card.h"

namespace handtest {

class TCardTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(TCardTest);

  CPPUNIT_TEST(TestConstructor);
  CPPUNIT_TEST(TestSet1);
  CPPUNIT_TEST(TestGet1);
  CPPUNIT_TEST(TestGetMask1);
  CPPUNIT_TEST(TestToLower1);
  CPPUNIT_TEST(TestEqual1);
  CPPUNIT_TEST(TestOperators1);
  CPPUNIT_TEST(TestStringToCard1);
  CPPUNIT_TEST(TestCardToString1);

  CPPUNIT_TEST_SUITE_END();

 public:
  TCardTest();

  void setUp();
  void tearDown();

  void TestConstructor();

  void TestSet1();
  void TestGet1();
  void TestGetMask1();
  void TestToLower1();
  void TestEqual1();
  void TestOperators1();
  void TestStringToCard1();
  void TestCardToString1();
};  // class TCardTest

}  // namespace handtest

#endif  // TESTS_CARD_TEST_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

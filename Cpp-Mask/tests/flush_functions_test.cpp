/*

// Copyright 2024 robert haramoto

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  TFlushFunctionsTest1 implementation                   ###
  ###    namespace HandTest                                  ###
  ###                                                        ###
  ###  last updated April 17, 2024                           ###
  ###    new card mask definition                            ###
  ###                                                        ###
  ###  updated July 17, 2022                                 ###
  ###                                                        ###
  ###  updated January 19, 2015                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/flush_functions_test.h"

#include <cppunit/SourceLine.h>

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cctype>       // std::tolower

#include <algorithm>
#include <string>
#include <vector>
#include <map>

#include "sources/card.h"
#include "sources/hand.h"
#include "sources/flush_functions.h"

CPPUNIT_TEST_SUITE_REGISTRATION(handtest::TFlushFunctionsTest1);

namespace handtest {

// #############################################################
// #############################################################
TFlushFunctionsTest1::TFlushFunctionsTest1() {
}

// #############################################################
// #############################################################
TFlushFunctionsTest1::~TFlushFunctionsTest1() {
}

// #############################################################
// #############################################################
void TFlushFunctionsTest1::setUp() {
}

// #############################################################
// #############################################################
void TFlushFunctionsTest1::tearDown() {
}

// #############################################################
// #############################################################
void TFlushFunctionsTest1::TestCanFormAFlush1() {
  cardlib::CCard card_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    std::map<cardlib::CCard::kcard_suit, int64_t>
    shouldbe_suit_map;
    cardlib::CCard::kcard_suit shouldbe_suit;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5h"), cardlib::CCard("jh"),
        cardlib::CCard("7h"), cardlib::CCard("9h"),
        cardlib::CCard("2d"),
      },
      { { cardlib::CCard::kcard_suit::kSpades, 0 },
        { cardlib::CCard::kcard_suit::kHearts, 6 },
        { cardlib::CCard::kcard_suit::kDiamonds, 1 },
        { cardlib::CCard::kcard_suit::kClubs, 0 },
      },
      cardlib::CCard::kcard_suit::kHearts,
    },
    { { cardlib::CCard("2d"), cardlib::CCard("3d"),
        cardlib::CCard("5d"), cardlib::CCard("jd"),
        cardlib::CCard("7d"), cardlib::CCard("9d"),
        cardlib::CCard("kd"),
      },
      { { cardlib::CCard::kcard_suit::kSpades, 0 },
        { cardlib::CCard::kcard_suit::kHearts, 0 },
        { cardlib::CCard::kcard_suit::kDiamonds, 7 },
        { cardlib::CCard::kcard_suit::kClubs, 0 },
      },
      cardlib::CCard::kcard_suit::kDiamonds,
    },
    { { cardlib::CCard("2c"), cardlib::CCard("3c"),
        cardlib::CCard("5c"), cardlib::CCard("jc"),
        cardlib::CCard("7c"), cardlib::CCard("9c"),
        cardlib::CCard("2d"),
      },
      { { cardlib::CCard::kcard_suit::kSpades, 0 },
        { cardlib::CCard::kcard_suit::kHearts, 0 },
        { cardlib::CCard::kcard_suit::kDiamonds, 1 },
        { cardlib::CCard::kcard_suit::kClubs, 6 },
      },
      cardlib::CCard::kcard_suit::kClubs,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("3s"),
        cardlib::CCard("5s"), cardlib::CCard("js"),
        cardlib::CCard("7s"), cardlib::CCard("9s"),
        cardlib::CCard("2s"),
      },
      { { cardlib::CCard::kcard_suit::kSpades, 6 },
        { cardlib::CCard::kcard_suit::kHearts, 1 },
        { cardlib::CCard::kcard_suit::kDiamonds, 0 },
        { cardlib::CCard::kcard_suit::kClubs, 0 },
      },
      cardlib::CCard::kcard_suit::kSpades,
    },
    { { cardlib::CCard("ah"), cardlib::CCard("3c"),
        cardlib::CCard("js"), cardlib::CCard("kd"),
        cardlib::CCard("ac"), cardlib::CCard("9h"),
        cardlib::CCard("ts"),
      },
      { { cardlib::CCard::kcard_suit::kSpades, 2 },
        { cardlib::CCard::kcard_suit::kHearts, 2 },
        { cardlib::CCard::kcard_suit::kDiamonds, 1 },
        { cardlib::CCard::kcard_suit::kClubs, 2 },
      },
      cardlib::CCard::kcard_suit::kNullSuit,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  std::map<cardlib::CCard::kcard_suit, int64_t>
      shouldbe_suit_count_map, result_suit_count_map;
  std::map<cardlib::CCard::kcard_suit, int64_t>::const_iterator
      msi_cit;
  cardlib::CCard::kcard_suit shouldbe_suit, result_suit;
  int64_t shouldbe_count, result_count;
  std::string sub_name = {
    static_cast<std::string>("TFlushFunctionsTest1::")
    + static_cast<std::string>("TestCanFormAFlush1:") };
  std::string error_message, err_start, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    shouldbe_suit_count_map = va_cit->shouldbe_suit_map;
    shouldbe_suit = va_cit->shouldbe_suit;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    result_suit_count_map.clear();
    flushf_obj.MakeSuitCountMap(
        &input_card_vector, &result_suit_count_map);

    result_suit =
        flushf_obj.CanFormAFlush(&result_suit_count_map);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("flush suit shouldbe = ")
        + std::to_string(shouldbe_suit)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_suit);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_suit, result_suit);

    for (msi_cit = shouldbe_suit_count_map.cbegin();
         msi_cit != shouldbe_suit_count_map.cend(); ++msi_cit) {
      shouldbe_suit = msi_cit->first;
      shouldbe_count = msi_cit->second;

      result_count = result_suit_count_map[shouldbe_suit];

      error_message =
          err_start
          + static_cast<std::string>("for suit ")
          + std::to_string(shouldbe_suit)
          + static_cast<std::string>(" : shouldbe count = ")
          + std::to_string(shouldbe_count)
          + static_cast<std::string>(" : result count = ")
          + std::to_string(result_count);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message, shouldbe_count, result_count);
    }

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFlushFunctionsTest1::TestGetFlushSuit1() {
  cardlib::CCard card_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    cardlib::CCard::kcard_suit shouldbe_suit;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5h"), cardlib::CCard("jh"),
        cardlib::CCard("7h"), cardlib::CCard("9h"),
        cardlib::CCard("2d"),
      },
      cardlib::CCard::kcard_suit::kHearts,
    },
    { { cardlib::CCard("2d"), cardlib::CCard("3d"),
        cardlib::CCard("5d"), cardlib::CCard("jd"),
        cardlib::CCard("7d"), cardlib::CCard("9d"),
        cardlib::CCard("kd"),
      },
      cardlib::CCard::kcard_suit::kDiamonds,
    },
    { { cardlib::CCard("2c"), cardlib::CCard("3c"),
        cardlib::CCard("5c"), cardlib::CCard("jc"),
        cardlib::CCard("7c"), cardlib::CCard("9c"),
        cardlib::CCard("2d"),
      },
      cardlib::CCard::kcard_suit::kClubs,
    },
    { { cardlib::CCard("2h"), cardlib::CCard("3s"),
        cardlib::CCard("5s"), cardlib::CCard("js"),
        cardlib::CCard("7s"), cardlib::CCard("9s"),
        cardlib::CCard("2s"),
      },
      cardlib::CCard::kcard_suit::kSpades,
    },
    { { cardlib::CCard("ah"), cardlib::CCard("3c"),
        cardlib::CCard("js"), cardlib::CCard("kd"),
        cardlib::CCard("ac"), cardlib::CCard("9h"),
        cardlib::CCard("ts"),
      },
      cardlib::CCard::kcard_suit::kNullSuit,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  cardlib::CCard::kcard_suit shouldbe_suit, result_suit;
  std::string sub_name = {
    static_cast<std::string>("TFlushFunctionsTest1::")
    + static_cast<std::string>("TestGetFlushSuit1:") };
  std::string error_message, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    shouldbe_suit = va_cit->shouldbe_suit;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    result_suit =
        flushf_obj.GetFlushSuit(&input_card_vector);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : flush suit shouldbe = ")
        + std::to_string(shouldbe_suit)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_suit);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_suit, result_suit);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFlushFunctionsTest1::TestIsFlush1() {
  cardlib::CCard card_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input_vector;
    int64_t hand_order;
    int64_t prime7_signature;
    int64_t prime5_signature;
    cardlib::CHand::khand_value shouldbe_handvalue;
  };
  std::vector<atest_struct> test_vector = {
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5h"), cardlib::CCard("jh"),
        cardlib::CCard("7h"), cardlib::CCard("9h"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("jh").GetCardOrder()
        | cardlib::CCard("9h").GetCardOrder()
        | cardlib::CCard("7h").GetCardOrder()
        | cardlib::CCard("5h").GetCardOrder()
        | cardlib::CCard("3h").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpTwo
      },
      { cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpSeven
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
      },
      cardlib::CHand::khand_value::kFlush,
    },
    { { cardlib::CCard("2d"), cardlib::CCard("3d"),
        cardlib::CCard("5d"), cardlib::CCard("qd"),
        cardlib::CCard("8d"), cardlib::CCard("9d"),
        cardlib::CCard("2c"),
      },
      { cardlib::CCard("qd").GetCardOrder()
        | cardlib::CCard("9d").GetCardOrder()
        | cardlib::CCard("8d").GetCardOrder()
        | cardlib::CCard("5d").GetCardOrder()
        | cardlib::CCard("3d").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpTwo
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpTwo
      },
      { cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpEight
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpThree
      },
      cardlib::CHand::khand_value::kFlush,
    },
    { { cardlib::CCard("as"), cardlib::CCard("3c"),
        cardlib::CCard("5c"), cardlib::CCard("qc"),
        cardlib::CCard("ac"), cardlib::CCard("9c"),
        cardlib::CCard("tc"),
      },
      { cardlib::CCard("ac").GetCardOrder()
        | cardlib::CCard("qc").GetCardOrder()
        | cardlib::CCard("tc").GetCardOrder()
        | cardlib::CCard("9c").GetCardOrder()
        | cardlib::CCard("5c").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpFive
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpTen
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpQueen
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpFive
      },
      cardlib::CHand::khand_value::kFlush,
    },
    { { cardlib::CCard("ah"), cardlib::CCard("3s"),
        cardlib::CCard("js"), cardlib::CCard("ks"),
        cardlib::CCard("as"), cardlib::CCard("9s"),
        cardlib::CCard("ts"),
      },
      { cardlib::CCard("as").GetCardOrder()
        | cardlib::CCard("ks").GetCardOrder()
        | cardlib::CCard("js").GetCardOrder()
        | cardlib::CCard("ts").GetCardOrder()
        | cardlib::CCard("9s").GetCardOrder()
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpThree
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpNine
        * cardlib::CCard::kprime_value::kpTen
      },
      { cardlib::CCard::kprime_value::kpAce
        * cardlib::CCard::kprime_value::kpKing
        * cardlib::CCard::kprime_value::kpJack
        * cardlib::CCard::kprime_value::kpTen
        * cardlib::CCard::kprime_value::kpNine
      },
      cardlib::CHand::khand_value::kFlush,
    },
    { { cardlib::CCard("ah"), cardlib::CCard("3c"),
        cardlib::CCard("js"), cardlib::CCard("kd"),
        cardlib::CCard("ac"), cardlib::CCard("9h"),
        cardlib::CCard("ts"),
      },
      -1L, -1L, -1L,
      cardlib::CHand::khand_value::kHandValueNull,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input_card_vector;
  cardlib::CCard::kcard_suit flush_suit;
  cardlib::CHand::khand_value shouldbe_handvalue;
  cardlib::CHand::khand_value result_handvalue;
  int64_t shouldbe_order, result_order;
  int64_t shouldbe7_signature, result7_signature;
  int64_t shouldbe5_signature, result5_signature;
  std::string sub_name = {
    static_cast<std::string>("TFlushFunctionsTest1::")
    + static_cast<std::string>("TestIsFlush1:") };
  std::string err_start, error_message, svector_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_card_vector = va_cit->input_vector;
    shouldbe_order = va_cit->hand_order;
    shouldbe7_signature = va_cit->prime7_signature;
    shouldbe5_signature = va_cit->prime5_signature;
    shouldbe_handvalue = va_cit->shouldbe_handvalue;

    svector_string =
        card_obj.CardVectorToString(&input_card_vector);

    flush_suit =
        flushf_obj.GetFlushSuit(&input_card_vector);

    flushf_obj.IsFlush(
        flush_suit, &input_card_vector,
        &result_order, &result7_signature,
        &result5_signature, &result_handvalue);

    err_start =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : card vector = ")
        + svector_string
        + static_cast<std::string>(" : flush suit = ")
        + std::to_string(flush_suit)
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("5-card order shouldbe = ")
        + std::to_string(shouldbe_order)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_order);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_order, result_order);

    error_message =
        err_start
        + static_cast<std::string>("7-card signature shouldbe = ")
        + std::to_string(shouldbe7_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result7_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe7_signature, result7_signature);

    error_message =
        err_start
        + static_cast<std::string>("5-card signature shouldbe = ")
        + std::to_string(shouldbe5_signature)
        + static_cast<std::string>(", result = ")
        + std::to_string(result5_signature);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe5_signature, result5_signature);

    error_message =
        err_start
        + static_cast<std::string>("hand value shouldbe = ")
        + std::to_string(shouldbe_handvalue)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_handvalue);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_handvalue, result_handvalue);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFlushFunctionsTest1::TestFlushGreaterFlush1() {
  cardlib::CCard card_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // top card of flush is greater
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5h"), cardlib::CCard("jh"),
        cardlib::CCard("7h"), cardlib::CCard("9h"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5h"), cardlib::CCard("th"),
        cardlib::CCard("7h"), cardlib::CCard("9h"),
        cardlib::CCard("2d"),
      },
      true,
    },
    // second card of flush is greater
    { { cardlib::CCard("2d"), cardlib::CCard("3d"),
        cardlib::CCard("qd"), cardlib::CCard("jd"),
        cardlib::CCard("7d"), cardlib::CCard("9d"),
        cardlib::CCard("2h"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("qh"), cardlib::CCard("th"),
        cardlib::CCard("7h"), cardlib::CCard("9h"),
        cardlib::CCard("2d"),
      },
      true,
    },
    // third card of flush is greater
    { { cardlib::CCard("2c"), cardlib::CCard("3c"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("7c"), cardlib::CCard("9c"),
        cardlib::CCard("2h"),
      },
      { cardlib::CCard("2c"), cardlib::CCard("3c"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("7c"), cardlib::CCard("8c"),
        cardlib::CCard("2d"),
      },
      true,
    },
    // fourth card of flush is greater
    { { cardlib::CCard("2s"), cardlib::CCard("3s"),
        cardlib::CCard("qs"), cardlib::CCard("js"),
        cardlib::CCard("8s"), cardlib::CCard("9s"),
        cardlib::CCard("2h"),
      },
      { cardlib::CCard("2s"), cardlib::CCard("3s"),
        cardlib::CCard("qs"), cardlib::CCard("js"),
        cardlib::CCard("7s"), cardlib::CCard("9s"),
        cardlib::CCard("2d"),
      },
      true,
    },
    // fifth card of flush is greater
    { { cardlib::CCard("2h"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("8h"), cardlib::CCard("9h"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("2s"), cardlib::CCard("5s"),
        cardlib::CCard("qs"), cardlib::CCard("js"),
        cardlib::CCard("8s"), cardlib::CCard("9s"),
        cardlib::CCard("2d"),
      },
      true,
    },
    // fifth card of flush is lesser
    { { cardlib::CCard("2d"), cardlib::CCard("6d"),
        cardlib::CCard("qd"), cardlib::CCard("jd"),
        cardlib::CCard("8d"), cardlib::CCard("9d"),
        cardlib::CCard("2c"),
      },
      { cardlib::CCard("2c"), cardlib::CCard("7c"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("8c"), cardlib::CCard("9c"),
        cardlib::CCard("2d"),
      },
      false,
    },
    // fifth card of flush is the same
    { { cardlib::CCard("2d"), cardlib::CCard("6d"),
        cardlib::CCard("qd"), cardlib::CCard("jd"),
        cardlib::CCard("8d"), cardlib::CCard("9d"),
        cardlib::CCard("2c"),
      },
      { cardlib::CCard("2c"), cardlib::CCard("6c"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("8c"), cardlib::CCard("9c"),
        cardlib::CCard("2d"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TFlushFunctionsTest1::")
    + static_cast<std::string>("TestFlushGreaterFlush1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        flushf_obj.FlushGreaterFlush(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFlushFunctionsTest1::TestFlushLessThanFlush1() {
  cardlib::CCard card_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // top card of flush is greater
    { { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5h"), cardlib::CCard("jh"),
        cardlib::CCard("7h"), cardlib::CCard("9h"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5h"), cardlib::CCard("th"),
        cardlib::CCard("7h"), cardlib::CCard("9h"),
        cardlib::CCard("2d"),
      },
      true,
    },
    // second card of flush is greater
    { { cardlib::CCard("2d"), cardlib::CCard("3d"),
        cardlib::CCard("qd"), cardlib::CCard("jd"),
        cardlib::CCard("7d"), cardlib::CCard("9d"),
        cardlib::CCard("2h"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("qh"), cardlib::CCard("th"),
        cardlib::CCard("7h"), cardlib::CCard("9h"),
        cardlib::CCard("2d"),
      },
      true,
    },
    // third card of flush is greater
    { { cardlib::CCard("2c"), cardlib::CCard("3c"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("7c"), cardlib::CCard("9c"),
        cardlib::CCard("2h"),
      },
      { cardlib::CCard("2c"), cardlib::CCard("3c"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("7c"), cardlib::CCard("8c"),
        cardlib::CCard("2d"),
      },
      true,
    },
    // fourth card of flush is greater
    { { cardlib::CCard("2s"), cardlib::CCard("3s"),
        cardlib::CCard("qs"), cardlib::CCard("js"),
        cardlib::CCard("8s"), cardlib::CCard("9s"),
        cardlib::CCard("2h"),
      },
      { cardlib::CCard("2s"), cardlib::CCard("3s"),
        cardlib::CCard("qs"), cardlib::CCard("js"),
        cardlib::CCard("7s"), cardlib::CCard("9s"),
        cardlib::CCard("2d"),
      },
      true,
    },
    // fifth card of flush is greater
    { { cardlib::CCard("2h"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("8h"), cardlib::CCard("9h"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("2s"), cardlib::CCard("5s"),
        cardlib::CCard("qs"), cardlib::CCard("js"),
        cardlib::CCard("8s"), cardlib::CCard("9s"),
        cardlib::CCard("2d"),
      },
      true,
    },
    // fifth card of flush is lesser
    { { cardlib::CCard("2d"), cardlib::CCard("6d"),
        cardlib::CCard("qd"), cardlib::CCard("jd"),
        cardlib::CCard("8d"), cardlib::CCard("9d"),
        cardlib::CCard("2c"),
      },
      { cardlib::CCard("2c"), cardlib::CCard("7c"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("8c"), cardlib::CCard("9c"),
        cardlib::CCard("2d"),
      },
      false,
    },
    // fifth card of flush is the same
    { { cardlib::CCard("2d"), cardlib::CCard("6d"),
        cardlib::CCard("qd"), cardlib::CCard("jd"),
        cardlib::CCard("8d"), cardlib::CCard("9d"),
        cardlib::CCard("2c"),
      },
      { cardlib::CCard("2c"), cardlib::CCard("6c"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("8c"), cardlib::CCard("9c"),
        cardlib::CCard("2d"),
      },
      false,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TFlushFunctionsTest1::")
    + static_cast<std::string>("TestFlushLessThanFlush1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        flushf_obj.FlushGreaterFlush(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TFlushFunctionsTest1::TestFlushEqualFlush1() {
  cardlib::CCard card_obj;
  cardlib::CFlushFunctions flushf_obj;
  struct atest_struct {
    std::vector<cardlib::CCard> input1_vector;
    std::vector<cardlib::CCard> input2_vector;
    bool bshouldbe;
  };
  std::vector<atest_struct> test_vector = {
    // flush is equal
    { { cardlib::CCard("2h"), cardlib::CCard("4h"),
        cardlib::CCard("5h"), cardlib::CCard("jh"),
        cardlib::CCard("7h"), cardlib::CCard("9h"),
        cardlib::CCard("8h"),
      },
      { cardlib::CCard("2h"), cardlib::CCard("3h"),
        cardlib::CCard("5h"), cardlib::CCard("8h"),
        cardlib::CCard("7h"), cardlib::CCard("9h"),
        cardlib::CCard("jh"),
      },
      true,
    },
    // flush is equal
    { { cardlib::CCard("2d"), cardlib::CCard("3d"),
        cardlib::CCard("qd"), cardlib::CCard("jd"),
        cardlib::CCard("7d"), cardlib::CCard("9d"),
        cardlib::CCard("8d"),
      },
      { cardlib::CCard("5h"), cardlib::CCard("3h"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("8h"), cardlib::CCard("9h"),
        cardlib::CCard("7h"),
      },
      true,
    },
    // high card not equal
    { { cardlib::CCard("2c"), cardlib::CCard("3c"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("7c"), cardlib::CCard("9c"),
        cardlib::CCard("2h"),
      },
      { cardlib::CCard("2c"), cardlib::CCard("3c"),
        cardlib::CCard("qc"), cardlib::CCard("kc"),
        cardlib::CCard("7c"), cardlib::CCard("9c"),
        cardlib::CCard("2d"),
      },
      false,
    },
    // second high card not equal
    { { cardlib::CCard("2s"), cardlib::CCard("3s"),
        cardlib::CCard("qs"), cardlib::CCard("js"),
        cardlib::CCard("8s"), cardlib::CCard("9s"),
        cardlib::CCard("2h"),
      },
      { cardlib::CCard("2s"), cardlib::CCard("3s"),
        cardlib::CCard("qs"), cardlib::CCard("ts"),
        cardlib::CCard("7s"), cardlib::CCard("9s"),
        cardlib::CCard("2d"),
      },
      false,
    },
    // third high card not equal
    { { cardlib::CCard("2h"), cardlib::CCard("6h"),
        cardlib::CCard("qh"), cardlib::CCard("jh"),
        cardlib::CCard("8h"), cardlib::CCard("9h"),
        cardlib::CCard("2d"),
      },
      { cardlib::CCard("2s"), cardlib::CCard("6s"),
        cardlib::CCard("qs"), cardlib::CCard("js"),
        cardlib::CCard("8s"), cardlib::CCard("7s"),
        cardlib::CCard("2d"),
      },
      false,
    },
    // fourth card of flush not equal
    { { cardlib::CCard("2d"), cardlib::CCard("6d"),
        cardlib::CCard("qd"), cardlib::CCard("jd"),
        cardlib::CCard("8d"), cardlib::CCard("9d"),
        cardlib::CCard("2c"),
      },
      { cardlib::CCard("2c"), cardlib::CCard("7c"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("5c"), cardlib::CCard("9c"),
        cardlib::CCard("2d"),
      },
      false,
    },
    // fifth card of flush not equal
    { { cardlib::CCard("2d"), cardlib::CCard("6d"),
        cardlib::CCard("qd"), cardlib::CCard("jd"),
        cardlib::CCard("8d"), cardlib::CCard("9d"),
        cardlib::CCard("2c"),
      },
      { cardlib::CCard("2c"), cardlib::CCard("5c"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("8c"), cardlib::CCard("9c"),
        cardlib::CCard("2d"),
      },
      false,
    },
    // flush is equal
    { { cardlib::CCard("2d"), cardlib::CCard("6d"),
        cardlib::CCard("qd"), cardlib::CCard("jd"),
        cardlib::CCard("8d"), cardlib::CCard("9d"),
        cardlib::CCard("2c"),
      },
      { cardlib::CCard("2c"), cardlib::CCard("6c"),
        cardlib::CCard("qc"), cardlib::CCard("jc"),
        cardlib::CCard("8c"), cardlib::CCard("9c"),
        cardlib::CCard("2d"),
      },
      true,
    },
  };
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_label_index;
  std::vector<cardlib::CCard> input1_vector, input2_vector;
  cardlib::CHand hand1, hand2;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TFlushFunctionsTest1::")
    + static_cast<std::string>("TestFlushEqualFlush1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input1_vector = va_cit->input1_vector;
    input2_vector = va_cit->input2_vector;
    shouldbe_bool = va_cit->bshouldbe;

    hand1.Calculate(&input1_vector);

    hand2.Calculate(&input2_vector);

    result_bool =
        flushf_obj.FlushEqualFlush(&hand1, &hand2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(") : hand 1 cards = ")
        + card_obj.CardVectorToString(&input1_vector)
        + static_cast<std::string>(" : hand 2 cards = ")
        + card_obj.CardVectorToString(&input2_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (shouldbe_bool ? "true" : "false")
        + static_cast<std::string>(", result = ")
        + (result_bool ? "true" : "false");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

}  // namespace handtest
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

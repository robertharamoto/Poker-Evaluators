;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  Poker hand evaluation speed test                     ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

This is a repository for poker hand evaluations algorithms.

It's a collection of various hand evaluator speed tests,
written in the same spirit as
https://suffe.cool/poker/evaluator.html
and http://evaluation554.rssing.com/chan-9639794/latest.php

The Guile2, and the Cpp-Naive version use a naive approach,
cards are sorted and compared to see if they are a straight flush,
four-of-a-kind,...  The Cpp-Mask version pre-computes the hands,
and assigns an int64_t integer to each.  Turns out there are only
7462 unique 5-card hands. See https://www.suffecool.net/poker/evaluator.html

There are even fewer unique 7-card hands, since some high card hands
are impossible when you choose the best 5-cards out of 7.


This program is dedicated to the public domain.
See the UNLICENSE file or http://unlicense.org/

Assumes that guile 2.2 is in /usr/bin (for gen-make.scm),
and gcc version 8.3 or better is installed.  Also cpplint.py
should be installed in your path,
(see https://github.com/cpplint/cpplint).


Written by Robert Haramoto

;;;#############################################################
;;;#############################################################
History

last updated <2024-07-23 Tue>
updated <2020-02-09 Sun>

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
